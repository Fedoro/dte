# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import fields, models, api
from openerp.exceptions import Warning
from openerp.tools.translate import _

class l10n_cl_dte_invoice_sale_order(models.Model):
	_inherit = 'sale.order'

	journal_id = fields.Many2one('account.journal', 'Document Type',
			states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
			domain="[('type','=','sale'),('company_id','=',company_id),('for_sale','!=',False)]")

	@api.model
	def _prepare_invoice(self, order, lines):
		inv_lines_pool = self.env['account.invoice.line']
		res = super(l10n_cl_dte_invoice_sale_order, self)._prepare_invoice(order, lines)

		journal = order.journal_id or \
				res.get('journal_id', None) and \
				self.env['account.journal'].browse(res['journal_id']) or None

		if journal:
			res.update({
				'journal_id': journal.id,
				'dte': journal.doc_type_id and journal.doc_type_id.sii_code in \
						['33','34','61','110','111','112'] and True or False
			})
		res.update({'date_invoice': fields.Date.context_today(self)})
		return res
l10n_cl_dte_invoice_sale_order()

class l10n_cl_dte_invoice_sale_order_line(models.Model):
	_inherit = 'sale.order.line'

	@api.model
	def _prepare_order_line_invoice_line(self, line, account_id=False):
		res = super(l10n_cl_dte_invoice_sale_order_line, self)._prepare_order_line_invoice_line(line, account_id=account_id)
		if line.order_id.journal_id:
			if line.order_id.journal_id.doc_type_id and line.order_id.journal_id.doc_type_id.sii_code in ['1', '32', '34']:
				res.update({'exension_index': 1})
		return res
l10n_cl_dte_invoice_sale_order_line()
