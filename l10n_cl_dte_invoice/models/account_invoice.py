# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

#from openerp.tools.amount_to_text_cl import amount_to_text
from datetime import datetime
import openerp.addons.decimal_precision as dp

from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _
from openerp import workflow
from openerp import netsvc

#Cambiar por lxml para mayor compatibilidad.
#from lxml import etree
import xml.etree.ElementTree as etree

from suds.client import Client
from suds import WebFault

import os, requests, tempfile

from elaphe import barcode
import base64


class l10n_cl_dte_account_invoice(models.Model):
	_inherit = 'account.invoice'
	_order = 'date_invoice DESC, id DESC'

############################### COMENTAR AL TERMINAR ##################################
#	@api.multi
#	def validate_dte(self):
#		self.signal_workflow('invoice_open')
#		return True
#######################################################################################		

	@api.multi
	def get_send_state(self):
		for invoice in self:
			self.env['l10n_cl_dte.sii'].get_send_state(invoice)
		return True

	@api.multi
	def sign_document(self):
		dte_attach = self.env['l10n_cl_dte.attachment']
		account_invoice_tax = self.env['account.invoice.tax']

		for invoice in self:
			invoice.button_reset_taxes()

			signed_xml = self.env['l10n_cl_dte.signer'].sign_xml(invoice)
			result = base64.b64encode(signed_xml)

			attch_vals = dte_attach.get_attachment_values(invoice, result, exten='.xml')
			dte_attach.add_attachment(invoice, attch_vals)

			invoice.write({'state': 'ready_to_send', 'signed_xml': base64.b64encode(signed_xml[44:]), 'failed_text': None})
		return True

	@api.multi
	def send_document(self):
		for invoice in self:
			if invoice.send_date:
				ahora = datetime.now()
				anterior = datetime.strptime(invoice.send_date, '%Y-%m-%d %H:%M:%S')
				if (ahora - anterior).seconds < 5:
					raise Warning(_('You must wait 5 seconds to send the same document.'))

			if not invoice.tax_line:
				raise Warning(_('You can\'t send a document without any tax line, please check the documents tax.'))
			self.env['l10n_cl_dte.sii'].send_sii(invoice)
		return True

	@api.multi
	def signed_to_draft(self):
		self.write({'state': 'draft'})
		self.signal_workflow('sign_to_draft')
		return True

	@api.multi
	def failed_to_draft(self):
		moves = self.env['account.move']
		for inv in self:
			if inv.move_id:
				moves += inv.move_id
			if inv.payment_ids:
				for move_line in inv.payment_ids:
					if move_line.reconcile_partial_id.line_partial_ids:
						raise Warning(_('You cannot back to draft an invoice which is partially paid. You need to unreconcile related payment entries first.'))

		# First, set the invoices as cancelled and detach the move ids
		self.write({'state': 'draft', 'move_id': False})
		if moves:
			# second, invalidate the move(s)
			moves.button_cancel()
			# delete the move this invoice was pointing to
			# Note that the corresponding move_lines and move_reconciles
			# will be automatically deleted too
			moves.unlink()
		self.signal_workflow('failed_to_draft')
		return True

	@api.multi
	def test_waiting_sii(self):
		for invoice in self:
			if invoice.dte_status == 0:
				return True
		return False	

	@api.multi
	def test_fail_sii(self):
		for invoice in self:
			#TODO mejorar al tener los retornos de SII.
			if invoice.dte_status != 0:
				return True
		return False	

	@api.multi
	def clean_slate(self):
		self.write({'status_text': None, 'failed_text': None})

	@api.one
	@api.depends('invoice_line.price_subtotal', 'tax_line.amount', 'type')
	def _compute_amount(self):
		super(l10n_cl_dte_account_invoice, self)._compute_amount()

		if self.type in ('out_invoice', 'out_refund'):
			tax_code = 'EXEV'
			amount_tax = self.amount_tax
		elif self.type in ('in_invoice', 'in_refund'):
			tax_code = 'EXEC'
			if self.journal_id.doc_type_id.sii_code == '46':
				amount_tax = 0
				amount_tax_ret = 0
				for tax in self.tax_line:
					if tax.tax_code_id.code not in ('15', '30', '31', '32', '33', '34', '36', '37', '38', '39', '41', '46', '47', '48'):
						amount_tax += tax.tax_amount
					else:
						amount_tax_ret += tax.tax_amount
				self.amount_tax = amount_tax
				self.amount_tax_retained = amount_tax_ret

		self.amount_exempt = sum(line.price_subtotal or 0 for line in self.invoice_line if line.invoice_line_tax_id and \
							all(line_tax.tax_code_id.code == tax_code for line_tax in line.invoice_line_tax_id))
		self.amount_untaxed = sum(line.price_subtotal for line in self.invoice_line) - self.amount_exempt

		if self.global_discount_percent:
			self.amount_untaxed -= self.global_discount_percent * self.amount_untaxed / 100
			self.amount_exempt -= self.global_discount_percent * self.amount_exempt / 100
		if self.global_discount_percent_untaxed:
			self.amount_untaxed -= self.global_discount_percent_untaxed * self.amount_untaxed / 100
		if self.global_discount_percent_exempt:
			self.amount_exempt -= self.global_discount_percent_exempt * self.amount_exempt / 100

		self.amount_total = self.amount_tax - self.amount_tax_retained + self.amount_untaxed + self.amount_exempt

#	@api.onchange('reference_lines', 'reference_lines.reference_type')
#	def _onchange_reference_lines(self):
#		if self.type == 'in_refund' and self.journal_id.doc_type_id and self.journal_id.doc_type_id.sii_code in ('56', '61'):
#			self.dte = True if '46' in [line.reference_type.sii_code for line in self.reference_lines] else False
#		self.dte = False

	@api.onchange('journal_id', 'type')
	def _onchange_journal_id(self):
		dte = False
		export = False
		if self.type in ('out_invoice','out_refund'):
			dte = self.journal_id.type in ['sale', 'sale_refund'] and self.journal_id.doc_type_id and \
				self.journal_id.doc_type_id.sii_code in ['33','34','56','61','110','111','112'] or False
			export = self.journal_id.doc_type_id.sii_code in ['110','111','112'] or False
		elif self.type == 'in_invoice':
			dte = self.journal_id.type == 'purchase' and self.journal_id.doc_type_id and \
								self.journal_id.doc_type_id.sii_code == '46' or False

		res = super(l10n_cl_dte_account_invoice, self).onchange_journal_id(journal_id=self.journal_id.id)

		self.dte = dte
		self.is_export = export

		if res and dte:
			caf = self.env['l10n_cl_dte.caf'].get_caf(self.journal_id.id)
			caf_number = caf.end_number - caf.next_number
			if caf_number < 10 and caf_number >= 0 and self.type in ('out_invoice','out_refund'):
				res.update({'warning': {'title': _('Warning!'), 'message': _('There are few folios for the selected document type')}})
		return res

	@api.depends('ted')
	def _get_pdf417(self):
		for record in self:
			if not record.ted:
				continue
			image_tmp_file = tempfile.NamedTemporaryFile()
			code = barcode('pdf417', record.ted.encode('iso-8859-1'), dict(columns=18, rows=5, eclevel=5) )
			image_path = image_tmp_file.name
			code.save(image_path, format='png')
			with open(image_path, 'rb') as imageFile:
				image = base64.encodestring(imageFile.read())
			record.pdf_417 = image
			image_tmp_file.close()

	@api.one
	@api.depends('invoice_line')
	def _get_max_lines(self):
		if len(self.invoice_line) > self.company_id.lines_invoice:
			self.max_invoice_lines = True
		else:
			self.max_invoice_lines = False

	@api.depends('state','journal_id','company_id','dte')
	def _send_method(self):
		for record in self:
			record.batch_send = record.company_id.batch_send

	@api.multi
	def split_invoice(self):
		""" Split the invoice when the lines exceed the maximum set for the company"""
		sale_pool = self.env['sale.order']
		for inv in self:
			inv_id = False
			if not inv.company_id.lines_invoice or inv.company_id.lines_invoice < 1:
				raise Warning(_('Please set the number of lines at:\n'+\
							'Administration->Company->Configuration->Invoice Lines'))
			if inv.dte:
				if len(inv.invoice_line) > inv.company_id.lines_invoice:
					lst = []
					invoice = inv.read(['name', 'type', 'number', 'origin', \
								'supplier_invoice_number', 'comment', 'date_due', 'dte', \
								'partner_id', 'payment_term', 'account_id', 'currency_id', \
								'invoice_line', 'tax_line', 'journal_id', 'period_id', 'user_id'])
					invoice = invoice[0]
					invoice.update({'state': 'draft', 'number': False, 'invoice_line': [], 'tax_line': []})
					# take the id part of the tuple returned for many2one fields
					invoice.pop('id', None)
					for field in ('partner_id', 'account_id', 'currency_id', 'payment_term', \
										'journal_id', 'period_id', 'user_id'):
						invoice[field] = invoice[field] and invoice[field][0]
					new_inv = self.create(invoice)
					inv.next_invoice = new_inv
					sale_ids = sale_pool.search([('invoice_ids.id','=',inv.id)], limit=1)
					if sale_ids:
						sale_ids.write({'invoice_ids': [(4, new_inv.id, False)]})
					cont = 0
					lst = inv.invoice_line
					lst = list(lst)
					while cont < inv.company_id.lines_invoice:
						lst.pop(0)
						cont += 1
					for il in lst:
						il.write({'invoice_id': new_inv.id})
					inv.button_compute(set_total=True)
			if new_inv:
				new_inv.button_compute(set_total=True)
		return True

	@api.one
	@api.depends('global_discount_exempt', 'global_discount_percent_exempt',
			'global_discount_untaxed', 'global_discount_percent_untaxed',
			'global_discount', 'global_discount_percent')
	def _compute_discount(self):
		if not self.specific_discounts:
			self.total_discount = round(self.global_discount_percent * \
							sum(line.price_subtotal for line in self.invoice_line) / 100, 0)
		else:
			untaxed = sum(line.price_subtotal or 0 for line in self.invoice_line if line.invoice_line_tax_id and \
					all(line_tax.tax_code_id.code == 'IDF' for line_tax in line.invoice_line_tax_id))
			exempt = sum(line.price_subtotal or 0 for line in self.invoice_line if line.invoice_line_tax_id and \
					all(line_tax.tax_code_id.code == 'EXEV' for line_tax in line.invoice_line_tax_id))

			self.total_discount = round(((self.global_discount_percent_untaxed * untaxed) + \
								(self.global_discount_percent_exempt * exempt)) / 100, 0)

			###################### BORRAR SI AL TERMINAR EL ASIENTO ESPECIAL FUNCIONA BIEN EL RESIDUO #############################
#	@api.one
#	@api.depends('state', 'currency_id', 'invoice_line.price_subtotal', 'move_id.line_id.account_id.type', 'move_id.line_id.amount_residual',
#			# Fixes the fact that move_id.line_id.amount_residual, being not stored and old API, doesn't trigger recomputation
#			'move_id.line_id.reconcile_id', 'move_id.line_id.amount_residual_currency', 'move_id.line_id.currency_id',
#			'move_id.line_id.reconcile_partial_id.line_partial_ids.invoice.type')
#	def _compute_residual(self):
#		super(l10n_cl_dte_account_invoice, self)._compute_residual()
#		if self.type in ('in_invoice', 'in_refund'):
#			print "do something"


	#TODO REFACTORIZAR, PAKETON
	@api.onchange('specific_discounts', 'amount_percent_discount', 'global_discount_percent', 'global_discount',\
		'global_discount_percent_untaxed', 'global_discount_untaxed', 'global_discount_percent_exempt', 'global_discount_exempt')
	def _onchange_discount(self):
		if not self.invoice_line and (self.global_discount_percent or self.global_discount or \
						self.global_discount_untaxed or self.global_discount_exempt or \
						self.global_discount_percent_untaxed or self.global_discount_percent_exempt):
			self.global_discount_percent = self.global_discount = \
				self.global_discount_untaxed = self.global_discount_exempt = \
					self.global_discount_percent_untaxed = self.global_discount_percent_exempt = 0
			return {
				'warning': {
					'title': _('Warning!'),
					'message': _('You can\'t add a global discount without invoice lines.')
				}
			}

		if not self.specific_discounts:
			self.global_discount_untaxed = self.global_discount_exempt = self.global_discount_percent_untaxed = \
						self.global_discount_percent_exempt = global_discount_percent = 0
			if not self.amount_percent_discount:
				total = sum(line.price_subtotal for line in self.invoice_line)
				if total:
					global_discount_percent = self.global_discount * 100 / total

				if global_discount_percent:
					global_discount = global_discount_percent * total / 100
					if global_discount > total or global_discount < 0:
						self.global_discount = 0
						return {
							'warning': {
								'title': _('Warning!'),
								'message': _('The global discount must be between 0 and %s.' % (total))
							}
						}
				self.global_discount_percent = global_discount_percent
			else:
				self.global_discount = 0
				if self.global_discount_percent > 100 or self.global_discount_percent < 0:
					self.global_discount_percent = 0
					return {
						'warning': {
							'title': _('Warning!'),
							'message': _('The global discount percent must be a number between 0 and 100.')
						}
					}
		else:
			self.global_discount_percent = self.global_discount = \
					global_discount_percent_untaxed = global_discount_percent_exempt = 0
			if not self.amount_percent_discount:
				# Untaxed Discount
				total_untaxed = sum(line.price_subtotal or 0 for line in self.invoice_line if line.invoice_line_tax_id and \
						all(line_tax.tax_code_id.code == 'IDF' for line_tax in line.invoice_line_tax_id))
				if total_untaxed:
					global_discount_percent_untaxed = self.global_discount_untaxed * 100 / total_untaxed

				if global_discount_percent_untaxed:
					global_discount_untaxed = global_discount_percent_untaxed * total_untaxed / 100
					if global_discount_untaxed > total_untaxed or global_discount_untaxed < 0:
						self.global_discount_percent_untaxed = 0
						return {
							'warning': {
								'title': _('Warning!'),
								'message': _('The untaxed global discount must be between 0 and %s.' % (total_untaxed))
							}
						}
				self.global_discount_percent_untaxed = global_discount_percent_untaxed

				# Exemp Discount
				total_exempt = sum(line.price_subtotal or 0 for line in self.invoice_line if line.invoice_line_tax_id and \
							all(line_tax.tax_code_id.code == 'EXEV' for line_tax in line.invoice_line_tax_id))
				if total_exempt:
					global_discount_percent_exempt = self.global_discount_exempt * 100 / total_exempt

				if global_discount_percent_exempt:
					global_discount_exempt = global_discount_percent_exempt * total_exempt / 100
					if global_discount_exempt > total_exempt or global_discount_exempt < 0:
						self.global_discount_percent_exempt = 0
						return {
							'warning': {
								'title': _('Warning!'),
								'message': _('The exempt global discount must be between 0 and %s.' % (total_exempt))
							}
						}
				self.global_discount_percent_exempt = global_discount_percent_exempt
			else:
				self.global_discount_untaxed = 0
				self.global_discount_exempt = 0

				# Untaxed Discount
				if self.global_discount_percent_untaxed > 100 or self.global_discount_percent_untaxed < 0:
					self.global_discount_percent_untaxed = 0
					return {
						'warning': {
							'title': _('Warning!'),
							'message': _('The untaxed global discount percent must be a number between 0 and 100.')
						}
					}

				# Exempt Discount
				if self.global_discount_percent_exempt > 100 or self.global_discount_percent_exempt < 0:
					self.global_discount_percent_exempt = 0
					return {
						'warning': {
							'title': _('Warning!'),
							'message': _('The exempt global discount percent must be a number between 0 and 100.')
						}
					}
	#### FIN REFACTORIZAR ####

	@api.depends('currency_id','date_invoice')
	def _compute_currency_rate(self):
		for invoice in self:
			company_currency = invoice.company_id.currency_id
			currency = invoice.currency_id.with_context(date=invoice.date_invoice or fields.Date.context_today(self))
			if company_currency and currency:
				invoice.currency_rate = "{0:.2f}".format(currency.compute(1, company_currency, round=False))
			else:
				invoice.currency_rate = 0

	number = fields.Char('Number', related='', copy=False)
	ted = fields.Text('TED', copy=False)
	caf = fields.Many2one('l10n_cl_dte.caf', 'CAF utilizado para este documento', domain="[('journal_id','=',journal_id)]", copy=False)
	invoice_number = fields.Char('Invoice Number', readonly=True, copy=False,\
					help="Unique number of the invoice, computed automatically when the invoice is created.")
	pdf_417 = fields.Binary('PDF 417', compute='_get_pdf417',copy=False)
	purchase_dte = fields.Boolean('Purchase DTE', default=False)
	signed_xml = fields.Binary('Signed XML', copy=False)
	dte_status = fields.Integer('DTE Status', copy=False)
	track_id = fields.Char('Track Id', copy=False)
	failed_text = fields.Text('Reason for Failure', copy=False)
	status_text = fields.Text('Status Description', copy=False)
	send_date = fields.Datetime('Send to SII Date Time', copy=False)

	batch_send = fields.Boolean('this invoice need send in bash?', compute='_send_method',copy=False, store=True)
	
	state = fields.Selection(selection_add=[('ready_to_send', 'Ready to Send'),('waiting_sii', 'Waiting SII'),('failed_sii','Failed SII')])
	dte = fields.Boolean('Is DTE Document?', states={'draft': [('readonly', False)]})
	sii_document_type = fields.Char('SII Document Type', related='journal_id.doc_type_id.sii_code')#, store=True)
	reference_lines = fields.One2many('l10n_cl_dte.document_references', 'invoice_id', 'Reference Lines', \
							readonly=True, states={'draft': [('readonly', False)]}, copy=False)
	date_invoice = fields.Date('Invoice Date', readonly=True, states={'draft': [('readonly', False)]}, index=True,
					help='Keep empty to use the current date', copy=False, required=True,
							default=lambda self: fields.Date.context_today(self))
	specific_discounts = fields.Boolean('Specific Discounts', readonly=True, states={'draft': [('readonly', False)]}, copy=True,
					help='Check if you want to write specific discounts for exempts and untaxed lines.')
	amount_percent_discount = fields.Boolean('Discount Percentage?', default=True, readonly=True, states={'draft': [('readonly', False)]},
			help='Check if you want to write the discount percentaje, to write the discount amount keep unchecked.', copy=True)
	global_discount = fields.Integer('Discount', readonly=True, states={'draft': [('readonly', False)]}, copy=True)
	global_discount_percent = fields.Float('Discount (%)', digits=(3,2), readonly=True, states={'draft': [('readonly', False)]}, copy=True)
	global_discount_untaxed = fields.Integer('Untaxed Discount', readonly=True, states={'draft': [('readonly', False)]}, copy=True)
	global_discount_percent_untaxed = fields.Float('Untaxed Discount (%)', digits=(3,2), readonly=True,
									states={'draft': [('readonly', False)]}, copy=True)
	global_discount_exempt = fields.Integer('Exempt Discount', readonly=True, states={'draft': [('readonly', False)]}, copy=True)
	global_discount_percent_exempt = fields.Float('Exempt Discount (%)', digits=(3,2), readonly=True,
									states={'draft': [('readonly', False)]}, copy=True)
	total_discount = fields.Integer('Total Discount', compute='_compute_discount')
	amount_untaxed = fields.Float(string='Amount Untaxed', digits=dp.get_precision('account'),
					store=True, readonly=True, compute='_compute_amount', track_visibility='always')
	amount_exempt = fields.Float(string='Amount Exempt', digits_compute=dp.get_precision('account'),
					store=True, readonly=True, compute='_compute_amount', track_visibility='always')
	amount_tax = fields.Float(string='Amount Tax', digits_compute=dp.get_precision('account'),
					store=True, readonly=True, compute='_compute_amount', track_visibility='always')
	amount_tax_retained = fields.Float(string='Amount Tax Retained', digits_compute=dp.get_precision('account'),
					store=True, readonly=True, compute='_compute_amount', track_visibility='always')
	amount_total = fields.Float(string='Amount Total', digits_compute=dp.get_precision('account'),
					store=True, readonly=True, compute='_compute_amount', track_visibility='always')
	xml = fields.Char('XML de la factura firmado', copy=False)
	max_invoice_lines = fields.Boolean('Max Dte Lines', compute=_get_max_lines, default=False, store=True)
	next_invoice = fields.Many2one('account.invoice', 'Next Invoice', copy=False)
	carrier_id = fields.Many2one('l10n_cl_dte.carrier', 'Carrier', readonly=True, states={'draft': [('readonly', False)]})
	driver_id = fields.Many2one('l10n_cl_dte.driver', 'Driver', domain="[('carrier_id','=',carrier_id)]", 
									readonly=True, states={'draft': [('readonly', False)]})
	vehicle_id = fields.Many2one('l10n_cl_dte.vehicle', 'Vehicle', domain="[('carrier_id','=',carrier_id)]",
									readonly=True, states={'draft': [('readonly', False)]})
	#################################################################################################################################

	# IndNoRebaja, Solo para Notas de Credito que no tienen derecho a Rebaja del Debito
	no_discount_rate = fields.Integer('Indice de no rebaja') 

	# TipoDespacho, indica si el documento acompana bienes y el despacho es por cuenta del vendedor o del comprador.
	# No se incluye si el documento no acompana bienes o se trata de una Factura o Nota correspondiente a la prestacion de servicios.
	# 1: Despacho por cuenta del receptor del  documento (cliente o  vendedor  en caso de Facturas de compra.)
	# 2: Despacho por cuenta del  emisor a instalaciones del  cliente
	# 3: Despacho por cuenta del emisor a otras instalaciones (Ejemplo: entrega en Obra)
	picking_type = fields.Selection([(1,'Despacho por cuenta del receptor'),
					(2,'Despacho por cuenta del emisor a instalaciones del cliente'),
					(3,' Despacho por cuenta del emisor a otras instalaciones')], 'Tipo despacho',
									readonly=True, states={'draft': [('readonly', False)]})

	# IndServicio, indica si la transaccion corresponde a la prestacion de un servicio
	# 1: Factura de servicios periodicos domiciliarios
	# 2: Factura de otros servicios periodicos
	# 3: Factura de Servicios. (en caso de Factura de Exportacion: Servicios calificados como tal por Aduana) 
	# Solo para Facturas de Exportacion:
	# 4: Servicios de Hoteleria 
	# 5: Servicio de Transporte Terrestre Internacional
	service_index = fields.Selection([(1, 'Factura de servicios periodicos domiciliarios'), 
					  (2, 'Factura de otros servicios periodicos'),
					  (3, 'Factura de Servicios.'),
					  (4, 'Servicios de Hotelerìa'),
					  (5, 'Servicio de Transporte Terrestre Internacional'),
					  (6, 'Servicios prestados y utilizados totalmente en el extranjero')], 
				'Service Index', readonly=True, states={'draft': [('readonly', False)]})

	# MntBruto, indicador Montos Bruto: indica si las lineas de detalle, descuentos y recargos  se expresan 
	# en montos brutos. (Solo para documentos sin impuestos adicionales) .
	tax_included = fields.Boolean('Gross Amount Index', default=False, readonly=True, states={'draft': [('readonly', False)]})
	
	#payment_type = fields.Selection([(1, 'Contado'), (2, 'Credito'), (3, 'Sin costo (entrega gratuita)')], 'Tipo de pago')
	#payment_type_exp = fields.Char('Forma de pago exportacion')

	# FchCancel, Fecha de cancelación: Sólo se utiliza si la factura ha sido cancelada antes de la fecha de emisión. (AAAA-MM-DD)
	# Fecha válida entre 2002-08-01 y 2050-12-31 
	# Campo Obligatorio para Factura de exportación cuando en "Forma de Pago Exportación" se indique "anticipo"
	date_cancel = fields.Date('Date Cancel')

	# PeriodoDesde, Período de facturación para Servicios Periódicos. Fecha desde
	# (Fecha inicial del servicio facturado)
	date_from = fields.Date('Fecha desde')

	# PeriodoHasta, Período de facturación para Servicios Periódicos. Fecha Hasta
	# (Fecha final del servicio facturado)
	date_to = fields.Date('Fecha Hasta')

	# MedioPago, Medio de Pago
	# Indica en que modalidad se pagara
	payment_form_dte = fields.Selection([('CH', 'Cheque'), ('CF', 'Cheque a fecha'), ('LT', 'Letra'),
					('EF', 'Efectivo'), ('PE', 'Pago A Cta. Cte'), ('TC', 'Tarjeta Credito'),
					('OT', 'Otro')], 'Medio de Pago')

	# TipoCtaPago, Tipo Cuenta de Pago
	payment_form_type = fields.Selection([('CT', 'Cuenta corriente'), ('AH', 'Ahorro'), ('OT', 'Otra')], 'Tipo de Cuenta de Pago')

	# NumCtaPago, Numero de la cuenta

	payment_account = fields.Char('Cuenta Pago', help="Cuenta a la que se debe realizar el pago")

	# BcoPago, Banco de la cuenta

	payment_bank = fields.Char('Banco del Pago', help="Banco al que esta asociada la cuenta del pago.")

	# TermPagoCdg , Terminos del Pago-Codigo
	# Es un codigo acordado entre las empresas , que indica terminos de referencia
	# Ejemplos: Fecha Recepcion Factura (FRF), o Fecha entregaMercaderias (FEM), etc.

	payment_cod_term = fields.Char('Codigo de terminos de pago', size=4)

	# TermPagoGlosa, Terminos del pago-glosa
	# Glosa que describe las condiciones del pago del documento, codificado en el campo: "Terminos del pago-Codigo"
	payment_comment_term = fields.Char('Terminos de pago')

	# TermPagoDias, Terminos de pago-Dias
	# Cantidad de dias de acuerdo al codigo de terminos de pago:
	# Ejemplo, 5 dias Fecha entrega mercaderias (Dia = 5, Codigo = FEM)

	payment_days_term = fields.Integer('Dias de Terminos de Pago')

	# RUTMandante, Rut Mandante
	# Corresponde al RUT del mandante si el TOTAL de la venta o servicio es por cuenta de otro el 
	# cual es responsable del IVA devengadoen el periodo con guion y digito verificador
	principal_id = fields.Many2one('res.partner', 'Mandante', copy=False)

	# RUTSolicita, Rut Solicita
	# Es obligatorio si es distinto de rut receptor o rut receptor es persona juridica
	applicant_id = fields.Many2one('res.partner', 'Solicitante', copy=False)

	# Exportacion
	is_export = fields.Boolean('Export', states={'draft': [('readonly', False)]}, default=False)
	sale_mode = fields.Selection([(1,'A Firme'),(2,'Bajo Condicion'),(3,'En Consignacion Libre'),
				(4,'En Consignacion Con Un Minimo A Firme'),(9,'Sin Pago')], 'Sale Mode',
					readonly=True, states={'draft': [('readonly', False)]}) #74
	exp_sale_clause = fields.Selection([(1,'CIF'),(2,'CFR'),(3,'EXW'),(4,'FAS'),(5,'FOB'),(6,'S/CL'),
				(8,'Otros'),(9,'DDP'),(10,'FCA'),(11,'CPT'),(12,'CIP'),(17,'DAT'),(18,'DAP')], 'Sale Clause',
					readonly=True, states={'draft': [('readonly', False)]}) #75
	exp_tot_sale_clause = fields.Float('Total Sale Clause', default=0.00, readonly=True, states={'draft': [('readonly', False)]}) #76
	code_via_transp = fields.Selection([(1,'Maritima, Fluvial y Lacustre'),(4,'Aereo'),(5,'Postal'),
				(6,'Ferroviario'),(7,'Carretero/Terrestre'),(8,'Oleoductos, Gaseoductos'),
				(9,'Tendido Electrico (aereo,subt.)'),(10,'Otra')], 'Transport Route',
					readonly=True, states={'draft': [('readonly', False)]}) #77
	carrier_conv_name = fields.Char('Carrier Conveyance Name', readonly=True, states={'draft': [('readonly', False)]},
				help="Corresponds to the name or gloss of the carrier conveyance.") #78
	carrier_rut = fields.Char('Carrier Rut', readonly=True, states={'draft': [('readonly', False)]},
			 help="For documents used for export, indicate the Single Tax Role (RUT) of the "+\
                "transport company indicated in the DUS. If this is a foreigner, indicate the RUT of the agency that represents it in Chile.") #79
	carrier_name = fields.Char('Carrier Name', readonly=True, states={'draft': [('readonly', False)]}) #80
	aux_code_carrier = fields.Char('Aux Code Carrier', readonly=True, states={'draft': [('readonly', False)]}) #81
	booking = fields.Char('Booking', help="Booking number or operator reservation",
			readonly=True, states={'draft': [('readonly', False)]}) #82
	operator_code = fields.Char('Operator Code', readonly=True, states={'draft': [('readonly', False)]}) #83
	shipping_port_id = fields.Many2one('l10n_cl_dte.port', 'Shipping Port',
				readonly=True, states={'draft': [('readonly', False)]}) #84
	aux_shipping_port_code = fields.Char('Aux Shipping Port Code', readonly=True, states={'draft': [('readonly', False)]}) #85
	landing_port_id = fields.Many2one('l10n_cl_dte.port', 'Landing Port',
				readonly=True, states={'draft': [('readonly', False)]}) #86
	aux_landing_port_code = fields.Char('Aux Landing Port Code', readonly=True, states={'draft': [('readonly', False)]}) #87
	tare = fields.Integer('Tare', default=0, readonly=True, states={'draft': [('readonly', False)]}) #88
	#Agregar la categoria para tara?
	tare_uom_id = fields.Many2one('product.uom', 'Tare UoM', domain="[('category_id.name','=','DTE')]",
				readonly=True, states={'draft': [('readonly', False)]}) #89
	gross_weight = fields.Float('Gross Weight', default=0.00, readonly=True, states={'draft': [('readonly', False)]}) #90
	#Agregar la categoria para peso total?
	gross_weight_uom_id = fields.Many2one('product.uom', 'Gross Weight UoM', domain="[('category_id.name','=','DTE')]",
					readonly=True, states={'draft': [('readonly', False)]}) #91
	net_weight = fields.Float('Net Weight', default=0.00, readonly=True, states={'draft': [('readonly', False)]}) #92
	#Agregar la categoria para peso neto?
	net_weight_uom_id = fields.Many2one('product.uom', 'Net Weight UoM',
				domain="[('category_id.name','=','DTE')]",
			readonly=True, states={'draft': [('readonly', False)]}) #93
	tot_items = fields.Integer('Total Items', default=0, readonly=True, states={'draft': [('readonly', False)]}) #94
	tot_packages = fields.Integer('Total Packages', readonly=True, states={'draft': [('readonly', False)]}) #95
	package_code_ids = fields.One2many('l10n_cl_dte.package_code', 'invoice_id', 'Package Codes',
				readonly=True, states={'draft': [('readonly', False)]}) #96
	#Monto Flete
	freight_amount = fields.Float('Sale Currency Freight Amount', default=0.0000,
				readonly=True, states={'draft': [('readonly', False)]}) #102
	insurance_amount = fields.Float('Sale Currency Insurance Amount', default=0.0000,
				readonly=True, states={'draft': [('readonly', False)]}) #103
	host_country_id = fields.Many2one('res.country', 'Host Country', domain="[('sii_code','!=',False)]",
				readonly=True, states={'draft': [('readonly', False)]}) #104
	destination_country_id = fields.Many2one('res.country', 'Destination Country', domain="[('sii_code','!=',False)]",
					readonly=True, states={'draft': [('readonly', False)]}) #105
	#Monto Flete
	currency_rate = fields.Float('Currency Rate', compute='_compute_currency_rate', store=True)

	@api.model
	def create(self, vals):
		if vals.get('dte', False):
                        caf = self.env['l10n_cl_dte.caf'].get_caf(vals['journal_id'])
		return super(l10n_cl_dte_account_invoice, self).create(vals)

	@api.multi
	def action_number(self):
		"""OverWrite the original function in order to addapt it to DTE needs."""
		self.write({})

		for inv in self:
			if inv.invoice_number:
				continue

			sequence = inv.journal_id.invoice_sequence_id
			if not sequence:
				raise Warning(_('Journal %s has no sequence defined for invoices.' % inv.journal_id.name))

			move_sequence = inv.journal_id.sequence_id
			if not move_sequence:
				raise Warning(_('Journal %s has no sequence defined for account journal.' % inv.journal_id.name))

			ctx = self._context.copy()
			ctx['fiscalyear_id'] = self.env['account.period'].browse(inv.period_id.id).fiscalyear_id.id or None

			inv_number = self.pool.get('ir.sequence').get_id(self._cr, self._uid, sequence.id, context=ctx)
			move_number = self.pool.get('ir.sequence').get_id(self._cr, self._uid, move_sequence.id, context=ctx)

			if inv.type in ('in_invoice', 'in_refund'):
				if inv.dte:
					self.write({'invoice_number': move_number})
					ref = inv.number
				else:
					self.write({'invoice_number': inv_number, 'number': inv_number})
					ref = inv.supplier_invoice_number if not inv.reference else inv.reference
			else:
				if inv.dte:
					self.write({'invoice_number': move_number})
					ref = inv.number
				else:
					self.write({'internal_number': inv_number, 'invoice_number': move_number, 'number': inv_number})
					ref = inv_number

			self._cr.execute(""" UPDATE account_move SET name=%s, ref=%s
						WHERE id=%s""",	(move_number, ref, inv.move_id.id))
			self._cr.execute(""" UPDATE account_move_line SET ref=%s
						WHERE move_id=%s""", (ref, inv.move_id.id))
			self._cr.execute(""" UPDATE account_analytic_line SET ref=%s
						FROM account_move_line
						WHERE account_move_line.move_id = %s AND
						account_analytic_line.move_id = account_move_line.id""", (ref, inv.move_id.id))
			self.invalidate_cache()
		return True

	@api.multi
	def action_date_assign(self):
		for inv in self:
			if inv.dte and inv.state == 'draft':
				if inv.max_invoice_lines:
					inv.split_invoice()
				return super(l10n_cl_dte_account_invoice, self).action_date_assign()
			elif not inv.dte:
				return super(l10n_cl_dte_account_invoice, self).action_date_assign()
		return True

	@api.multi
	def compute_invoice_totals(self, company_currency, ref, invoice_move_lines):
		total = 0
		total_currency = 0
		if self.journal_id.doc_type_id.sii_code == '46':
			for line in invoice_move_lines:
				if self.currency_id != company_currency:
					currency = self.currency_id.with_context(date=self.date_invoice or fields.Date.context_today(self))
					line['currency_id'] = currency.id
					line['amount_currency'] = currency.round(line['price'])
					line['price'] = currency.compute(line['price'], company_currency)
				else:
					line['currency_id'] = False
					line['amount_currency'] = False
					line['price'] = self.currency_id.round(line['price'])
				line['ref'] = ref
				if self.type in ('out_invoice','in_refund'):
					total += line_price
					total_currency += line['amount_currency'] or line['price']
					line['price'] = - line['price']
				else:
					if line['type'] == 'tax':
						if not line['tax_code_id']:
							raise Warning(_('You must configure the \'Account Tax Code\' field to the %s tax.') % (line['name']))
						tax_code = self.env['account.tax.code'].browse(line['tax_code_id'])
						if tax_code.code in ('15', '30', '31', '32', '33', '34', '36', '37', '38', '39', '41', '46', '47', '48'):
							line['price'] = - line['price']
					total -= line['price']
					total_currency -= line['amount_currency'] or line['price']
			return total, total_currency, invoice_move_lines
		return super(l10n_cl_dte_account_invoice, self).compute_invoice_totals(company_currency, ref, invoice_move_lines)

	@api.multi
	def dte_reconcile(self):
		for inv in self:
			if inv.payment_ids:
				continue

			if inv.reference_lines and inv.journal_id.doc_type_id.sii_code in ['61','112']:
				for line in inv.reference_lines:
					if line.reference_type.sii_code in ('30','33','34','39','41','55','56','60', \
										'61','101','110','104','111','106','112'):
						o_inv = self.search([('number','=',line.reference_folio),\
										('date_invoice','=',line.reference_date)])
						if o_inv:
							if o_inv.amount_total == inv.amount_total:
								to_reconcile_ids = {}
								for line in o_inv.move_id.line_id:
									if line.account_id.id == o_inv.account_id.id:
										to_reconcile_ids.setdefault(line.account_id.id, line)
#						VER SI LO PONGO, SI SE PAGA UNA NC CON UNA ND SE pitea la conciliacion al primer documento
#									if line.reconcile_id:
#										line.reconcile_id.unlink()
								for tmpline in inv.move_id.line_id:
									if tmpline.account_id.id == o_inv.account_id.id:
										to_reconcile_ids[tmpline.account_id.id] += tmpline
								for account in to_reconcile_ids:
									to_reconcile_ids[account].reconcile(
												writeoff_period_id = o_inv.period_id.id,
												writeoff_journal_id = o_inv.journal_id.id,
												writeoff_acc_id = o_inv.account_id.id)
############################################# PROBAR QUE FUNCIONE BIEN PARA DESCOMENTAR ###########################################
#					elif line.reference_type.sii_code in ('35', '38', '39', '41'):
#						o_receipt = self.search([('folio','=',line.reference_folio),\
#										('chilean_date','=',line.reference_date)])
						
#						if o_receipt:
#							account_id = order.partner_id and order.partner_id.property_account_receivable \
#									and order.partner_id.property_account_receivable.id or False
#							to_reconcile_ids = {}
#							for line in o_receipt.move_id.line_id:
#								if line.account_id.id == o_receipt.account_id.id:
#									to_reconcile_ids.setdefault(line.account_id.id, []).append(line.id)
#								if line.reconcile_id:
#									line.reconcile_id.unlink()
#							for tmpline in inv.move_id.line_id:
#								if tmpline.account_id.id == o_receipt.account_id.id:
#									to_reconcile_ids[tmpline.account_id.id].append(tmpline.id)
#							for account in to_reconcile_ids:
#								self.env['account.move.line'].reconcile(to_reconcile_ids[account],
#										writeoff_period_id = o_receipt.account_move.period_id.id,
#										writeoff_journal_id = o_receipt.account_move.journal_id.id,
#										writeoff_acc_id = account_id)
###################################################################################################################################
		return True

	def discount_analytic_op(self, res, percent=None):
		res['price'] -= round(percent * res['price'] / 100, 13)
		res['tax_amount'] -= round(percent * res['tax_amount'] / 100, 2)

#	@api.multi
#	def _get_analytic_lines(self):
#		tax_code_pool = self.env['account.tax.code']
#		res = super(l10n_cl_dte_account_invoice, self)._get_analytic_lines()
#		for line in res:
#			print line
#
#		if self.global_discount_percent or self.global_discount_percent_untaxed or self.global_discount_percent_exempt:
#			for line in res:
#				if self.global_discount_percent:
#					print "global"
#					self.discount_analytic_op(line, self.global_discount_percent)
#				if self.global_discount_percent_untaxed:
#					print "exento"
#					tax_code = tax_code_pool.browse(line['tax_code_id'])
#					if tax_code.code != 'EXEV':
#						self.discount_analytic_op(line, self.global_discount_percent_untaxed)
#				if self.global_discount_percent_exempt:
#					print "afecto"
#					tax_code = tax_code_pool.browse(line['tax_code_id'])
#					if tax_code.code == 'EXEV':
#						self.discount_analytic_op(line, self.global_discount_percent_exempt)
#		for line in res:
#			print line
#
#		return res

#	@api.model
#	def line_get_convert(self, line, part, date):
#		tax_code_pool = self.env['account.tax.code']
#		tax_accounts = []
#		total = 0

#		res = super(l10n_cl_dte_account_invoice, self).line_get_convert(line, part, date)
#		for inv in self:
#			for tax in inv.tax_line:
#				if self.account_id.id == res['account_id']:
#					total += tax.base + tax.amount
#				tax_accounts.append(tax.account_id.id)

#		if res['account_id'] in tax_accounts:
#			return res

#		if self.account_id.id == res['account_id']:
#			if res['debit']:
#				res['debit'] = res['tax_amount'] = total
#			if res['credit']:
#				res['credit'] = res['tax_amount'] = total
#			return res

#		if self.global_discount_percent or self.global_discount_percent_untaxed or self.global_discount_percent_exempt:
		    #TODO REFACTORIZAR
#			if self.global_discount_percent:
#				res['tax_amount'] -= self.global_discount_percent * res['tax_amount'] / 100
#				if 'credit' in res:
#					res['credit'] -= self.global_discount_percent * res['credit'] / 100
#				if 'debit' in res:
#					res['debit'] -= self.global_discount_percent * res['debit'] / 100
#			if self.global_discount_percent_untaxed:
#				tax_code = tax_code_pool.browse(line['tax_code_id'])
#				if tax_code.code != 'EXEV':
#					res['tax_amount'] -= self.global_discount_percent_untaxed * res['tax_amount'] / 100
#					if 'credit' in res:
#						res['credit'] -= self.global_discount_percent_untaxed * res['credit'] / 100
#					if 'debit' in res:
#						res['debit'] -= self.global_discount_percent_untaxed * res['debit'] / 100
#			if self.global_discount_percent_exempt:
#				tax_code = tax_code_pool.browse(line['tax_code_id'])
#				if tax_code.code == 'EXEV':
#					res['tax_amount'] -= self.global_discount_percent_exempt * res['tax_amount'] / 100
#					if 'credit' in res:
#						res['credit'] -= self.global_discount_percent_exempt * res['credit'] / 100
#					if 'debit' in res:
#						res['debit'] -= self.global_discount_percent_exempt * res['debit'] / 100
#		return res
l10n_cl_dte_account_invoice()

class account_invoice_line_dte(models.Model):
	_inherit = 'account.invoice.line'

	@api.model
	def default_get(self, fields):
		res = super(account_invoice_line_dte, self).default_get(fields)
		if 'journal_id' in self._context:
			journal = self.env['account.journal'].browse(self._context['journal_id'])
			if journal.doc_type_id and journal.doc_type_id.sii_code in ['1','32','34','41','110','111','112']:
				res.update({'exension_index': 1})
		return res

	exension_index = fields.Selection([(1,'IVA Exempt'), (2,'Product or Service Non-Billable'),\
				(3,'Guarantee Deposit by Containers'), (4,'Item no Sale'),(5,'Item Rebate'),\
				(6,'Product or Service not Negative Billable'),	(7,'Does not Apply')], 'Exempt Index', default=7)
account_invoice_line_dte()

class account_invoice_tax_dte(models.Model):
	_inherit = 'account.invoice.tax'

	def calculos(self, res, key, percent=None):
		res[key]['base'] -= percent * res[key]['base'] / 100
		res[key]['amount'] -= percent * res[key]['amount'] / 100
		res[key]['tax_amount'] -= percent * res[key]['tax_amount'] / 100
		res[key]['base_amount'] -= percent * res[key]['base_amount'] / 100

	@api.v8
	def compute(self, invoice):
		tax_code_pool = self.env['account.tax.code']
		res = super(account_invoice_tax_dte, self).compute(invoice)
		if invoice.global_discount_percent:
			for key in res.keys():
				self.calculos(res, key, percent=invoice.global_discount_percent)
		if invoice.global_discount_percent_untaxed:
			for key in res.keys():
				tax_code = tax_code_pool.browse(res[key]['tax_code_id'])
				if tax_code.code != 'EXEV':
					self.calculos(res, key, percent=invoice.global_discount_percent_untaxed)
		if invoice.global_discount_percent_exempt:
			for key in res.keys():
				tax_code = tax_code_pool.browse(res[key]['tax_code_id'])
				if tax_code.code == 'EXEV':
					self.calculos(res, key, percent=invoice.global_discount_percent_exempt)
		return res
account_invoice_tax_dte()

class l10n_cl_dte_packages_code_account_invoice(models.Model):
	_inherit = 'l10n_cl_dte.package_code'

	invoice_id = fields.Many2one('account.invoice', 'Invoice')
l10n_cl_dte_packages_code_account_invoice()
