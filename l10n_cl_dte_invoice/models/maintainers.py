# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _

class l10n_cl_dte_invoice_crm_case_section(models.Model):
	_inherit = 'crm.case.section'

	sii_sub_name = fields.Char('SII Subsidiary Name', size=20)
	sii_sub_code = fields.Integer('SII Subsidiary Code', size=9)
	sii_sub_street = fields.Char('SII Subsidiary Address')
l10n_cl_dte_invoice_crm_case_section()

class l10n_cl_dte_document_references(models.Model):
	_name = 'l10n_cl_dte.document_references'
	_description = 'References for Documents'

	@api.model
	def _get_type(self):
		journal_id = self._context.get('journal', None)
		journal = self.env['account.journal'].browse(journal_id)
		return journal.doc_type_id.sii_code

	invoice_id = fields.Many2one('account.invoice', 'Invoice')
	reference_type = fields.Many2one('l10n_cl_dte.document_type', 'Reference Document Type', required=True)
	reference_folio = fields.Char('Reference Folio', size=18, required=True)
	reference_date = fields.Date('Reference Date', required=True)
	reference_reason = fields.Selection([(1, '1.- Override Document'), (2,'2.- Fixes Text'), (3,'3.- Fixes Amount')], 'Cause')
	reference_reason_text = fields.Char('Reason')
	reference_invoice_type = fields.Char('Invoice Type', default=_get_type)
l10n_cl_dte_document_references()

