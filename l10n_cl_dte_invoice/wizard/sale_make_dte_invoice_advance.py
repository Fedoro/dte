# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _

class sale_advance_payment_dte_inv(models.TransientModel):
	_inherit = 'sale.advance.payment.inv'

	@api.multi
	def _prepare_advance_invoice_vals(self):
		result = []
		sale_obj = self.env['sale.order']

		res = super(sale_advance_payment_dte_inv, self)._prepare_advance_invoice_vals()

		for sale_id, vals in res:
			order = sale_obj.browse(sale_id)
			if order.journal_id:
				vals.update({
					'journal_id': order.journal_id.id,
					'dte': order.journal_id.type in ['sale', 'sale_refund'] and \
						order.journal_id.doc_type_id and order.journal_id.doc_type_id.sii_code in \
							['33', '34', '43', '46', '61', '110', '111', '112'] and True or False
				})

				if order.journal_id.doc_type_id.sii_code in ['1', '32', '34']:
					for line in vals['invoice_line']:
						line[2].update({'exension_index': 1})
			result.append((sale_id, vals))
		return result
sale_advance_payment_dte_inv()
