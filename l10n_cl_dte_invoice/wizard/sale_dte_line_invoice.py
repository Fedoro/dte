# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _

class sale_order_line_dte_make_invoice(models.TransientModel):
	_inherit = 'sale.order.line.make.invoice'

	@api.model
	def _prepare_invoice(self, order, lines):
		res = super(sale_order_line_dte_make_invoice, self)._prepare_invoice(order, lines)

		journal = order.journal_id or \
				res.get('journal_id', None) and \
				self.env['account.journal'].browse(res['journal_id']) or None

		if journal:
			res.update({
				'journal_id': journal.id,
				'dte': journal.type in ['sale', 'sale_refund'] and journal.doc_type_id and journal.doc_type_id.sii_code in \
						['33','34', '43', '46', '61', '110', '111', '112'] and True or False
			})
		return res
sale_order_line_dte_make_invoice()
