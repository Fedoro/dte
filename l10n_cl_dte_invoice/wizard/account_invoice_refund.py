# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _

class l10n_cl_dte_account_invoice_refund(models.TransientModel):
	_inherit = 'account.invoice.refund'

	@api.multi
	def compute_refund(self, mode='refund'):
		inv_obj = self.env['account.invoice']
		wizard = self[0]
		ref_inv = inv_obj.browse(self._context.get('active_ids')[0])

		if ref_inv.dte and mode != 'refund':
			#Solo puedes usar el método "Crear una factura rectificativa borrador" al ser un documento DTE.
			raise Warning(_('You can only use the "Create a Draft Refund" method as a DTE document."'))

		result = super(l10n_cl_dte_account_invoice_refund, self).compute_refund(mode=mode)
		if 'domain' in result:
			for cond in result['domain']:
				if 'id' in cond:
					if isinstance(cond[2], list):
						for inv in inv_obj.browse(cond[2]):
							#Only New Documents Get Reference Line
							if inv.journal_id.type in ['sale', 'sale_refund'] \
										and inv.journal_id.doc_type_id.sii_code \
									in ('33','34','39','41','43','46','56','61','110','111','112'):
								global_discount_percent_untaxed = global_discount_percent_exempt = \
									global_discount_untaxed = global_discount_exempt = \
									global_discount_percent = global_discount = 0
								if not ref_inv.specific_discounts:
									if ref_inv.amount_percent_discount:
										if ref_inv.global_discount_percent:
											global_discount_percent = ref_inv.global_discount_percent
									else:
										if ref_inv.global_discount:
											global_discount = ref_inv.global_discount
								else:
									if ref_inv.amount_percent_discount:
										if ref_inv.global_discount_percent_untaxed:
											global_discount_percent_untaxed = ref_inv.global_discount_percent_untaxed
										if ref_inv.global_discount_percent_exempt:
											global_discount_percent_exempt = ref_inv.global_discount_percent_exempt
									else:
										if ref_inv.global_discount_untaxed:
											global_discount_untaxed = ref_inv.global_discount_untaxed
										if ref_inv.global_discount_exempt:
											global_discount_exempt = ref_inv.global_discount_exempt
								vals = {
									'dte': True,
									'tax_included': ref_inv.tax_included,
									'global_discount_percent': global_discount_percent,\
									'global_discount_percent_untaxed': global_discount_percent_untaxed, \
									'global_discount_percent_exempt': global_discount_percent_exempt, \
									'global_discount': global_discount, \
									'global_discount_untaxed': global_discount_untaxed, \
									'global_discount_exempt': global_discount_exempt
								}
								if inv.journal_id.doc_type_id.sii_code in ('110','111','112'):
									vals.update({'is_export': True})
								inv.write(vals)

								#Update Taxes
								inv.sudo().button_reset_taxes()
								self.env['l10n_cl_dte.document_references'].create({
									'invoice_id': inv.id,
									'reference_type': ref_inv.journal_id.doc_type_id.id,
									'reference_folio': ref_inv.number,
									'reference_date': ref_inv.date_invoice,
									'reference_reason_text': wizard.description,
									'reference_invoice_type': inv.journal_id.doc_type_id.sii_code
								})
		return result
l10n_cl_dte_account_invoice_refund()
