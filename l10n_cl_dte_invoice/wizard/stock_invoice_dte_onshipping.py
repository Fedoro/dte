# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _

class stock_invoice_dte_onshipping(models.TransientModel):
	_inherit = 'stock.invoice.onshipping'

	@api.model
	def _get_journal(self):
		journal_type = self._get_journal_type()
		if journal_type == 'sale': 
			picking_id = self._context.get('active_id', None)
			picking = self.env['stock.picking'].browse(picking_id)
			if picking.group_id:
				sale = self.env['sale.order'].search([('procurement_group_id','=',picking.group_id.id)])
				if sale.journal_id:
					return sale.journal_id.id or False
		elif journal_type == 'sale_refund':
			journals = self.env['account.journal'].search([('type','=',journal_type),('doc_type_id.sii_code','=','61')])
			return journals and journals[0].id or False

		journals = self.env['account.journal'].search([('type', '=', journal_type)])
		return journals and journals[0].id or False

	journal_id = fields.Many2one('account.journal', 'Destination Journal', required=True, default=_get_journal)
	invoice_date = fields.Date('Invoice Date', default=lambda self: fields.Date.context_today(self))

#Parece no ser necesaria
#	@api.multi
#	def create_invoice(self):
#		res = super(stock_invoice_dte_onshipping, self).create_invoice()
		
#136         data = self.browse(cr, uid, ids[0], context=context)
#137         journal2type = {'sale':'out_invoice', 'purchase':'in_invoice', 'sale_refund':'out_refund', 'purchase_refund':'in_refund'}
#138         context['date_inv'] = data.invoice_date
#139         acc_journal = self.pool.get("account.journal")
#140         inv_type = journal2type.get(data.journal_type) or 'out_invoice'
#141         context['inv_type'] = inv_type
#142
#143         active_ids = context.get('active_ids', [])
#144         res = picking_pool.action_invoice_create(cr, uid, active_ids,
#145               journal_id = data.journal_id.id,
#146               group = data.group,
#147               type = inv_type,
#148               context=context)

#		return res
stock_invoice_dte_onshipping()
