# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* l10n_cl_dte_stock
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 8.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-10-05 07:49+0000\n"
"PO-Revision-Date: 2018-10-05 07:49+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: l10n_cl_dte_stock
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_form
msgid "(update)"
msgstr "(update)"

#. module: l10n_cl_dte_stock
#: model:ir.actions.act_window,help:l10n_cl_dte_stock.l10n_cl_dte_stock_action
msgid "<p class=\"oe_view_nocontent_create\">\n"
"					Click to create a DTE Dispatch Order.\n"
"				</p>\n"
"			"
msgstr "<p class=\"oe_view_nocontent_create\">\n"
"					Click to create a DTE Dispatch Order.\n"
"				</p>\n"
"			"

#. module: l10n_cl_dte_stock
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_form
msgid "Additional notes..."
msgstr "Additional notes..."

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,picking_code:0
msgid "Agente de Aduana"
msgstr "Agente de Aduana"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock_lines,price_subtotal:0
msgid "Amount"
msgstr "Amount"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,amount_tax:0
#: field:l10n_cl_dte.stock,manual_amount_tax:0
msgid "Amount Tax"
msgstr "Amount Tax"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,amount_total:0
#: field:l10n_cl_dte.stock,manual_amount_total:0
msgid "Amount Total"
msgstr "Amount Total"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,amount_untaxed:0
#: field:l10n_cl_dte.stock,manual_amount_untaxed:0
msgid "Amount Untaxed"
msgstr "Amount Untaxed"

#. module: l10n_cl_dte_stock
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_form_admin
msgid "Back to Draft"
msgstr "Back to Draft"

#. module: l10n_cl_dte_stock
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_form
msgid "Base Information"
msgstr "Base Information"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,caf:0
msgid "CAF utilizado para este documento"
msgstr "CAF utilizado para este documento"

#. module: l10n_cl_dte_stock
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_form_admin
msgid "Cancel Dispatch"
msgstr "Cancel Dispatch"

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,state:0
msgid "Canceled"
msgstr "Canceled"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,carrier_id:0
msgid "Carrier"
msgstr "Carrier"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,picking_code:0
msgid "Codigo de traslado"
msgstr "Codigo de traslado"

#. module: l10n_cl_dte_stock
#: model:ir.model,name:l10n_cl_dte_stock.model_res_company
msgid "Companies"
msgstr "Companies"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,company_id:0
#: field:l10n_cl_dte.stock_lines,company_id:0
msgid "Company"
msgstr "Company"

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,mov_ind:0
msgid "Consignaciones"
msgstr "Consignaciones"

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,picking_code:0
msgid "Contribuyente autorizado expresamente por el SII."
msgstr "Contribuyente autorizado expresamente por el SII."

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,create_uid:0
#: field:l10n_cl_dte.stock_lines,create_uid:0
msgid "Created by"
msgstr "Created by"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,create_date:0
#: field:l10n_cl_dte.stock_lines,create_date:0
msgid "Created on"
msgstr "Created on"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,manual_currency_id:0
msgid "Currency"
msgstr "Currency"

#. module: l10n_cl_dte_stock
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_form
msgid "DTE"
msgstr "DTE"

#. module: l10n_cl_dte_stock
#: model:res.groups,name:l10n_cl_dte_stock.group_dispatch_orders
msgid "DTE Dispatch Orders Group"
msgstr "DTE Dispatch Orders Group"

#. module: l10n_cl_dte_stock
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_search
#: field:l10n_cl_dte.stock,date:0
msgid "Date"
msgstr "Date"

#. module: l10n_cl_dte_stock
#: help:l10n_cl_dte.stock,message_last_post:0
msgid "Date of the last message posted on the record."
msgstr "Date of the last message posted on the record."

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock_lines,name:0
msgid "Description"
msgstr "Description"

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,picking_type:0
msgid "Despacho por cuenta del emisor a instalaciones del cliente"
msgstr "Despacho por cuenta del emisor a instalaciones del cliente"

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,picking_type:0
msgid "Despacho por cuenta del emisor a otras instalaciones"
msgstr "Despacho por cuenta del emisor a otras instalaciones"

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,picking_type:0
msgid "Despacho por cuenta del receptor"
msgstr "Despacho por cuenta del receptor"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock_lines,discount:0
msgid "Discount (%)"
msgstr "Discount (%)"

#. module: l10n_cl_dte_stock
#: view:stock.picking:l10n_cl_dte_stock.l10n_cl_dte_stock_picking
msgid "Dispatch"
msgstr "Dispatch"

#. module: l10n_cl_dte_stock
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_form
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_tree
#: field:l10n_cl_dte.stock_lines,dispatch_order_id:0
msgid "Dispatch Order"
msgstr "Dispatch Order"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,dispatch_order_line:0
msgid "Dispatch Order Lines"
msgstr "Dispatch Order Lines"

#. module: l10n_cl_dte_stock
#: field:stock.move,distpatch_order_ids:0
msgid "Dispatch Order Move Lines"
msgstr "Dispatch Order Move Lines"

#. module: l10n_cl_dte_stock
#: model:ir.actions.act_window,name:l10n_cl_dte_stock.action_dispatch_order_all_tree
#: model:ir.actions.act_window,name:l10n_cl_dte_stock.l10n_cl_dte_stock_action
#: model:ir.ui.menu,name:l10n_cl_dte_stock.l10n_cl_dte_stock_menu
msgid "Dispatch Orders"
msgstr "Dispatch Orders"

#. module: l10n_cl_dte_stock
#: field:stock.picking,dispatch_orders:0
msgid "Dispatched Orders"
msgstr "Dispatched Orders"

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,state:0
msgid "Draft"
msgstr "Draft"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,driver_id:0
msgid "Driver"
msgstr "Driver"

#. module: l10n_cl_dte_stock
#: model:ir.model,name:l10n_cl_dte_stock.model_l10n_cl_dte_stock
msgid "Email Thread"
msgstr "Email Thread"

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,mov_ind:0
msgid "Entrega Gratuita"
msgstr "Entrega Gratuita"

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,state:0
msgid "Error"
msgstr "Error"

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,picking_code:0
msgid "Exportador"
msgstr "Exportador"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,auth_date:0
msgid "Fecha de Autorización"
msgstr "Fecha de Autorización"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,folio:0
msgid "Folio"
msgstr "Folio"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,auth_number:0
msgid "Folio Autorización"
msgstr "Folio Autorización"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,message_follower_ids:0
msgid "Followers"
msgstr "Followers"

#. module: l10n_cl_dte_stock
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_form
msgid "Followup Number"
msgstr "Followup Number"

#. module: l10n_cl_dte_stock
#: field:stock.picking,is_fully_dispatched:0
msgid "Fully Dispached"
msgstr "Fully Dispached"

#. module: l10n_cl_dte_stock
#: view:stock.picking:l10n_cl_dte_stock.l10n_cl_dte_stock_picking
msgid "Generate Dispatch Order"
msgstr "Generate Dispatch Order"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,tax_included:0
msgid "Gross Amount Index"
msgstr "Gross Amount Index"

#. module: l10n_cl_dte_stock
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_search
msgid "Group By"
msgstr "Group By"

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,mov_ind:0
msgid "Guia de Devolucion"
msgstr "Guia de Devolucion"

#. module: l10n_cl_dte_stock
#: help:l10n_cl_dte.stock,message_summary:0
msgid "Holds the Chatter summary (number of messages, ...). This summary is directly in html format in order to be inserted in kanban views."
msgstr "Holds the Chatter summary (number of messages, ...). This summary is directly in html format in order to be inserted in kanban views."

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,id:0
#: field:l10n_cl_dte.stock_lines,id:0
msgid "ID"
msgstr "ID"

#. module: l10n_cl_dte_stock
#: help:l10n_cl_dte.stock,message_unread:0
msgid "If checked new messages require your attention."
msgstr "If checked new messages require your attention."

#. module: l10n_cl_dte_stock
#: field:stock.move,in_dispatch:0
msgid "In Dispatch"
msgstr "In Dispatch"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,mov_ind:0
msgid "Indicador de Traslado"
msgstr "Indicador de Traslado"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,internal_number:0
msgid "Internal Number"
msgstr "Internal Number"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,message_is_follower:0
msgid "Is a Follower"
msgstr "Is a Follower"

#. module: l10n_cl_dte_stock
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_search
#: field:l10n_cl_dte.stock,journal_id:0
msgid "Journal"
msgstr "Journal"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,message_last_post:0
msgid "Last Message Date"
msgstr "Last Message Date"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,write_uid:0
#: field:l10n_cl_dte.stock_lines,write_uid:0
msgid "Last Updated by"
msgstr "Last Updated by"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,write_date:0
#: field:l10n_cl_dte.stock_lines,write_date:0
msgid "Last Updated on"
msgstr "Last Updated on"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,principal_id:0
msgid "Mandante"
msgstr "Mandante"

#. module: l10n_cl_dte_stock
#: field:res.company,stock_lines:0
msgid "Max Dispatch Order Lines"
msgstr "Max Dispatch Order Lines"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,max_stock_lines:0
msgid "Max Dte Lines"
msgstr "Max Dte Lines"

#. module: l10n_cl_dte_stock
#: view:res.company:l10n_cl_dte_stock.l10n_cl_dte_stock_company_form
#: help:res.company,stock_lines:0
msgid "Max number of lines per dispatch order"
msgstr "Max number of lines per dispatch order"

#. module: l10n_cl_dte_stock
#: view:res.company:l10n_cl_dte_stock.l10n_cl_dte_stock_company_form
msgid "Max number of lines per invoice"
msgstr "Max number of lines per invoice"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,message_ids:0
msgid "Messages"
msgstr "Messages"

#. module: l10n_cl_dte_stock
#: help:l10n_cl_dte.stock,message_ids:0
msgid "Messages and communication history"
msgstr "Messages and communication history"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,move_lines:0
msgid "Move Lines"
msgstr "Move Lines"

#. module: l10n_cl_dte_stock
#: code:addons/l10n_cl_dte_stock/models/stock_picking.py:69
#, python-format
msgid "New Dispatch Order"
msgstr "New Dispatch Order"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,next_dispatch_order:0
msgid "Next Dispatch Order"
msgstr "Next Dispatch Order"

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,impresion_type:0
msgid "Normal"
msgstr "Normal"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,comment:0
msgid "Note"
msgstr "Note"

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,mov_ind:0
msgid "Operacion Constituye Venta"
msgstr "Operacion Constituye Venta"

#. module: l10n_cl_dte_stock
#: view:stock.picking:l10n_cl_dte_stock.l10n_cl_dte_stock_picking
msgid "Orders"
msgstr "Orders"

#. module: l10n_cl_dte_stock
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_form
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_search
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_tree
msgid "Origin"
msgstr "Origin"

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,mov_ind:0
msgid "Otros Traslados no Venta"
msgstr "Otros Traslados no Venta"

#. module: l10n_cl_dte_stock
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_search
#: field:l10n_cl_dte.stock,partner_id:0
msgid "Partner"
msgstr "Partner"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,picking_id:0
msgid "Picking"
msgstr "Picking"

#. module: l10n_cl_dte_stock
#: model:ir.model,name:l10n_cl_dte_stock.model_stock_picking
msgid "Picking List"
msgstr "Picking List"

#. module: l10n_cl_dte_stock
#: code:addons/l10n_cl_dte_stock/models/l10n_cl_dte_stock.py:223
#, python-format
msgid "Please set the number of lines at:\n"
"Administration -> Company -> Configuration -> Dispatch Order Lines"
msgstr "Please set the number of lines at:\n"
"Administration -> Company -> Configuration -> Dispatch Order Lines"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock_lines,product_id:0
msgid "Product"
msgstr "Product"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock_lines,quantity:0
msgid "Quantity"
msgstr "Quantity"

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,state:0
msgid "Ready to Send"
msgstr "Ready to Send"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,failed_text:0
msgid "Reason for Failure"
msgstr "Reason for Failure"

#. module: l10n_cl_dte_stock
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_form
msgid "Reason for Failure:"
msgstr "Reason for Failure:"

#. module: l10n_cl_dte_stock
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_form
msgid "Recompute taxes and total"
msgstr "Recompute taxes and total"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,reference_lines:0
msgid "Reference Lines"
msgstr "Reference Lines"

#. module: l10n_cl_dte_stock
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_form
msgid "References"
msgstr "References"

#. module: l10n_cl_dte_stock
#: model:ir.model,name:l10n_cl_dte_stock.model_l10n_cl_dte_document_references
msgid "References for Documents"
msgstr "References for Documents"

#. module: l10n_cl_dte_stock
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_form
msgid "SII validation:"
msgstr "SII validation:"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,user_id:0
msgid "Salesperson"
msgstr "Salesperson"

#. module: l10n_cl_dte_stock
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_search
msgid "Search Dispatch Order"
msgstr "Search Dispatch Order"

#. module: l10n_cl_dte_stock
#: model:ir.model,name:l10n_cl_dte_stock.model_l10n_cl_dte_sii
msgid "Send Sii Functions"
msgstr "Send Sii Functions"

#. module: l10n_cl_dte_stock
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_form_admin
msgid "Send to SII"
msgstr "Send to SII"

#. module: l10n_cl_dte_stock
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_form_admin
msgid "Sign"
msgstr "Sign"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,signed_xml:0
msgid "Signed XML"
msgstr "Signed XML"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,state:0
msgid "State"
msgstr "State"

#. module: l10n_cl_dte_stock
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_search
msgid "Status"
msgstr "Status"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,status_text:0
msgid "Status Description"
msgstr "Status Description"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.document_references,stock_id:0
msgid "Stock"
msgstr "Stock"

#. module: l10n_cl_dte_stock
#: model:ir.model,name:l10n_cl_dte_stock.model_stock_move
msgid "Stock Move"
msgstr "Stock Move"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,message_summary:0
msgid "Summary"
msgstr "Summary"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,ted:0
msgid "TED"
msgstr "TED"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock_lines,dispatch_order_line_tax_id:0
msgid "Taxes"
msgstr "Taxes"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,batch_send:0
msgid "This dispatch order need send in bash?"
msgstr "This dispatch order need send in bash?"

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,impresion_type:0
msgid "Ticket"
msgstr "Ticket"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,impresion_type:0
msgid "Tipo de Impresion"
msgstr "Tipo de Impresion"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,picking_type:0
msgid "Tipo despacho"
msgstr "Tipo despacho"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock_lines,amount_tax:0
msgid "Total Taxes"
msgstr "Total Taxes"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,track_id:0
msgid "Track Id"
msgstr "Track Id"

#. module: l10n_cl_dte_stock
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_form
msgid "Transport"
msgstr "Transport"

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,mov_ind:0
msgid "Traslado para Exportacion. (No Venta)"
msgstr "Traslado para Exportacion. (No Venta)"

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,mov_ind:0
msgid "Traslados Internos"
msgstr "Traslados Internos"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock_lines,price_unit:0
msgid "Unit Price"
msgstr "Unit Price"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock_lines,uom_id:0
msgid "Unit of Measure"
msgstr "Unit of Measure"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,message_unread:0
msgid "Unread Messages"
msgstr "Unread Messages"

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,state:0
msgid "Validated"
msgstr "Validated"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,vehicle_id:0
msgid "Vehicle"
msgstr "Vehicle"

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,picking_code:0
msgid "Vendedor"
msgstr "Vendedor"

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,mov_ind:0
msgid "Venta para Exportacion"
msgstr "Venta para Exportacion"

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,mov_ind:0
msgid "Ventas por Efectuar"
msgstr "Ventas por Efectuar"

#. module: l10n_cl_dte_stock
#: selection:l10n_cl_dte.stock,state:0
msgid "Waiting Validation"
msgstr "Waiting Validation"

#. module: l10n_cl_dte_stock
#: field:l10n_cl_dte.stock,website_message_ids:0
msgid "Website Messages"
msgstr "Website Messages"

#. module: l10n_cl_dte_stock
#: help:l10n_cl_dte.stock,website_message_ids:0
msgid "Website communication history"
msgstr "Website communication history"

#. module: l10n_cl_dte_stock
#: code:addons/l10n_cl_dte_stock/models/l10n_cl_dte_stock.py:269
#, python-format
msgid "You can't delete a dispatch order which is not draft."
msgstr "You can't delete a dispatch order which is not draft."

#. module: l10n_cl_dte_stock
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_form_admin
#: view:l10n_cl_dte.stock:l10n_cl_dte_stock.l10n_cl_dte_stock_tree_admin
msgid "true"
msgstr "true"

