# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################


import openerp.addons.decimal_precision as dp
from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _
from elaphe import barcode
import tempfile, base64

class l10n_cl_dte_stock_lines(models.Model):
	_name = 'l10n_cl_dte.stock_lines'

	@api.one
	@api.depends('price_unit', 'discount', 'dispatch_order_line_tax_id', 'company_id', 'quantity',
		'product_id', 'dispatch_order_id.partner_id', 'dispatch_order_id.currency_id', 'dispatch_order_id.manual_currency_id')
	def _compute_price(self):
		tax = 0
		price = self.price_unit * (1 - (self.discount or 0.0) / 100.0)
		taxes = self.dispatch_order_line_tax_id.compute_all(price, self.quantity,\
								product=self.product_id, partner=self.dispatch_order_id.partner_id)
		if taxes['taxes']:
			for c in taxes['taxes']:
				tax += c.get('amount', 0.0)
		self.amount_tax = tax
		self.price_subtotal = taxes['total']
		if self.dispatch_order_id and self.dispatch_order_id.currency_id:
			self.amount_tax = self.dispatch_order_id.currency_id.round(self.amount_tax)
			self.price_subtotal = self.dispatch_order_id.currency_id.round(self.price_subtotal)
		elif self.dispatch_order_id and self.dispatch_order_id.manual_currency_id:
			self.amount_tax = self.dispatch_order_id.manual_currency_id.round(self.amount_tax)
			self.price_subtotal = self.dispatch_order_id.manual_currency_id.round(self.price_subtotal)

	@api.onchange('product_id')
	def onchange_product_id(self):
		name = self.product_id.partner_ref
		if self.product_id.description_sale:
			name += '\n' + self.product_id.description_sale
		if self.product_id.taxes_id:
			taxes = self.env['account.fiscal.position'].map_tax(self.product_id.taxes_id)
			self.dispatch_order_line_tax_id = taxes.ids if taxes else None
		self.name = name
		self.price_unit = self.product_id.lst_price

	dispatch_order_id = fields.Many2one('l10n_cl_dte.stock', 'Dispatch Order', copy=False)
	name = fields.Char('Description', required=True)
	product_id = fields.Many2one('product.product', 'Product', ondelete='restrict')
	uom_id = fields.Many2one('product.uom', 'Unit of Measure', ondelete='set null')
	quantity = fields.Float('Quantity', digits= dp.get_precision('Product Unit of Measure'), required=True, default=1)
	price_unit = fields.Float('Unit Price', required=True, digits= dp.get_precision('Product Price'), default=1.0)
	discount = fields.Float('Discount (%)', digits= dp.get_precision('Discount'), default=0.0)
	dispatch_order_line_tax_id = fields.Many2many('account.tax', 'dispatch_order_lines_tax_rel', 'line_id', 'tax_id',
				string='Taxes', domain=[('parent_id','=',False),('type_tax_use','in',['sale'])])
	amount_tax = fields.Float('Total Taxes', digits=dp.get_precision('Account'), store=True, compute='_compute_price')
	price_subtotal = fields.Float('Amount', digits=dp.get_precision('Account'), store=True, readonly=True, compute='_compute_price')
	company_id = fields.Many2one('res.company', 'Company',
				default=lambda self: self.env['res.company']._company_default_get('l10n_cl_dte.stock_lines'))
l10n_cl_dte_stock_lines()

class l10n_cl_dte_stock(models.Model):
	_name = 'l10n_cl_dte.stock'
	_inherit = ['mail.thread']
	_rec_name = 'folio'
	_order = 'date DESC, id DESC'

	@api.multi
	def sign_document(self):
		dte_attach = self.env['l10n_cl_dte.attachment']

		for order in self:
			order.sudo().button_dummy()

			if order.max_stock_lines:
				order.split_dispatch_order()

			signed_xml = self.env['l10n_cl_dte.signer'].sign_xml(order)
			result = base64.b64encode(signed_xml)

			attch_vals = dte_attach.get_attachment_values(order, result, exten='.xml')
			dte_attach.add_attachment(order, attch_vals)

			order.write({'state': 'ready_to_send', 'signed_xml': base64.b64encode(signed_xml[44:]), 'failed_text': None})
			order.move_lines.write({'in_dispatch': True})
		return True

	@api.multi
	def send_document(self):
		for order in self:
			self.env['l10n_cl_dte.sii'].send_sii(order)
		return True
	
	@api.multi
	def get_send_state(self):
		for order in self:
			self.env['l10n_cl_dte.sii'].get_send_state(order)
		return True

	@api.multi
	def cancel_dispach(self):
		for dispatch_order in self:
			dispatch_order.state = 'canceled'

	@api.multi
	def back_to_draft(self):
		for dispatch_order in self:
			dispatch_order.move_lines.write({'in_dispatch': False})
		self.write({'state': 'draft'})

	@api.depends('state', 'journal_id','company_id')
	def _send_method(self):
		for record in self:
			record.batch_send = record.company_id.batch_send

	@api.one
	@api.depends('dispatch_order_line', 'dispatch_order_line.price_subtotal',
			'dispatch_order_line.amount_tax', 'move_lines', 'move_lines.sale_subtotal')
	def _compute_amounts(self):
		if self.picking_id:
			self.amount_untaxed = sum(line.sale_subtotal for line in self.move_lines)
			self.amount_tax = sum(line.sale_taxes for line in self.move_lines)
			self.amount_total = self.amount_untaxed + self.amount_tax
		else:
			self.manual_amount_untaxed = sum(line.price_subtotal for line in self.dispatch_order_line)
			self.manual_amount_tax = sum(line.amount_tax for line in self.dispatch_order_line)
			self.manual_amount_total = self.manual_amount_untaxed + self.manual_amount_tax

	@api.multi
	def button_dummy(self):
		for dispatch in self:
			dispatch._compute_amounts()
		return True

	@api.multi
	def clean_slate(self):
		self.write({'status_text': None, 'failed_text': None})

	@api.model
	@api.depends('company_id')
	def _get_default_currency(self):
		if self.company_id:
			return self.company_id.currency_id
		else:
			return self.env['res.users'].search([('id','=',self._uid)]).company_id.currency_id

	@api.model
	def _get_default_journal(self):
		journal_obj = self.env['account.journal']
		journal = journal_obj.search([('doc_type_id.sii_code','=', '52')], limit=1)
		return journal

	@api.depends('ted')
	def _get_pdf417(self):
		for record in self:
			if not record.ted:
				continue
			image_tmp_file = tempfile.NamedTemporaryFile()
			code = barcode('pdf417', record.ted.encode('iso-8859-1'), dict(columns=18, rows=5, eclevel=5) )
			image_path = image_tmp_file.name
			code.save(image_path, format='png')
			with open(image_path, 'rb') as imageFile:
				image = base64.encodestring(imageFile.read())
			record.pdf_417 = image
			image_tmp_file.close()

	@api.onchange('picking_id')
	def onchange_picking_id(self):
		self.partner_id = self.picking_id.partner_id or None

	@api.one
	@api.depends('picking_id')
	def _get_salesman(self):
		if self.picking_id:
			sale = self.env['sale.order'].search([('procurement_group_id','=',self.picking_id.group_id.id)], limit=1)
			if sale:
				self.user_id = sale[0].user_id
			
	@api.one
	@api.depends('move_lines', 'dispatch_order_line')
	def _get_max_lines(self):
		if self.picking_id:
			lines = self.move_lines
		else:
			lines = self.dispatch_order_line

		if len(lines) > self.company_id.stock_lines:
			self.max_stock_lines = True
		else:
			self.max_stock_lines = False

	@api.multi
	def split_dispatch_order(self):
		""" Split the dispatch order when the lines exceed the maximum set for the company"""
		for order in self:
			order_id = False
			if not order.company_id.stock_lines or order.company_id.stock_lines < 1:
				raise Warning(_('Please set the number of lines at:\n'+\
							'Administration -> Company -> Configuration -> Dispatch Order Lines'))
			if order.picking_id:
				lines = order.move_lines
			else:
				lines = order.dispatch_order_line

			if len(lines) > order.company_id.stock_lines:
				lst = []
				dispatch_order = order.read(['folio', 'internal_number', 'picking_id', 'state', 'comment', 'date', \
							'dte', 'mov_ind', 'partner_id', 'journal_id', 'user_id', 'currency_id', \
							'manual_currency_id', 'carrier_id', 'driver_id', 'vehicle_id', 'move_lines', \
							'dispatch_order_line', 'journal_id', 'user_id', 'picking_type'])
				dispatch_order = dispatch_order[0]
				dispatch_order.update({'state': 'draft', 'folio': False, 'move_lines': [], 'dispatch_order_line': []})
				dispatch_order.pop('id', None)
				# take the id part of the tuple returned for many2one fields
				for field in ('partner_id', 'carrier_id', 'currency_id', 'manual_currency_id', \
							'driver_id', 'vehicle_id', 'picking_id', 'journal_id', 'user_id'):
					dispatch_order[field] = dispatch_order[field] and dispatch_order[field][0]
				new_order = self.create(dispatch_order)
				order.next_dispatch_order = new_order.id
				
				cont = 0
				lst = list(lines)

				while cont < order.company_id.stock_lines:
					lst.pop(0)
					cont += 1

				for il in lst:
					if order.picking_id:
						order.write({'move_lines': [(3,il.id,False)]})
						new_order.write({'move_lines': [(4,il.id,False)]})
					else:
						il.write({'dispatch_order_id': new_order.id})

				order.button_dummy()
			if new_order:
				new_order.button_dummy()
		return True
	
        @api.multi
        def unlink(self):
		for order in self:
			if order.state != 'draft':
				raise Warning(_('You can\'t delete a dispatch order which is not draft.'))
		return super(l10n_cl_dte_stock, self).unlink()

        batch_send = fields.Boolean('This dispatch order need send in bash?', compute='_send_method',copy=False,store=True)
	picking_id = fields.Many2one('stock.picking', 'Picking', copy=False,
								readonly=True, states={'draft': [('readonly', False)]})
	folio = fields.Char('Folio', copy=False, readonly=True, states={'draft': [('readonly', False)]})
	internal_number = fields.Char('Internal Number', related='picking_id.name', copy=False, store=True)
	state = fields.Selection([('draft','Draft'),('ready_to_send','Ready to Send'),('error','Error'),\
					('waiting_validation','Waiting Validation'),('validated','Validated'),\
						('canceled','Canceled')], 'State', copy=False, default='draft')
	pdf_417 = fields.Binary('PDF Ted Img',compute='_get_pdf417', copy=False)
	signed_xml = fields.Binary('Signed XML', copy=False)
	track_id = fields.Char('Track Id', copy=False)
	failed_text = fields.Text('Reason for Failure', copy=False)
	status_text = fields.Text('Status Description', copy=False)
	ted = fields.Text('TED', copy=False)# -> Se usa para generar el PDF417 con la imagen que viene en la respuesta, 
						# si no lo voy a hacer lo obvio.
	caf = fields.Many2one('l10n_cl_dte.caf', 'CAF utilizado para este documento', domain="[('journal_id','=',journal_id)]", copy=False)
	partner_id = fields.Many2one('res.partner', 'Partner', required=True, domain="[('customer','=',True)]",
								readonly=True, states={'draft': [('readonly', False)]})
	date = fields.Date('Date', required=True, readonly=True, states={'draft': [('readonly', False)]})
	journal_id = fields.Many2one('account.journal', 'Journal', required=True, readonly=True, states={'draft': [('readonly', False)]},
						default=_get_default_journal, domain="[('doc_type_id.sii_code','=','52')]")
#	reception_ack = fields.Boolean('Reception Ack', default=False, copy=False)
#	reception_ack_text = fields.Text('Reception Ack Text', copy=False)
#	comercial_ack = fields.Boolean('Comercial Ack', default=False, copy=False)
#	comercial_ack_text = fields.Text('Comercial Ack Text', copy=False)
	reference_lines = fields.One2many('l10n_cl_dte.document_references', 'stock_id', 'Reference Lines', 
							copy=False, readonly=True, states={'draft': [('readonly', False)]})
	mov_ind = fields.Selection([(1,'Operacion Constituye Venta'),\
						# Operation is Sale
					(2,'Ventas por Efectuar'),\
						# Sales to do
					(3,'Consignaciones'),\
						# Consignment
					(4,'Entrega Gratuita'),\
						# Free Delivery
					(5,'Traslados Internos'),\
						# Internal movements
					(6,'Otros Traslados no Venta'),\
						# Other Movements doesn't Sale
					(7,'Guia de Devolucion'),\
						# Return picking
					(8,'Traslado para Exportacion. (No Venta)'),\
						# Export movement
					(9,'Venta para Exportacion')], 'Indicador de Traslado', default=1,
						# Export Sale / Movement Indicator
							required=True, readonly=True, states={'draft': [('readonly', False)]})
	company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env['res.company'].\
			_company_default_get('l10n_cl_dte.stock'), readonly=True, states={'draft': [('readonly', False)]})
	currency_id = fields.Many2one('res.currency', 'Currency', related='picking_id.currency_id')
	manual_currency_id = fields.Many2one('res.currency', 'Currency', default=_get_default_currency,
								readonly=True, states={'draft': [('readonly', False)]})
	move_lines = fields.Many2many('stock.move', 'dispatch_order_move_lines_rel', 'order_id', 'move_id',
						string='Move Lines', readonly=True, states={'draft': [('readonly', False)]})
	dispatch_order_line = fields.One2many('l10n_cl_dte.stock_lines', 'dispatch_order_id', 'Dispatch Order Lines', copy=True,
								readonly=True, states={'draft': [('readonly', False)]})
	amount_untaxed = fields.Float('Amount Untaxed', compute='_compute_amounts', digits=dp.get_precision('DTE'), store=True)
	amount_tax = fields.Float('Amount Tax', compute='_compute_amounts', digits=dp.get_precision('DTE'), store=True)
	amount_total = fields.Float('Amount Total', compute='_compute_amounts', digits=dp.get_precision('DTE'), store=True)
	manual_amount_untaxed = fields.Float('Amount Untaxed', compute='_compute_amounts', digits=dp.get_precision('DTE'), store=True)
	manual_amount_tax = fields.Float('Amount Tax', compute='_compute_amounts', digits=dp.get_precision('DTE'), store=True)
	manual_amount_total = fields.Float('Amount Total', compute='_compute_amounts', digits=dp.get_precision('DTE'), store=True)
	user_id = fields.Many2one('res.users', 'Salesperson', compute=_get_salesman, store=True)
	max_stock_lines = fields.Boolean('Max Dte Lines', compute=_get_max_lines, default=False, store=True)
	next_dispatch_order = fields.Many2one('l10n_cl_dte.stock', 'Next Dispatch Order', copy=False)
	carrier_id = fields.Many2one('l10n_cl_dte.carrier', 'Carrier', readonly=True, states={'draft': [('readonly', False)]})
	driver_id = fields.Many2one('l10n_cl_dte.driver', 'Driver', domain="[('carrier_id','=',carrier_id)]", 
									readonly=True, states={'draft': [('readonly', False)]})
	vehicle_id = fields.Many2one('l10n_cl_dte.vehicle', 'Vehicle', domain="[('carrier_id','=',carrier_id)]",
									readonly=True, states={'draft': [('readonly', False)]})
	comment = fields.Text('Note')
	tax_included = fields.Boolean('Gross Amount Index', default=False, readonly=True, states={'draft': [('readonly', False)]})
	is_export = fields.Boolean('Export', default=False)
##############################################################################################################################
	# FchCancel, Fecha de cancelación: Sólo se utiliza si la factura ha sido cancelada antes de la fecha de emisión. (AAAA-MM-DD)
	# Fecha válida entre 2002-08-01 y 2050-12-31 
	# Campo Obligatorio para Factura de exportación cuando en "Forma de Pago Exportación" se indique "anticipo"
	date_cancel = fields.Date('Date Cancel')

	# TipoDespacho, indica si el documento acompana bienes y el despacho es por cuenta del vendedor o del comprador.
	# No se incluye si el documento no acompana bienes o se trata de una Factura o Nota correspondiente a la prestacion de servicios.
	# 1: Despacho por cuenta del receptor del  documento (cliente o  vendedor  en caso de Facturas de compra.)
	# 2: Despacho por cuenta del  emisor a instalaciones del  cliente
	# 3: Despacho por cuenta del emisor a otras instalaciones (Ejemplo: entrega en Obra)
	picking_type = fields.Selection([(1,'Despacho por cuenta del receptor'), 
					 (2,'Despacho por cuenta del emisor a instalaciones del cliente'),
					 (3,'Despacho por cuenta del emisor a otras instalaciones')], 'Tipo despacho',
										readonly=True, states={'draft': [('readonly', False)]})

	# TpoImpresion Describe modalidad de Impresion de la representacion impresa en formato normal o en formato Ticket
	impresion_type = fields.Selection([('N','Normal'), ('T','Ticket')], 'Tipo de Impresion', default='N')
	# CdgTraslado , Código Emisor Traslado Excepcional
	# Solo para Guia de Despacho:
	# 1: Exportador
	# 2: Agente de Aduana (En la devolución de mercaderías de Aduanas.
	# 3: Vendedor (Entre otros, se refiere a aquel Productor que vende mercadería con entrega en Zona Primaria).
	# 4: Contribuyente autorizado expresamente por el SII.
	picking_code = fields.Selection([(1,'Exportador'), (2,'Agente de Aduana'), (3,'Vendedor'),
                                            (4,'Contribuyente autorizado expresamente por el SII.')], 'Codigo de traslado')

	# FolioAut, Folio Autorización 
	# Sólo para Guía de Despacho: Corresponde al N° de Resolución del SII donde en casos especiales se autoriza al contribuyente a emitir guías de despacho.
	# Campo obligatorio cuando se indique 4 en el Código Emisor Traslado Excepcional.
	auth_number = fields.Integer("Folio Autorización")

	# FchAut, Fecha Autorización 
	# Sólo para Guía de Despacho: Fecha de emisión de la Resolución de autorización (AAAA-MM-DD)
	# Campo obligatorio cuando se indique 4 en el Código Emisor Traslado Excepcional.
	auth_date = fields.Date('Fecha de Autorización')

	# RUTMandante, Rut Mandante
	# Corresponde al RUT del mandante si el TOTAL de la venta o servicio es por cuenta de otro el 
	# cual es responsable del IVA devengado en el periodo con guion y digito verificador
	principal_id = fields.Many2one('res.partner', 'Mandante', copy=False)
l10n_cl_dte_stock()

