# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _

class l10n_cl_dte_stock_move(models.Model):
	_inherit = 'stock.move'

	in_dispatch = fields.Boolean('In Dispatch', default=False)
	distpatch_order_ids = fields.Many2many('l10n_cl_dte.stock', 'dispatch_order_move_lines_rel', 'move_id', 'order_id',
					string='Dispatch Order Move Lines', readonly=True, states={'draft': [('readonly', False)]})
l10n_cl_dte_stock_move()

class l10n_cl_dte_stock_picking(models.Model):
	_inherit = 'stock.picking'

	@api.one
	@api.depends('dispatch_orders', 'dispatch_orders.state', 'move_lines', 'move_lines.in_dispatch')
	def _fully_dispatched(self):
		cond = True
		line_ids = [line.id for dispatch in self.dispatch_orders for line in dispatch.move_lines]
		for move_line in self.move_lines:
			if move_line.id not in line_ids:
				cond = False
				break
		self.is_fully_dispatched = cond

	@api.multi
	def generate_dipatch_order(self):
		dispatch_orders = []
		for picking in self:
			vals = {'partner_id': picking.partner_id and picking.partner_id.id or None,
					'date': fields.Date.context_today(self), 'picking_id': picking.id, 'move_lines': []}
			for line in picking.move_lines:
				vals['move_lines'].append((4,line.id,False))
			dispatch_orders.append(self.env['l10n_cl_dte.stock'].create(vals))
			picking.is_fully_dispatched = True

		return {
			'domain': "[('id', 'in', " + str([dispatch.id for dispatch in dispatch_orders]) + ")]",
			'name': _('New Dispatch Order'),
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'l10n_cl_dte.stock',
			'type': 'ir.actions.act_window',
			'context': self._context,
		}

	dispatch_orders = fields.One2many('l10n_cl_dte.stock', 'picking_id', 'Dispatched Orders')
	is_fully_dispatched = fields.Boolean('Fully Dispached', compute=_fully_dispatched, store=True)
l10n_cl_dte_stock_picking()
