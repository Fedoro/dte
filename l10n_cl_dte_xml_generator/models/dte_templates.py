# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from datetime import datetime as dt
from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _
import time, base64

class DteTemplates(models.Model):
	_name = 'dte.templates'

	def document_2_ted(self, document):
		#TODO quitar toos los condenados unicode cuando si funcione la porqueria de impresora termica.
		data = {}

		if document._name == 'pos.order':
			data['rut_emisor'] = document.rut_emisor
		else:
			data['rut_emisor']  = document.company_id.partner_id.rut
		
		if document._name == "pos.order":
			data['document_type'] = document.document_type
		else:
			data['document_type'] = document.journal_id.doc_type_id.sii_code
		
		if document._name == 'account.invoice':
			data['folio'] = document.number
		elif document._name == 'l10n_cl_dte.stock':
			data['folio'] = document.folio
		elif document._name == 'pos.order':
			data['folio'] = document.folio

		if document._name == 'account.invoice':
			data['date'] = document.date_invoice[:10]
		elif document._name == 'l10n_cl_dte.stock':
			data['date'] = document.date
		elif document._name == "pos.order":
			data['date'] = document.date

		if document._name == "pos.order":
			data['receptor_rut'] = document.receptor_rut
		else:
			if document.journal_id.doc_type_id.sii_code in ('39', '41'):
				data['receptor_rut'] = document.partner_id.rut or document.partner_id.parent_id.rut or '66666666-6'
			elif document.journal_id.doc_type_id.sii_code in ('110', '111', '112'):
				data['receptor_rut'] = '55555555-5'
			else:
				if document.partner_id.rut or document.partner_id.parent_id.rut:
					data['receptor_rut'] = document.partner_id.rut or document.partner_id.parent_id.rut
				else:
					raise Warning(_('You must set the customer %s Rut.' % (document.partner_id.parent_id and document.partner_id.parent_id.name or document.partner_id.name)))

		if document._name == "pos.order":
			data['receptor_name'] = document.receptor_name[:40].decode('utf-8', errors='ignore')
		else:
			data['receptor_name'] = document.partner_id.name[:40].decode('utf-8', errors='ignore')

		if document._name == 'account.invoice':
			data['total_amount'] = int(round(document.amount_total, 0))
		elif document._name == 'l10n_cl_dte.stock':
			if document.picking_id:
				data['total_amount'] = int(round(document.amount_total, 0))
			else:
				data['total_amount'] = int(round(document.manual_amount_total, 0))
		elif document._name == 'pos.order':
			data['total_amount'] = document.total_amount

		if document._name == 'account.invoice':
			if document.invoice_line[0].product_id.name:
				data['first_item'] = document.invoice_line[0].product_id.name[:40].decode('utf-8', errors='ignore')
			else:
				data['first_item'] = document.invoice_line[0].name[:40].decode('utf-8', errors='ignore')
		elif document._name == 'l10n_cl_dte.stock':
			if document.picking_id:
				if document.move_lines[0].product_id:
					data['first_item'] = document.move_lines[0].product_id.name[:40].decode('utf-8', errors='ignore')
				else:
					data['first_item'] = document.move_lines[0].name[:40].decode('utf-8', errors='ignore')
			else:
				if document.dispatch_order_line[0].product_id:
					data['first_item'] = document.dispatch_order_line[0].product_id.name[:40].decode('utf-8', errors='ignore')
				else:
					data['first_item'] = document.dispatch_order_line[0].name[:40].decode('utf-8', errors='ignore')
		elif document._name == 'pos.order':
			data['first_item'] = document.first_item.decode('utf-8', errors='ignore')

		if document._name == 'pos.order':
			data['caf'] = document.caf
		else:
			data['caf'] = document.caf.caf
		return data

	def get_ted(self, document):
		if document._name in ['account.invoice','l10n_cl_dte.stock', 'pos.order']:
			data = self.document_2_ted(document)

		template = """<TED version="1.0">\n"""
		template += """<DD>\n"""
		template += """<RE>%s</RE>\n""" % data['rut_emisor'].replace('.','').replace('k','K')
		template += """<TD>%s</TD>\n""" % data['document_type']
		template += """<F>%s</F>\n""" % data['folio'] #
		template += """<FE>%s</FE>\n""" % data['date']
		template += """<RR>%s</RR>\n""" % data['receptor_rut'].replace('.','').replace('k','K')
		template += """<RSR>%s</RSR>\n""" % data['receptor_name'].replace('&','&amp;').replace('<','&lt;').replace('>','&gt;')
		template += """<MNT>%s</MNT>\n""" % int(data['total_amount'])
		template += """<IT1>%s</IT1>\n""" % data['first_item'].replace('&','&amp;').replace('<','&lt;').replace('>','&gt;')
		template += """%s""" % data['caf']
		template += """<TSTED>%s</TSTED>\n""" % dt.now().strftime('%Y-%m-%dT%H:%M:%S')
		template += """</DD>\n"""
		template += """</TED>\n"""
		return template

	def get_signature(self, reference=False):
		signature = """<Signature xmlns="http://www.w3.org/2000/09/xmldsig#">\n"""
		signature += """<SignedInfo>\n""" 
		signature += """<CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" />\n""" 
		signature += """<SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1" />\n""" 
		signature += """<Reference URI="%s">\n""" % (('#' + reference) if reference else "")
		signature += """<DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1" />\n""" 
		signature += """<DigestValue>\n"""
		signature += """</DigestValue>\n""" 
		signature += """</Reference>\n""" 
		signature += """</SignedInfo>\n""" 
		signature += """<SignatureValue>\n"""
		signature += """</SignatureValue>\n""" 
		signature += """<KeyInfo>\n""" 
		signature += """<KeyValue>\n""" 
		signature += """</KeyValue>\n""" 
		signature += """<X509Data>\n""" 
		signature += """<X509Certificate>\n"""
		signature += """</X509Certificate>\n""" 
		signature += """</X509Data>\n""" 
		signature += """</KeyInfo>\n""" 
		signature += """</Signature>\n""" 
		return signature

	def get_encabezado(self, document):
		sii_code = document.journal_id.doc_type_id.sii_code
		template = """<Encabezado>\n"""
		template += """<IdDoc>\n"""
		template += """<TipoDTE>%s</TipoDTE>\n""" % sii_code

		if document._name == 'account.invoice':
			if document.number:
				template += """<Folio>%s</Folio>\n""" % document.number
			else:
				raise Warning(_('No Folio Number'))
		elif document._name == 'l10n_cl_dte.stock':
			if document.folio:
				template += """<Folio>%s</Folio>\n""" % document.folio
			else:
				raise Warning(_('No Folio Number'))

		#fecha de emision entre 2003-04-01 y 2050-12-31
		if document._name == 'account.invoice':
			if document.date_invoice:
				template += """<FchEmis>%s</FchEmis>\n""" % document.date_invoice
			else:
				raise Warning(_('No Date.'))
		elif document._name == 'l10n_cl_dte.stock':
			if document.date:
				template += """<FchEmis>%s</FchEmis>\n""" % document.date
			else:
				raise Warning(_('No Date.'))

		if document._name == 'account.invoice':
			#  Sólo para Notas de Crédito que no tienen derecho a Rebaja del Débito
			if sii_code == '61' and document.no_discount_rate:
				template += """<IndNoRebaja>%s</IndNoRebaja>\n""" % document.no_discount_rate

		if document.picking_type and sii_code not in ('40', '43', '55', '56', '60', '61', '104', '106', '111', '112'):
			template += """<TipoDespacho>%s</TipoDespacho>\n""" % document.picking_type

		if document._name == 'l10n_cl_dte.stock':
			if document.mov_ind:
				template += """<IndTraslado>%s</IndTraslado>\n""" % document.mov_ind
			else:
				raise Warning(_('No Move Index.'))
			if document.impresion_type :
				template += """<TpoImpresion>%s</TpoImpresion>\n""" % document.impresion_type

		if document._name == 'account.invoice':
			if sii_code in ('39', '41'):
				template += """<IndServicio>%s</IndServicio>\n""" % document.service_type
			elif document.service_index:
				if document.service_index in (4, 5): # indice de servicio hoteleria y servicio de transporte terrestre internacional
					if sii_code in ('101', '110'): # para factura de exportacion y exportacion electronica
						template += """<IndServicio>%s</IndServicio>\n""" % document.service_index
					else:
						raise Warning(_('The service index %s, only can be use with ' +\
										'export invoices.' % document.service_index))
				else:
					template += """<IndServicio>%s</IndServicio>\n""" % document.service_index

		if document.tax_included:
			if sii_code == '39':
				if not document.tax_included:
					template += """<IndMntNeto>%s</IndMntNeto>\n""" % (1)
			elif sii_code not in ('110', '111', '112'):
				template += """<MntBruto>%s</MntBruto>\n""" % (document.tax_included and 1)

		if document._name == 'account.invoice':
			if sii_code in ('110', '111', '112'):
				if document.payment_term:
					template += """<FmaPago>%s</FmaPago>\n""" % document.payment_term.sii_payment_term
				if document.payment_term.sii_exp_payment_term:
					template += """<FmaPagExp>%s</FmaPagExp>\n""" % document.payment_term.sii_exp_payment_term
					if sii_code == '110' and \
								document.payment_term.sii_exp_payment_term == 'anticipo':
						if document.date_cancel:
							template += """<FchCancel>%s</FchCancel>\n""" % document.date_cancel
						else:
							raise Warning(_('You must set the field Date Cancel.'))
					elif document.date_cancel:
						template += """<FchCancel>%s</FchCancel>\n""" % document.date_cancel
			elif sii_code in ('33', '34', '56', '61'):
				if document.payment_term:
					if document.payment_term.sii_payment_term:
						template += """<FmaPago>%s</FmaPago>\n""" % document.payment_term.sii_payment_term
					else:
						raise Warning(_('No SII payment term found. Please Check.'))
				else:
					raise Warning(_('No payment term found, Please Check.'))
				if document.date_cancel:
					template += """<FchCancel>%s</FchCancel>\n""" % document.date_cancel
		elif document._name == 'l10n_cl_dte.stock':
			if document.date_cancel:
				template += """<FchCancel>%s</FchCancel>\n""" % document.date_cancel

		# <MntCancel> totalmente opcional, y es al momento de emitirse el documento, siendo el monto cancelado (pagado)
		# <SaldoInsol> totalmente opcional, y es al momento de emitirse el documento , siendo el monto que queda por pagar 

		# Por cada pago programado hasta 30
		# <MntPagos>
		# <FchPago></FchPago> es requerido 
		# <MntPago></MntPago> es requerido 
		# <GlosaPagos></GlosaPagos> opcional, menos para la factura de exportacion electronica 
		# </MntPagos>
		# Para servicios Periodicos, las dos fechas que estan a continuacion

		if document._name == 'account.invoice':
			if document.date_from:
				template += """<PeriodoDesde>%s</PeriodoDesde>\n""" % document.date_from
			if document.date_to:
				template += """<PeriodoHasta>%s</PeriodoHasta>\n""" % document.date_to
	
			if document.payment_form_dte:
				template += """<MedioPago>%s</MedioPago>\n""" % document.payment_form_dte
			if document.payment_form_type:
				template += """<TipoCtaPago>%s</TipoCtaPago>\n""" % document.payment_form_type
			if document.payment_account:
				template += """<NumCtaPago>%s</NumCtaPago>\n""" % document.payment_account
			if document.payment_bank:
				template += """<BcoPago>%s</BcoPago>\n""" % document.payment_bank

############TODO Estos son igual para la guia de despacho, pero como no se han agregado los campos los deje solo para la factura############
### QUITAR DESPUES ###
		if document._name == 'account.invoice' and sii_code not in ('39', '41'):
######################
			if document.payment_cod_term:
				template += """<TermPagoCdg>%s</TermPagoCdg>\n""" % document.payment_cod_term
				if sii_code in ('110', '111', '112'):
					if document.payment_comment_term:
						template += """<TermPagoGlosa>%s</TermPagoGlosa>\n""" % document.payment_comment_term
					else:
						raise Warning(_('You must set the field Payment Comment Term for export documents if you add payment comment term.'))
			if document.payment_comment_term and sii_code not in ('110', '111', '112'):
				template += """<TermPagoGlosa>%s</TermPagoGlosa>\n""" % document.payment_comment_term
			if document.payment_days_term: 
				template += """<TermPagoDias>%s</TermPagoDias>\n""" % document.payment_days_term
############################################################################################################################################
		if document._name == 'account.invoice':
			if sii_code in ('39', '41') and document.service_type in ('3', '4'):
				pass
			elif document.date_due:
				template += """<FchVenc>%s</FchVenc>\n""" % document.date_due

		template += """</IdDoc>\n"""
		template += """<Emisor>\n"""
		
		if document.company_id.partner_id.rut: 
			template += """<RUTEmisor>%s</RUTEmisor>\n""" % document.company_id.partner_id.rut.replace('.','').replace('k','K')
		else:
			raise Warning(_('No Company rut.'))

		if document.company_id.partner_id.name:
			if sii_code in ('39', '41'):
				template += """<RznSocEmisor>%s</RznSocEmisor>\n""" % (document.company_id.partner_id.name[:100].\
										replace('&','&amp;').replace('<','&lt;').replace('>','&gt;'))
			else:
				template += """<RznSoc>%s</RznSoc>\n""" % (document.company_id.partner_id.name[:100].replace('&','&amp;').\
												replace('<','&lt;').replace('>','&gt;'))
		else:
			raise Warning(_('No Company Fantasy Name.'))

		if document.company_id.partner_id.line_of_business:
			if sii_code in ('39', '41'):
				template += """<GiroEmisor>%s</GiroEmisor>\n""" % (document.company_id.partner_id.line_of_business[:80].\
										replace('&','&amp;').replace('<','&lt;').replace('>','&gt;'))
			else:
				template += """<GiroEmis>%s</GiroEmis>\n""" % (document.company_id.partner_id.line_of_business[:80].\
										replace('&','&amp;').replace('<','&lt;').replace('>','&gt;'))
		else:
			raise Warning(_('No Company Line of Business.'))

		if sii_code not in ('39', '41'):
			if document.company_id.partner_id.phone:
				template += """<Telefono>%s</Telefono>\n""" % document.company_id.partner_id.phone[:20]
			if document.company_id.partner_id.mobile:
				template += """<Telefono>%s</Telefono>\n""" % document.company_id.partner_id.mobile[:20]
			if document.company_id.partner_id.email:
				template += """<CorreoEmisor>%s</CorreoEmisor>\n""" % document.company_id.partner_id.email[:80]

		if len(document.company_id.partner_id.economic_activities_ids) > 0 and sii_code not in ('39', '41'):
			for activity in document.company_id.partner_id.economic_activities_ids:
				#TODO: Solo vamos a colocar el primero por el momento por que el giro tiene que ser relacionado a la venta, no todos 
				template += """<Acteco>%s</Acteco>\n""" %  document.company_id.partner_id.economic_activities_ids[0].code
		elif sii_code in ('39', '41'):
			pass
		else:
			raise Warning(_('No Company economic activities.'))

		if document._name == 'l10n_cl_dte.stock':
			if document.mov_ind in (8, 9):
				if document.picking_code:
					template += """<CdgTraslado>%s</CdgTraslado>\n""" % document.picking_code
				else:
					raise Warning(_('For Transfer Indicator %s it\'s necessary select a Transfer Code.' % mov_ind))

			if document.picking_code == 4:
				if document.auth_number:
					template += """<FolioAut>%s</FolioAut>\n""" % document.auth_number
				else:
					raise Warning(_('For Transfer Code %s it\'s necessary ' +\
									'provide an Authorization Number.' % document.picking_code))
				if document.auth_date:
					template += """<FchAut>%s</FchAut>\n""" % document.auth_date
				else:
					raise Warning(_('For Transfer Code %s it\'s necessary ' +\
								'provide an Autorizacion Date.' % document.picking_code))

		if sii_code not in ('39', '41'):
			if document.user_id and document.user_id.default_section_id and document.user_id.default_section_id.sii_sub_name:
				template += """<Sucursal>%s</Sucursal>\n""" % (document.user_id.default_section_id.sii_sub_name.replace('&','&amp;').\
													replace('<','&lt;').replace('>','&gt;'))
			if document.user_id and document.user_id.default_section_id and document.user_id.default_section_id.sii_sub_code:
				template += """<CdgSIISucur>%s</CdgSIISucur>\n""" % (document.user_id.default_section_id.sii_sub_code)
		#TODO: <CodAdicSucur> "codigo de identificacion para uso libre" es opcional para los documentos de exportacion , los otros no lo necesitan

		if document.company_id.partner_id.street and document.company_id.partner_id.street2:
			dirOrigen = '%s %s' % (document.company_id.partner_id.street.replace('&','&amp;').replace('<','&lt;').\
								replace('>','&gt;'), document.company_id.partner_id.street2.\
								replace('&','&amp;').replace('<','&lt;').replace('>','&gt;'))
			template += """<DirOrigen>%s</DirOrigen>\n""" % (dirOrigen[:70])
		elif document.company_id.partner_id.street:
			template += """<DirOrigen>%s</DirOrigen>\n""" % (document.company_id.partner_id.street[:70].\
										replace('&','&amp;').replace('<','&lt;').replace('>','&gt;'))
		elif document.company_id.partner_id.street2:
			template += """<DirOrigen>%s</DirOrigen>\n""" % (document.company_id.partner_id.street2[:70].\
										replace('&','&amp;').replace('<','&lt;').replace('>','&gt;'))
		elif sii_code in ('56', '61', '111', '112'):
			template += """<DirOrigen></DirOrigen>\n"""
		else:
			raise Warning(_('No Company Street.'))

		if document.company_id.partner_id.state_id.name:
			template += """<CmnaOrigen>%s</CmnaOrigen>\n""" % (document.company_id.partner_id.state_id.name[:20].\
										replace('&','&amp;').replace('<','&lt;').replace('>','&gt;'))
		elif sii_code not in ('33', '34', '52'):
			template += """<CmnaOrigen></CmnaOrigen>\n"""
		else:
			raise Warning(_('No Company State.'))

		if document.company_id.partner_id.city:
			template += """<CiudadOrigen>%s</CiudadOrigen>\n""" % (document.company_id.partner_id.city[:20].\
										replace('&','&amp;').replace('<','&lt;').replace('>','&gt;'))
		elif sii_code != '110':
			template += """<CiudadOrigen></CiudadOrigen>\n"""
		else:
			raise Warning(_('No Company City.'))

		if sii_code not in ('39', '41'):
			if document.user_id:
				template += """<CdgVendedor>%s</CdgVendedor>\n""" % (document.user_id.name[:60].\
									replace('&','&amp;').replace('<','&lt;').replace('>','&gt;'))
			if document.company_id.partner_id.aditional_code and sii_code in ('52', '110', '111', '112'):
				template += """<IdAdicEmisor>%s</IdAdicEmisor>\n""" % (document.company_id.partner_id.aditional_code)

		template += """</Emisor>\n"""

		if document.principal_id and sii_code in ('33', '34', '52', '56', '61'):
			template +=  """<RUTMandante>%s</RUTMandante>\n""" % document.principal_id.rut.replace('.','').replace('k','K')

		template += """<Receptor>\n"""

		if sii_code in ('110', '111', '112'):
			template += """<RUTRecep>%s</RUTRecep>\n""" % ('55555555-5')
		elif document.partner_id.parent_id and document.partner_id.parent_id.rut:
			template += """<RUTRecep>%s</RUTRecep>\n""" % (document.partner_id.parent_id.rut.replace('.','').replace('k','K'))
		elif document.partner_id.rut:
			template += """<RUTRecep>%s</RUTRecep>\n""" % (document.partner_id.rut.replace('.','').replace('k','K'))
		elif sii_code in ('39', '41'):
			template += """<RUTRecep>%s</RUTRecep>\n""" % ('66666666-6')
		else:
			raise Warning(_('No Customer Rut.'))

		if document.partner_id.internal_code:
			template +=  """<CdgIntRecep>%s</CdgIntRecep>\n""" % document.partner_id.internal_code
		if document.partner_id.parent_id and document.partner_id.parent_id.name:
			template += """<RznSocRecep>%s</RznSocRecep>\n""" % (document.partner_id.parent_id.name[:100].replace('&','&amp;').\
												replace('<','&lt;').replace('>','&gt;'))
		elif document.partner_id.name:
			template += """<RznSocRecep>%s</RznSocRecep>\n""" % (document.partner_id.name[:100].replace('&','&amp;').\
												replace('<','&lt;').replace('>','&gt;'))
		else:
			raise Warning(_('No Customer Name.'))

		if document.partner_id.foreign_rut and sii_code not in ('39', '41', '43') and \
				(document.partner_id.rut or document.partner_id.nationality_id or document.partner_id.foreign_internal_code):
			template += """<Extranjero>\n"""
			if document.partner_id.rut:
				template += """<NumId>%s</NumId>\n""" % document.partner_id.rut
			if document.partner_id.nationality_id:
				template += """<Nacionalidad>%s</Nacionalidad>\n""" % document.partner_id.nationality_id.code
			if document.partner_id.foreign_internal_code and sii_code in ('110', '111', '112'):
				template += """<IdAdicRecep>%s</IdAdicRecep>\n""" % document.partner_id.foreign_internal_code[:20]
			template += """</Extranjero>\n"""

		if sii_code in ('39', '41'):
			pass
		elif document.partner_id.parent_id and document.partner_id.parent_id.line_of_business:
			template += """<GiroRecep>%s</GiroRecep>\n""" % document.partner_id.parent_id.line_of_business[:40].\
										replace('&','&amp;').replace('<','&lt;').replace('>','&gt;')
		elif document.partner_id.line_of_business:
			template += """<GiroRecep>%s</GiroRecep>\n""" % document.partner_id.line_of_business[:40].\
										replace('&','&amp;').replace('<','&lt;').replace('>','&gt;')
		# no juntamos esta condicion con la primera por que si esta en lo coloca antes, esto esta solo para que no salte el 
		# raise que esta al final para los documentos de exportacion que este campo es opcional			
		elif sii_code in ('56', '61', '110', '111', '112'): 
			pass
		else:
			raise Warning(_('No Customer Line of Business.'))

		#TODO Modificar y agregar al contacto un campo que se incluya para ingresarlo aki.
#		if len(document.partner_id.child_ids) > 0:
#			template += """<Contacto>%s</Contacto>\n""" % document.partner_id.child_ids[0].name[:80]
#			template += """<CorreoRecep>%s</CorreoRecep>\n""" % document.partner_id.child_ids[0].email[:80]

		if sii_code in ('39', '41') and document.service_type in ('3', '4'):
			pass
		elif document.partner_id.street and document.partner_id.street2:
			dirRecep = '%s %s' % (document.partner_id.street.replace('&','&amp;').replace('<','&lt;').replace('>','&gt;'), \
							document.partner_id.street2.replace('&','&amp;').replace('<','&lt;').replace('>','&gt;'))
			template += """<DirRecep>%s</DirRecep>\n""" % (dirRecep[:70])
		elif document.partner_id.street:
			template += """<DirRecep>%s</DirRecep>\n""" % (document.partner_id.street[:70].replace('&','&amp;').\
												replace('<','&lt;').replace('>','&gt;'))
		elif document.partner_id.street2:
			template += """<DirRecep>%s</DirRecep>\n""" % (document.partner_id.street2[:70].replace('&','&amp;').\
												replace('<','&lt;').replace('>','&gt;'))
		elif sii_code in ('56', '61', '111', '112'):
			template += """<DirRecep></DirRecep>\n"""
		else:
			raise Warning(_('No Customer Street.'))

		if sii_code in ('39', '41') and document.service_type in ('3', '4'):
			pass
		elif document.partner_id.state_id:
			template += """<CmnaRecep>%s</CmnaRecep>\n""" % (document.partner_id.state_id.name[:20].replace('&','&amp;').\
												replace('<','&lt;').replace('>','&gt;'))
		elif sii_code in ('56', '61', '111', '112'):
			template += """<CmnaRecep></CmnaRecep>\n"""
		else:
			raise Warning(_('No Customer State.'))

		if sii_code in ('39', '41') and document.service_type in ('3', '4'):
			pass
		elif document.partner_id.city:
			template += """<CiudadRecep>%s</CiudadRecep>\n""" % (document.partner_id.city[:20].replace('&','&amp;').\
												replace('<','&lt;').replace('>','&gt;'))
		elif sii_code != '110':
			template += """<CiudadRecep></CiudadRecep>\n"""
		else:
			raise Warning(_('No Customer City.'))
		#No voy a colocar los campos DirPostal, CmnaPostal, y CiudadPostal por que son analogos a los campos de direccion del receptor
		template += """</Receptor>\n"""

		if document._name == 'account.invoice':
			if document.applicant_id and sii_code not in ('39', '41', '43', '110', '111', '112'):
				# En caso de venta a publico los documentos ('33', '34', '46', '56', '61')
				# Se requiere si el rut receptor es persona juridica
				# Si el rut de el comprador es distinto al rut del receptor
				template += """<RUTSolicita>%s</RUTSolicita>\n""" % document.applicant_id.rut.replace('.','').replace('k','K')

		if not document.carrier_id and document.picking_type in (2, 3) and \
				sii_code not in ('39', '41', '43', '56', '61', '110', '111', '112'):
			raise Warning(_('No Carrier Selected.'))

		if sii_code != '43':
			if document.carrier_id or sii_code in ('52', '110', '111', '112'):
				template += """<Transporte>\n"""
				if document.carrier_id:
					if sii_code not in ('56', '61', '111', '112'):
						if document.vehicle_id:
							template += """<Patente>%s</Patente>\n""" % document.vehicle_id.license_plate[:8]

						if document.carrier_id.rut:
							template += """<RUTTrans>%s</RUTTrans>\n""" % document.carrier_id.rut.replace('.','').replace('k','K')
						elif sii_code == '110':
							pass
						else:
							raise Warning(_('No Carrier rut'))

					if document._name == 'l10n_cl_dte.stock' and not document.driver_id:
						raise Warning(_('No Driver Selected.'))


					if document.driver_id:
						template += """<Chofer>\n"""
						template += """<RUTChofer>%s</RUTChofer>\n""" % document.driver_id.rut.replace('.','').replace('k','K')
						template += """<NombreChofer>%s</NombreChofer>\n""" % (document.driver_id.name[:30].replace('&','&amp;').\
													replace('<','&lt;').replace('>','&gt;'))
						template += """</Chofer>\n"""

			# Esto colocarlo para la v12 del erp 
			#if document.partner_id != document.partner_shipping_id:
			#	Agregar campo 
			# if document.partner_id.street and document.partner_id.street2:
			# 	dirDest = '%s %s' % (document.partner_id.street.replace('&','&amp;').replace('<','&lt;').replace('>','&gt;'), \
			# 			document.partner_id.street2.replace('&','&amp;').replace('<','&lt;').replace('>','&gt;'))
			# 	template += """<DirDest>%s</DirDest>\n"""  % (dirDest[:70])
			# elif document.partner_id.street:
			# 	template += """<DirDest>%s</DirDest>\n"""  % (document.partner_id.street[:70].replace('&','&amp;').\
			# 									replace('<','&lt;').replace('>','&gt;'))
			# elif document.company_id.partner_id.street2:
			# 	template += """<DirDest>%s</DirDest>\n"""  % (document.partner_id.street2[:70].replace('&','&amp;').\
			# 									replace('<','&lt;').replace('>','&gt;'))
			# else:
			# 	raise Warning(_('No Customer Street.'))

					template += """<CmnaDest>%s</CmnaDest>\n""" % (document.partner_id.state_id.name[:20].replace('&','&amp;').\
			 									replace('<','&lt;').replace('>','&gt;'))

			# Esto colocarlo para la v12 del erp 
			#if document.partner_id.city != document.partner_shipping_id.city:
			#	Agregar campo 
			# Esto colocarlo para la v12 del erp
			# template += """<CiudadDest>%s</CiudadDest>\n""" % (document.partner_id.city[:20].replace('&','&amp;').\
			# 									replace('<','&lt;').replace('>','&gt;'))

			# ver como traspasar las guias a exportacion

				if document.is_export:
				# if  and sii_code in ('52', '110', '111', '112'):
					#TODO Validar
					template += """<Aduana>\n"""
					if document.sale_mode:
						template += """<CodModVenta>%s</CodModVenta>\n""" % document.sale_mode
					elif (document.service_index in (3, 4, 5) and sii_code == '110') or sii_code in ('111', '112'):
						pass
					else:
						raise Warning(_('You must set a "Sale Mode".'))

					if document.exp_sale_clause:
						template += """<CodClauVenta>%s</CodClauVenta>\n""" % document.exp_sale_clause
					elif (document.service_index in (3, 4, 5) and sii_code == '110') or sii_code in ('111', '112'):
						pass
					else:
						raise Warning(_('You must set a "Sale Clause".'))

					if document.exp_tot_sale_clause:
						template += """<TotClauVenta>%s</TotClauVenta>\n""" % document.exp_tot_sale_clause
					elif (document.service_index in (3, 4, 5) and sii_code == '110') or sii_code in ('111', '112'):
						pass
					else:
						raise Warning(_('You mus set a "Total Sale Clause".'))

					if document.code_via_transp:
						template += """<CodViaTransp>%s</CodViaTransp>\n""" % document.code_via_transp
					elif (document.service_index == 4 and sii_code == '110') or sii_code in ('111', '112'):
						pass
					else:
						raise Warning(_('You must set a "Transport Route".'))

					### Opcionales ###
					if document.carrier_conv_name:
						template += """<NombreTransp>%s</NombreTransp>\n""" % docunent.carrier_conv_name[:40]
					if document.carrier_rut:
						template += """<RutCiaTransp>%s</RutCiaTransp>\n""" % document.carrier_rut
					if document.carrier_name:
						template += """<NomCiaTransp>%s</NomCiaTransp>\n""" % document.carrier_name[:40]
					if document.aux_code_carrier:
						template += """<IdAdicTransp>%s</IdAdicTransp>\n""" %	document.aux_code_carrier[:20]
					if document.booking:
						template += """<Booking>%s</Booking>\n""" % document.booking[:20]
					if document.operator_code:
						template += """<Operador>%s</Operador>\n""" % document.operator_code[:20]
					##################
					#TODO Validar validando, 
					if document.shipping_port_id:
						template += """<CodPtoEmbarque>%s</CodPtoEmbarque>\n""" % document.shipping_port_id.code
					elif (document.service_index == 4 and sii_code == '110') or sii_code in ('111', '112'):
						# or (sii_code == '') Agregar la guia de despacho para los document.mov_ind in ('8','9')
						pass
					else:
						raise Warning(_('You must set a "Shipping Port".'))

		# if document.mov_ind in ('8','9') y si es guia de despacho 
		# 	if document.shipping_port_id :
		# 		template += """<CodPtoEmbarque>%s</CodPtoEmbarque>""" % document.shipping_port_id.code
		#else:
		#		alegar
					if document.aux_shipping_port_code:
						template += """<IdAdicPtoEmbarque>%s</IdAdicPtoEmbarque>\n""" % document.aux_shipping_port_code[:20]

					if document.landing_port_id:
						template += """<CodPtoDesemb>%s</CodPtoDesemb>\n""" % document.landing_port_id.code
					elif (document.service_index == 4 and sii_code == '110') or sii_code in ('111', '112'):
						pass
					else:
						raise Warning(_('You must set a "Landing Port".'))
					
					### Opcionales ###
					if document.aux_landing_port_code:
						template += """<IdAdicPtoDesemb>%s</IdAdicPtoDesemb>\n""" % document.aux_landing_port_code[:20]
					#Entero
					if document.tare:
						template += """<Tara>%s</Tara>\n""" % document.tare
					#Unidades de medida aduana
					if document.tare_uom_id:
						template += """<CodUnidMedTara>%s</CodUnidMedTara>\n""" % document.tare_uom_id.sii_code
		
					##################
					# Validar
					if document.gross_weight:
						template += """<PesoBruto>%s</PesoBruto>\n""" % document.gross_weight
		#if document.mov_ind in ('8','9') y si es guia de despacho 
		#	if document.gross_weight:
		#		template += """<PesoBruto>%s</PesoBruto>""" % document.gross_weight
		#	else:
		#		alegar
					if document.gross_weight_uom_id:
						template += """<CodUnidPesoBruto>%s</CodUnidPesoBruto>\n""" % document.gross_weight_uom_id.sii_code
					if document.net_weight:
						template += """<PesoNeto>%s</PesoNeto>\n""" % document.net_weight
					if  document.net_weight_uom_id:
						template += """<CodUnidPesoNeto>%s</CodUnidPesoNeto>\n""" % document.net_weight_uom_id.sii_code
					if  document.tot_items:
						template += """<TotItems>%s</TotItems>\n""" % document.tot_items
					if  document.tot_packages and sii_code == '110' and document.tot_packages > 0:
							#Agregar condicion para la guia, obligatorio con el ind traslado == 8 o 9
						template += """<TotBultos>%s</TotBultos>\n""" % document.tot_packages
					elif sii_code in ('111', '112'):
						pass
					else:
						raise Warning(_('You must set the "Total Packages".\n' +\
							'Or, if you are seeing this error, the qty must be greater than 0.'))
		#if document.mov_ind in ('8','9') y si es guia de despacho 
		#	if document.tot_packages:
		#		template += """<TotBultos>%s</TotBultos>""" % document.tot_packages
		#	else:
		#		alegar
					if document.tot_packages:
						for package in document.package_code_ids:
							template += """<TipoBultos>\n"""
							### Tabla de codigos segun tabla Tipo de Bultos en aduana 
							if package.package_code_id:
								template += """<CodTpoBultos>%s</CodTpoBultos>\n""" % package.package_code_id.code
							if package.package_qty:
								template += """<CantBultos>%s</CantBultos>\n""" % package.package_qty
							else:
								raise Warning(_('You must set the "Packages Qty" at line %s.') % (package.package_code_id.name))
							if package.brands:
								template += """<Marcas>%s</Marcas>\n""" % package.brands[:255]
							else:
								raise Warning(_('You must set the "Brands" at line %s.') % (package.package_code_id.name))

							### Cuando el tipo de bulto es contenedor se utilizan los 3 siguientes ###

							if package.package_code_id.code in ('73','74','75','76','78'):
								if package.container_code:
									template += """<IdContainer>%s</IdContainer>\n""" % package.container_code[:25]
								else:
									raise Warning(_('You must set a "Container Code" at line %s') % (package.package_code_id.name))
								if package.container_stamp:
									template += """<Sello>%s</Sello>\n""" % package.container_stamp[:20]
								else:
									raise Warning(_('You must set a "Container Stamp" at line %s') % (package.package_code_id.name))
								if package.container_stamp_issuer:	
									template += """<EmisorSello>%s</EmisorSello>\n""" % package.container_stamp_issuer[:70]
								else:
									raise Warning(_('You must set a "Container Stamp Issuer" at line %s') % (package.package_code_id.name))
							##########################################################################
							template += """</TipoBultos>\n"""

		# TODO: hacer para la guia !!!!
		# if tot_packages:
		# 	for package in document.package_code_ids:
		# 		template += """<TipoBultos>"""
		# 		### Tabla de codigos segun tabla Tipo de Bultos en aduana 
		# 		if package.package_code_id:
		# 			template += """<CodTpoBultos>%s</CodTpoBultos>""" % package.package_code_id.code
		#		else:
		#			raise Warning(_('No package_code_id.'))
		# 		if package.package_qty:
		# 			template += """<CantBultos>%s</CantBultos>""" % package.package_qty
		# 		else:
		# 			raise Warning(_('No packages_qty'))
		# 		if package.brands:
		# 			template += """<Marcas>%s</Marcas>""" % package.brands[:255]
		# 		else:
		# 			raise Warning(_('No brands'))

		# 		### Cuando el tipo de bulto es contenedor se utilizan los 3 siguientes ###

		# 		if package.package_code_id.code in ('73','74','75','76','78'):
		# 			if package.container_code:
		# 				template += """<IdContainer>%s</IdContainer>""" % package.container_code[:25]
		# 			else:
		# 				raise Warning(_('No'))
		# 			if package.container_stamp:
		# 				template += """<Sello>%s</Sello>""" % package.container_stamp[:20]
		# 			else:
		# 				raise Warning(_('No container_stamp'))
		# 			if package.container_stamp_issuer:	
		# 				template += """<EmisorSello>%s</EmisorSello>""" % package.container_stamp_issuer[:70]
		# 			else:
		# 				raise Warning(_('No container_stamp_issuer'))
		# 		##########################################################################
		# 		template += """</TipoBultos>"""


					### Opcional ###
					if document.freight_amount:
						template += """<MntFlete>%s</MntFlete>\n""" % str(document.freight_amount)[:18]
					if document.insurance_amount:
						template += """<MntSeguro>%s</MntSeguro>\n""" % str(document.insurance_amount)[:18]
					################
					## Indicar codigos de paices de acuerdo a tabla de aduana.
					# este es requerido solo para la 110 (factura de exportacion)) 
					if document.host_country_id:
						template += """<CodPaisRecep>%s</CodPaisRecep>\n""" % document.host_country_id.sii_code
					elif sii_code in ('52', '111', '112'):
						pass
					else:
						raise Warning(_('You must set a "Host Country".'))
		# TODO: Opcional para la guia 
		#if document.host_country_id:
		#	template += """<CodPaisRecep>%s</CodPaisRecep>""" % document.host_country_id.sii_code
					if document.destination_country_id:
						template += """<CodPaisDestin>%s</CodPaisDestin>\n""" % document.destination_country_id.sii_code
					elif sii_code in ('52', '111', '112'):
						pass
					else:
						raise Warning(_('You must set a "Destination Country".'))
					template += """</Aduana>\n"""
				template += """</Transporte>\n"""
		template += """<Totales>\n"""

		if document._name == 'account.invoice':
			if sii_code in ('110', '111', '112'):
				template += """<TpoMoneda>%s</TpoMoneda>\n""" % (document.currency_id.sii_code)
			elif sii_code not in ('110', '111', '112'):
				template += """<MntNeto>%s</MntNeto>\n""" % int(round(document.amount_untaxed, 0))
			else:
				raise Warning(_('You must set a SII currency code for the company currency.'))
			
		elif document._name == 'l10n_cl_dte.stock':
			if document.picking_id:
				template += """<MntNeto>%s</MntNeto>\n""" % int(round(document.amount_untaxed, 0))
			else:
				template += """<MntNeto>%s</MntNeto>\n""" % int(round(document.manual_amount_untaxed, 0))

		if document._name == 'account.invoice' and document.amount_exempt:
			template += """<MntExe>%s</MntExe>\n""" % int(round(document.amount_exempt, 0))

#		if sii_code in ('33', '56', '61'):
			# Monto Base Faenamiento Carne
			#template += """<MntBase>%s</MntBase>"""
			# Monto Base de Margenes de Comercializacion
			#template += """<MntMargenCom>%s</MntMargenCom>"""

		other_taxes = []
		percent = amount = 0
		if document._name == 'account.invoice' and sii_code not in ('110', '111', '112'):
			for tax in document.tax_line:
				if tax.tax_code_id.code in ('IDF', 'ICF'):
					percent = round(tax.tax_amount * 100 / tax.base_amount, 0) if tax.base_amount else 0
					amount = int(round(tax.tax_amount, 0))
				elif tax.tax_code_id.code in ('EXEV', 'EXEC'):
					continue
				else:
					other_taxes.append({
						'code': tax.tax_code_id.code or tax.name.split(' - ')[0],
						'tasa': round(tax.tax_amount * 100 / tax.base_amount, 0) if tax.base_amount else 0,
						'amount': int(round(tax.tax_amount, 0))
					})
			if sii_code in ('56', '61'):
				if percent:
					template += """<TasaIVA>%s</TasaIVA>\n""" % (percent)
				if amount:
					template += """<IVA>%s</IVA>\n""" % (amount)
			elif sii_code not in ('1', '34', '38', '41'):
				if sii_code == '39':
					pass
				elif percent:
					template += """<TasaIVA>%s</TasaIVA>\n""" % (percent)
				else:
					raise Warning(_('The IVA tax is not valid, please check.'))
				if amount:
					template += """<IVA>%s</IVA>\n""" % (amount)
				else:
					raise Warning(_('The IVA amount is not valid, please check.'))
		elif document._name == 'l10n_cl_dte.stock':
			template += """<TasaIVA>%s</TasaIVA>\n""" % (19.00)
			if document.picking_id:
				template += """<IVA>%s</IVA>\n""" % (int(round(document.amount_tax, 0)))
			else:
				template += """<IVA>%s</IVA>\n""" % (int(round(document.manual_amount_tax, 0)))

#		< que iva, En liquidaciones de factura puede tomar valor negativo.
#		Las empresa que venden por cuenta de un mandatario, pueden opcional, separar el IVA propio de terceros.
#		template += """<IVAProp></IVAProp>"""
#		template += """<IVATerc></IVATerc>"""

		if document._name == 'account.invoice' and sii_code not in ('39', '41', '110', '111', '112'):
			if other_taxes:
				template += """<ImptoReten>\n"""
				for tax in other_taxes:
					template += """<TipoImp>%s</TipoImp>\n""" % (tax['code'])
					template += """<TasaImp>%s</TasaImp>\n""" % (tax['tasa'])
					template += """<MontoImp>%s</MontoImp>\n""" % (tax['amount'])
				template += """</ImptoReten>\n"""
#			if document._name == 'account.invoice' and sii_code in ('46', '56', '61'):
#				if amount:
#					if sii_code == '46':
#						template += """<IvaNoRet>%s</IvaNoRet>\n""" % (amount)
#					elif '46' in [ref.reference_type.sii_code for ref in document.reference_lines]:
#						template += """<IvaNoRet>%s</IvaNoRet>\n""" % (amount)
		if document._name == 'account.invoice':
			#Revisar con agregado de factura de compra
			template += """<MntTotal>%s</MntTotal>\n""" % int(round(document.amount_total, 0))
		elif document._name == 'l10n_cl_dte.stock':
			if document.picking_id:
				template += """<MntTotal>%s</MntTotal>\n""" % int(round(document.amount_total, 0))
			else:
				template += """<MntTotal>%s</MntTotal>\n""" % int(round(document.manual_amount_total, 0))
		template += """</Totales>\n"""
		if document._name == 'account.invoice' and sii_code in ('110', '111', '112'):
			template += """<OtraMoneda>\n"""
			if document.currency_id.sii_code:
				template += """<TpoMoneda>%s</TpoMoneda>\n""" % (document.currency_id.sii_code)
			else:
				raise Warning(_('You must set a SII currency code for the document currency.'))
			if document.currency_rate:
				template += """<TpoCambio>%s</TpoCambio>\n""" % (document.currency_rate)
			else:
				raise Warning(_('The currency rate must be greater than 0.'))
			template += """<MntExeOtrMnda>%s</MntExeOtrMnda>\n""" % round(document.amount_exempt * document.currency_rate, 4)
			template += """<MntTotOtrMnda>%s</MntTotOtrMnda>\n""" % round(document.amount_total * document.currency_rate, 4)
			template += """</OtraMoneda>\n"""
		template += """</Encabezado>\n"""
		return template

	def get_detalle(self, document):
		template = ""
		cont = 1 
		if document._name == 'account.invoice':
			if len(document.invoice_line) > 0 :
				for line in document.invoice_line:
					template += '<Detalle>\n'
					template += '<NroLinDet>%s</NroLinDet>\n' % cont
					if line.exension_index != 7:
						if line.exension_index != (1, 2, 6) and document.journal_id.doc_type_id.sii_code in ('39', '41'):
							raise Warning(_('The exension index must be 1, 2 or 6 for the 39 and 41 sii document types.'))
						template += '<IndExe>%s</IndExe>\n' % line.exension_index
					if line.product_id.name and line.name:
						template += '<NmbItem>%s</NmbItem>\n' %  (line.product_id.name[:80].replace('&','&amp;').\
												replace('<','&lt;').replace('>','&gt;'))
						template += '<DscItem>%s</DscItem>\n' %  (line.name[:1000].replace('&','&amp;').\
												replace('<','&lt;').replace('>','&gt;'))
					else:
						template += '<NmbItem>%s</NmbItem>\n' %  (line.name[:80].replace('&','&amp;').\
												replace('<','&lt;').replace('>','&gt;'))
					if line.quantity > 0:
						template += '<QtyItem>%s</QtyItem>\n' %  line.quantity
					if line.price_unit > 0:
						template += '<PrcItem>%s</PrcItem>\n' %  line.price_unit
					if line.discount:
						template += '<DescuentoPct>%s</DescuentoPct>\n' % line.discount
						template += '<DescuentoMonto>%s</DescuentoMonto>\n' % int(round((line.price_unit * line.quantity) * (line.discount / 100), 0))
					tax_count = 0
					if document.journal_id.doc_type_id.sii_code not in ('39', '41'):
						for tax in line.invoice_line_tax_id:
							if tax_count < 2 and tax.tax_code_id.code not in ('ICF', 'EXEC', 'IDF', 'EXEV'):
								template += '<CodImpAdic>%s</CodImpAdic>\n' % tax.tax_code_id.code
								tax_count += 1
					if document.tax_included:
						price = line.price_unit * (1 - line.discount / 100.0)
						template += '<MontoItem>%s</MontoItem>\n' % int(round(line.invoice_line_tax_id.compute_all(\
								price, line.quantity, product=line.product_id or None)['total_included'], 0))
					else:
						template += '<MontoItem>%s</MontoItem>\n' % int(round(line.price_subtotal, 0))
					template += '</Detalle>\n'
					cont = cont + 1
			else:
				raise Warning(_('You need some lines.'))
		elif document._name == 'l10n_cl_dte.stock':
			if document.picking_id:
				if len(document.move_lines) > 0 :
					for line in document.move_lines:
						template += '<Detalle>\n'
						template += '<NroLinDet>%s</NroLinDet>\n' % cont
						if line.product_id.name and line.name:
							template += '<NmbItem>%s</NmbItem>\n' % (line.product_id.name[:80].\
													replace('&','&amp;').\
													replace('<','&lt;').replace('>','&gt;'))
							template += '<DscItem>%s</DscItem>\n' % (line.name[:1000].\
													replace('&','&amp;').\
													replace('<','&lt;').replace('>','&gt;'))
						else:
							template += '<NmbItem>%s</NmbItem>\n' % (line.name[:80].replace('&','&amp;').\
													replace('<','&lt;').replace('>','&gt;'))
						template += '<QtyItem>%s</QtyItem>\n' %  line.product_uom_qty
						template += '<PrcItem>%s</PrcItem>\n' %  line.sale_price_unit
						if line.sale_discount:
							template += '<DescuentoPct>%s</DescuentoPct>\n' % line.sale_discount
							template += '<DescuentoMonto>%s</DescuentoMonto>\n' % int(round((line.sale_price_unit * line.product_uom_qty) * (line.sale_discount / 100), 0))
						if document.tax_included:
							template += '<MontoItem>%s</MontoItem>\n' % int(round(line.sale_subtotal + line.sale_taxes, 0))
						else:
							template += '<MontoItem>%s</MontoItem>\n' % int(round(line.sale_subtotal, 0))
						template += '</Detalle>\n'
						cont = cont + 1
				else:
					raise Warning(_('You need some lines.'))
			else:
				if len(document.dispatch_order_line) > 0 :
					for line in document.dispatch_order_line:
						template += '<Detalle>\n'
						template += '<NroLinDet>%s</NroLinDet>\n' % cont
						if line.product_id.name and line.name:
							template += '<NmbItem>%s</NmbItem>\n' % (line.product_id.name[:80].\
													replace('&','&amp;').\
													replace('<','&lt;').replace('>','&gt;'))
							template += '<DscItem>%s</DscItem>\n' % (line.name[:1000].\
													replace('&','&amp;').\
													replace('<','&lt;').replace('>','&gt;'))
						else:
							template += '<NmbItem>%s</NmbItem>\n' % (line.name[:80].replace('&','&amp;').\
													replace('<','&lt;').replace('>','&gt;'))
						template += '<QtyItem>%s</QtyItem>\n' %  line.quantity
						template += '<PrcItem>%s</PrcItem>\n' %  line.price_unit
						if line.discount:
							template += '<DescuentoPct>%s</DescuentoPct>\n' % line.discount
							template += '<DescuentoMonto>%s</DescuentoMonto>\n' %  int(round((line.price_unit * line.quantity) * (line.discount / 100), 0))
						if document.tax_included:
							price = line.price_unit * (1 - line.discount / 100.0)
							template += '<MontoItem>%s</MontoItem>\n' % int(round(line.dispatch_order_line_tax_id.\
										compute_all(price, line.quantity, \
										product=line.product_id or None)['total_included'], 0))
						else:
							template += '<MontoItem>%s</MontoItem>\n' % int(round(line.price_subtotal, 0))
						template += '</Detalle>\n'
						cont = cont + 1
				else:
					raise Warning(_('You need some lines.'))
		return template

	def get_references(self, document):
		template = ""
		cont = 1

		for reference in document.reference_lines:
			if reference.reference_reason == 1:
				reference_reason = 'Re-Escribe Documento'
			elif reference.reference_reason == 2:
				reference_reason = 'Corrige Texto'
				if document._name == 'account.invoice':
					if document.journal_id.doc_type_id.sii_code in ('56', '61') and document.amount_total != 0:
						raise Warning(_('The total amount of documents with sii code \'56\' and \'61\' must be 0.'))
			elif reference.reference_reason == 3:
				reference_reason = 'Corrige Monto'
			else:
				reference_reason = ''

			template += '<Referencia>\n'
			template += '<NroLinRef>%s</NroLinRef>\n' %  cont
			template += '<TpoDocRef>%s</TpoDocRef>\n' % str(reference.reference_type.sii_code)
			template += '<FolioRef>%s</FolioRef>\n' %  reference.reference_folio
			if document.journal_id.doc_type_id.sii_code not in ('39', '41'):
				template += '<FchRef>%s</FchRef>\n' %  reference.reference_date
			if reference.reference_reason:
				template += '<CodRef>%s</CodRef>\n' % reference.reference_reason
				template += '<RazonRef>%s</RazonRef>\n' %  reference.reference_reason_text
			template += '</Referencia>\n'
			cont = cont + 1

		return template

	def get_book(self, document):
		semilla = time.strftime('%s')
		template = """<?xml version="1.0" encoding="ISO-8859-1"?>\n"""
		if document.operation_type == 'LibroGuia':
			type_of_book = 'LibroGuia'
			template += """<LibroGuia xmlns="http://www.sii.cl/SiiDte" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sii.cl/SiiDte LibroGuia_v10.xsd" version="1.0">\n"""
		elif document.operation_type == 'COMPRA' or document.operation_type == 'VENTA':
			type_of_book = 'LibroCompraVenta'
			template += """<LibroCompraVenta xmlns="http://www.sii.cl/SiiDte" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sii.cl/SiiDte LibroCV_v10.xsd" version="1.0">\n"""		
		
		template += """<EnvioLibro ID="L%s%s">\n""" % (document.operation_type,semilla)
		template += """<Caratula>\n"""
		template += """<RutEmisorLibro>%s</RutEmisorLibro>\n""" % document.company_id.partner_id.rut.replace('.','').replace('k','K')
		template += """<RutEnvia>%s</RutEnvia>\n""" % document.company_id.certificate_ids[0].owner_rut.replace('.','').replace('k','K')
		template += """<PeriodoTributario>%s-%s</PeriodoTributario>\n""" % (document.period_id.name[3:], document.period_id.name[:2])
		if document.company_id.resolution_date:
			template += """<FchResol>%s</FchResol>\n""" % document.company_id.resolution_date
		else:
			raise Warning(_('No Resolution Date in company data.'))
		if document.company_id.resolution_number or document.company_id.resolution_number == 0:
			template += """<NroResol>%s</NroResol>\n""" % document.company_id.resolution_number
		else:
			raise Warning(_('No Resolution Number in company data.'))
		if document.book_type != 'LibroGuia':
			template += """<TipoOperacion>%s</TipoOperacion>\n""" % document.operation_type
		template += """<TipoLibro>%s</TipoLibro>\n""" % document.book_type
		############# Ver el tipo AJUSTE, para correccion, puede que se mande con el tupo de RECTIFICA ##########
		template += """<TipoEnvio>TOTAL</TipoEnvio>\n"""
		if document.notification_folio:
			template += """<FolioNotificacion>%s</FolioNotificacion>\n""" % document.notification_folio
		if  document.number_amend:
			template += """<CodAutRec>%s</CodAutRec>\n""" % document.number_amend

		template += """</Caratula>\n"""

		if len(document.journal_ids) != 0:
			template += """<ResumenPeriodo>\n"""

			for resume in document.journal_ids:
				if type_of_book == 'LibroGuia':
#					template += """<TotGuiaAnulada>%s</TotGuiaAnulada>\n""" % document_head[document_total]['total_documentos_anulados']
					template += """<TotGuiaAnulada>%s</TotGuiaAnulada>\n""" % (0)
					template += """<TotGuiaVenta>%s</TotGuiaVenta>\n""" % resume.documents_total
					template += """<TotMntGuiaVta>%s</TotMntGuiaVta>\n""" % int(round(resume.amount_total, 0))
					if doc.mov_ind != 1:
						template += """<TotTraslado>\n"""
						template += """<TpoTraslado>%s</TpoTraslado>\n""" % resume.dte_type
						template += """<CantGuia>%s</CantGuia>\n""" % resume.documents_total
						template += """<MntGuia>%s</MntGuia>\n""" % resume.amount_total
						template += """</TotTraslado>\n"""
				elif type_of_book == 'LibroCompraVenta':
					template += """<TotalesPeriodo>\n"""
					template += """<TpoDoc>%s</TpoDoc>\n"""  % resume.dte_type
					############### Verificar el tag <TotOpExe> a ver de que se trata
					template += """<TotDoc>%s</TotDoc>\n""" % resume.documents_total
					if resume.dte_type in ('60', '61', '106', '112'):
						amount_exempt = resume.amount_exempt * -1
						amount_untaxed = resume.amount_untaxed * -1
						amount_tax = resume.amount_tax * -1
                                                amount_tax_no_rec = resume.amount_total_no_rec
						amount_total = resume.amount_total * -1
					else:
						amount_exempt = resume.amount_exempt
						amount_untaxed = resume.amount_untaxed 
						amount_tax = resume.amount_tax
                                                amount_tax_no_rec = resume.amount_total_no_rec
						amount_total = resume.amount_total

					template += """<TotMntExe>%s</TotMntExe>\n""" % int(round(amount_exempt, 0))
					template += """<TotMntNeto>%s</TotMntNeto>\n""" % int(round(amount_untaxed, 0))
				#	template += """<TotOpIVARec>%s</TotOpIVARec>""" % cantidadDocs
					template += """<TotMntIVA>%s</TotMntIVA>\n""" % int(round(amount_tax, 0))
				#	template += """<TotIVANoRec>"""
				#	template += """</TotIVANoRec>"""
					if amount_tax_no_rec:
						template += """<TotImpSinCredito>%s</TotImpSinCredito>\n""" % int(round(amount_tax_no_rec, 0))
					template += """<TotMntTotal>%s</TotMntTotal>\n""" % int(round(amount_total, 0))
					template += """</TotalesPeriodo>\n"""
			template += """</ResumenPeriodo>\n"""

		for line in document.document_ids:
			if line.dte_type not in ['35', '38', '39', '41']:
				if type_of_book == 'LibroGuia':
					template += """<Detalle>\n"""
					template += """<Folio>%s</Folio\n>""" % line.folio
					template += """<TpoOper>%s</TpoOper\n>""" % line.mov_ind or 0
					template += """<FchDoc>%s</FchDoc\n>""" % line.doc_date
					template += """<RUTDoc>%s</RUTDoc\n>""" % line.rut.replace('.','').replace('k','K')
					template += """<RznSoc>%s</RznSoc\n>""" % line.partner_name[:50].replace('&','&amp;').\
												replace('<','&lt;').replace('>','&gt;')
					template += """<MntNeto>%s</MntNeto\n>""" % int(round(line.amount_untaxed or 0, 0))
						# porcentaje de impuestos por el momento lo voy a dejar en 19
					template += """<TasaImp>%s</TasaImp\n>""" % 19
					template += """<MntTotal>%s</MntTotal\n>""" % int(round(line.amount_total or 0, 0))

				elif type_of_book == 'LibroCompraVenta':
					template += """<Detalle>\n"""
					template += """<TpoDoc>%s</TpoDoc>\n""" % line.dte_type
					template += """<NroDoc>%s</NroDoc>\n""" % line.folio
					#template += """<Anulado>%s</Anulado>\n""" % 'A' aki agragar si es anulado ;)
						# porcentaje de impuestos por el momento lo voy a dejar en 19
					#template += """<TpoImp>%s</TpoImp>\n""" % (line.tipo_imp) #Valores: 1 - IVA; 2 - Ley 18.211
					template += """<TasaImp>%s</TasaImp>\n""" % 19
					#template += """<IndServicio>%s</IndServicio>\n""" % () #Valores:
									#1 - Facturacion de Servicios Periodicos Domiciliarios
									#2 - Facturacion de Otros Servicios Periodicos
									#3 - Facturacion de Servicios No Periodicos
					#template += """<IndSinCosto>%s</IndSinCosto>\n""" % () 1 - Entrega Gratuita
					template += """<FchDoc>%s</FchDoc>\n""" % line.doc_date
					#template += """<CdgSIISucur>%s</CdgSIISucur>\n""" % line.cod_suc #CodSucursa Dada por SII
					template += """<RUTDoc>%s</RUTDoc>\n""" % line.rut.replace('.','').replace('k','K')
					template += """<RznSoc>%s</RznSoc>\n""" % line.partner_name[:50].replace('&','&amp;').\
												replace('<','&lt;').replace('>','&gt;')
					#template += """<Extranjero>\n"""
					#template += """<NumId></NumId>\n""" #Numero de Identificacion entre 1 y 20 de largo (str)
					#template += """<Nacionalidad></Nacionalidad>\n""" #Nacionalidad entre 1 y 3 de largo (str)
					#template += """</Extranjero>\n"""
					#template += """<TpoDocRef>%s</TpoDocRef>\n""" $ () Tipo doc de referencia...
					#template += """<FolioDocRef>%s</FolioDocRef>\n""" % () Folio del doc referenciado...
					template += """<MntExe>%s</MntExe>\n""" % int(round(line.amount_exempt or 0, 0))
					template += """<MntNeto>%s</MntNeto>\n""" % int(round(line.amount_untaxed or 0, 0))
					template += """<MntIVA>%s</MntIVA>\n""" % int(round(line.amount_tax or 0, 0))
					#template += """<IVANoRec>\n""" puede tener 5 iteraciones max
					#template += """<CodIVANoRec>%s</CodIVANoRec>\n""" Valores:
									#1 - Compras Destinadas a Generar Operaciones No Gravadas o Exentas
									#2 - Facturas de Proveedores Registradas Fuera de Plazo
									#3 - Gastos Rechazados
									#4 - Entrega Gratuita
									#9 - Otros
					#template += """<MntIVANoRec>%s</MntIVANoRec>\n""" % () 
					#template += """</IVANoRec>\n"""
						# creo que no aplica para el libro de venta
					#template += """<IVAUsoComun>%s</IVAUsoComun>\n""" % int(round(0, 0))
					#template += """<IVAFueraPlazo>%s</IVAFueraPlazo>\n""" % () #Solo para NC fuera de plazo
					#template += """<Ley18211>%s</Ley18211>\n""" % () #IVA Ley 18211
					#template += """<OtrosImp>\n""" #Aki se declaran los cod de imps extras como ILA, etc.
					#template += """<CodImp>%s</CodImp>\n""" % ()
					#template += """<TasaImp>%s</TasaImp>\n""" % ()
					#template += """<MntImp>%s</MntImp>\n""" % () #Monto del Impuesto o Recargo. En el Libro Compra (LC) Registrar Solo el Monto Con Derecho a Credito
					#template += """</OtrosImp>\n"""
					if line.amount_tax_no_rec:
	                                        template += """<MntSinCred>%s</MntSinCred>\n""" % int(round(line.amount_tax_no_rec, 0))
					template += """<MntTotal>%s</MntTotal>\n""" % int(round(line.amount_total or 0, 0))
					template += """</Detalle>\n"""
		template += """<TmstFirma>%s</TmstFirma>\n""" % dt.now().strftime('%Y-%m-%dT%H:%M:%S')
		template += """</EnvioLibro>\n"""
		template += self.get_signature(reference='L%s%s' % (document.operation_type,semilla))
		template += """</LibroCompraVenta>\n"""

		return template

#PARA REFACTORIZACION
#	def global_discount_xml(self, discount, percent, cont):
#		template = """"""
#		template += "<DscRcgGlobal>\n"
#		template += "<NroLinDR>%s</NroLinDR>\n" % (cont)
#		template += "<TpoMov>D</TpoMov>\n"
#		template += "<GlosaDR>Descuento Global Afectos</GlosaDR>\n"
#		template += "<TpoValor>%</TpoValor>\n"
#		template += "<ValorDR>%s</ValorDR>\n" % (document.global_discount_percent)
#		template += "</DscRcgGlobal>\n"
#		return template
		
	def get_global_discount(self, document):
		template = """"""
		if not document.specific_discounts: # Agregar un booleano para los no facturables.
			if document.global_discount_percent: #Valida que venga un valor en el descuento global
				cont = 0
				if not document.amount_untaxed and not document.amount_exempt:
					raise Warning(_('You must have an untaxed or exempt amount if you want to apply a discount.'))

				if document.amount_untaxed:
					cont += 1
#LLAMADO A FUNCION REFACCTORIZACION
#					self.global_discount_xml(document.global_discount_percent, True, cont)
					template += "<DscRcgGlobal>\n"
					template += "<NroLinDR>%s</NroLinDR>\n" % (cont)
					template += "<TpoMov>D</TpoMov>\n"
					template += "<GlosaDR>Descuento Global Afectos</GlosaDR>\n"
					template += "<TpoValor>%</TpoValor>\n"
					template += "<ValorDR>%s</ValorDR>\n" % (document.global_discount_percent)
					template += "</DscRcgGlobal>\n"
				if document.amount_exempt:
					cont += 1
					template += "<DscRcgGlobal>\n"
					template += "<NroLinDR>%s</NroLinDR>\n" % (cont)
					template += "<TpoMov>D</TpoMov>\n"
					template += "<GlosaDR>Descuento Global Exentos</GlosaDR>\n"
					template += "<TpoValor>%</TpoValor>\n"
					template += "<ValorDR>%s</ValorDR>\n" % (document.global_discount_percent)
					template += "<IndExeDR>1</IndExeDR>\n"
					template += "</DscRcgGlobal>\n"
		else:
			cont = 0

			#Valida que venga el monto del desuento para los afectos
			if document.amount_untaxed and (document.global_discount_percent_untaxed or document.global_discount_untaxed):
				if not document.amount_untaxed and not document.amount_exempt:
					raise Warning(_('You must have an untaxed or exempt amount if you want to apply a discount.'))

				cont += 1
				template += "<DscRcgGlobal>\n"
				template += "<NroLinDR>%s</NroLinDR>\n" % (cont)
				template += "<TpoMov>D</TpoMov>\n"
				template += "<GlosaDR>Descuento Global Afectos</GlosaDR>\n"
				if document.amount_percent_discount:
					template += "<TpoValor>%</TpoValor>\n"
					template += "<ValorDR>%s</ValorDR>\n" % (document.global_discount_percent_untaxed)
				else:
					template += "<TpoValor>$</TpoValor>\n"
					template += "<ValorDR>%s</ValorDR>\n" % (document.global_discount_untaxed)
				template += "</DscRcgGlobal>\n"
			#Valida que venga el monto del desuento para los exentos
			if document.amount_exempt and (document.global_discount_percent_exempt or document.global_discount_exempt):
				if not document.amount_untaxed and not document.amount_exempt:
					raise Warning(_('You must have an untaxed or exempt amount if you want to apply a discount.'))
				cont += 1
				template += "<DscRcgGlobal>\n"
				template += "<NroLinDR>%s</NroLinDR>\n" % (cont)
				template += "<TpoMov>D</TpoMov>\n"
				template += "<GlosaDR>Descuento Global Exentos</GlosaDR>\n"
				if document.amount_percent_discount:
					template += "<TpoValor>%</TpoValor>\n"
					template += "<ValorDR>%s</ValorDR>\n" % (document.global_discount_percent_exempt)
				else:
					template += "<TpoValor>$</TpoValor>\n"
					template += "<ValorDR>%s</ValorDR>\n" % (document.global_discount_exempt)
				template += "<IndExeDR>1</IndExeDR>\n"
				template += "</DscRcgGlobal>\n"
		return template

	def get_document(self, document):
		template = """<?xml version="1.0" encoding="ISO-8859-1"?>\n<DTE version="1.0">\n"""

		if document.journal_id.doc_type_id.sii_code in ('110', '111', '112'):
			reference = 'F%s' % (document.number)
			reference_id_line = """<Exportaciones ID="%s">\n""" % (reference)
		elif document._name == 'l10n_cl_dte.stock':
			reference = 'F%s' % (document.folio)
			reference_id_line = """<Documento ID="%s">\n""" % (reference)
		else:
			reference = 'F%s' % (document.number)
			reference_id_line = """<Documento ID="%s">\n""" % (reference)
		template += reference_id_line

		template += self.get_encabezado(document)
		template += self.get_detalle(document)

		# descuentos globales
		if document._name == 'account.invoice':
			template += self.get_global_discount(document)

		# Referencias
		template += self.get_references(document)

		# ted
		template += document.ted

		#TmstFirma
		template += """<TmstFirma>%s</TmstFirma>\n""" % dt.now().strftime('%Y-%m-%dT%H:%M:%S')

		if document.journal_id.doc_type_id.sii_code in ('110', '111', '112'):
			template += """</Exportaciones>\n"""
		else:
			template += """</Documento>\n"""

		template += self.get_signature(reference=reference)
		template += """</DTE>\n"""

		return template
		
	def get_seed(self, semilla):
		template =  """<?xml version="1.0"?>\n"""
		template += """<getToken>\n"""
		template += """<item>\n"""
		template += """<Semilla>%s</Semilla>\n""" % semilla
		template += """</item>\n"""
		template += """<Signature xmlns="http://www.w3.org/2000/09/xmldsig#">\n"""
		template += """<SignedInfo>\n"""
		template += """<CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"/>\n"""
		template += """<SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1"/>\n"""
		template += """<Reference URI="">\n"""
		template += """<Transforms>\n"""
		template += """<Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>\n"""
		template += """</Transforms>\n"""
		template += """<DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1"/>\n"""
		template += """<DigestValue></DigestValue>\n"""
		template += """</Reference>\n"""
		template += """</SignedInfo>\n"""
		template += """<SignatureValue>\n"""
		template += """</SignatureValue>\n"""
		template += """<KeyInfo>\n"""
		template += """<KeyValue>\n"""
		template += """</KeyValue>\n"""
		template += """<X509Data>\n"""
		template += """<X509Certificate>\n"""
		template += """</X509Certificate>\n"""
		template += """</X509Data>\n"""
		template += """</KeyInfo>\n"""
		template += """</Signature>\n"""
		template += """</getToken>\n"""
		return template

	def get_daily_book_resume(self, document, doc_type):
		start_folio = 0
		template = ""
		values = document.get_documents_by_type(doc_type)
		if len(values['lines']) > 0:
			template += """<Resumen>\n"""
			template += """<TipoDocumento>%s</TipoDocumento>\n""" % doc_type
			template += """<MntNeto>%s</MntNeto>\n""" % values['amount_untaxed']
			template += """<MntIva>%s</MntIva>\n""" % values['amount_tax']
			template += """<TasaIVA>%s</TasaIVA>\n""" % values['iva_rate']
			template += """<MntExento>%s</MntExento>\n""" % values['amount_exempt']
			template += """<MntTotal>%s</MntTotal>\n""" % values['amount_total']
			template += """<FoliosEmitidos>%s</FoliosEmitidos>\n""" % values['total_issued']
			template += """<FoliosAnulados>%s</FoliosAnulados>\n""" % values['total_annuled']
			template += """<FoliosUtilizados>%s</FoliosUtilizados>\n""" % values['total_used']

			count = 1

			used_lines = [line for line in values['lines'] if line.state == 'used' ]
			annuled_lines  = [line for line in values['lines'] if line.state == 'annuled' ]

			if len(used_lines) > 0:
				for line in used_lines:
					if line.folio and start_folio == 0 :
						template += """<RangoUtilizados>\n"""
						template += """<Inicial>%s</Inicial>\n""" % line.folio 
						start_folio = line.folio
					if (line.parent_right - line.folio ) > 1:
						template += """<Final>%s</Final>\n""" % line.folio
						template += """</RangoUtilizados>\n"""
						start_folio = 0
					elif line.parent_right == 0:
						template += """<Final>%s</Final>\n""" % line.folio
						template += """</RangoUtilizados>\n"""
						start_folio = 0
					elif len(used_lines) == count :
						template += """<Final>%s</Final>\n""" % line.folio
						template += """</RangoUtilizados>\n"""
						start_folio = 0
					count += 1
			count = 0 
			if len(annuled_lines) > 0:
				for line in annuled_lines:
					if line.folio and start_folio == 0 :
						template += """<RangoUtilizados>\n"""
						template += """<Inicial>%s</Inicial>\n""" % line.folio 
						start_folio = line.folio
					if (line.parent_right - line.folio ) > 1:
						template += """<Final>%s</Final>\n""" % line.folio
						template += """</RangoUtilizados>\n"""
						start_folio = 0
					elif line.parent_right == 0:
						template += """<Final>%s</Final>\n""" % line.folio
						template += """</RangoUtilizados>\n"""
						start_folio = 0
					elif len(annuled_lines) == count :
						template += """<Final>%s</Final>\n""" % line.folio
						template += """</RangoUtilizados>\n"""
						start_folio = 0
					count += 1
			count = 0 
		else:
			template += """<Resumen>\n"""
			template += """<TipoDocumento>%s</TipoDocumento>\n""" % doc_type
			template += """<MntNeto>%s</MntNeto>\n""" % 0
			template += """<MntIva>%s</MntIva>\n""" % 0
			template += """<TasaIVA>%s</TasaIVA>\n""" % 19.0
			template += """<MntExento>%s</MntExento>\n""" % 0
			template += """<MntTotal>%s</MntTotal>\n""" % 0
			template += """<FoliosEmitidos>%s</FoliosEmitidos>\n""" % 0
			template += """<FoliosAnulados>%s</FoliosAnulados>\n""" % 0
			template += """<FoliosUtilizados>%s</FoliosUtilizados>\n""" % 0

		template += """</Resumen>\n"""

		return template

	def get_daily_book(self, document):
		types_supported = ['39','41','61']
		id_of_book = 'CD%s' % time.strftime('%s')
		template = """<?xml version="1.0" encoding="ISO-8859-1"?>\n"""
		template += """<ConsumoFolios xmlns="http://www.sii.cl/SiiDte" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sii.cl/SiiDte ConsumoFolio_v10.xsd" version="1.0">\n"""
		template += """<DocumentoConsumoFolios ID="%s">\n""" %id_of_book
		template +=  """<Caratula version="1.0">\n"""
		template += """<RutEmisor>%s</RutEmisor>\n""" % document.company_id.partner_id.rut.replace('.','').replace('k','K')
		template += """<RutEnvia>%s</RutEnvia>\n""" % document.company_id.certificate_ids[0].owner_rut.replace('.','').replace('k','K')
		if document.company_id.resolution_date:
			template += """<FchResol>%s</FchResol>\n""" % document.company_id.resolution_date
		else:
			raise Warning(_('No Resolution Date in company data.'))
		if document.company_id.resolution_number or document.company_id.resolution_number == 0:
			template += """<NroResol>%s</NroResol>\n""" % document.company_id.resolution_number
		else:
			raise Warning(_('No Resolution Number in company data.'))
		template += """<FchInicio>%s</FchInicio>\n""" % document.date_documents 
		template += """<FchFinal>%s</FchFinal>\n""" % document.date_documents 
		template += """<SecEnvio>%s</SecEnvio>\n""" % document.send_sequence
		template += """<TmstFirmaEnv>%s</TmstFirmaEnv>\n""" % dt.now().strftime('%Y-%m-%dT%H:%M:%S')
		template += """</Caratula>\n"""
		for doc_type in types_supported:
			template += self.get_daily_book_resume(document, doc_type)
		template += """</DocumentoConsumoFolios>\n"""
		template += self.get_signature(reference=id_of_book)
		template += """</ConsumoFolios>\n"""
		return template

	def get_dispatch(self, document, client=False):
		template = """<?xml version="1.0" encoding="ISO-8859-1"?>\n"""
		if document.journal_id.doc_type_id.sii_code in ('39', '41'):
			template += """<EnvioBOLETA xmlns="http://www.sii.cl/SiiDte" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sii.cl/SiiDte EnvioBOLETA_v11.xsd" version="1.0">\n"""
		else:
			template += """<EnvioDTE xmlns="http://www.sii.cl/SiiDte" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sii.cl/SiiDte EnvioDTE_v10.xsd" version="1.0">\n"""
		id_set = "SetDoc%s" % (dt.now().strftime('%Y%m%d%H%M%S'))
		template += """<SetDTE ID="%s">\n""" % (id_set)
		template += """<Caratula version="1.0">\n"""
		template += """<RutEmisor>%s</RutEmisor>\n""" % document.company_id.partner_id.rut.replace('.','').replace('k','K')
		template += """<RutEnvia>%s</RutEnvia>\n""" % document.company_id.certificate_ids[0].owner_rut.replace('.','').replace('k','K')
		if client:
			template += """<RutReceptor>%s</RutReceptor>\n""" % ((document.partner_id.rut or \
										document.partner_id.parent_id.rut).replace('.','').replace('k','K'))
		else:
			template += """<RutReceptor>60803000-K</RutReceptor>\n"""
		if document.company_id.resolution_date:
			template += """<FchResol>%s</FchResol>\n""" % document.company_id.resolution_date
		else:
			raise Warning(_('No Resolution Date in company data.'))
		if document.company_id.resolution_number or document.company_id.resolution_number == 0:
			template += """<NroResol>%s</NroResol>\n""" % document.company_id.resolution_number
		else:
			raise Warning(_('No Resolution Number in company data.'))
		template += """<TmstFirmaEnv>%s</TmstFirmaEnv>\n""" % dt.now().strftime('%Y-%m-%dT%H:%M:%S')
		template += """<SubTotDTE>\n"""
		template += """<TpoDTE>%s</TpoDTE>\n""" % document.journal_id.doc_type_id.sii_code
		template += """<NroDTE>1</NroDTE>\n"""
		template += """</SubTotDTE>\n"""
		template += """</Caratula>\n"""
		template += (base64.b64decode(document.signed_xml)).decode('iso-8859-1')
		template += """</SetDTE>\n"""
		template += self.get_signature(reference=id_set)
		if document.journal_id.doc_type_id.sii_code in ('39', '41'):
			template += """</EnvioBOLETA>\n"""
		else:
			template += """</EnvioDTE>\n"""
		return template

	####################################################### ACK #######################################################
	def get_header(self, ack_type, tmpl_id='SendAck'):
		if ack_type == 'receiving_goods':
			template = """<EnvioRecibos version="1.0" xmlns="http://www.sii.cl/SiiDte" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sii.cl/SiiDte EnvioRecibos_v10.xsd">\n"""
			template += """<SetRecibos ID="%s">\n""" % (tmpl_id)
		else:
			template = """<RespuestaDTE xmlns="http://www.sii.cl/SiiDte" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.0" xsi:schemaLocation="http://www.sii.cl/SiiDte RespuestaEnvioDTE_v10.xsd">\n"""
			template += """<Resultado ID="%s">\n""" % (tmpl_id)
		return template
		
	def get_schema(self, vals, ack_type):
		template = """<Caratula version="1.0">\n"""
		template += """<RutResponde>%s</RutResponde>\n""" % (vals['rut_recep'])# -> Company Rut
		template += """<RutRecibe>%s</RutRecibe>\n""" % (vals['rut_emisor'])
		if ack_type != 'receiving_goods':
			template += """<IdRespuesta>%s</IdRespuesta>\n""" % (vals['id_resp'])
			template += """<NroDetalles>1</NroDetalles>\n"""
		template += """<NmbContacto>%s</NmbContacto>\n""" % (vals['contact_name'])
		template += """<FonoContacto>%s</FonoContacto>\n""" % (vals['contact_phone'])
		template += """<MailContacto>%s</MailContacto>\n""" % (vals['contact_email'])
		template += """<TmstFirmaResp>%s</TmstFirmaResp>\n""" % (dt.now().strftime('%Y-%m-%dT%H:%M:%S'))
		template += """</Caratula>\n"""
		return template

	def get_ack_dte_data(self, vals):
		template = """<TipoDTE>%s</TipoDTE>\n""" % (vals['tipo_dte'])
		template += """<Folio>%s</Folio>\n""" % (vals['folio'])
		template += """<FchEmis>%s</FchEmis>\n""" % (vals['fch_emis'])
		template += """<RUTEmisor>%s</RUTEmisor>\n""" % (vals['rut_emisor'])
		template += """<RUTRecep>%s</RUTRecep>\n""" % (vals['rut_recep'])
		template += """<MntTotal>%s</MntTotal>\n""" % (vals['mnt_total'])
		return template

	def get_receiving_goods(self, vals):
		template = """<Recibo version="1.0">\n""" #-> Recepcion de Mercaderia
		template += """<DocumentoRecibo ID="%s">\n""" % (vals['id_resp'])
		template += self.get_ack_dte_data(vals)
               	template += """<Recinto>%s</Recinto>\n""" % (vals['enclosure'][:80])
		template += """<RutFirma>%s</RutFirma>\n""" % (vals['sign_rut'])
		template += """<Declaracion>El acuse de recibo que se declara en este acto, de acuerdo a lo dispuesto en la letra b) del Art. 4, y la letra c) del Art. 5 de la Ley 19.983, acredita que la entrega de mercaderias o servicio(s) prestado(s) ha(n) sido recibido(s).</Declaracion>\n"""
		template += """<TmstFirmaRecibo>%s</TmstFirmaRecibo>\n""" % (dt.now().strftime('%Y-%m-%dT%H:%M:%S'))
		template += """</DocumentoRecibo>\n"""
		template += self.get_signature(reference=vals['id_resp'])
		template += """</Recibo>\n"""
		return template

	def get_footer(self, tmpl_id='SendAck'):
		template = """</Resultado>\n"""
		template += self.get_signature(reference=tmpl_id)
		template += """</RespuestaDTE>"""
		return template
	
	def get_ack(self, vals, ack_type):
		template = """<?xml version="1.0" encoding="ISO-8859-1"?>\n"""

		template += self.get_header(ack_type) #-> agregar el tmpl_id cuando generemos los id del SetDTE
		template += self.get_schema(vals, ack_type)

		if ack_type == 'receiving_goods':
			template += vals['sign_xml_recibo']
			template += """</SetRecibos>\n"""
			template += self.get_signature(reference='SendAck')
			template += """</EnvioRecibos>\n"""
		else:
			if ack_type == 'ack':
				template += """<RecepcionEnvio>\n""" #-> Acuse Recibo
				template += """<NmbEnvio>%s</NmbEnvio>\n""" % (vals['attach_name'])
				template += """<FchRecep>%s</FchRecep>\n""" % (vals['create_date'])
				template += """<CodEnvio>%s</CodEnvio>\n""" % (vals['send_cdg'])
				template += """<EnvioDTEID>%s</EnvioDTEID>\n""" % (vals['dte_id'])
				#template += """<Digest>%s</Digest>\n""" % (vals['digest'])
				template += """<RutEmisor>%s</RutEmisor>\n""" % (vals['rut_emisor'])
				template += """<RutReceptor>%s</RutReceptor>\n""" % (vals['rut_recep'])
				template += """<EstadoRecepEnv>%s</EstadoRecepEnv>\n""" % (vals['ack_state'])
				template += """<RecepEnvGlosa>%s</RecepEnvGlosa>\n""" % (vals['ack_note'])
				template += """<NroDTE>1</NroDTE>\n"""
				template += """<RecepcionDTE>\n"""
				template += self.get_ack_dte_data(vals)
				template += """<EstadoRecepDTE>%s</EstadoRecepDTE>\n""" % (vals['ack_doc_state'])
				template += """<RecepDTEGlosa>%s</RecepDTEGlosa>\n""" % (vals['ack_doc_note'])
				template += """</RecepcionDTE>\n"""
				template += """</RecepcionEnvio>\n"""
			elif ack_type == 'commercial':
				template += """<ResultadoDTE>\n""" #-> Aceptacion
				template += self.get_ack_dte_data(vals)
				template += """<CodEnvio>%s</CodEnvio>\n""" % (vals['send_cdg'])
				template += """<EstadoDTE>%s</EstadoDTE>\n""" % (vals['acceptance_state'])
				template += """<EstadoDTEGlosa>%s</EstadoDTEGlosa>\n""" % (vals['acceptance_note'])
				template += """</ResultadoDTE>\n"""

			template += self.get_footer(self) #-> agregar el tmpl_id cuando generemos los id de documentos distintos
		return template

	def get_dte_assignment(self, obj):
		reference = 'DC%s%s%s' % (obj.number, obj.journal_id.doc_type_id.sii_code, dt.now().strftime('%Y%m%d%H%M%S'))

		template = """<DocumentoDTECedido ID="%s">\n""" % (reference)
		template += """%s""" % (base64.b64decode(obj.signed_xml)).decode('iso-8859-1')
#		temlpate += """<ImagenDTE></ImagenDTE>\n"""   #Base64
#		template += """<Recibo>\n"""
#		template += """%s""" % ('') #Buscar de donde sacarlo, si no esta en documentos, habilitar campo para subirlo.
#		template += """</Recibo>\n"""
	### SI SE SUBE LA IMAGEN DEBE SER EN ESTE CAMPO (en caso de ser manual, deberia quedar habilitado)
#		template += """<ImagenAr>\n"""
#		template += """%s""" % ('')  #  Base 64
#		template += """</ImagenAr>\n"""
		template += """<TmstFirma>%s</TmstFirma>\n""" % (dt.now().strftime('%Y-%m-%dT%H:%M:%S'))
		template += """</DocumentoDTECedido>\n"""
		template += self.get_signature(reference=reference)
		return template

	def get_assignment(self, obj, doc, count):
		reference = 'CF%s%s' % (doc.number, doc.journal_id.doc_type_id.sii_code)

		template = """<DocumentoCesion ID="%s">\n""" % (reference)
		template += """<SeqCesion>%s</SeqCesion>\n""" % count
		template += """<IdDTE>\n"""
		template += """<TipoDTE>%s</TipoDTE>\n""" % (doc.journal_id.doc_type_id.sii_code)
		template += """<RUTEmisor>%s</RUTEmisor>\n""" % (doc.company_id.partner_id.rut.replace('.','').replace('k','K'))
		template += """<RUTReceptor>%s</RUTReceptor>\n""" % (doc.partner_id.rut.replace('.','').replace('k','K'))
		template += """<Folio>%s</Folio>\n""" % (doc.number)
		template += """<FchEmis>%s</FchEmis>\n""" % (doc.date_invoice)
		template += """<MntTotal>%s</MntTotal>\n""" % (int(round(doc.amount_total, 0)))
		template += """</IdDTE>\n"""
		############# DEBIERA SER LA COMPANY, REVISAR #################
		template += """<Cedente>\n"""
		template += """<RUT>%s</RUT>\n""" % (obj.provider_company_id.rut.replace('.','').replace('k','K'))
		template += """<RazonSocial>%s</RazonSocial>\n""" % (obj.provider_company_id.partner_id.name[:100].\
									replace('&','&amp;').replace('<','&lt;').replace('>','&gt;'))
		if obj.provider_company_id.partner_id.street and obj.provider_company_id.partner_id.street2:
			addr = '%s %s' % (obj.provider_company_id.partner_id.street.replace('&','&amp;').\
									replace('<','&lt;').replace('>','&gt;'), \
						obj.provider_company_id.partner_id.street2.replace('&','&amp;').\
									replace('<','&lt;').replace('>','&gt;'))
			template += """<Direccion>%s</Direccion>\n""" % (addr[:60])
		elif obj.provider_company_id.partner_id.street:
			template += """<Direccion>%s</Direccion>\n""" % (obj.provider_company_id.partner_id.street[:60].replace('&','&amp;').\
											replace('<','&lt;').replace('>','&gt;'))
		elif obj.provider_company_id.company_id.partner_id.street2:
			template += """<Direccion>%s</Direccion>\n""" % (obj.provider_company_id.partner_id.street2[:60].replace('&','&amp;').\
											replace('<','&lt;').replace('>','&gt;'))
		else:
			raise Warning(_('No provider address'))
		template += """<eMail>%s</eMail>\n""" % (obj.provider_company_id.partner_id.email)
		########### TOMAR EL USUARIO Y CON ESTE GRUPO TE MUESTRA LAS COSAS DE CESION; CREO QUE ES LA MEJOR IDEA #######
		user = self.env['res.users'].browse(self._uid)
		template += """<RUTAutorizado>\n"""
		if user.partner_id.rut:
			template += """<RUT>%s</RUT>\n""" % (user.partner_id.rut.replace('.','').replace('k','K'))
		else:
			raise Warning(_('The user %s hasn\'t rut set, please check.') % (user.partner_id.name))
		template += """<Nombre>%s</Nombre>\n""" % (user.partner_id.name)
		template += """</RUTAutorizado>\n"""
######## SOLO va si son documentos no electronicos
#		template += """<DeclaracionJurada>Se declara bajo juramento que %s, RUT ha puesto a disposición del cesionario %s, RUT %s el o los documentos donde constan los recibos de las mercaderías entregadas o servicios prestados, entregados por parte del deudor de la factura %s, RUT %s, deacuerdo a lo establecido en la Ley N° 19.983.""" % (user.partner_id.name, user.partner_id.rut.replace(',','').replace('k','K'), vals['cesionario_name'], vals['rut_cesionario'], obj.partner_id.name, obj.partner_id.rut.replace('.','').replace('k','K'))
		template += """</Cedente>\n"""
		template += """<Cesionario>\n"""
		template += """<RUT>%s</RUT>\n""" % (obj.assignee_id.rut.replace('.','').replace('k','K'))
		template += """<RazonSocial>%s</RazonSocial>\n""" % (obj.assignee_id.name[:100].\
									replace('&','&amp;').replace('<','&lt;').replace('>','&gt;'))
		if obj.assignee_id.street and obj.assignee_id.street2:
			addr = '%s %s' % (obj.assignee_id.street.replace('&','&amp;').replace('<','&lt;').replace('>','&gt;'), \
						obj.assignee_id.street2.replace('&','&amp;').replace('<','&lt;').replace('>','&gt;'))
			template += """<Direccion>%s</Direccion>\n""" % (addr[:60])
		elif obj.assignee_id.street:
			template += """<Direccion>%s</Direccion>\n""" % (obj.assignee_id.street[:60].replace('&','&amp;').\
											replace('<','&lt;').replace('>','&gt;'))
		elif obj.assignee_id.street2:
			template += """<Direccion>%s</Direccion>\n""" % (obj.assignee_id.street2[:60].replace('&','&amp;').\
											replace('<','&lt;').replace('>','&gt;'))
		else:
			raise Warning(_('No assignee address'))
		template += """<eMail>%s</eMail>\n""" % (obj.assignee_id.cessor_email)
		template += """</Cesionario>\n"""
#		print "esta es la suma de los impuestos de documentos:", sum(tax_line.amount for tax_line in doc.tax_line if tax_line.tax_code_id.code == 'IDF')
		template += """<MontoCesion>%s</MontoCesion>\n""" % (int(round(sum(tax_line.amount for tax_line in doc.tax_line if tax_line.tax_code_id.code == 'IDF'), 0)))
		template += """<UltimoVencimiento>%s</UltimoVencimiento>\n""" % (doc.date_due)
		template += """<OtrasCondiciones>%s</OtrasCondiciones>\n""" % (obj.comment)
		template += """<eMailDeudor>%s</eMailDeudor>\n""" % (doc.partner_id.email)
		template += """<TmstCesion>%s</TmstCesion>\n""" % (dt.now().strftime('%Y-%m-%dT%H:%M:%S'))
		template += """</DocumentoCesion>\n"""
		template += self.get_signature(reference=reference)
		return template

	def get_assignment_aec(self, obj, signed_dte_assignment_xml, signed_assignment_xml):
		reference = 'T%s%s' % (obj.id, dt.now().strftime('%Y%m%d%H%M%S'))

		template = """<?xml version="1.0" encoding="ISO-8859-1"?>\n"""
		template += """<AEC xmlns="http://www.sii.cl/SiiDte" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sii.cl/SiiDte AEC_v10.xsd" version="1.0">\n"""
		template += """<DocumentoAEC ID="%s">\n""" % (reference)
		template += """<Caratula version="1.0">\n"""
		template += """<RutCedente>%s</RutCedente>\n""" % obj.provider_company_id.rut.replace('.','').replace('k','K')
		template += """<RutCesionario>%s</RutCesionario>\n""" % obj.assignee_id.rut.replace('.','').replace('k','K')
		template += """<NmbContacto>%s</NmbContacto>\n""" % obj.contact_name[:40]
		template += """<FonoContacto>%s</FonoContacto>\n""" % obj.contact_phone[:40]
		template += """<MailContacto>%s</MailContacto>\n""" % obj.contact_email[:40]
		template += """<TmstFirmaEnvio>%s</TmstFirmaEnvio>\n""" % (dt.now().strftime('%Y-%m-%dT%H:%M:%S'))
		template += """</Caratula>\n"""
		template += """<Cesiones>\n"""
		template += """<DTECedido version="1.0">\n"""
		template += (signed_dte_assignment_xml).decode('iso-8859-1')
		template += """</DTECedido>\n"""
		template += """<Cesion version="1.0">\n"""
		template += (signed_assignment_xml).decode('iso-8859-1')
		template += """</Cesion>\n"""
		template += """</Cesiones>\n"""
		template += """</DocumentoAEC>\n"""
		template += self.get_signature(reference=reference)
		template += """</AEC>"""
		return template
DteTemplates()

