# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _

class l10n_cl_dte_validator(models.Model):
 	_name = 'l10n_cl_dte.validator'

	@api.model
	def validate_logic(self, obj):
		self.env['res.company'].comprobar_modulo()
		obj.ensure_one()

		if obj._name in ('account.invoice', 'l10n_cl_dte.stock'):
			if not (obj.partner_id.parent_id and obj.partner_id.parent_id.distribution_email) \
									and not obj.partner_id.distribution_email:
				raise Warning(_('No distribution email set for the selected customer, please check.'))
			#Se debe dejar para ambos, pero en la guia aun no ponemos los campos
			if obj._name == 'account.invoice' and obj.tot_packages:
				if not obj.package_code_ids:
					raise Warning(_('You have the Total Packages field set, you must set at least one Package Code.'))
				if len(obj.package_code_ids) > 10:
					raise Warning(_('The Package Code lines mustn\'t be more than 10.'))


		if obj._name == 'account.invoice':
			if obj.journal_id.doc_type_id.sii_code not in ('33', '34', '39', '41', '46', '56', '61', '110', '111', '112'):
				raise Warning(_('The journal is\'nt electronic one, please check.'))

			if not obj.invoice_line:
				raise Warning(_('There must be at least one invoice line.'))

			if obj.amount_untaxed and obj.amount_untaxed < 5:
				raise Warning(_('The untaxed amount must be greater than 5.'))

			for line in obj.invoice_line:
				exev = idf = other = False
				if not line.invoice_line_tax_id:
					raise Warning(_('The lines %s doesn\'t have defined tax') % (line.product_id.name or line.name))

				for tax in line.invoice_line_tax_id:
					if tax.tax_code_id.code in ('EXEV', 'EXEC'):
						exev = True
					elif tax.tax_code_id.code in ('IDF', 'ICF'):
						idf = True
					else:
						other = True
				if exev and (idf or other):
					raise Warning(_('You can\'t have the EXEV and another tax in the same line.\nLine %s.') % \
													(line.product_id.name or line.name))
				if obj.journal_id.doc_type_id.sii_code == '34' and (idf or other):
					raise Warning(_('For Sales Exempt Journal the line %s have a unpermitted tax.') % \
													(line.product_id.name or line.name))
				if obj.journal_id.doc_type_id.sii_code in ('110','111','112') and (idf or other):
					raise Warning(_('For Export Invoices, Credit Notes and Debit Notes the line %s must be exempt.') % \
													(line.product_id.name or line.name))
				if obj.tax_included and (other or exev):
					raise Warning(_('You can\'t have the Tax Included field selected and not IDF or ICF tax.'))

			if obj.tax_included:
				for line in obj.invoice_line:
					if not all(tax.price_include for tax in line.invoice_line_tax_id):
						raise Warning(_('You have the Tax Included field selected, all the taxes must be price included.'))
			else:
				for line in obj.invoice_line:
					if not all(not tax.price_include for tax in line.invoice_line_tax_id):
						raise Warning(_('You have\'nt the Tax Included field selected, the taxes must not be price included.'))

		elif obj._name == 'l10n_cl_dte.stock':
			if not obj.move_lines and obj.picking_id:
				raise Warning(_('There must be at least one line in the dispatch order.'))

			if not obj.dispatch_order_line and not obj.picking_id:
				raise Warning(_('There must be at least one line in the dispatch order.'))

			if len(obj.dispatch_order_line) > obj.company_id.stock_lines and not obj.picking_id:
				raise Warning(_('You can\'t generate dispatch orders with more than %s lines.') %s (obj.company_id.stock_lines))

			if obj.manual_amount_untaxed and obj.manual_amount_untaxed < 5:
				raise Warning(_('The untaxed amount must be greater than 5.'))

			for line in obj.dispatch_order_line:
				other = False
				if not line.dispatch_order_line_tax_id:
					raise Warning(_('The line %s doesn\'t have defined tax') % (line.product_id.name or line.name))

				for tax in line.dispatch_order_line_tax_id:
					if tax.tax_code_id.code == 'EXEV':
						raise Warning(_('You can\'t have a EXEV tax in the dispatch order line.\nLine %s.') % \
													(line.product_id.name or line.name))
					elif tax.tax_code_id.code != 'IDF':
						other = True

				if obj.tax_included and other:
					raise Warning(_('You can\'t have the Tax Included field selected and not IDF tax.'))

			if obj.tax_included:
				for line in obj.dispatch_order_line:
					if not all(tax.price_include for tax in line.dispatch_order_line_tax_id):
						raise Warning(\
							_('You have the Tax Included field selected, all the taxes must be price included.'))
			else:
				for line in obj.dispatch_order_line:
					if not all(not tax.price_include for tax in line.dispatch_order_line_tax_id):
						raise Warning(\
							_('You have\'nt the Tax Included field selected, the taxes must not be price included.'))
		elif obj._name == 'pos.order':
			#Agregar cuando hagan falta, si no, borrar
			return True
		return True
l10n_cl_dte_validator()

class l10n_cl_dte_attachment(models.Model):
	_name = 'l10n_cl_dte.attachment'

	@api.model
	def get_attachment_values(self, obj, attachment, exten='.pdf'):
		if obj._name == 'account.invoice':
			if obj.journal_id.doc_type_id.sii_code == '56' and obj.type in ('out_invoice', 'in_invoice'):
				doc_name = _('Debit Note')
			elif obj.journal_id.doc_type_id.sii_code == '111' and obj.type == 'out_invoice':
				doc_name = _('Export Debit Note')
			elif obj.journal_id.doc_type_id.sii_code in ('39', '41') and obj.type == 'out_invoice':
				doc_name = _('Receipt')
			elif obj.journal_id.doc_type_id.sii_code in ('33', '34', '46', '110') and obj.type in ('out_invoice', 'in_invoice'):
				doc_name = _('Invoice')
			elif obj.journal_id.doc_type_id.sii_code == '110' and obj.type == 'out_invoice':
				doc_name = _('Export Invoice')
			elif obj.journal_id.doc_type_id.sii_code == '61' and obj.type in ('out_refund', 'in_refund'):
				doc_name = _('Credit Note')
			elif obj.journal_id.doc_type_id.sii_code == '112' and obj.type == 'out_refund':
				doc_name = _('Export Credit Note')
			else:
				raise Warning(_('The document isn\'t a supported type'))

			name = '%s.%s_%s%s' % (obj.journal_id.doc_type_id.sii_code, doc_name, obj.number, exten)
		elif obj._name == 'l10n_cl_dte.stock':
			name = '%s.%s_%s%s' % (obj.journal_id.doc_type_id.sii_code, _('Dispatch Order'), obj.folio, exten) 
		elif obj._name in ('l10n_cl_dte.books', 'l10n_cl_dte_invoice_receipt.daily_book'):
			name = '%s%s' % (obj.name, exten)
		elif obj._name == 'l10n_cl_dte.assignment':
			name = 'aec_%s_%s_%s%s' % (obj.provider_company_id.partner_id.rut.replace('.',''), obj.assignee_id.rut.replace('.',''), obj.id, exten)

		return {
			'name': name,
			'res_name': name,
			'res_model': obj._name,
			'res_id': obj.id,
			'datas': attachment,
			'datas_fname': name,
			'type': 'binary'
		}

	@api.model
	def add_attachment(self, obj, attch):
		ir_pool = self.env['ir.attachment']

		for old in ir_pool.search([('name','=',attch['name']),('res_model','=',obj._name)]):
			ir_pool += old
		ir_pool.sudo().unlink()
		ir_pool.sudo().create(attch)
		return True
l10n_cl_dte_attachment()
