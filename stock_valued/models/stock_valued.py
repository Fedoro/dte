# -*- encoding: utf-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (c) 2014-2015 Acysos S.L. (http://acysos.com)
#    All Rights Reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.exceptions import Warning
from openerp import models, fields, api
from openerp.addons.decimal_precision import decimal_precision as dp


class StockPicking(models.Model):
	_inherit = 'stock.picking'

	@api.model
	def _default_currency(self):
		user = self.env['res.users'].browse(self._uid)
		return user.company_id and user.company_id.currency_id

	@api.multi
	@api.depends('sale_id.amount_untaxed', 'sale_id.amount_tax',
					'sale_id.amount_total', 'move_lines')
	def _compute_amount(self):
		for picking in self:
			if picking.pack_operation_ids:
				for operation in picking.pack_operation_ids:
					picking.amount_untaxed += operation.sale_subtotal
					picking.amount_tax += operation.sale_taxes
				picking.amount_total = picking.amount_untaxed + picking.amount_tax
			else:
				for move in picking.move_lines:
					picking.amount_untaxed += move.sale_subtotal
					picking.amount_tax += move.sale_taxes
				picking.amount_total = picking.amount_untaxed + picking.amount_tax

	currency_id = fields.Many2one('res.currency', 'Currency', default=_default_currency,\
						 readonly=True, states={'draft': [('readonly', False)]})
	amount_untaxed = fields.Float('Untaxed Amount', compute='_compute_amount', digits_compute=dp.get_precision('Account'))
	amount_tax = fields.Float('Taxes', compute='_compute_amount', digits_compute=dp.get_precision('Account'))
	amount_total = fields.Float('Total', compute='_compute_amount', digits_compute=dp.get_precision('Account'))
	stock_valued = fields.Boolean('Stock Valued')
StockPicking()


class StockMove(models.Model):
	_inherit = 'stock.move'

	@api.model
	def _default_currency(self):
		user = self.env['res.users'].browse(self._uid)
		return user.company_id and user.company_id.currency_id

	@api.multi
	@api.depends('procurement_id.sale_line_id',
			'procurement_id.sale_line_id.price_unit',
			'procurement_id.sale_line_id.discount',
			'procurement_id.sale_line_id.price_subtotal',
			'procurement_id.sale_line_id.price_reduce',
			'procurement_id.sale_line_id.order_id',
			'product_qty', 'manual_price_unit', 'manual_discount', 'manual_tax_id')
	def _sale_prices(self):
		for move in self:
			if not move.product_qty:
				p_untax = p_unit = tax = disc = subt = 0
			elif move.procurement_id.sale_line_id:
				sale_line = move.procurement_id.sale_line_id
				old_cur = move.procurement_id.sale_line_id.order_id.currency_id

				if not sale_line.product_uom_qty:
					p_untax = p_unit = tax = disc = subt = 0
				elif move.currency_id.id != old_cur.id:
					p_untax = round((sale_line.price_reduce / old_cur.rate) * move.currency_id.rate, 0)
					p_unit = round((sale_line.price_unit / old_cur.rate) * move.currency_id.rate, 0)
					tax = round(((sale_line.order_id._amount_line_tax(sale_line) / sale_line.product_uom_qty * move.product_qty) / old_cur.rate) * move.currency_id.rate, 0)
					disc = sale_line.discount
					subt = round((sale_line.price_reduce * move.product_qty / old_cur.rate) * move.currency_id.rate, 0)
				else:
					p_untax = round(sale_line.price_reduce)
					p_unit = round(sale_line.price_unit)
					tax = move.sale_taxes = round(sale_line.order_id._amount_line_tax(sale_line) / sale_line.product_uom_qty * move.product_qty, 0)
					disc = sale_line.discount
					subt = round(sale_line.price_reduce * move.product_qty, 0)
			else:
				p_untax = tax = subt = 0.0

				p_unit = round(move.manual_price_unit)
				disc = move.manual_discount
				price = round(move.manual_price_unit * (1-(move.manual_discount or 0.0)/100.0), 0)
				if move.product_id:
					taxes = move.manual_tax_id.compute_all(price, move.product_qty, product=move.product_id, \
													partner=move.picking_id.partner_id)
					if taxes:
						for c in taxes['taxes']:
							tax += c.get('amount', 0.0)
					subt = round(taxes['total'], 0)
					if move.product_qty:
						p_untax = round(taxes['total'] / move.product_qty, 0)

			move.sale_price_unit = p_unit
			move.sale_discount = disc
			move.sale_price_untaxed = p_untax
			move.sale_taxes = round(tax, 0)
			move.sale_subtotal = subt
		return True

	@api.onchange('product_id', 'manual_price_unit')
	def onchange_product_id(self, prod_id=False, loc_id=False, loc_dest_id=False, partner_id=False):
		res = super(StockMove, self).onchange_product_id(prod_id=self.product_id.id,loc_id=self.location_id.id,
										loc_dest_id=self.location_dest_id.id,
										partner_id=self.picking_id.partner_id.id)

		if res.get('value', False):
			cond = self.manual_price_unit and self.product_id and self.product_id.taxes_id and True or False

			if cond:
				res['value'].update({'manual_tax_id': [(4, tax.id, False) for tax in self.product_id.taxes_id]})
			else:
				res['value'].update({'manual_tax_id': [(5, False, False)]})

			self.product_uos_qty = res['value']['product_uos_qty']
			self.name = res['value']['name']
			self.product_uom = res['value']['product_uom']
			self.manual_tax_id = res['value']['manual_tax_id']
			self.product_uom_qty = res['value']['product_uom_qty']
			self.location_dest_id = res['value']['location_dest_id']
			self.product_uos = res['value']['product_uos']
			self.location_id = res['value']['location_id']
		return res

	manual_price_unit = fields.Float('Manual Price Unit')
	manual_discount = fields.Float('Manual Discount (%)')
	manual_tax_id = fields.Many2many('account.tax', 'stock_move_tax_rel', 'move_id', 'tax_id', 'Taxes')
	sale_subtotal = fields.Float('Subtotal', compute='_sale_prices', digits_compute=dp.get_precision('Account'))
	sale_price_unit = fields.Float('Price', compute='_sale_prices', digits_compute=dp.get_precision('Account'))
	sale_price_untaxed = fields.Float('Price Untaxed', compute='_sale_prices', digits_compute=dp.get_precision('Account'))
	sale_taxes = fields.Float('Total Taxes', compute='_sale_prices', digits_compute=dp.get_precision('Account'))
	sale_discount = fields.Float('Discount (%)', compute='_sale_prices', digits_compute=dp.get_precision('Account'))
	currency_id = fields.Many2one('res.currency', 'Currency', default=_default_currency,\
						readonly=True, states={'draft': [('readonly', False)]})
StockMove()

class StockPackOperation(models.Model):
	_inherit = "stock.pack.operation"

	@api.multi
	def _sale_prices(self):
		for op in self:
			p_unit = discount = p_untax = tax = subt = 0

			if op.linked_move_operation_ids and op.product_qty:
				move = op.linked_move_operation_ids[0].move_id

				if move.product_qty:
					if move.procurement_id.sale_line_id:
						tax = round(move.sale_taxes / move.product_qty * op.product_qty, 0)
						p_untax = move.sale_price_untaxed
						p_unit = move.sale_price_unit
						discount = move.sale_discount
						subt = round(move.sale_price_untaxed * op.product_qty, 0)
					else:
						if move.product_id:
							p_unit = round(move.sale_price_unit, 0)
							p_untax = round(move.sale_price_untaxed, 0)
							discount = move.sale_discount
							tax = round(move.sale_taxes / move.product_qty * op.product_qty, 0)
							subt = round(op.sale_price_untaxed * op.product_qty, 0)

			op.sale_price_unit = p_unit
			op.sale_discount = discount
			op.sale_price_untaxed = p_untax
			op.sale_taxes = tax
			op.sale_subtotal = subt

		return True

	sale_subtotal = fields.Float('Subtotal', compute='_sale_prices', digits_compute=dp.get_precision('Account'))
	sale_price_unit = fields.Float('Price', compute='_sale_prices', digits_compute=dp.get_precision('Account'))
	sale_price_untaxed = fields.Float('Price Untaxed', compute='_sale_prices', digits_compute=dp.get_precision('Account'))
	sale_taxes = fields.Float('Total Taxes', compute='_sale_prices', digits_compute=dp.get_precision('Account'))
	sale_discount = fields.Float('Discount (%)', compute='_sale_prices', digits_compute=dp.get_precision('Account'))
StockPackOperation()
