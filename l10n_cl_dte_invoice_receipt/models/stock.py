# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _

class l10n_cl_dte_account_stock_receipt(models.Model):
	_inherit = 'stock.picking'

	@api.model
	def _get_invoice_vals(self, key, inv_type, journal_id, move):
		res = super(l10n_cl_dte_account_stock_receipt, self)._get_invoice_vals(key, inv_type, journal_id, move)

		journal_id = res.get('journal_id', None)
		if journal_id:
			journal = self.env['account.journal'].browse(journal_id)
			res.update({'is_receipt': journal.doc_type_id and journal.doc_type_id.sii_code in ['39','41'] or False})
		return res
l10n_cl_dte_account_stock_receipt()
