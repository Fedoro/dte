# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DF

from openerp.exceptions import Warning
from openerp.tools.translate import _
from datetime import datetime, timedelta

import xml.etree.ElementTree as etree
from suds.client import Client
from suds import WebFault
import requests, base64

class DailyBook(models.Model):
	# este libro es solo para documentos 39 (boleta electronica), 41 (boleta exenta electronica) y 61 (nota de credito)
	# el periodo es el dia informado 

	# nota SII : Para los documentos que se exija este resumen en forma diaria la fecha de Inicio del Resumen debe ser 
	# igual a la fecha de Final del Resumen.  

	_name = 'l10n_cl_dte_invoice_receipt.daily_book'
	_inherit = ['mail.thread']
	_order = 'date DESC'

	@api.multi
	@api.depends('date_documents')
	def _book_name(self):
		for book in self:
			if book.date_documents:
				date = fields.Date.from_string(book.date_documents)
				book.name = 'Consumo de folios %s' % (date.strftime('%d-%m-%Y'))
			else:
				book.name = 'Consumo de folios sin fecha'

	def _book_date(self):
		return fields.Date.context_today(self, timestamp = datetime.now())

	@api.multi
	@api.depends('date')
	def _documents_date(self):
		for book in self:
			if book.date:
				date = fields.Date.from_string(book.date)
				book.date_documents = (date - timedelta(days=1)).strftime(DF)

	@api.multi
	@api.depends('document_ids')
	def _calc_totals(self):
		for book in self:
			if book.document_ids:
				for document in book.document_ids:
					if document.state == 'annuled':
						book.total_annuled += 1
					elif document.state == 'used':
						book.amount_untaxed += document.amount_untaxed  
						book.amount_tax += document.amount_tax 
						book.amount_exempt += document.amount_exempt 
						book.amount_total += document.amount_total 
						book.total_used += 1
					else:
						print 'No se que estado seria este xD...'
					book.total_issued += 1 
			else:
					book.amount_untaxed = 0
					book.amount_tax = 0
					book.amount_exempt = 0
					book.amount_total = 0
					book.total_issued = 0
					book.total_annuled = 0
					book.total_used = 0

 	@api.multi
 	@api.depends('document_ids')
	def calc_types(self):
		for book in self:
			book.electronic_receipt_count = 0
			book.electronic_except_receipt_count = 0
			book.electronic_credit_notes_count = 0
			if book.document_ids:
				for document in book.document_ids:
					if document.document_type.sii_code == '39':
						book.electronic_receipt_count += 1
					if document.document_type.sii_code == '41':
						book.electronic_except_receipt_count += 1
					if document.document_type.sii_code == '61':
						book.electronic_credit_notes_count += 1

	@api.multi
	def get_documents_by_type(self, document_type):
		self.ensure_one()
		lines = self.env['l10n_cl_dte_invoice_receipt.daily_book_line']

		amount_untaxed = 0
		amount_tax = 0
		amount_exempt = 0
		amount_total = 0
		total_issued = 0
		total_annuled = 0
		total_used = 0

		document_type_id = self.env['l10n_cl_dte.document_type'].search([('sii_code','=',document_type)]).id
		lines = lines.search([('document_type','=',document_type_id),('book_id','=', self.id)])

		for line in lines:
			if line.state == 'annuled':
				total_annuled += 1 if line.state == 'annuled' else 0
			else:
				amount_untaxed += line.amount_untaxed
				amount_tax += line.amount_tax
				amount_exempt += line.amount_exempt
				amount_total += line.amount_total
				total_used += 1 if line.state == 'used' else 0
			total_issued += 1 

		values = {
			'amount_untaxed': amount_untaxed,
			'amount_tax': amount_tax,
			'iva_rate': '19.0',
			'amount_exempt': amount_exempt,
			'amount_total': amount_total,
			'total_issued': total_issued,
			'total_annuled': total_annuled,
			'total_used': total_used,
			'lines': lines
		}
		return values

	name = fields.Char('Name', compute='_book_name', store=True)
	# Comañia a la cual pertenece el libro 
	company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env['res.company'].\
							_company_default_get('l10n_cl_dte_invoice_receipt.daily_book'), \
							required=True, readonly=True, states={'draft': [('readonly', False)]})
	# Dia que fue generado este libro
	date = fields.Date('Day of Book', default=_book_date)
	# Dia al que pertenecen los documentos de este libro.
	date_documents = fields.Date('Day of Documents', compute='_documents_date', store=True)
	# Estado de envio 
	state = fields.Selection([('draft','Draft'),('signed','Signed Book'),('error','Error'),
					 ('waiting_validation','Waiting Validation'),('validate','Validate')], 'State', default='draft')
	# # tipo de documento informados en este libro diario
	# document_type = fields.Many2one('l10n_cl_dte.document_type', 'Document Type',copy=False)
	# valor sin impuestos (neto)
	amount_untaxed = fields.Integer('Total Value without tax' , compute='_calc_totals')
	# valor impuestos 
	amount_tax = fields.Integer('Total Tax', compute='_calc_totals')
	# valor exento
	amount_exempt = fields.Integer('Total Exempt', compute="_calc_totals")
	# valor total
	amount_total = fields.Integer('Total', compute="_calc_totals")
	# total informados
	total_issued = fields.Integer('Number of documents informed', compute="_calc_totals")
	# total anulados
	total_annuled = fields.Integer('Number of documents annuled', compute="_calc_totals")
	# total usados
	total_used = fields.Integer('All documents used', compute="_calc_totals")
	# tasa IVA 
	iva_rate = fields.Float('IVA rate', default=19.0)
	# Folio de notificacion
	notification_folio = fields.Integer('Notification Folio')
	# Track de seguimiento
	track_id = fields.Char('Track id')
	# Error in sending documents
	failed_text = fields.Text('Error')
	# coments 
	comment = fields.Text('Comments')
	# secuencia de envio
	send_sequence = fields.Integer('Send sequence', default=0)
	document_ids = fields.One2many('l10n_cl_dte_invoice_receipt.daily_book_line', 'book_id', 'Documents')
	electronic_receipt_count = fields.Integer('Total Documents', compute='calc_types')
	electronic_except_receipt_count = fields.Integer('The book has electronic except receipt?', compute='calc_types')
	electronic_credit_notes_count = fields.Integer('The book has electronic credit notes?', compute='calc_types')

	@api.multi
	def return_to_draft(self):
		self.ensure_one()
		self.state = 'draft'

	@api.model
	def search_receipts_journals(self):
		"""Busca todos los diarios que sean para boletas"""
		# documentos autorizados son el 39, 41, 61 
		receipts_document_type = self.env['l10n_cl_dte.document_type'].search([('sii_code','in',['39' , '41' , '61'])])

		journals = self.env['account.journal'].search([('doc_type_id','in', [doc_type.id for doc_type in receipts_document_type])])
		journals_ids = []
		for journal in journals:
			journals_ids.append(journal.id) 
		return journals_ids

	@api.multi
	def calc_yesterday(self):
		return ( fields.Date.from_string(self.date) - timedelta(days=1)).strftime(DF)


	@api.multi
	def search_documents(self, models=['account.invoice']):
		yesterday = self.calc_yesterday()
		journals = self.search_receipts_journals()

		for model in models:
			documents = self.env[model].search([('date_invoice','=',yesterday),('journal_id','in',journals)], order='number desc')
		return documents

	@api.model
	def prepare_lines(self, obj):
		return {
			'book_id': self.id,
			'document_type': obj.journal_id.doc_type_id.id,
			'state': 'annuled' if obj.state == 'cancel' else 'used',
			'amount_untaxed': obj.amount_untaxed,
			'amount_tax': obj.amount_tax,
			'amount_exempt': obj.amount_exempt or 0,
			'amount_total': obj.amount_total,
			'folio': obj.number
		}

	@api.multi
	def get_documents(self):
		self.ensure_one()
		self.document_ids.unlink()
		lines = self.env['l10n_cl_dte_invoice_receipt.daily_book_line']

		documents = self.search_documents()
		if len(documents) > 0:
			for document in documents:
				cond = document._name == 'account.invoice' and document.journal_id.doc_type_id.sii_code == '61' and True or False
				if document.number > 0:
					if cond:
						for_receipt = [True for ref in document.reference_lines \
										if ref.reference_type.sii_code in ('39', '41')]
						if True in for_receipt and document.state in ('paid', 'open'):
							lines.create(self.prepare_lines(document))
					else:
						lines.create(self.prepare_lines(document))
		return True

	@api.multi
	def clean_slate(self):
		self.write({'failed_text': None})

	@api.multi
	def sign_book(self):
		dte_attach = self.env['l10n_cl_dte.attachment']

		for book in self:
			book.send_sequence += 1
			signed_xml = self.env['l10n_cl_dte.signer'].sign_xml(book)
			result = base64.b64encode(signed_xml)
			attch_vals = dte_attach.get_attachment_values(book, result, exten='.xml')
			dte_attach.add_attachment(book, attch_vals)
			book.state = 'signed'
		return True

	@api.multi
	def send_book(self):
		for book in self:
			self.env['l10n_cl_dte.sii'].send_sii(book)
		return True

	@api.multi
	def get_send_state(self):
		for book in self:
			self.env['l10n_cl_dte.sii'].get_send_state(book)
		return True

	@api.model
	def make_books(self):
		# TODO: Enviar un email de acuse cuando el libro se envia al SII, a un usuario autorizado de la empresa
		companies = self.env['res.company'].search([])
		for company in companies:
			book = self.create({'company_id': company.id})
			book.get_documents()
			book.sign_book()
			book.send_book()
		return True

	@api.multi
	def unlink(self):
		for book in self:
			if book.state != 'draft':
				raise Warning(_('You can\'t delete a book which is not draft.'))
		return super(DailyBook, self).unlink()
DailyBook()

class DailyBookLines(models.Model):
	_name = 'l10n_cl_dte_invoice_receipt.daily_book_line'
	_order = "folio asc"

	@api.multi
	@api.depends('folio')
	def _calc_right(self):
		"""Calcula el numero siguiente  """
		for record in self:
			if record.folio:
				parent_right = record.search([
					('state','=',record.state),
					('folio','>',record.folio),
					('document_type','=', record.document_type.id),
					('book_id','=', record.book_id.id)
					], order="folio asc")

				if parent_right:
					record.parent_right =  parent_right[0].folio
				else: 
					record.parent_right = 0

	@api.multi
	@api.depends('folio')
	def _calc_left(self):
		"""Calcula el numero anterior """
		for record in self:
			if record.folio:
				parent_left = record.search([('state','=',record.state), ('folio','<',record.folio),\
										('document_type','=', record.document_type.id)])	
				if parent_left:
					count = len(parent_left) - 1
					record.parent_left = parent_left[count].folio

	# campo de control para saber cual es el proximo folio agregado
	parent_right = fields.Integer('Folio siguiente' , compute="_calc_right",store=True)
	# campo de control para saber cual es el anterior folio entregado
	parent_left = fields.Integer('Folio anterior' , compute="_calc_left", store=True)
	# Documento a cual pertenece esta linea
	book_id = fields.Many2one('l10n_cl_dte_invoice_receipt.daily_book', 'Daily book' , copy=False)
	# Tipo de documento en el SII
	document_type = fields.Many2one('l10n_cl_dte.document_type', 'Document Type',copy=False)
	# Folio for this document
	folio = fields.Integer('Folio')
	# State of document
	state = fields.Selection([('used','Used'),('annuled','Anulled')], 'State')
	# valor sin impuestos (neto)
	amount_untaxed = fields.Integer('Total Untaxed')
	# valor impuestos 
	amount_tax = fields.Integer('Total Tax')
	# valor exento
	amount_exempt = fields.Integer('Total Exempt')
	# valor total
	amount_total = fields.Integer('Total')
DailyBookLines()

class l10n_cl_dte_books_inherit(models.Model):
	_inherit = 'l10n_cl_dte.books'

	@api.one
	def collect_documents(self):
		super(l10n_cl_dte_books_inherit,self).collect_documents()
		group = self.env['l10n_cl_dte.books.journal_summarization']
		books = self.env['l10n_cl_dte_invoice_receipt.daily_book']

		if self.operation_type == 'VENTA':
			journal_vals = {}
			amount_untaxed = amount_exempt = amount_tax = amount_total = 0
			date_start = self.period_id.date_start
			date_stop = self.period_id.date_stop

			books = books.search([('date_documents','>=',date_start),('date_documents','<=',date_stop),
							('state','=','validate'),('company_id','=',self.company_id.id)])

			for book in books:
				for doc_type in ['39', '41']:
					vals = book.get_documents_by_type(doc_type)
					if doc_type not in journal_vals.keys():
						journal_vals[doc_type] = {'amount_untaxed': 0, 'amount_exempt': 0, 'amount_other_tax': 0, \
									'documents_total': 0, 'amount_tax': 0, 'amount_total_no_rec': 0, \
								'amount_total': 0, 'book_id': self.id, 'dte_type': doc_type, 'cancel_total': 0}
					journal_vals[doc_type]['amount_exempt'] += int(round(vals['amount_exempt'], 0))
					journal_vals[doc_type]['amount_untaxed'] += int(round(vals['amount_untaxed'], 0))
					journal_vals[doc_type]['amount_tax'] += int(round(vals['amount_tax'], 0))
					journal_vals[doc_type]['amount_total'] += int(round(vals['amount_total'], 0))
					journal_vals[doc_type]['documents_total'] += vals['total_issued']
					journal_vals[doc_type]['cancel_total'] += vals['total_annuled']

			for key in journal_vals.keys():
				if journal_vals[key]['documents_total'] != 0:
					group.create(journal_vals[key])
l10n_cl_dte_books_inherit()
