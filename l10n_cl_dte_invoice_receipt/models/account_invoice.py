# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT as DF
from datetime import datetime
import base64

class l10n_cl_dte_account_invoice_receipt(models.Model):
	_inherit = 'account.invoice'

	@api.multi
	def get_send_state(self):
		for invoice in self:
			if invoice.is_receipt:
				self.env['l10n_cl_dte.sii'].get_send_state_rest(invoice)
			else:
				self.env['l10n_cl_dte.sii'].get_send_state(invoice)
		return True

	@api.multi
	def split_invoice(self):
		for inv in self:
			if inv.is_receipt:
				inv.max_invoice_lines = False
			else:
				super(l10n_cl_dte_account_invoice_receipt, self).split_invoice()
		return True

	@api.multi
	def sign_document(self):
		dte_attach = self.env['l10n_cl_dte.attachment']

		for invoice in self:
			super(l10n_cl_dte_account_invoice_receipt, self).sign_document()
			if invoice.journal_id.doc_type_id.sii_code in ['39','41']:
				pdf_report = self.env['report'].get_pdf(self , 'l10n_cl_dte_templates.report_chilean_invoice')
				result = base64.b64encode(pdf_report)
				attch_vals = dte_attach.get_attachment_values(invoice, result)
				dte_attach.add_attachment(invoice, attch_vals)
		return True

	@api.onchange('journal_id', 'type')
	def _onchange_journal_id(self):
		res = super(l10n_cl_dte_account_invoice_receipt, self)._onchange_journal_id()
		if not self.dte:
			self.dte = self.journal_id.type in ['sale', 'sale_refund'] and self.journal_id.doc_type_id and \
									self.journal_id.doc_type_id.sii_code in ['39', '41'] or False
		self.is_receipt = self.journal_id.type in ['sale', 'sale_refund'] and self.journal_id.doc_type_id and \
									self.journal_id.doc_type_id.sii_code in ['39', '41'] or False
		return res

	# def _calc_chilean_datetime(self):
	# 	return fields.Datetime.context_timestamp(self, timestamp=datetime.now())

	# chilean_date = fields.Datetime('Chilean Date', default=_calc_chilean_datetime)
	is_receipt = fields.Boolean('Is Receipt?')
	service_type = fields.Selection([('1','Boletas de servicios periodicos'),('2','Boletas de servicios periodicos domiciliarios'),\
						('3','Boletas de venta y servicios'),('4','Boletas de espectaculo emitida por cuenta de terceros')], 'Service Type', default='3')
l10n_cl_dte_account_invoice_receipt()
