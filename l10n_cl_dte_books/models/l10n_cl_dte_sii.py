# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _

class l10n_cl_dte_sii_books_cron(models.Model):
	_inherit = 'l10n_cl_dte.sii'

	@api.model
	def get_send_status_cron(self, automatic=False, use_new_cursor=False):
		super(l10n_cl_dte_sii_books_cron, self).get_send_status_cron(automatic=automatic, use_new_cursor=use_new_cursor)
		books = self.env['l10n_cl_dte.books'].search([('state','=','waiting_validation')])
		for book in books:
			try:
				self.get_send_state(book)
			except:
				print 'pa variar se callo sii'
		return True
l10n_cl_dte_sii_books_cron()
