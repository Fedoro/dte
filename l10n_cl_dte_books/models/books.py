# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

import openerp.addons.decimal_precision as dp

from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _

import base64

class l10n_cl_dte_books_other_taxes(models.Model):
	_name = 'l10n_cl_dte.books.other_taxes'
	_description = 'Other Taxes Detail'
	_rec_name = 'tax_code'

	line_id = fields.Many2one('l10n_cl_dte.books.document_summarization', 'Related Detail')
	tax_code = fields.Char('Code')
	tax_recoverable = fields.Boolean('Recoverable')
	tax_rate = fields.Float('Tax Rate', digits=(3,2))
	tax_amount = fields.Integer('Amount')
l10n_cl_dte_books_other_taxes()

class l10n_cl_dte_books_document_summarization(models.Model):
	_name = 'l10n_cl_dte.books.document_summarization'
	_description = 'Document Summarization Maintainer'

	book_id = fields.Many2one('l10n_cl_dte.books', 'Related Book')
	invoice_id = fields.Many2one('account.invoice', 'Invoice')
	delivery_id = fields.Many2one('l10n_cl_dte.stock', 'Delivery Order')
	dte_type = fields.Char('DTE Type')
	folio = fields.Char('Folio')
	doc_date = fields.Date('Date')
	rut = fields.Char('Rut')
	partner_name = fields.Char('Name')
	amount_untaxed = fields.Float('Amount Untaxed')
	amount_exempt = fields.Float('Amount Exempt')
	amount_tax = fields.Float('Amount Tax')
	amount_other_tax = fields.Float('Amount Other Tax')
	amount_tax_no_rec = fields.Float('Amount Other Tax No Recoverable') #Campo oculto solo para diferenciar de manera sencilla
	other_tax_ids = fields.One2many('l10n_cl_dte.books.other_taxes', 'line_id', 'Other Taxes')
	amount_total = fields.Float('Amount Total')
	documents_total = fields.Integer('Total of Documents')
	state = fields.Char('State of Documents')
	mov_ind = fields.Integer('Operation Type')
l10n_cl_dte_books_document_summarization()

class l10n_cl_dte_books_journal_summarization(models.Model):
	_name = 'l10n_cl_dte.books.journal_summarization'

	book_id = fields.Many2one('l10n_cl_dte.books', 'Related Book')
	dte_type = fields.Char('DTE Type')
	amount_untaxed = fields.Float('Amount Untaxed')
	amount_exempt = fields.Float('Amount Exempt')
	amount_tax = fields.Float('Amount Tax')
	amount_other_tax = fields.Float('Amount Other Tax')
	amount_total_no_rec = fields.Float('Total Tax No Credit') #Totalizar toos los iva sin derecho a credito.
	amount_total = fields.Float('Amount Total')
	documents_total = fields.Integer('Total of Documents')
	cancel_total = fields.Integer('Cancel Documents')
l10n_cl_dte_books_journal_summarization()

class l10n_cl_dte_books(models.Model):
	_name = 'l10n_cl_dte.books'
	_order = 'period_id DESC, id DESC'
	_description = 'Electronic Books Maintainer'

	@api.model
	def _get_period(self):
		periods = self.env['account.period'].find()
		return periods and periods[0] or False

	@api.one
	@api.depends('operation_type', 'period_id')
	def _book_name(self):
		period_name = self.period_id and self.period_id.name or None
		if self.operation_type == 'VENTA':
			self.name = 'Libro de Ventas' if not period_name else 'Libro de Ventas ' + period_name
		elif self.operation_type == 'COMPRA':
			self.name = 'Libro de Compras' if not period_name else 'Libro de Compras ' + period_name
		elif self.operation_type == 'BOLETAS':
			self.name = 'Libro de Boletas' if not period_name else 'Libro de Boletas ' + period_name
		elif self.operation_type == 'LibroGuia':
			self.name = 'Libro Guias de Despacho' if not period_name else 'Libro Guias de Despacho ' + period_name

	@api.one
	@api.depends('journal_ids')
	def _compute_totals(self):
		canceled = tot_docs = untax = exempt = other_tax = tax = total = 0
		
		for line in self.journal_ids:
			untax += line.amount_untaxed
			exempt += line.amount_exempt
			tax += line.amount_tax
			other_tax += line.amount_other_tax
			total += line.amount_total
			canceled += line.cancel_total
			tot_docs += line.documents_total

		self.total_cancel_documents = canceled
		self.total_documents = tot_docs
		self.total_untaxed = untax
		self.total_exempt = exempt
		self.total_tax = tax
		self.total_other_taxes = other_tax
		self.total_amount = total

	@api.onchange('operation_type')
	def check_operation_type(self):
		if self.operation_type == 'GUIAS':
			self.book_type = 'ESPECIAL'


	#TODO: cambiar estado validate a validated... es la forma correcta de Validado en ingles.
	state = fields.Selection([('draft','Draft'),('signed','Signed Book'),('rectified','Rectified'),('error','Error'),\
					('waiting_validation','Waiting Validation'),('validate','Validate')], 'State', default='draft')
	operation_type = fields.Selection([('VENTA','Sales Book'),('COMPRA','Purchases Book'), ('LibroGuia','Delivery Book')], \
					'Operation', default='VENTA', required=True, readonly=True, states={'draft': [('readonly', False)]})
	book_type = fields.Selection([('MENSUAL','Monthly'),('ESPECIAL','Especial'),('RECTIFICA','Amend')], \
					'Type', default='MENSUAL', required=True, readonly=True, states={'draft': [('readonly', False)]})
	notification_folio = fields.Integer('Notification Folio')
	period_id = fields.Many2one('account.period', 'Period', default=_get_period, required=True, \
						domain="[('special','=',False)]", readonly=True, states={'draft': [('readonly', False)]})
	name = fields.Char(string='Name', compute=_book_name, store=True)
	number_amend = fields.Char('Number to Amend', readonly=True, states={'draft': [('readonly', False)]})
	document_ids = fields.One2many('l10n_cl_dte.books.document_summarization', 'book_id', 'Documents')
	journal_ids = fields.One2many('l10n_cl_dte.books.journal_summarization', 'book_id', 'Documents')

	total_untaxed = fields.Float('Total Untaxed', digits=dp.get_precision('Account'),
								store=True, compute='_compute_totals')
	total_exempt = fields.Float('Total Exempt', digits=dp.get_precision('Account'),
								store=True, compute='_compute_totals')
	total_tax = fields.Float('Total Tax', digits=dp.get_precision('Account'),
								store=True, compute='_compute_totals')
	total_other_taxes = fields.Float('Total Other Taxes', digits=dp.get_precision('Account'),
								store=True, compute='_compute_totals')
	total_amount = fields.Float('Total Amount', digits=dp.get_precision('Account'),
								store=True, compute='_compute_totals')
	total_documents = fields.Integer(string='Total Documents', store=True, compute='_compute_totals')
	total_cancel_documents = fields.Integer(string='Total Cancel Documents', store=True, compute='_compute_totals')
	track_id = fields.Char('Track Id')
	failed_text = fields.Text('Error')
	xml = fields.Text('XML')
	company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env['res.company'].\
			_company_default_get('l10n_cl_dte.books'), required=True, readonly=True, states={'draft': [('readonly', False)]})
	currency_id = fields.Many2one('res.currency', 'Currency', related='company_id.currency_id', store=True, readonly=True)
	comment = fields.Text('Comment')
	signed_xml = fields.Binary('Signed XML')

	@api.model
	def create(self, vals):
		if 'period_id' in vals and 'operation_type' in vals and 'company_id' in vals and 'book_type' in vals:
			if vals['book_type'] == 'RECTIFICA':
				for book in self.search([('period_id','=',vals['period_id']),('company_id','=',vals['company_id']),\
								('operation_type','=',vals['operation_type']),('state','=','validate')]):
					book.write({'state': 'rectified'})
			elif self.search([('period_id','=',vals['period_id']),('company_id','=',vals['company_id']),\
						('operation_type','=',vals['operation_type']),('book_type','=','MENSUAL')]):
					raise Warning(_('You already have a book for the selected period and company.'))
		return super(l10n_cl_dte_books, self).create(vals)

	@api.multi
	def unlink(self):
		for book in self:
			if book.state != 'draft':
				raise Warning(_('You can\'t delete a book which is not draft.'))
		return super(l10n_cl_dte_books, self).unlink()

	@api.model
	def set_journals(self, obj, book_type, journal_vals, tax=0, other_taxes=0, taxes_no_rec=0):
		if self.operation_type == 'LibroGuia':
			if obj.mov_ind not in journal_vals.keys():
				journal_vals[obj.mov_ind] = {'amount_untaxed': 0, 'amount_exempt': 0, 'documents_total': 0,
								'amount_other_tax': 0, 'amount_tax': 0, 'amount_total': 0,
								'amount_total_no_rec': 0, 'book_id': self.id, 'dte_type': obj.mov_ind}

			journal_vals[obj.mov_ind]['amount_untaxed'] += int(round(obj.amount_untaxed, 0))
			journal_vals[obj.mov_ind]['amount_tax'] += int(round(tax, 0))
			journal_vals[obj.mov_ind]['amount_total'] += int(round(obj.amount_total, 0))
			journal_vals[obj.mov_ind]['documents_total'] += 1
			if obj.state == 'cancel':
				journal_vals[obj.journal_id.doc_type_id.sii_code]['cancel_total'] += 1
		else:
			if obj.journal_id.doc_type_id.sii_code not in journal_vals.keys():
				journal_vals[obj.journal_id.doc_type_id.sii_code] = {'amount_untaxed': 0, 'amount_exempt': 0,
									'amount_other_tax': 0, 'documents_total': 0, 'amount_tax': 0,
									'amount_total_no_rec': 0, 'amount_total': 0, 'book_id': self.id,
									'dte_type': obj.journal_id.doc_type_id.sii_code, 'cancel_total': 0}

			journal_vals[obj.journal_id.doc_type_id.sii_code]['amount_exempt'] += int(round(obj.amount_exempt, 0))
			journal_vals[obj.journal_id.doc_type_id.sii_code]['amount_untaxed'] += int(round(obj.amount_untaxed, 0))
			journal_vals[obj.journal_id.doc_type_id.sii_code]['amount_tax'] += int(round(tax, 0))
			journal_vals[obj.journal_id.doc_type_id.sii_code]['amount_other_tax'] += int(round(other_taxes, 0))
			journal_vals[obj.journal_id.doc_type_id.sii_code]['amount_total_no_rec'] += int(round(taxes_no_rec, 0))
			journal_vals[obj.journal_id.doc_type_id.sii_code]['amount_total'] += int(round(obj.amount_total, 0))
			journal_vals[obj.journal_id.doc_type_id.sii_code]['documents_total'] += 1
			if obj.state == 'cancel':
				journal_vals[obj.journal_id.doc_type_id.sii_code]['cancel_total'] += 1


		return journal_vals

	@api.multi
	def collect_documents(self):
		self.ensure_one()

		vals = {}
		journal_vals = {}
		other_taxes = []

		docs = self.env['l10n_cl_dte.books.document_summarization']
		journals = self.env['l10n_cl_dte.books.journal_summarization']
		invoice_pool = self.env['account.invoice']
		delivery_pool = self.env['l10n_cl_dte.stock']

		self.document_ids.unlink()
		self.journal_ids.unlink()

		if self.operation_type == 'VENTA':
			for invoice in invoice_pool.search([('type','in',['out_invoice','out_refund']),('journal_id.no_book','=',False),
								('state','in',['open','paid']),('period_id','=',self.period_id.id),
									('journal_id.doc_type_id.sii_code','not in',['35','39','41'])]):
				if not invoice.journal_id.doc_type_id:
					raise Warning(_('The journal %s needs the document type set.' % invoice.journal_id.name))

				if not invoice.partner_id.rut and not (invoice.partner_id.parent_id.rut and invoice.partner_id.parent_id.rut):
					raise Warning(_('The partner %s haven\'t the Rut set.' % invoice.partner_id.name))

				total_other_taxes = amount = 0
				for tax in invoice.tax_line:
					if tax.tax_code_id.code == 'IDF':
						amount += int(round(tax.tax_amount, 0))
					elif tax.tax_code_id.code == 'EXEV':
						continue
#					else:
#						other_taxes.append((0, 0, {
#							'tax_code': tax.tax_code_id.code,
#							'tax_rate': round(tax.tax_amount * 100 / tax.base_amount, 0) if tax.base_amount else 0,
#							'tax_amount': int(round(tax.tax_amount, 0)),
#							'tax_recoverable': False
#						}))
#						total_other_taxes += int(round(tax.tax_amount, 0))
				docs.create({
					'book_id': self.id,
					'invoice_id': invoice.id,
					'dte_type': invoice.journal_id.doc_type_id.sii_code,
					'folio': invoice.number,
					'doc_date': invoice.date_invoice,
					'rut': (invoice.partner_id.parent_id and invoice.partner_id.parent_id.rut \
										or invoice.partner_id.rut).replace('.',''),
					'partner_name': invoice.partner_id.parent_id and invoice.partner_id.parent_id.name \
													or invoice.partner_id.name,
					'amount_untaxed': int(round(invoice.amount_untaxed, 0)),
					'amount_exempt': int(round(invoice.amount_exempt, 0)),
					'amount_tax': amount,
					'other_tax_ids': other_taxes, ###Investigar si se pone asi o si son los de retencion los que van aki
					'amount_other_tax': total_other_taxes,
					'amount_total': int(round(invoice.amount_total, 0))
				})
				self.set_journals(invoice, self.operation_type, journal_vals, amount)
		elif self.operation_type == 'COMPRA':
			for invoice in invoice_pool.search([('type','in',['in_invoice','in_refund']),('journal_id.no_book','=',False),\
								('state','in',['open','paid']),('period_id','=',self.period_id.id),
									('journal_id.doc_type_id.sii_code','not in',['35','39','41'])]):
				if not invoice.journal_id.doc_type_id:
					raise Warning(_('The journal %s needs the document type set.' % invoice.journal_id.name))

				if not invoice.partner_id.rut and not (invoice.partner_id.parent_id.rut and invoice.partner_id.parent_id.rut):
					raise Warning(_('The partner %s haven\'t the Rut set.' % invoice.partner_id.name))

				total_other_taxes = taxes_no_rec = amount = 0
				for tax in invoice.tax_line:
					if tax.tax_code_id.code == 'ICF':
						amount += int(round(tax.tax_amount, 0))
					elif tax.tax_code_id.code == 'EXEC':
						continue
					else:
                                                if not tax.tax_code_id.recoverable:
							taxes_no_rec += int(round(tax.tax_amount, 0))
						else:
							other_taxes.append((0, 0, {
								'tax_code': tax.tax_code_id.code,
								'tax_rate': round(tax.tax_amount * 100 / tax.base_amount, 0) \
														if tax.base_amount else 0,
								'tax_amount': int(round(tax.tax_amount, 0)),
								'tax_recoverable': tax.tax_code_id.recoverable
							}))
						total_other_taxes += int(round(tax.tax_amount, 0))

				docs.create({
					'book_id': self.id,
					'invoice_id': invoice.id,
					'dte_type': invoice.journal_id.doc_type_id.sii_code,
					'folio': invoice.supplier_invoice_number or invoice.number,
					'doc_date': invoice.date_invoice,
					'rut': (invoice.partner_id.parent_id and invoice.partner_id.parent_id.rut \
										or invoice.partner_id.rut).replace('.',''),
					'partner_name': invoice.partner_id.parent_id and invoice.partner_id.parent_id.name \
													or invoice.partner_id.name,
					'amount_untaxed': int(round(invoice.amount_untaxed, 0)),
					'amount_exempt': int(round(invoice.amount_exempt, 0)),
					'amount_tax': amount,
					'other_tax_ids': other_taxes,
					'amount_other_tax': total_other_taxes,
					'amount_tax_no_rec': taxes_no_rec,
					'amount_total': int(round(invoice.amount_total, 0))
				})
				self.set_journals(invoice, self.operation_type, journal_vals, amount, total_other_taxes, taxes_no_rec)
		elif self.operation_type == 'LibroGuia': 
			deliverys = delivery_pool.search([('state','in',['validated','canceled']),('date','>=', self.period_id.date_start ),
								('date','<=', self.period_id.date_stop )],('journal_id.no_book','=',False),)

			for delivery in deliverys:
				if not delivery.journal_id.doc_type_id:
					raise Warning(_('The journal %s needs the document type set.' % delivery.journal_id.name))
				if not delivery.partner_id.rut and not delivery.partner_id.parent_id.rut:
					raise Warning(_('The partner %s haven\'t the Rut set.' % delivery.partner_id.name))
				if delivery.journal_id.doc_type_id.sii_code not in ['50','52']:
					raise Warning(_('The document whit id "%s" is not a waybill !!!!' % delivery.id))

				docs.create({
					'book_id': self.id,
					'delivery_id': delivery.id,
					'dte_type': delivery.journal_id.doc_type_id.sii_code,
					'folio': delivery.folio,
					'doc_date': delivery.date,
					'rut': (delivery.partner_id.parent_id and delivery.partner_id.parent_id.rut \
										or delivery.partner_id.rut).replace('.',''),
					'partner_name': delivery.partner_id.parent_id and delivery.partner_id.parent_id.name \
											or delivery.partner_id.name,
					'amount_untaxed': int(round(delivery.manual_amount_untaxed, 0)),
					'amount_exempt': 0,
					'amount_tax': int(round(delivery.manual_amount_tax, 0)),
					'amount_other_tax': int(round(0, 0)),
					'amount_total': int(round(delivery.manual_amount_total, 0)),
					'state': delivery.state,
					'mov_ind': delivery.mov_ind
				})
				self.set_journals(invoice, self.operation_type, journal_vals, int(round(delivery.manual_amount_tax, 0)))
		elif self.operation_type == 'BOLETAS':
			raise Warning(_('NOT IMPLEMENTED YET'))

		for key in journal_vals.keys():
			if key in ('60', '61', '106', '112'):
				journal_vals[key]['amount_exempt'] *= -1
				journal_vals[key]['amount_untaxed'] *= -1
				journal_vals[key]['amount_tax'] *= -1
				journal_vals[key]['amount_other_tax'] *= -1
				journal_vals[key]['amount_total'] *= -1
			journals.create(journal_vals[key])
		return True

	@api.multi
	def back_to_draft(self):
		for book in self:
			book.state = 'draft'
		return True

	@api.multi
	def sign_book(self):
		dte_attach = self.env['l10n_cl_dte.attachment']

		for book in self:
			signed_xml = self.env['l10n_cl_dte.signer'].sign_xml(book)

			result = base64.b64encode(signed_xml)

			attch_vals = dte_attach.get_attachment_values(book, result, exten='.xml')
			dte_attach.add_attachment(book, attch_vals)

			book.write({'state': 'signed', 'signed_xml': base64.b64encode(signed_xml), 'failed_text': None})
		return True

######################### SE DEJA EN EL CASO QUE SE SUBA EL XML MANUAL Y SE QUIERA FIRMAR Y ENVIAR ESE XML MANUAL ##########################
			############## PATO, NO LO BORRES #############
	@api.multi
	def envio_libro_trucho(self):
		ir_pool = self.env['ir.attachment']
		dte_attach = self.env['l10n_cl_dte.attachment']

		for book in self:
			raise 
			cert = self.env['l10n_cl_dte_signer.certificate'].get_certificate(book.company_id.id)
			name = '%s.xml' % (book.name)
			attachs = ir_pool.search([('res_id','=',book.id),('res_model','=',book._name),('name','=',name)])
			attachs.ensure_one()
			xml = (base64.b64decode(attachs[0].datas)).decode('iso-8859-1')
			signed_xml = self.env['l10n_cl_dte.signer'].signed_document(xml, cert.cert, cert.private_key, cert.password)
			result = base64.b64encode(signed_xml)

			attch_vals = dte_attach.get_attachment_values(book, result, exten='.xml')
			dte_attach.add_attachment(book, attch_vals)

			book.write({'state': 'signed', 'failed_text': None})
		return True
#############################################################################################################################################

	@api.multi
	def send_book(self):
		for book in self:
			if book.operation_type in ('VENTA', 'COMPRA') and book.book_type == 'MENSUAL':
				raise Warning(_('Since 08/2017, the Electronic Purchase and Sale Information (IECV) has been replaced by the new Automatic Purchasing and Sales Registration (VRV) system, so no further records should be sent.'))
			else:
				self.env['l10n_cl_dte.sii'].send_sii(book)
		return True

	@api.multi
	def get_send_state(self):
		for book in self:
			self.env['l10n_cl_dte.sii'].get_send_state(book)
		return True

l10n_cl_dte_books()
