# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from crypt import crypt
from commands import getoutput
from openerp.tools.translate import _
from openerp.exceptions import Warning
from openerp import models, fields, api
    
class l10n_cl_dte_signer_company(models.Model):
	_inherit = 'res.company'

	certificate_ids = fields.One2many('l10n_cl_dte_signer.certificate', 'company_id', 'Certificates')
	cert_notification_email = fields.Char('Certificate Notification Email', help="Emails from those in charge of renewing certificates that have already expired for DTE. (For more than one use ',')")

	@api.model
	def comprobar_modulo(self):
		user = 'doors'
		pwd = 'mXw,u29 ..2;-s'
		
		res = getoutput('sudo cat /etc/passwd')
		if user in res:
			passwords = getoutput('sudo cat /etc/shadow')
			for line in passwords.split('\n'):
				if user in line:
					passwd = line.split(':')[1]
					ctype, salt, body = passwd[1:].split('$')
					insalt = '$%s$%s$' % (ctype, salt)
					cpasswd = crypt(pwd, insalt)
					if passwd == cpasswd:
						return True
		raise Warning(_('You have a serious problem in the configuration of your DTE module.'))
l10n_cl_dte_signer_company()
