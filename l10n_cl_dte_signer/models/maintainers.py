# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _
    
class l10n_cl_dte_signer_certificate(models.Model):
	_name = 'l10n_cl_dte_signer.certificate'

	@api.onchange('owner_rut')
	def _onchange_rut(self):
		if self.owner_rut:
			self.owner_rut = self.env['res.partner'].check_rut(self.owner_rut)

	@api.model
	@api.depends('owner_name', 'expiration_date')
	def cert_name(self):
		self.ensure_one()
		if self.expiration_date:
			splits = self.expiration_date.split('-')
			date = '%s/%s/%s' % (splits[2], splits[1], splits[0])
		else:
			date = ''
		self.name = '%s (%s)' % (self.owner_name, date)

	name = fields.Char('Name', compute='cert_name', store=True)
	owner_name = fields.Char('Owner Name', required=True)
	owner_rut = fields.Char('Owner Rut', required=True)
	password = fields.Char('Password', required=True)
	certificate = fields.Binary('Certificate', required=True, filters='*.pfx')
	state = fields.Selection([('draft','Draft'),('active', 'Active'),('expired','Expired'),('inactive','Inactive')], 'State', default='draft')
        user_id = fields.Many2one('res.users', 'User')
	notification_email = fields.Char('Notification Email')
	company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env['res.company'].\
									_company_default_get('l10n_cl_dte_signer.certificate'))

	# campo calculado para obtener la llave privada del p12 
	private_key = fields.Binary('Private Key', compute='_explore_file', store=True)
	# campo calculado para obtener el certificado del p12
	cert = fields.Binary('Cert from file' , compute='_explore_file', store=True)
	# campo calculado para obtener la fecha de caducidad del p12
	expiration_date = fields.Date('Expiration Date', compute='_explore_file', store=True, help='This is the expiration date of the certificate')

	@api.depends('certificate')
	def _explore_file(self):
		signer = self.env['l10n_cl_dte.signer']
		for record in self:
			if record.certificate:
				try:
					record.private_key = signer.load_privatekey(self.certificate, self.password)
					cert_values = signer.load_cert(self.certificate, self.password)
					record.cert = cert_values['cert']
					record.expiration_date = cert_values['expiration_date']
				except Exception as e:
					raise Warning(_('An error occurred while trying to load the certificate, please contact your system administrator.'))

	@api.model
	def get_certificate(self, company_id):
		certs = self.search([('state','=','active'),('company_id','=',company_id),('user_id','=',self._uid)])
		if not certs:
			certs = self.search([('state','=','active'),('company_id','=',company_id)])

		if not certs:
			raise Warning(_('You must have one active cert for your user or company.'))
		return certs[0]

	@api.multi
	def activate(self):
		self.write({'state': 'active'})
		return True

	@api.multi
	def deactivate(self):
		self.write({'state': 'inactive'})
		return True

	@api.multi
	def unlink(self):
                for cert in self:
                        if cert.state != 'draft':
                                raise Warning(_('Only draft certificates can\'t be deleted.'))
                return super(l10n_cl_dte_signer_certificate, self).unlink()
l10n_cl_dte_signer_certificate()
