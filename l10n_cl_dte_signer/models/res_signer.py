# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.exceptions import Warning
from OpenSSL import crypto 
from lxml import etree 
from datetime import datetime

import rsa, base64, os, tempfile, time

class l10n_cl_dte_signer(models.Model):
	_name = 'l10n_cl_dte.signer'

	def load_privatekey(self, p12, password=None):
		""" 
			Extrae desde un archivo p12 la llave privada
		"""
		pkcs12 = crypto.load_pkcs12(base64.b64decode(p12), password)
		return base64.b64encode(crypto.dump_privatekey(crypto.FILETYPE_PEM , pkcs12.get_privatekey()))

	def load_cert(self, p12, password=None):
		""" 
			Extrae desde un archivo p12 el certificado digital 
		"""
		pkcs12 = crypto.load_pkcs12(base64.b64decode(p12), password)
		certificate = crypto.dump_certificate(crypto.FILETYPE_PEM , pkcs12.get_certificate())
		expiration = crypto.load_certificate(crypto.FILETYPE_PEM, certificate)
		expiration = datetime.strptime(expiration.get_notAfter().decode('ascii'),"%Y%m%d%H%M%SZ") 
		return {'cert' : base64.b64encode(certificate), 'expiration_date': expiration}

	def _make_temp_file(self, data_to_file):
		file = tempfile.NamedTemporaryFile()
		file.write(data_to_file)
		file.seek(0)
		return file

	def signed_document(self, xml, cert, key, password):
		"""
			Firma un Documento electronico y los envios que se realicen 
		"""

		sign_attribute = ''
		xpath = ' '
		number_of_documents=0
		# first_line = xml.splitlines()[0]

		if xml.find('DocumentoAEC') != -1:
			sign_attribute = ' --id-attr:ID DocumentoAEC '
			sign_count = xml.count('<Signature xmlns')
			xpath = """ --node-xpath "(//*[local-name()='Signature'])[%s]" """ % sign_count
		elif xml.find('SetDTE') != -1:
			sign_attribute = ' --id-attr:ID SetDTE '
			sign_count = xml.count('<Signature xmlns')
			xpath = """ --node-xpath "(//*[local-name()='Signature'])[%s]" """ % sign_count
		elif xml.find('SetRecibos') != -1:
			sign_attribute = ' --id-attr:ID SetRecibos '
			sign_count = xml.count('<Signature xmlns')
			xpath = """ --node-xpath "(//*[local-name()='Signature'])[%s]" """ % sign_count
		elif xml.find('DocumentoDTECedido') != -1:
			sign_attribute = ' --id-attr:ID DocumentoDTECedido '
			sign_count = xml.count('<Signature xmlns')
			xpath = """ --node-xpath "(//*[local-name()='Signature'])[%s]" """ % sign_count
		elif xml.find('DocumentoConsumoFolios') != -1:
			sign_attribute = ' --id-attr:ID DocumentoConsumoFolios '
		elif xml.find('DocumentoRecibo') != -1:
			sign_attribute = ' --id-attr:ID DocumentoRecibo '
		elif xml.find('DocumentoCesion') != -1:
			sign_attribute = ' --id-attr:ID DocumentoCesion '
		elif xml.find('EnvioLibro') != -1:
			sign_attribute = ' --id-attr:ID EnvioLibro '
		elif xml.find('Documento') != -1:
			sign_attribute = ' --id-attr:ID Documento '
		elif xml.find('Exportaciones') != -1:
			sign_attribute = ' --id-attr:ID Exportaciones '
		elif xml.find('Resultado') != -1:
			sign_attribute = ' --id-attr:ID Resultado '
		else:
			sign_attribute = ' '

		cert = base64.b64decode(cert)
		key = base64.b64decode(key)
		key_file = self._make_temp_file(key)
		cert_file = self._make_temp_file(cert)
		to_signed = self._make_temp_file(xml.encode('iso-8859-1'))
		signed_path = '/tmp/%s.xml' % time.strftime('%s')
		sign_command = """xmlsec1 --sign --privkey-pem %s,%s --pwd '%s' %s %s %s > %s""" 

		sign_command = sign_command % (key_file.name , cert_file.name , password, sign_attribute , xpath ,to_signed.name, signed_path)
		os.system(sign_command)

		signed_file = open(signed_path , "r") 

		# limpio todos los archivos temporados que se han utilizado
		key_file.close()
		cert_file.close()
		to_signed.close()
		signed_xml = signed_file.read()
		signed_file.close()
		os.system('rm -rf %s' % signed_path)

		return signed_xml

	def signed_ted(self, ted_xml, private_key):
		"""
			Firma un ted pre-armado.
		"""
		root = etree.fromstring(ted_xml)
		date_signed_tag = root.find('DD')
		privkey = rsa.PrivateKey.load_pkcs1(private_key)

		# for element in date_signed_tag.iter():
		# 	if (element.tag  is not 'CAF') and (element is not None ) and (element.text is not None):
		# 		element.text = self.clean_tag(element.text)
		to_sign = etree.tostring(date_signed_tag, encoding='ISO-8859-1').replace("""<?xml version='1.0' encoding='ISO-8859-1'?>\n""",'')
		to_sign = to_sign.replace('>\n','>')
		signature = rsa.sign(to_sign, privkey, 'SHA-1')
		signature = base64.encodestring(signature)
		if signature[-1:] == '\n':
			signature = signature[:-1] 
		sing_tag = etree.SubElement(root, 'FRMT', attrib={'algoritmo': "SHA1withRSA"})
		sing_tag.text = signature
		return etree.tostring(root, encoding='ISO-8859-1').replace("""<?xml version='1.0' encoding='ISO-8859-1'?>\n""",'')

	@api.model
	def sign_xml(self, obj):
		self.env['l10n_cl_dte.validator'].validate_logic(obj)
		cert = self.env['l10n_cl_dte_signer.certificate'].get_certificate(obj.company_id.id)

		if obj._name == 'account.invoice':
			if not obj.number:
				caf = self.env['l10n_cl_dte.caf'].get_caf(obj.journal_id.id)

				folio = caf.folio_consume()
				obj.number = folio[0]
				if obj.journal_id.doc_type_id.sii_code == '46':
					obj.supplier_invoice_number = folio[0]
				obj.caf = caf

			if not obj.internal_number:
				obj.internal_number = self.env['ir.sequence'].get_id(obj.journal_id.invoice_sequence_id.id)

			obj.ted = self.env['l10n_cl_dte.signer'].signed_ted(self.env['dte.templates'].get_ted(obj), obj.caf.private_key)
			xml = self.env['dte.templates'].get_document(obj)
		elif obj._name == 'l10n_cl_dte.stock':
			if not obj.folio:
				caf = self.env['l10n_cl_dte.caf'].get_caf(obj.journal_id.id)

				folio = caf.folio_consume()
				obj.folio = folio[0]
				obj.caf = caf

			if not obj.internal_number:
				if obj.picking_id:
					obj.internal_number = obj.picking_id.name
				else:
					obj.internal_number = self.env['ir.sequence'].get_id(obj.journal_id.sequence_id.id)

			obj.ted = self.env['l10n_cl_dte.signer'].signed_ted(self.env['dte.templates'].get_ted(obj), obj.caf.private_key)
			xml = self.env['dte.templates'].get_document(obj)
		elif obj._name == 'l10n_cl_dte.books':
			xml = self.env['dte.templates'].get_book(obj)
		elif obj._name == 'l10n_cl_dte_invoice_receipt.daily_book':
			xml = self.env['dte.templates'].get_daily_book(obj)
		elif obj._name == 'l10n_cl_dte.assignment':
			count = 1
			signed_dte_assignment_xml = ''
			signed_assignment_xml = ''

			for line in obj.document_ids:
				dte_assignment_xml = self.env['dte.templates'].get_dte_assignment(line)
				dte_assignment_xml = """<?xml version="1.0" encoding="ISO-8859-1"?>\n<DTECedido>\n%s</DTECedido>""" % dte_assignment_xml
				signed_dte_assignment_xml += self.signed_document(dte_assignment_xml, cert.cert, cert.private_key, cert.password)

				assignment_xml = self.env['dte.templates'].get_assignment(obj, line, count=count)
				assignment_xml = """<?xml version="1.0" encoding="ISO-8859-1"?>\n<Cesion>\n%s</Cesion>""" % assignment_xml
				signed_assignment_xml += self.signed_document(assignment_xml, cert.cert, cert.private_key, cert.password)
				count += 1
			signed_dte_assignment_xml = signed_dte_assignment_xml.replace("""<?xml version="1.0" encoding="ISO-8859-1"?>\n<DTECedido>\n""",'').replace("""</DTECedido>\n""", '')
			signed_assignment_xml = signed_assignment_xml.replace("""<?xml version="1.0" encoding="ISO-8859-1"?>\n<Cesion>\n""",'').replace("""</Cesion>\n""", '')
			xml = self.env['dte.templates'].get_assignment_aec(obj, signed_dte_assignment_xml, signed_assignment_xml)
		try:
			signed_xml = self.signed_document(xml, cert.cert, cert.private_key, cert.password)
		except Exception, e:
			raise Warning(str(e))

		return signed_xml
l10n_cl_dte_signer()

