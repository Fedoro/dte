# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import Warning

class l10n_cl_base_partner(models.Model):
	_inherit = 'res.partner'

	@api.model
	@api.constrains('rut')
	def _check_unique_rut(self):
		if self.rut and not self.foreign_rut and self.search([('id','not in',self._ids),('rut','=',self.rut)]):
			raise Warning(_('The partner Rut is already set in the system, please check it.'))

	@api.model
	def check_rut(self, rut):
		format = '%s.%s.%s-%s'
		#format_rut= ''

		if len(rut) < 9:
			raise Warning(_('The Rut isn\'t valid, please check it.'))
		if not '-' in rut:
			raise Warning(_('The Rut format is xxxxxxxx-x, please check it.'))

		base, vdig = rut.replace(' ','').replace('.','').lstrip('0').split('-')
		if vdig == 'k':
			vdig = 'K'

		operar = str( 11 - sum([int(digit) * factor for digit, factor in zip(base[::-1], 2 * range(2,8))]) % 11 )
		if operar == '11':
			operar = '0'
		elif operar == '10':
			operar = 'K'
		if operar != vdig:
			raise Warning(_('The Rut base an verification digit doesn\'t match, please check it.'))

		return format % (base[:-6], base[-6:-3], base[-3:], vdig)

	line_of_business = fields.Char('Line of Business')
	fantasy_name = fields.Char('Fantasy Name')
	economic_activities_ids = fields.Many2many('res.economic_activity', 'partner_economic_activities_rel', 'partner_id',
											'activity_id', string='Economic Activities')
	rut = fields.Char('Rut')
	foreign_rut = fields.Boolean('Foreign Rut?', default=False)

	@api.model
	def create(self, vals):
		if vals.get('rut') and not vals.get('foreign_rut'):
			vals.update({'rut': self.check_rut(vals['rut'])})
		return super(l10n_cl_base_partner, self).create(vals)

	@api.multi
	def write(self, vals):
		for partner in self:
			foreign_rut = vals['foreign_rut'] if 'foreign_rut' in vals else partner.foreign_rut
			rut = vals['rut'] if 'rut' in vals else None #partner.rut

			if rut and not foreign_rut:
				vals.update({'rut': partner.check_rut(rut)})
		return super(l10n_cl_base_partner, self).write(vals)

	@api.model
	def name_search(self, name, args=None, operator='ilike', limit=80):
		res = super(l10n_cl_base_partner, self).name_search(name, args=args, operator=operator, limit=limit)

		if name:
			ids = self.search([('rut', operator, name),('id','not in',[x[0] for x in res])] + args, limit=limit)
			res += ids.name_get()
		return res

	@api.multi
	def name_get(self):
		aux = []
		resp = super(l10n_cl_base_partner, self).name_get()

		for record in resp:
			partner = self.browse(record[0])
			if partner.rut:
				name = "[%s] %s" % (partner.rut, record[1])
				aux.append((record[0], name))
			else:
				aux.append(record)
		return aux if aux else resp
l10n_cl_base_partner()

