
import xmlrpclib, psycopg2

url = 'http://olimex.cl'
base = 'PRODUCCION'
common = xmlrpclib.ServerProxy ('%s/xmlrpc/common' % (url))
models = xmlrpclib.ServerProxy('%s/xmlrpc/2/object' % (url))
uid = common.authenticate(base, 'admin', '2K1151#3N5jH', {})
print "Adquido el UID, %s" % (uid)

#Se usa script propio: mesesEntreFechas.py
fechas = ['2017-05-10', '2017-05-11', '2017-05-12', '2017-05-13', '2017-05-14', '2017-05-15', '2017-05-16', '2017-05-17']

for date in fechas:
	book_id = models.execute_kw(base, uid, '2K1151#3N5jH', 'l10n_cl_dte_invoice_receipt.daily_book', 'create', [{'date': date}])
	print 'fecha: %s, id libro: %s' % (date, book_id)
	models.execute_kw(base, uid, '2K1151#3N5jH', 'l10n_cl_dte_invoice_receipt.daily_book', 'get_documents', [book_id])
#	models.execute_kw(base, uid, '2K1151#3N5jH', 'l10n_cl_dte_invoice_receipt.daily_book', 'sign_book', [book_id])
#	models.execute_kw(base, uid, '2K1151#3N5jH', 'l10n_cl_dte_invoice_receipt.daily_book', 'write', [[book_id], {'state': 'validate'}])
	
