
import psycopg2

try:
	db1 = psycopg2.connect(user='Fedoro', dbname='migracion_mci', password='', host='localhost')
	db2 = psycopg2.connect(user='Fedoro', dbname='MCI_PRODUCCION', password='', host='localhost')
except:
	print 'No se pudo conectar a la base de datos'
else:
	nueva = db1.cursor()
	antigua = db2.cursor()
	print 'Conexion establecida'

antigua.execute("""SELECT active, journal_id, begin_folio, end_folio, next_value, file_code, public_key, private_key, xml, caf, file \
                        FROM l10n_cl_dte_folio_history ORDER BY file_code;""")
aux = None
for active, journal_id, begin_folio, end_folio, next_value, file_code, public_key, private_key, xml, caf, caf_file in antigua.fetchall():
	if file_code != aux:
		antigua.execute("SELECT name FROM l10n_cl_dte_document_type WHERE sii_code = '%s'" % (file_code))
		doc_name = antigua.fetchone()
		if doc_name:
			doc_name = doc_name[0]

	if active:
		empty = False
		state = 'active'
	else:
		empty = True
		state = 'finished'

	name = '%s - %s folios desde %s hasta %s' % (file_code, doc_name, begin_folio, end_folio)

	if begin_folio and end_folio and file_code:
		sql = nueva.mogrify("INSERT INTO l10n_cl_dte_caf (state, journal_id, start_number, end_number, next_number, " +\
				"empty, code, public_key, private_key, xml, caf, caf_file, name) VALUES (%s, %s, %s, %s, %s, %s, %s, " +\
				"%s, %s, %s, %s, %s, %s)", (state, journal_id, begin_folio, end_folio, next_value, empty, file_code, \
										public_key, private_key, xml, caf, caf_file, name))
		nueva.execute(sql)
		db1.commit()

db1.close()
db2.close()

