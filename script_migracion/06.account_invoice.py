
import psycopg2

try:
	db1 = psycopg2.connect(user='Fedoro', dbname='migracion_mci', password='', host='localhost')
	db2 = psycopg2.connect(user='Fedoro', dbname='MCI_PRODUCCION', password='', host='localhost')
except:
	print 'No se pudo conectar a la base de datos'
else:
	nueva = db1.cursor()
	antigua = db2.cursor()
	print 'Conexion establecida'

antigua.execute("SELECT id, journal_id, state, number, internal_number, dte_status, sii_track_id, failed_text, sii_failed_text, origin, " +\
				"ted_xml, pdf_img, dte, amount_untaxed, amount_exent, amount_tax, amount_total FROM account_invoice;")
for inv_id, journal_id, old_state, number, int_number, dte_status, sii_track_id, failed_text, sii_failed_text, origin, \
			ted_xml, pdf_img, dte, amount_untaxed, amount_exent, amount_tax, amount_total in antigua.fetchall():
	if old_state == 'dte_waiting':
		state = 'waiting_sii'
	elif old_state == 'dte_failed':
		state = 'failed_sii'
	else:
		state = old_state

	#CAF
	caf = None
	sql_caf = nueva.mogrify("SELECT id FROM l10n_cl_dte_caf WHERE journal_id = %s AND " +\
					"start_number <= %s AND end_number >= %s;", (journal_id, number, number))
	nueva.execute(sql_caf)
	for caf_id in nueva.fetchall():
		if dte:
			caf = caf_id
		else:
			print "Ta cuestion ta rara en el caf, revisalo.\n\tcaf_id: %s\n\tjournal_id: %s" % (caf_id, journal_id) +\
										"\n\tdte: %s\n\tinv_id: %s" % (dte, inv_id)
			dte = True
#			exit()

	if number < 0:
		number = None

	sql1 = nueva.mogrify("UPDATE account_invoice SET state=%s, number=%s, internal_number=%s, dte_status=%s, " +\
			"track_id=%s, failed_text=%s, status_text=%s, dte=%s, amount_untaxed=%s, amount_exempt=%s, " +\
			"amount_tax=%s, amount_total=%s, ted=%s, caf=%s WHERE id=%s;", (state, number, int_number, dte_status, sii_track_id, \
				failed_text, sii_failed_text, dte, amount_untaxed, amount_exent, amount_tax, amount_total, ted_xml, caf, inv_id))
	nueva.execute(sql1)
	db1.commit()

	#Sale Journal
	antigua.execute("SELECT id FROM sale_order WHERE name = '%s'" % (origin))
	sale_id = antigua.fetchone()
	if sale_id:
		sale_id = sale_id[0]
		sql_sale_inv = nueva.mogrify("UPDATE sale_order SET journal_id = %s WHERE id = %s", (journal_id, sale_id))
		nueva.execute(sql_sale_inv)
	else:
		nueva.execute("SELECT order_id FROM sale_order_invoice_rel WHERE invoice_id = %s" % (inv_id))
		sale_id = nueva.fetchone()
		if sale_id:
			sale_id = sale_id[0]
			sql_sale_inv = nueva.mogrify("UPDATE sale_order SET journal_id = %s WHERE id = %s", (journal_id, sale_id))
			nueva.execute(sql_sale_inv)
		else:
			sale_id = s_state = None
			print "Documento sin sale order, id: %s, number: %s" % (inv_id, number)
	db1.commit()

antigua.execute("SELECT id, date_invoice FROM account_invoice WHERE date_invoice IS NOT NULL;")
for inv_id, date_invoice in antigua.fetchall():
	sql2 = nueva.mogrify("UPDATE account_invoice SET date_invoice=%s WHERE id=%s", (date_invoice, inv_id))
	nueva.execute(sql2)
	db1.commit()

antigua.execute("SELECT id, ind_exencion FROM account_invoice_line;")
for l_id, ind_exe in antigua.fetchall():
	sql3 = nueva.mogrify("UPDATE account_invoice_line SET exension_index=%s WHERE id=%s", (ind_exe, l_id))
        nueva.execute(sql3)
	db1.commit()

#Workflow
nueva.execute("SELECT wfa.id, wfa.name FROM wkf_activity AS wfa INNER JOIN wkf AS wff ON (wfa.wkf_id=wff.id) " +\
			"WHERE wff.osv = 'account.invoice' AND wfa.name = 'Waiting SII' OR wfa.name = 'Failed SII';")
for wfa_id, wfa_name in nueva.fetchall():
	if wfa_name == 'Waiting SII':
		waiting = wfa_id
	elif wfa_name == 'Failed SII':
		failed = wfa_id
	else:
		print "Salio un estado raro, revisalo:\n\tid: %s\n\t.nombre: %s." % (wfa_id, wfa_name)
		exit()

if not waiting or not failed:
	print "Error!, uno de los estado no fue encontrado:\n\twaiting: %s.\n\tfailed: %s." % (waiting, failed)
	exit()

antigua.execute("SELECT wfw.id, wfa.name FROM account_invoice AS ai INNER JOIN wkf_instance AS wfi ON (ai.id=wfi.res_id) " +\
			"INNER JOIN wkf_workitem AS wfw ON (wfw.inst_id=wfi.id) INNER JOIN wkf_activity AS wfa ON (wfw.act_id=wfa.id) " +\
			"WHERE res_type = 'account.invoice' AND wfa.name NOT IN ('paid','open','cancel','draft','re-open');")
for wfw_id, wfa_name in antigua.fetchall():
	if wfa_name == 'DTE Failed':
		act_id = failed
	elif wfa_name == 'DTE Waiting':
		act_id = waiting
	else:
		print "Revisa el estado, falta algo.\n\tNombre Estado:%s.\n\tId Workitem: wfw_id." % (wfa_name, wfw_id)
		exit()
	
	sql4 = nueva.mogrify("UPDATE wkf_workitem SET act_id=%s WHERE id=%s", (act_id, wfw_id))
	nueva.execute(sql4)
	db1.commit()

db1.close()
db2.close()

