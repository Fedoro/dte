
import psycopg2

try:
	db1 = psycopg2.connect(user='Fedoro', dbname='migracion_mci', password='', host='localhost')
	db2 = psycopg2.connect(user='Fedoro', dbname='MCI_PRODUCCION', password='', host='localhost')
except:
	print 'No se pudo conectar a la base de datos'
else:
	nueva = db1.cursor()
	antigua = db2.cursor()
	print 'Conexion establecida'

antigua.execute("SELECT r.id, r.invoice_id, dt.sii_code, r.ref_folio, r.ref_date, r.ref_reason " +\
			"FROM l10n_cl_dte_reference AS r INNER JOIN l10n_cl_dte_document_type AS dt ON (r.tpo_doc_ref=dt.id);")
for r_id, invoice_id, code, ref_folio, ref_date, ref_reason in antigua.fetchall():
	if invoice_id:
		nueva.execute("SELECT id FROM l10n_cl_dte_document_type WHERE sii_code='%s'" % (code))
		type_id = nueva.fetchone()
		if type_id:
			type_id = type_id[0]
		else:
			print "La referencia con codigo: %s no se encuentra entre los ingresados, id linea: %s" % (code, r_id)
			continue

		sql = nueva.mogrify("INSERT INTO l10n_cl_dte_document_references (invoice_id, reference_type, reference_folio, " +\
			"reference_date, reference_reason, reference_invoice_type) VALUES (%s, %s, %s, %s, %s, %s)", \
								(invoice_id, type_id, ref_folio, ref_date, ref_reason, code))
		nueva.execute(sql)
		db1.commit()

db1.close()
db2.close()

