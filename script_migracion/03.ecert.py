
import psycopg2, base64
from OpenSSL import crypto

try:
	db1 = psycopg2.connect(user='Fedoro', dbname='migracion_mci', password='', host='localhost')
	db2 = psycopg2.connect(user='Fedoro', dbname='MCI_PRODUCCION', password='', host='localhost')
except:
	print 'No se pudo conectar a la base de datos'
else:
	nueva = db1.cursor()
	antigua = db2.cursor()
	print 'Conexion establecida'

antigua.execute("""SELECT name, owner_rut, password, ecert, company_id FROM l10n_cl_dte_ecert;""")
for name, rut, passwd, ecert, company_id in antigua.fetchall():
	pkcs12 = crypto.load_pkcs12(base64.b64decode(ecert), passwd)
	priv_key = base64.b64encode(crypto.dump_privatekey(crypto.FILETYPE_PEM , pkcs12.get_privatekey()))
	cert = base64.b64encode(crypto.dump_certificate(crypto.FILETYPE_PEM , pkcs12.get_certificate()))

	sql = nueva.mogrify("INSERT INTO l10n_cl_dte_signer_certificate (owner_name, owner_rut, password, certificate, company_id, " +\
					"private_key, cert, expiration_date, state) VALUES (%s, %s, %s, %s, %s, %s, %s, '2018-01-01', 'active')", 
											(name, rut, passwd, ecert, company_id, priv_key, cert))
	nueva.execute(sql)
	db1.commit()

db1.close()
db2.close()

