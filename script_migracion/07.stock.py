
import psycopg2, xmlrpclib

common = xmlrpclib.ServerProxy ('http://localhost:9069/xmlrpc/common')
models = xmlrpclib.ServerProxy('http://localhost:9069/xmlrpc/2/object')
old_uid = common.authenticate('dte_old', 'admin', '2K1151#3N5jH', {})
print "Adquido el UID old, %s" % (old_uid)

try:
	db1 = psycopg2.connect(user='Fedoro', dbname='migracion_mci', password='', host='localhost')
	db2 = psycopg2.connect(user='Fedoro', dbname='MCI_PRODUCCION', password='', host='localhost')
except:
	print 'No se pudo conectar a la base de datos'
else:
	nueva = db1.cursor()
	antigua = db2.cursor()
	print 'Conexion establecida'

antigua.execute("SELECT id, fecha_guia, date_done, internal_number, name, sii_track_id, failed_text, sii_failed_text, partner_id, " +\
		"state, ted_xml, dte_status, dte_send, dte, journal_id, ind_traslado, company_id, write_uid FROM stock_picking WHERE dte = 't';")
for p_id, fecha_guia, date_done, int_number, name, track_id, failed_text, sii_failed_text, partner_id, state, ted_xml, dte_status, \
		dte_send, dte, journal_id, ind_traslado, company_id, user_id in antigua.fetchall():
	picking_fields = ['currency_id', 'amount_untaxed', 'amount_tax', 'amount_total']
	amounts = models.execute_kw('dte_old', old_uid, '2K1151#3N5jH', 'stock.picking', 'search_read', \
									[[('id','=',p_id)]], {'fields': picking_fields})
	if amounts:
		amounts = amounts[0]

		for key	in amounts.keys():
		        if amounts[key] == False:
				amounts[key] = 0

	if state == 'cancel':
		state = 'canceled'
	elif dte_send and dte_status == '1':
		state = 'validated'
	elif dte_send and dte_status == '0':
		state = 'error'
	elif not dte_send and failed_text:
		state = 'error'
	elif dte:
		state = 'draft'
	else:
		state = 'a'

	if not journal_id:
		#### Cambiar dependiendo del diario del cliente
		journal_id = 9

	#CAF
	caf = None
	try:
		int_number = int(int_number)
		sql_caf = nueva.mogrify("SELECT id FROM l10n_cl_dte_caf WHERE journal_id = %s AND " +\
						"start_number <= %s AND end_number >= %s;", (journal_id, int_number, int_number))
		nueva.execute(sql_caf)
		for caf_id in nueva.fetchall():
			caf = caf_id
	except:
		print 'Basura no DTE %s, id %s.' % (int_number, p_id)

	date = fecha_guia or date_done and date_done.strftime('%Y-%m-%d') or '2018-01-01'

	if not partner_id:
		print "Folio: %s, id: %s, sin partner" % (int_number, p_id)
		continue

	sql1 = nueva.mogrify("INSERT INTO l10n_cl_dte_stock (picking_id, folio, internal_number, state, ted, track_id, failed_text, " +\
			"status_text, partner_id, journal_id, mov_ind, company_id, amount_untaxed, amount_tax, " +\
			"amount_total, user_id, impresion_type, caf, manual_currency_id, manual_amount_untaxed, " +\
                        "manual_amount_tax, manual_amount_total, date) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, " +\
			"%s, %s, %s, %s, %s, 'N', %s, %s, %s, %s, %s, %s) RETURNING id", (p_id, int_number, name, state, ted_xml, \
										track_id, failed_text, sii_failed_text, partner_id, journal_id, \
										ind_traslado, company_id, amounts['amount_untaxed'], \
										amounts['amount_tax'], amounts['amount_total'], user_id, caf, \
						amounts['currency_id'][0] if amounts['currency_id'] else None, amounts['amount_untaxed'], \
										amounts['amount_tax'], amounts['amount_total'], date))
	nueva.execute(sql1)
	nueva_id = nueva.fetchone()[0]
	antigua.execute("SELECT id, manual_price_unit, manual_discount, name, product_id, product_uom, product_uom_qty, " +\
										"company_id FROM stock_move WHERE picking_id = %s;" % p_id)
	for m_id, price_unit, discount, move_name, prod_id, uom_id, qty, company_id in antigua.fetchall():
		if price_unit:
			subtotal = (price_unit - (1 - discount / 100.0)) * float(qty)
			sql2 = nueva.mogrify("INSERT INTO l10n_cl_dte_stock_lines (dispatch_order_id, name, product_id, uom_id, " +\
						"quantity, price_unit, discount, company_id, price_subtotal) VALUES (%s, %s, %s, %s, " +\
						"%s, %s, %s, %s, %s) RETURNING id", (nueva_id, move_name, prod_id, uom_id, qty, \
						price_unit, discount, company_id, subtotal))
			nueva.execute(sql2)
			line_id = nueva.fetchone()[0]
			antigua.execute("SELECT tax_id FROM stock_move_tax_rel WHERE move_id = %s" % (m_id))
			for tax_id in antigua.fetchall():
				tax_id = tax_id[0]
				nueva.execute("INSERT INTO dispatch_order_lines_tax_rel (line_id, tax_id) " +\
											"VALUES (%s, %s)" % (line_id, tax_id))
			nueva.execute("UPDATE l10n_cl_dte_stock SET picking_id=Null, " +\
					"amount_untaxed=0, amount_tax=0, amount_total=0 WHERE id=%s" % (nueva_id))
		else:
			nueva.execute("INSERT INTO dispatch_order_move_lines_rel (order_id, move_id) VALUES (%s, %s);" % (nueva_id, m_id))
			nueva.execute("UPDATE l10n_cl_dte_stock SET manual_amount_untaxed=0, manual_amount_tax=0, " +\
									"manual_amount_total=0 WHERE id=%s" % (nueva_id))

		nueva.execute("UPDATE stock_move SET in_dispatch='t' WHERE id=%s" % (m_id))
	nueva.execute("""UPDATE stock_picking SET is_fully_dispatched='t' WHERE id=%s""" % (p_id))
	db1.commit()

	antigua.execute("SELECT r.id, dt.sii_code, r.ref_folio, r.ref_date, r.ref_reason FROM l10n_cl_dte_reference AS r " +\
					"INNER JOIN l10n_cl_dte_document_type AS dt ON (r.tpo_doc_ref=dt.id) WHERE r.picking_id = %s;" % (p_id))
	for r_id, code, ref_folio, ref_date, ref_reason in antigua.fetchall():
		nueva.execute("SELECT id FROM l10n_cl_dte_document_type WHERE sii_code='%s'" % (code))
		type_id = nueva.fetchone()
		if type_id:
			type_id = type_id[0]
		else:
			print "La referencia con codigo: %s no se encuentra entre los ingresados, id linea: %s" % (code, type_id)
			continue

		sql_ref = nueva.mogrify("INSERT INTO l10n_cl_dte_document_references (stock_id, reference_type, reference_folio, " +\
						"reference_date, reference_reason, reference_invoice_type) VALUES (%s, %s, %s, %s, %s, %s)", \
										(nueva_id, type_id, ref_folio, ref_date, ref_reason, code))
		nueva.execute(sql_ref)
		db1.commit()

#antigua.execute("SELECT id, fecha_guia, date_done FROM stock_picking WHERE (fecha_guia IS NOT NULL OR date_done IS NOT NULL) AND dte = 't';")
#for p_id, date, date_done in antigua.fetchall():
#	if not date:
#		date = date_done.strftime('%Y-%m-%d')
#
#	sql = nueva.mogrify("UPDATE l10n_cl_dte_stock SET date=%s WHERE picking_id=%s", (date, p_id))
#	nueva.execute(sql)
#	db1.commit()

#Currency actualizado para MCI por error de opendrive xD...
#nueva.execute("UPDATE l10n_cl_dte_stock SET manual_currency_id = 46 WHERE picking_id IS NULL;")

db1.close()
db2.close()

