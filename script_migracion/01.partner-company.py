
import psycopg2

try:
	db1 = psycopg2.connect(user='Fedoro', dbname='migracion_mci', password='', host='localhost')
	db2 = psycopg2.connect(user='Fedoro', dbname='MCI_PRODUCCION', password='', host='localhost')
except:
	print 'No se pudo conectar a la base de datos'
else:
	nueva = db1.cursor()
	antigua = db2.cursor()
	print 'Conexion establecida'

antigua.execute("""SELECT id, giro, nombre_fantasia, rut, chilean_rut, email_dte FROM res_partner;""")
for p_id, giro, nombre_fantasia, rut, chilean_rut, email_dte in antigua.fetchall():
	sql1 = nueva.mogrify("UPDATE res_partner SET line_of_business=%s, fantasy_name=%s, rut=%s, " +\
								"foreign_rut=%s, distribution_email=%s WHERE id=%s;",
									(giro, nombre_fantasia, rut, chilean_rut, email_dte, p_id))
        nueva.execute(sql1)
	db1.commit()

antigua.execute("""SELECT partner_id, code FROM res_partner_eco_act_rel AS pea """+\
			"""INNER JOIN res_partner_eco_act AS rea ON (pea.eco_act_id=rea.id)""")
for p_id, code in antigua.fetchall():
	nueva.execute("""SELECT id FROM res_economic_activity WHERE code = '%s'""" % (code))
	act_id = antigua.fetchone()
	if act_id:
		act_id = act_id[0]
		sql2 = nueva.mogrify("INSERT INTO partner_economic_activities_rel (partner_id, activity_id) VALUES (%s, %s)", (p_id, act_id))
                nueva.execute(sql2)
		db1.commit()
	else:
		continue

antigua.execute("""SELECT id, resolution_date, resolution_number, sii_suc FROM res_company;""")
for c_id, res_date, res_number, sii_suc in antigua.fetchall():
	sql3 = nueva.mogrify("UPDATE res_company SET resolution_date=%s, resolution_number=%s, sii_office=%s, sii_env='test', "+\
									"batch_send='f' WHERE id = %s", (res_date, res_number, sii_suc, c_id))
        nueva.execute(sql3)
	db1.commit()

db1.close()
db2.close()

