
import psycopg2

asd = open('boleta_factura_ids', 'w')
asd.write('boleta - factura\n')
try:
	db1 = psycopg2.connect(user='Fedoro', dbname='migracion_mci', password='', host='localhost')
	db2 = psycopg2.connect(user='Fedoro', dbname='MCI_PRODUCCION', password='', host='localhost')
except:
	print 'No se pudo conectar a la base de datos'
else:
	nueva = db1.cursor()
	antigua = db2.cursor()
	print 'Conexion establecida'

nueva.execute("SELECT id FROM wkf_activity WHERE name = 'wait_invoice';")
wa_id = nueva.fetchone()
if wa_id:
	wa_id = wa_id[0]
else:
	print "Ta mala tu consulta del wait_invoice"
	exit()

antigua.execute("SELECT id, name, number, ticket_number, internal_number, origin, reference, state, date_ticket, date_due, create_date, " +\
		"partner_id, period_id, account_id, currency_id, move_id, company_id, journal_id, reconciled, residual, user_id, " +\
		"comment, commercial_partner_id, payment_term, amount_untaxed, amount_exent, amount_tax, amount_total, " +\
		"dte_status, ted_xml, sii_track_id, failed_text, sii_failed_text, dte, ind_servicio FROM account_ticket;")
for t_id, name, number, ticket_number, internal_number, origin, reference, state, date_ticket, date_due, create_date, partner_id, period_id, \
	account_id, currency_id, move_id, company_id, journal_id, reconciled, residual, user_id, comment, commercial_partner_id, payment_term, \
	amount_untaxed, amount_exent, amount_tax, amount_total, dte_status, ted_xml, sii_track_id, failed_text, sii_failed_text, dte, \
	ind_servicio in antigua.fetchall():

	#CAF
	caf = None
	sql_caf = nueva.mogrify("SELECT id FROM l10n_cl_dte_caf WHERE journal_id = %s AND " +\
					"start_number <= %s AND end_number >= %s;", (journal_id, number, number))
	nueva.execute(sql_caf)
	for caf_id in nueva.fetchall():
		if dte:
			caf = caf_id
		else:
			print "Ta cuestion ta rara en el caf, revisalo.\n\tcaf_id: %s\n\tcaf_journal_id: %s\n\tjournal_id: %s" +\
								"\n\tdte: %s\n\tinv_id: %s" % (caf_id, caf_journal_id, journal_id, dte, inv_id)
			exit()

	if not date_ticket:
		date_ticket = create_date.strftime('%Y-%m-%d')

	if number < 0:
		number = None

	sql1 = nueva.mogrify("INSERT INTO account_invoice (name, number, invoice_number, internal_number, origin, reference, state, " +\
		"date_invoice, date_due, partner_id, period_id, account_id, currency_id, move_id, company_id, journal_id, reconciled, " +\
		"residual, user_id, comment, commercial_partner_id, payment_term, amount_untaxed, amount_exempt, amount_tax, is_receipt, " +\
		"amount_total, dte_status, ted, track_id, failed_text, status_text, dte, service_index, caf, reference_type, type) " +\
		"VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, " +\
		"%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, 'none', 'out_invoice') RETURNING id", (name, number, ticket_number, \
					internal_number, origin, reference, state, date_ticket, date_due, partner_id, period_id, account_id, \
					currency_id, move_id, company_id, journal_id, reconciled, residual, user_id, comment, \
					commercial_partner_id, payment_term, amount_untaxed, amount_exent, amount_tax, 't', amount_total, \
					dte_status, ted_xml, sii_track_id, failed_text, sii_failed_text, dte, ind_servicio, caf))
	nueva.execute(sql1)
	nuevo_id = nueva.fetchone()[0]

	asd.write('%s - %s\n' % (t_id, nuevo_id))
	#Invoice for Sale
	antigua.execute("SELECT id, state FROM sale_order WHERE name = '%s'" % (origin))
	sale_fields = antigua.fetchone()
	if sale_fields:
		sale_id, s_state = sale_fields[0], sale_fields[1]
		sql_sale_inv = nueva.mogrify("INSERT INTO sale_order_invoice_rel (order_id, invoice_id) VALUES (%s, %s)", (sale_id, nuevo_id))
		nueva.execute(sql_sale_inv)
		nueva.execute("UPDATE sale_order SET journal_id = %s WHERE id = %s" % (journal_id, sale_id))
	else:
		antigua.execute("SELECT order_id FROM sale_order_ticket_rel WHERE ticket_id = %s" % (t_id))
		sale_id = antigua.fetchone()
		if sale_id:
			sale_id = sale_id[0]
			sql_sale_inv = nueva.mogrify("INSERT INTO sale_order_invoice_rel (order_id, invoice_id) VALUES (%s, %s)", \
														(sale_id, nuevo_id))
			nueva.execute(sql_sale_inv)
			nueva.execute("UPDATE sale_order SET journal_id = %s WHERE id = %s" % (journal_id, sale_id))

			antigua.execute("SELECT state FROM sale_order WHERE id = %s" % (sale_id))
			s_state = antigua.fetchone()
			if s_state:
				s_state = s_state[0]
			else:
				s_state = None
		else:
	                sale_id = s_state = None
			print "Invoice id: %s sin Sale Order." % (nuevo_id)
	db1.commit()

	#Tax Lines
	antigua.execute("SELECT account_id, sequence, company_id, manual, base_amount, tax_code_id, amount, base, account_analytic_id, " +\
						"tax_amount, tax_code_id, name FROM account_ticket_tax WHERE ticket_id = %s" % (t_id))
	for account_id, sequence, company_id, manual, base_amount, tax_code_id, amount, \
			base, account_analytic_id, tax_amount, tax_code_id, name in antigua.fetchall():
		t_sql = nueva.mogrify("INSERT INTO account_invoice_tax (account_id, sequence, company_id, manual, base_amount, " +\
				"tax_code_id, amount, base, invoice_id, account_analytic_id, tax_amount, tax_code_id, name) VALUES" +\
				"(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", (account_id, sequence, company_id, manual, \
				base_amount, tax_code_id, amount, base, nuevo_id, account_analytic_id, tax_amount, tax_code_id, name))
		nueva.execute(t_sql)
	db1.commit()

	#Adjunto
	nueva.execute("SELECT id FROM ir_attachment WHERE res_id = %s AND res_model = 'account.ticket'" % (t_id))
	for at_id in nueva.fetchall():
		at_sql = nueva.mogrify("UPDATE ir_attachment SET res_model = 'account.invoice', res_id = %s WHERE id = %s", (nuevo_id, at_id))
		nueva.execute(at_sql)
	db1.commit()
	###############################################

	#Lineas
	antigua.execute("SELECT id, name, origin, sequence, uos_id, product_id, account_id, price_unit, price_subtotal, quantity, discount, " +\
			"account_analytic_id, company_id, partner_id, ind_exencion FROM account_ticket_line WHERE ticket_id =%s;" % (t_id))
	for l_id, name, origin, sequence, uos_id, product_id, account_id, price_unit, price_subtotal, quantity, discount, \
								account_analytic_id, company_id, partner_id, ind_exencion in antigua.fetchall():
		sql2 = nueva.mogrify("INSERT INTO account_invoice_line (name, origin, sequence, invoice_id, uos_id, product_id, account_id, " +\
			"price_unit, price_subtotal, quantity, discount, account_analytic_id, company_id, partner_id, exension_index) VALUES " +\
				"(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING id", (name, origin, sequence, \
						nuevo_id, uos_id, product_id, account_id, price_unit, price_subtotal, quantity, discount, \
						account_analytic_id, company_id, partner_id, ind_exencion))
		nueva.execute(sql2)
		id_linea = nueva.fetchone()[0]
		antigua.execute("SELECT tax_id FROM account_ticket_line_tax WHERE ticket_line_id = %s;" % (l_id))
		for tax_id in antigua.fetchall():
			tax_id = tax_id[0]
			sql3 = nueva.mogrify("INSERT INTO account_invoice_line_tax (invoice_line_id, tax_id) " +\
											"VALUES (%s, %s)" % (id_linea, tax_id))
			nueva.execute(sql3)
	db1.commit()

	antigua.execute("SELECT r.id, dt.sii_code, r.ref_folio, r.ref_date, r.ref_reason FROM l10n_cl_dte_reference AS r " +\
				"INNER JOIN l10n_cl_dte_document_type AS dt ON (r.tpo_doc_ref=dt.id) WHERE r.ticket_id = %s;" % (t_id))
	for r_id, code, ref_folio, ref_date, ref_reason in antigua.fetchall():
		nueva.execute("SELECT id FROM l10n_cl_dte_document_type WHERE sii_code='%s'" % (code))
		type_id = nueva.fetchone()
		if type_id:
			type_id = type_id[0]
		else:
			print "La referencia con codigo: %s no se encuentra entre los ingresados, id linea: %s" % (code, type_id)
			continue

		sql_ref = nueva.mogrify("INSERT INTO l10n_cl_dte_document_references (invoice_id, reference_type, reference_folio, " +\
						"reference_date, reference_reason, reference_invoice_type) VALUES (%s, %s, %s, %s, %s, %s)", \
										(nuevo_id, type_id, ref_folio, ref_date, ref_reason, code))
		nueva.execute(sql_ref)
	db1.commit()

	#Workflow Boleta
        antigua.execute("SELECT wi.id, ww.id, wa.name FROM wkf_instance AS wi " +\
						"INNER JOIN wkf_workitem AS ww ON (wi.id=ww.inst_id) " +\
						"INNER JOIN wkf_activity AS wa ON (ww.act_id=wa.id) " +\
							"WHERE wi.res_type = 'account.ticket' AND wi.res_id = %s" % (t_id))
	for wi_id, ww_id, wa_name in antigua.fetchall():
		if wa_name == 'DTE Waiting':
			name = 'Waiting SII'
		elif wa_name == 'DTE Failed':
			name = 'Failed SII'
		else:
			name = wa_name

		nueva.execute("SELECT id FROM wkf_activity WHERE name = '%s'" % (name))
		act_id = nueva.fetchone()
		if act_id:
			act_id = act_id[0]
		else:
			print "Hay algo raro con la actividad: %s, revisa que onda pelao, la boleta es: %s" % (name, t_id)
			exit()

		nueva.execute("UPDATE wkf_instance SET res_id = %s, res_type = 'account.invoice' WHERE id = %s;" % (nuevo_id, wi_id))
		nueva.execute("UPDATE wkf_workitem SET act_id = %s WHERE id = %s;" % (act_id, ww_id))
	db1.commit()

	if sale_id:
		#Workflow sale.order
		nueva.execute("SELECT wi.id, ww.id FROM wkf_instance AS wi " +\
				"INNER JOIN wkf_workitem AS ww ON (wi.id=ww.inst_id) " +\
				"INNER JOIN wkf_activity AS wa ON (ww.act_id=wa.id) " +\
					"WHERE wa.name = 'wait_ticket' AND wi.res_type = 'sale.order' AND wi.res_id = %s" % (sale_id))
                for wi_id, ww_id in nueva.fetchall():
			sql_ww = nueva.mogrify("UDPATE wkf_workitem SET act_id = %s WHERE id = %s", (wa_id, ww_id))
			nueva.execute(sql_ww)
			if sale_id and s_state:
				if s_state == 'progress':
					sql_wi = nueva.mogrify("UPDATE wkf_instance SET state = 'complete' WHERE id = %s", (wi_id))
					nueva.execute(sql_wi)
	db1.commit()

db1.close()
db2.close()

