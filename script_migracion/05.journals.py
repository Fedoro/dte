
import psycopg2

try:
	db1 = psycopg2.connect(user='Fedoro', dbname='migracion_mci', password='', host='localhost')
	db2 = psycopg2.connect(user='Fedoro', dbname='MCI_PRODUCCION', password='', host='localhost')
except:
	print 'No se pudo conectar a la base de datos'
else:
	nueva = db1.cursor()
	antigua = db2.cursor()
	print 'Conexion establecida'

antigua.execute("SELECT id, invoice_sequence_id FROM account_journal;")
for j_id, invoice_sequence_id in antigua.fetchall():
	sql1 = nueva.mogrify("UPDATE account_journal SET invoice_sequence_id=%s WHERE id=%s", (invoice_sequence_id, j_id))
        nueva.execute(sql1)
	db1.commit()

antigua.execute("SELECT aj.id, dt.sii_code FROM account_journal AS aj " +\
			"INNER JOIN l10n_cl_dte_document_type AS dt ON (aj.sii_doc_type_id=dt.id);")
for j_id, sii_code in antigua.fetchall():
	nueva.execute("SELECT id FROM l10n_cl_dte_document_type WHERE sii_code = '%s';" % (sii_code))
	doc_type_id = nueva.fetchone()
	if doc_type_id:
		doc_type_id = doc_type_id[0]
		sql2 = nueva.mogrify("UPDATE account_journal SET doc_type_id=%s WHERE id=%s;", (doc_type_id, j_id))
		nueva.execute(sql2)
		db1.commit()
	else:
		continue

db1.close()
db2.close()

