# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

import openerp.addons.decimal_precision as dp

from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _

import base64

class l10n_cl_dte_assignment(models.Model):
	_name = 'l10n_cl_dte.assignment'
	_description = 'Assignment Maintainer'

	@api.one
	@api.depends('total_untaxed','total_notax','total_tax', 'total_amount', 'total_documents')
	def _compute_totals(self):
		tot_docs = untaxed = notax = tax = total = 0

		for document in self.document_ids:
			untaxed += document.amount_untaxed
			notax += document.amount_exempt
			tax += document.amount_tax
			total += document.amount_total
			tot_docs += 1

		self.total_documents = tot_docs
		self.total_untaxed = untaxed
		self.total_notax = notax
		self.total_tax = tax
		self.total_amount = total

	state = fields.Selection([('draft','Draft'),('signed','Signed'),('error','Error'),\
					('waiting_validation','Waiting Validation'),('validate','Validate')], 'State', default='draft')
	provider_company_id = fields.Many2one('res.company', 'Provider Company', required=True, default=lambda self: self.env['res.company'].\
				_company_default_get('l10n_cl_dte.assignment'), states={'draft': [('readonly', False)]})
	assignee_id = fields.Many2one('res.partner', 'Assignee', required=True, domain=[('assignee','=',True)])
	notification_email = fields.Char('Notification Email')
	contact_name = fields.Char('Contact Name')
	contact_phone = fields.Char('Contact Phone')
	contact_email = fields.Char('Contact Email')

	total_untaxed = fields.Float('Total Untaxed', digits=dp.get_precision('Account'),
								store=True, compute='_compute_totals')
	total_notax = fields.Float('Total Notax', digits=dp.get_precision('Account'),
								store=True, compute='_compute_totals')
	total_tax = fields.Float('Total Tax', digits=dp.get_precision('Account'),
								store=True, compute='_compute_totals')
	total_amount = fields.Float('Total Amount', digits=dp.get_precision('Account'),
								store=True, compute='_compute_totals')
	total_documents = fields.Integer(string='Total Documents', store=True, compute='_compute_totals')
	document_ids = fields.One2many('account.invoice', 'assignment_id', 'Documents', \
						domain=[('assign','=',False),('type','=','out_invoice'),('state','=','open')])
	track_id = fields.Char('Track Id')
	failed_text = fields.Text('Error')
	xml = fields.Text('XML')
	currency_id = fields.Many2one('res.currency', 'Currency', related='company_id.currency_id', store=True, readonly=True)
	comment = fields.Text('Comment')
	signed_xml = fields.Binary('Signed XML')
	company_id = fields.Many2one('res.company', 'Company', required=True, default=lambda self: self.env['res.company'].\
				_company_default_get('l10n_cl_dte.assignment'), states={'draft': [('readonly', False)]})

	@api.multi
	def unlink(self):
		for obj in self:
			if obj.state != 'draft':
				raise Warning(_('You can\'t delete a book which is not draft.'))
		return super(l10n_cl_dte_books, self).unlink()

	@api.multi
	def back_to_draft(self):
		for obj in self:
			obj.state = 'draft'
		return True

	@api.multi
	def sign(self):
		dte_attach = self.env['l10n_cl_dte.attachment']

		for obj in self:
			signed_xml = self.env['l10n_cl_dte.signer'].sign_xml(obj)
			b64_signed_xml = base64.b64encode(signed_xml)
			attch_vals = dte_attach.get_attachment_values(obj, b64_signed_xml, exten='.xml')
			dte_attach.add_attachment(obj, attch_vals)
                        obj.write({'state': 'signed', 'signed_xml': b64_signed_xml, 'failed_text': None})
		return True

	@api.multi
	def send(self):
		for obj in self:
			self.env['l10n_cl_dte.sii'].send_sii(obj)
		return True

	@api.multi
	def get_send_state(self):
		for obj in self:
			self.env['l10n_cl_dte.sii'].get_send_state(obj)
		return True

l10n_cl_dte_assignment()
