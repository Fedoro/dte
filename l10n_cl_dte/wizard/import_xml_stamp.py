# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

import base64, io
import xml.etree.ElementTree as etree 

from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _

class l10n_cl_dte_import_xml_stamp(models.TransientModel):
	_name = 'l10n_cl_dte.import_xml_stamp'

	caf_file = fields.Binary('SII File')

	@api.one
	def import_file(self):
		vals = {
			'caf': None,
			'xml': None,
			'code': None,
			'end_number': 0,
			'caf_file': None,
			'public_key': None,
			'next_number': 0,
			'private_key': None,
			'start_number': 0,
			'state': 'active'
		}

		caf = base64.b64decode(self.caf_file)
		caf_file = io.StringIO(caf.decode('iso-8859-1'))
		root = etree.fromstring(caf_file.read().encode('utf-8'))

		model_obj = self.env[self._context['active_model']]
		if 'active_model' and 'active_id' in self._context:
			reg = model_obj.browse(self._context['active_id'])
			if not reg.journal_id.company_id.partner_id.rut:
				raise Warning(_('You must set the rut field in the company.'))
		else:
			raise Warning(_('The system can\'t know which journal your are trying to import, ' +\
									'please contact the system administrator.'))
		try:
			if root.tag == 'AUTORIZACION':
				vals['xml'] = (caf.decode('iso-8859-1'))[:-1]
				vals['caf_file'] = self.caf_file
				caf_tag = root.find('CAF')
				data_tag = caf_tag.find('DA')
				if data_tag is not None:
					company_rut = data_tag.find('RE')
					if company_rut is not None:
						if company_rut.text != reg.journal_id.company_id.partner_id.rut.replace('.',''):
							raise Warning(_('The company rut doesn\'t match with the company rut of the file.'))
					else:
						raise Warning(_('This file not have company rut tag'))
				else:
					raise Warning(_('This file not have data tag'))

				vals['caf'] = (etree.tostring(caf_tag, encoding='utf-8'))[:-1]
				vals['code'] = data_tag.find('TD').text
				if vals['code'] != reg.journal_id.doc_type_id.sii_code:
					raise Warning(_('The document type code downloaded from the SII file doesn\'t match with ' +\
						'the document type code selected in the journal. Check the document type code ' +\
						'selected in the journal and the document type code downloaded from the SII are equal.'))

				rng_tag = data_tag.find('RNG')
				vals['start_number'] = vals['next_number'] = rng_tag.find('D').text
				vals['end_number'] = rng_tag.find('H').text

				vals['private_key'] = (root.find('RSASK').text)[:-1]
				vals['public_key'] = (root.find('RSAPUBK').text)[:-1]
		finally:
			reg.write(vals)
		return True
l10n_cl_dte_import_xml_stamp()
