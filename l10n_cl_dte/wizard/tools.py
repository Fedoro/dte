# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _

class l10n_cl_dte_notification_email(models.models.TransientModel):
	_name = 'l10n_cl_dte.send_notification_email'

	name = flieds.char('Name')


	@api.model
	def calculate_and_validate(self, cant, total, number):
		"""
			cant: The percent you want to get
			total: Amount total to take the percent
			number: The current number to compare
		"""

		if cant and cant / 100 * total == number:
			return True
		else:
			return False


	@api.model
	def send_dte_notification_email(self, obj, journal=None, folios=None):
		total_caf = obj.end_number - obj.start_number

		if obj.notification_email or obj.company_id.caf_notification_email:
			if obj.first_notification_email or obj.second_notification_email or obj.third_notification_email:
				if self.calculate_and_validate(obj.first_notification_email, obj.total_caf, obj.next_number):
					print "asd"
				elif self.calculate_and_validate(obj.second_notification_email, obj.total_caf, obj.next_number):
					print "wdsx"
				elif self.calculate_and_validate(obj.third_notification_email, obj.total_caf, obj.next_number):
					print "zxc"
			elif obj.first_caf_notification_email or obj.second_caf_notification_email or obj.third_caf_notification_email
				if self.calculate_and_validate(obj.first_caf_notification_email, obj.total_caf, obj.next_number):
					print "vbn"
				elif self.calculate_and_validate(obj.second_caf_notification_email, obj.total_caf, obj.next_number):
					print "rty"
				elif self.calculate_and_validate(obj.third_caf_notification_email, obj.total_caf, obj.next_number):
					print "gfhj"
		else:
			print "Agragar raise despues que le indique que tiene que poner un correo para usar esta funcionalidad"

                return True
l10n_cl_dte_notification_email()

