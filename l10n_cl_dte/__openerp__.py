# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

{
	'name': 'Base Fields for DTE',
	'version': '1.0',
	'author': '[BTeC Ltda.]',
	'sequence': 31,
	'website': 'www.raiquen.cl',
	'category': 'Accounting & Finance',
	'description': """
Módulo encargado de integración de campos con objeto account.invoice para factura, notas de crédito y notas de débito electrónicas.
""",
	'depends': [
		'document',
		'base_action_rule',
		'account_cancel',
		'account_accountant',
		'l10n_cl_base'
	],
	'data': [
		'security/l10n_cl_dte_security.xml',
		'views/dte_menu_root.xml',
		'wizard/import_xml_stamp_view.xml',
		'views/maintainers_view.xml', 
		'views/product_uom_view.xml', 
		'views/res_currency_view.xml',
		'views/res_partner_view.xml',
		'views/res_company_view.xml', 
		'views/res_country_view.xml',
		'views/account_journal_view.xml',
		'views/account_tax_code_view.xml',
		'views/account_payment_term_view.xml',
		'data/document_type_data.xml',
		'data/decimal_precision.xml',
		'data/sii_emails_cron.xml',
		'data/customs.xml',
		#'data/currency.xml', Modificar y poner solo como texto en currency
		'data/ports.xml',
		'data/package_type.xml',
		'data/res_country.xml',
		'data/uom.xml',
		'security/ir.model.access.csv'
	],
	'contributors': [
		'Patricio Felipe Caceres <patricio.caceres@raiquen.cl>',
		'David Acevedo Toledo <david.acevedo@raiquen.cl>'
	]
}
