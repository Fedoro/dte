# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api

class l10n_cl_dte_carrier(models.Model):
	_name = 'l10n_cl_dte.carrier'

	@api.onchange('rut')
	def onchange_rut(self):
		if self.rut:
			self.rut = self.env['res.partner'].check_rut(self.rut)
	
	name = fields.Char('Name', required=True)
	rut = fields.Char('RUT', required=True)
	driver_ids = fields.One2many('l10n_cl_dte.driver', 'carrier_id', 'Drivers')
	vehicle_ids = fields.One2many('l10n_cl_dte.vehicle', 'carrier_id', 'Vehicles')
l10n_cl_dte_carrier()

class l10n_cl_dte_driver(models.Model):
	_name = 'l10n_cl_dte.driver'

	@api.onchange('rut')
	def onchange_rut(self):
		if self.rut:
			self.rut = self.env['res.partner'].check_rut(self.rut)

	name = fields.Char('Name', required=True)
	rut = fields.Char('RUT', required=True)
	carrier_id = fields.Many2one('l10n_cl_dte.carrier', 'Carrier')
l10n_cl_dte_driver()

class l10n_cl_dte_vehicle(models.Model):
	_name = 'l10n_cl_dte.vehicle'
	_rec_name = 'license_plate'

	license_plate = fields.Char('License Plate', required=True)
	model = fields.Char('Model')
	brand = fields.Char('Brand')
	carrier_id = fields.Many2one('l10n_cl_dte.carrier', 'Carrier')
l10n_cl_dte_vehicle()
