# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api

class l10n_cl_dte_partner(models.Model):
	_inherit = 'res.partner'

	distribution_email = fields.Char('Email for distribution')
	dte_contact = fields.Char('Email DTE', help='This field is for DTE contact email who receives the generated PDF and XML file.')
	# IdAdicEmisor , Identificador adicional del emisor
	# Para documentos utilizados en exportaciones. Codigo de de identificacion adicional para uso libre
	aditional_code = fields.Char('Aditional ID',size=20) 

	# CdgIntRecep, Codigo interno del receptor
	# Para identificacion interna del Receptor, por ejemplo codigo del cliente, numero de medidor, etc.
	internal_code = fields.Char('Internal code for partner')
	nationality_id = fields.Many2one('res.country', 'Nationality', copy=False)

	# IdAdocRecep Identificador adicional del receptor extranjero
	# Solo para documentos de exportacion. Codigo de intentificacion adicional para uso libre
	foreign_internal_code = fields.Char('Foreign Extra Id', size=20)

	@api.model
	def create(self, vals):
		if vals.get('rut') and not vals.get('foreign_rut'):
			sii_email = self.env['l10n_cl_dte.sii_email'].search([('rut','=',self.check_rut(vals['rut']))])
			if sii_email:
				vals.update({'distribution_email': sii_email.email})
		return super(l10n_cl_dte_partner, self).create(vals)

	@api.multi
	def write(self, vals):
		if vals.get('rut') and not vals.get('foreign_rut'):
			sii_email = self.env['l10n_cl_dte.sii_email'].search([('rut','=',self.check_rut(vals['rut']))])
			if sii_email:
				vals.update({'distribution_email': sii_email.email})
		return super(l10n_cl_dte_partner, self).write(vals)
l10n_cl_dte_partner()

