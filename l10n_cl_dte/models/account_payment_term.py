# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _

class l10n_cl_dte_payment_term(models.Model):
	_inherit = 'account.payment.term'

	# FmaPago, Forma de pago :indica en que forma se pagara. 
	# En el caso de una Factura por "Entrega Gratuita", se debe indicar el 3. Una 
	# Factura de este tipo no tiene derecho a credito fiscal.
	sii_payment_term = fields.Selection([(1, 'Cash'), (2, 'Credit'), (3, 'Free')], 'SII Payment Term', \
						help='If a invoice is \'Free Delivery\' must be select the option 3. \
							A \'Free Delivery\' invoice haven\'t right to fiscal credit')

	# FmaPagExp Forma de Pago Exportacion, En el caso de Factura de exportacion se refiere a la forma de pago 
	# del importador extranjero indicada en el DUS (acreditivo, cobranza, anticipo, contado) En el caso de una 
	# Factura de exportacion por "Muestras sin caracter comercial", segun las normas de Aduanas, debe indicar el Cod. 21.
	# obs: Para Factura de Exportacion Corresponde al Codigo senalado en la Tabla Formas de Pago de Aduanas
	sii_exp_payment_term = fields.Selection([(1, 'One Year top collection'),(2, 'More than one year collection'),\
						(11, 'One year top credit'),(12, 'More than one year credit'),\
						(21, 'No Pay'),(32, 'Advance payment on the date of shipment')], 'SII Export Payment Term', \
					help='In the case of an export invoice, it refers to the form of payment of the foreign importer \
						indicated in the DUS (credit, collection, advance, counted) In the case of an export \
						invoice for "Samples without commercial character", according to the rules of \
						Customs, you must indicate the Cod. 21.')
l10n_cl_dte_payment_term()
