# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _

class l10n_cl_dte_caf(models.Model):
	_name = 'l10n_cl_dte.caf'
	_description = 'CAF for Document'
	_order = 'next_number DESC'

	@api.model
	def get_caf(self, journal_id):
		cafs = self.search([('journal_id','=',journal_id),('state','=','active')],)
		if not cafs:
			raise Warning(_('You must have one active caf for this journal. Please Check'))
		if (cafs[0].end_number - cafs[0].next_number) < 0:
			raise Warning(_('There are no more folios for these documents'))
		return cafs[0]

	@api.one
	def folio_consume(self):
		current_number = self.next_number
		next_number = current_number + 1

		if next_number > self.end_number:
			sql = """UPDATE l10n_cl_dte_caf SET next_number=%s, state='finished' WHERE id = %s""" % (next_number, self.id)
		else:
			sql = """UPDATE l10n_cl_dte_caf SET next_number=%s WHERE id = %s""" % (next_number, self.id)
		self._cr.execute(sql)
		return current_number

	@api.multi
	def activate(self):
		self.write( {'state':'active'})
		return True

	@api.multi
	def deactivate(self):
		self.write( {'state':'finished'})
		return True

	@api.one
	@api.depends('journal_id', 'code', 'start_number', 'end_number')
	def name_of_caf(self):
		self.name = '%s - %s folios desde %s hasta %s' % \
					(self.code, self.journal_id.doc_type_id.name, self.start_number, self.end_number)

	@api.one
	@api.depends('next_number', 'state')
	def _its_empty(self):
		self.empty = True if self.next_number > self.end_number else False

	@api.multi
	def unlink(self):
		for caf in self:
			if caf.state != 'draft':
				raise Warning(_('You can\'t delete a CAF which is not draft.'))
                return super(l10n_cl_dte_caf, self).unlink()

	@api.onchange('next_number')
	def _onchange_next_number(self):
		if self.next_number > self.end_number:
			self.state = 'finished'

	@api.onchange('journal_id')
	def _onchange_journal(self):
		if self.journal_id and self.code:
			j_obj = self.env['account.journal'].browse(self.journal_id.id)
			if j_obj.doc_type_id.sii_code != self.code:
				self.journal_id = self.browse(self.id).journal_id.id
				warning = {
					'title': _('Wrong Journal Code!'),
					'message' : _('The journal code dosen\'t match with the code of import file from SII.')
				}
				return {'warning': warning}

	@api.one
	@api.constrains('journal_id', 'state')
	def _check_unique(self):
		if self.search([('journal_id','=',self.journal_id.id),('state','=','active'),('id','!=',self.id)]) and self.state == 'active':
			raise Warning(_('You can have one CAF File active per journal at the same time.'))

	state = fields.Selection([('draft','Draft'),('active','Active'),('finished','Finished')], string='State',\
						track_visibility='onchange', copy=False, default='draft', index=True, readonly=True)
	name = fields.Char(compute='name_of_caf', store=True)
	journal_id = fields.Many2one('account.journal', string='Journal', ondelete='cascade', \
							readonly=True, states={'draft': [('readonly', False)]})
	company_id = fields.Many2one(related='journal_id.company_id', string='Associated Company', relation='res.company', copy=False)
	start_number = fields.Integer('Start Number', copy=False)
	end_number = fields.Integer('End Number', copy=False)
	next_number = fields.Integer('Next Number', copy=False)
	empty = fields.Boolean('Empty', compute=_its_empty, default=False, store=True, copy=False)
	code = fields.Char('Code', copy=False)
	public_key = fields.Text('Public Key', copy=False)
	private_key = fields.Text('Private Key', copy=False)
	xml = fields.Text('Original XML', copy=False)
	caf = fields.Text('XML CAF', copy=False)
	caf_file = fields.Binary('File', copy=False)
	notification_email = fields.Char('Notification Email')
l10n_cl_dte_caf()


