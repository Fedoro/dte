# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api

class l10n_cl_dte_package_code(models.Model):
	_name = 'l10n_cl_dte.package_code'
	_rec_name = 'package_code_id'

	package_code_id = fields.Many2one('l10n_cl_dte.package', 'Packages Code') #96
	package_qty = fields.Integer('Packages Qty') #97
	brands = fields.Char('Brands') #98
	container_code = fields.Char('Container Code') #99
	container_stamp = fields.Char('Container Stamp') #100
	container_stamp_issuer = fields.Char('Container Stamp Issuer') #101
l10n_cl_dte_package_code()

class l10n_cl_dte_package(models.Model):
	_name = 'l10n_cl_dte.package'

	name = fields.Char('Name', required=True)
	code = fields.Char('Code', required=True)

	@api.model
	def name_search(self, name, args=None, operator='ilike', limit=80):
		res = super(l10n_cl_dte_package, self).name_search(name, args=args, operator=operator, limit=limit)

		if name:
			ids = self.search([('code', operator, name),('id','not in',[x[0] for x in res])] + args, limit=limit)
			res += ids.name_get()

		return res

	@api.multi
	def name_get(self):
		aux = []
		resp = super(l10n_cl_dte_package, self).name_get()

		for record in resp:
			package = self.browse(record[0])
			if package.code:
				name = "[%s] %s" % (package.code, record[1])
				aux.append((record[0], name))
			else:
				aux.append(record)
		return aux if aux else resp
l10n_cl_dte_package()
