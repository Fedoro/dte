# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api

class l10n_cl_dte_company(models.Model):
	_inherit = 'res.company'

	@api.model
	def get_sii_env(self):
		if self.sii_env == 'prod':
			return 'palena'
		else:
			return 'maullin' 

	sii_env = fields.Selection([('test', 'Testing'),('registration','Registration'), ('prod', 'Production')], \
											'SII environment', default='test')
	resolution_date = fields.Date('Resolution Date', help='You can get the Resolution Date in the sii website')
	resolution_number = fields.Integer('Resolution Number', help='You can get the Resolution Number in the sii website')
	sii_office = fields.Char('SII Office', help='Corresponding to the address of the company headquarters')
	batch_send = fields.Boolean('Do you process DTE dispatch in batches?')
	branch_offices = fields.Text('Branch Office related to this company')
	caf_notification_email = fields.Char('CAF Notification Email', help="Emails from those in charge of obtaining CAF for DTE documents. (For more than one use ',')")
l10n_cl_dte_company()
