# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _

class l10n_cl_dte_account_journal(models.Model):
	_inherit = 'account.journal'

	@api.model
	@api.constrains('company_id','invoice_sequence_id')
	def _check_company(self):
		if self.invoice_sequence_id and self.invoice_sequence_id.company_id != self.company_id:
			raise Warning(_('Journal company and invoice sequence company do not match.'))

	@api.multi
	def write(self, vals):
		if 'doc_type_id' in vals:
			for journal in self:
				if self.env['l10n_cl_dte.caf'].search([('journal_id','=',journal.id),('state','=','active')]):
					raise Warning(_('You can\'t change the Document Type of this journal with an active CAF.'))
		return super(l10n_cl_dte_account_journal, self).write(vals)

	invoice_sequence_id = fields.Many2one('ir.sequence', 'Invoice Sequence', domain="[('company_id','=',company_id)]",
								help='The sequence used for invoice numbers in this journal.')
	doc_type_id = fields.Many2one('l10n_cl_dte.document_type', string='Document Type', copy=False)
	caf_ids = fields.One2many('l10n_cl_dte.caf', 'journal_id', string='CAF List', copy=False)
	for_sale = fields.Boolean('For Sale', help='If it check, the journal will be displayed in the Sale Order.')
	no_book = fields.Boolean('Doesn\'t Include in Electronic Books', \
					help='If check, the journal doesn\'t include in electronic sales or purchase book.')
l10n_cl_dte_account_journal()

