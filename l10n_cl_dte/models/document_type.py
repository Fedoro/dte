# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _

class l10n_cl_dte_document_type(models.Model):
	_name = 'l10n_cl_dte.document_type'
	_descripcion= 'Document Type'
	_order = 'sii_code asc'

	@api.model
	def name_search(self, name, args=None, operator='ilike', limit=100):
		res = super(l10n_cl_dte_document_type, self).name_search(name, args=args, operator=operator, limit=limit)

		if name:
			type_ids = self.search([('sii_code', operator, name),('id','not in',[x[0] for x in res])] + args, limit=limit)
			res += type_ids.name_get()
		return res

	@api.multi
	def name_get(self):
		res = []
		resp = super(l10n_cl_dte_document_type, self).name_get()

		for record in resp:
			doc_type = self.browse(record[0])
			name = "%s - %s" % (doc_type.sii_code, record[1])
			res.append((record[0], name))
		return res if res else resp


	
	name = fields.Char('Name', required=True, copy=False)
	sii_code = fields.Char('SII Code', required=True, copy=False)
	description = fields.Text('Description', copy=False)
l10n_cl_dte_document_type()

