# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api

class l10n_cl_dte_sii_email(models.Model):
	_name = 'l10n_cl_dte.sii_email'
	_rec_name = 'rut'

	rut = fields.Char('RUT', required=True)
	name = fields.Char('Name', required=True)
	email = fields.Char('Email', required=True)

	@api.multi
	def set_register(self):
		vals = {}
		
		arch = open('/opt/odoo/ce_empresas.csv', 'r')

		regs = self.search([])
		regs.unlink()

		for line in arch.readlines():
			if 'RUT' in line and 'RAZON SOCIAL' in line:
				continue
			if len(line.split(';')[0]) < 9:
				continue

			self.create({
				'rut': self.env['res.partner'].check_rut(line.split(';')[0]),
				'name': line.split(';')[1],
				'email': line.split(';')[4]
			})

		arch.close()
#		_logger.info('Fallo al actualizar partner id: %s , rut: %s, error: %s\n' % (partner.id, partner.rut, e))
		return True

	@api.multi
	def set_email_partner(self):
		partner_pool = self.env['res.partner']
		for partner in partner_pool.search([]):
			reg = self.search([('rut','=',partner.rut)])
			if reg:
				if partner.distribution_email and partner.dte_contact and partner.distribution_email not in partner.dte_contact:
					partner.dte_contact += ',%s' % partner.distribution_email
				elif partner.distribution_email:
					partner.dte_contact = partner.distribution_email
				partner.distribution_email = reg.email
		return True
l10n_cl_dte_sii_email()
