openerp.l10n_cl_dte = function(instance, m) {
	var _t = instance.web._t, QWeb = instance.web.qweb;
	module = instance.point_of_sale;

	instance.web.FormView.include({
		load_record: function(data){
			this._super.apply(this , arguments);
			var printer_button = document.getElementsByClassName('oe_print')[0];
			if (data.date_receipt != undefined){
				if (typeof(printer_button) != 'undefined') {
					printer_button.onclick = function(){
						order = new instance.web.Model('account.receipt'); //Aki cambio nombre
						order.call('receipt_for_print',[[data.id]]).then(function(receipt){ //Aki cambio nombre
							console.log(receipt);
							if (receipt.sii_code == 39){
								template = 'XmlReceipt';
							} else {
								template = 'XmlInvoice';
							}
							dte_document = QWeb.render(template,{ receipt: receipt, widget: self,});
							if(receipt.connection.iface_print_via_proxy){
								var options = {};
								options.url = receipt.connection.proxy_ip
								this.connection = new instance.web.Session(undefined,receipt.connection.proxy_ip, {use_cors: true});
								//this.connection.setup(options.url);
								var ret = new $.Deferred();
								console.log(this.connection.rpc('/hw_proxy/print_xml_receipt', {receipt: dte_document} || {}).done(function(result) { // Aki cambio nombre
									console.log('');
								}).fail(function(error) {
									alert('Impresora Desconectada');
								}));
								var resuelto = function(status){console.log(status);};
								console.log(ret.done(resuelto));
							}
						});
					}
				}
			}
		},
	}); 
} 

