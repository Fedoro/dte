openerp.l10n_cl_dte_pos = function(instance){
    module = instance.point_of_sale;
    var _t = instance.web._t;
    QWeb = instance.web.qweb;

	var order = module.Order.prototype;
	var Posmodel = module.PosModel.prototype;
	var PaymentScreenWidget = module.PaymentScreenWidget.prototype;

instance.web.FormView.include({

        load_record: function(data){
            this._super.apply(this , arguments);
            var printer_button = document.getElementsByClassName('oe_print')[0];
            if (typeof(printer_button) != 'undefined') {
                printer_button.onclick = function(){
                    order = new instance.web.Model('pos.order');
                    order.call('data_for_print',[[data.id]]).then(function(receipt){
                        console.log(receipt);
                        template = 'XmlReceipt';
                        dte_document = QWeb.render(template,{ receipt: receipt, widget: self,});
                        if(receipt.connection.iface_print_via_proxy){
                            var options = {};
                            options.url = receipt.connection.proxy_ip
                            this.connection = new instance.web.Session(undefined,receipt.connection.proxy_ip, { use_cors: true});
                            // this.connection.setup(options.url);

                            var ret = new $.Deferred();
                            console.log(this.connection.rpc('/hw_proxy/print_xml_receipt',  {receipt: dte_document}|| {}).done(function(result) {
                                console.log('');
                            
                            }).fail(function(error) {
                                alert('Impresora Desconectada');
                            }));

                            var resuelto = function(status){console.log(status);};

                            console.log(ret.done(resuelto));
                        }
                    });
                }    
            }
        }, 
    }); 


	module.PosModel = module.PosModel.extend({
		load_server_data: function(){
			for(var i=0; i<this.models.length; i++){
				var model=this.models[i];
				if(model.model === 'res.company'){
					model.fields.push('name','rut','resolution_date','resolution_number', 'sii_office','street','street2','city', 'line_of_business','fantasy_name','economic_activities_ids');
				} 
				if(model.model === 'res.partner'){
					model.fields.push('line_of_business','fantasy_name','economic_activities_ids');
				}
			}
			return Posmodel.load_server_data.call(this);
		},
	});

    module.Orderline = module.Orderline.extend({
        export_for_printing: function(){
            if (this.get_product().ean13) {
                product_name = '[' + this.get_product().ean13 + '] ' + this.get_product().display_name
            }
            else if (this.get_product().default_code){
                product_name = '[' + this.get_product().default_code + '] ' + this.get_product().display_name
            }
            else{
                product_name = this.get_product().display_name
            }

            return {
                quantity:           this.get_quantity(),
                unit_name:          this.get_unit().name,
                price:              this.get_unit_price(),
                discount:           this.get_discount(),
                product_name:       product_name,
                price_display :     this.get_display_price(),
                price_with_tax :    this.get_price_with_tax(),
                price_without_tax:  this.get_price_without_tax(),
                tax:                this.get_tax(),
                product_description:      this.get_product().description,
                product_description_sale: this.get_product().description_sale,
            };
        },
    })

	
	module.Order = module.Order.extend({
		export_for_printing: function() {

            if (this.pos.company.street2){
                street = this.pos.company.street + ' ' + this.pos.company.street2 + ', ' + this.pos.company.city;
            }
            else{
                street = this.pos.company.street + ', ' + this.pos.company.city;
            }
            resolution_date = this.pos.company.resolution_date;
            r_date =  resolution_date.substring(8,10) + '-' +resolution_date.substring(5,7) + '-' +resolution_date.substring(0,4);

		    export_for_printing_dict=order.export_for_printing.call(this);
		    export_for_printing_dict['company']['fantasy_name'] = this.pos.company.fantasy_name;
		    export_for_printing_dict['company']['giro'] = this.pos.company.line_of_business;
		    export_for_printing_dict['company']['economic_activities_ids'] = this.pos.company.economic_activities_ids;
		    export_for_printing_dict['company']['resolution_number'] = this.pos.company.resolution_number;
		    export_for_printing_dict['company']['resolution_date'] = r_date;
		    export_for_printing_dict['company']['sii_office'] = this.pos.company.sii_office;
            export_for_printing_dict['company']['rut']= this.pos.company.rut.replace('.','').replace('.','');
            export_for_printing_dict['company']['street'] = street
		    export_for_printing_dict['signature'] = '';
                    
            console.log(export_for_printing_dict);
		    return export_for_printing_dict;
		},
        export_as_JSON: function() {
            export_as_JSON_dict = order.export_as_JSON.call(this);
            export_as_JSON_dict.folio = this.get('folio');
            export_as_JSON_dict.signature = this.get('signature');
            export_as_JSON_dict.caf = this.get('caf');
            export_as_JSON_dict.date_order = this.get('date_order');

            return export_as_JSON_dict;
        },
		get_chilean_rut: function(){
			return this.pos.company.rut.replace('.','').replace('.','');
		},
        set_folio: function(number){
            this.set('sequence_number', number);
        },
        set_date_of_printed: function(date){
            this.set('date_order', date);
        },
        get_date_of_printed: function(){
            return this.get('date_order');
        },
		export_for_ted: function() {
			var ted_xml;
			var date = new Date();

			var order_obj = new instance.web.Model('pos.order');
			orderLines = [];

			(this.get('orderLines')).each(_.bind( function(item) {
				return orderLines.push([0, 0, item.export_for_printing()]);
			}, this));
			this.set_date_of_printed(date);
			data_for_ted =  {
				rut_emisor : this.get_chilean_rut(),
				document_type : this.document_type || 39,
				date : date.toISOString().slice(0, 10),
				receptor_rut : '66666666-6',
				receptor_name : 'CLIENTE OCACION' ,
				first_item: orderLines[0][2].product_name,
				total_amount: this.getTotalTaxIncluded(),
			};
			// order_obj.call('get_ted',[1, data_for_ted, this.pos.config.journal_id[0] ]).then(function(ted){ ted_xml =
		
			return order_obj.call('get_ted', {
                            'data': data_for_ted, 
                            'journal_id': this.pos.config.journal_id[0]})
		},
	});
    module.PaymentScreenWidget = module.ScreenWidget.extend({
        template: 'PaymentScreenWidget',
        back_screen: 'products',
        next_screen: 'receipt',
        init: function(parent, options) {
            var self = this;
            this._super(parent,options);

            this.pos.bind('change:selectedOrder',function(){
                    this.bind_events();
                    this.renderElement();
                },this);

            this.bind_events();
            this.decimal_point = instance.web._t.database.parameters.decimal_point;

            this.line_delete_handler = function(event){
                var node = this;
                while(node && !node.classList.contains('paymentline')){
                    node = node.parentNode;
                }
                if(node){
                    self.pos.get('selectedOrder').removePaymentline(node.line)   
                }
                event.stopPropagation();
            };

            this.line_change_handler = function(event){
                var node = this;
                while(node && !node.classList.contains('paymentline')){
                    node = node.parentNode;
                }
                if(node){
                    var amount;
                    try{
                        amount = instance.web.parse_value(this.value, {type: "float"});
                    }
                    catch(e){
                        amount = 0;
                    }
                    node.line.set_amount(amount);
                }
            };

            this.line_click_handler = function(event){
                var node = this;
                while(node && !node.classList.contains('paymentline')){
                    node = node.parentNode;
                }
                if(node){
                    self.pos.get('selectedOrder').selectPaymentline(node.line);
                }
            };

            this.hotkey_handler = function(event){
                if(event.which === 13){
                    self.validate_order();
                }else if(event.which === 27){
                    self.back();
                }
            };

        },
        show: function(){
            this._super();
            var self = this;
            
            this.enable_numpad();
            this.focus_selected_line();
            
            document.body.addEventListener('keyup', this.hotkey_handler);

            this.add_action_button({
                    label: _t('Back'),
                    icon: '/point_of_sale/static/src/img/icons/png48/go-previous.png',
                    click: function(){  
                        self.back();
                    },
                });

            this.add_action_button({
                    label: _t('Validate'),
                    name: 'validation',
                    icon: '/point_of_sale/static/src/img/icons/png48/validate.png',
                    click: function(){
                        self.validate_order();
                    },
                });
           
            if( this.pos.config.iface_invoicing ){
                this.add_action_button({
                        label: _t('Invoice'),
                        name: 'invoice',
                        icon: '/point_of_sale/static/src/img/icons/png48/invoice.png',
                        click: function(){
                            self.validate_order({invoice: true});
                        },
                    });
            }

            if( this.pos.config.iface_cashdrawer ){
                this.add_action_button({
                        label: _t('Cash'),
                        name: 'cashbox',
                        icon: '/point_of_sale/static/src/img/open-cashbox.png',
                        click: function(){
                            self.pos.proxy.open_cashbox();
                        },
                    });
            }

            this.update_payment_summary();

        },
        close: function(){
            this._super();
            this.disable_numpad();
            document.body.removeEventListener('keyup',this.hotkey_handler);
        },
        remove_empty_lines: function(){
            var order = this.pos.get('selectedOrder');
            var lines = order.get('paymentLines').models.slice(0);
            for(var i = 0; i < lines.length; i++){ 
                var line = lines[i];
                if(line.get_amount() === 0){
                    order.removePaymentline(line);
                }
            }
        },
        back: function() {
            this.remove_empty_lines();
            this.pos_widget.screen_selector.set_current_screen(this.back_screen);
        },
        bind_events: function() {
            if(this.old_order){
                this.old_order.unbind(null,null,this);
            }
            var order = this.pos.get('selectedOrder');
                order.bind('change:selected_paymentline',this.focus_selected_line,this);

            this.old_order = order;

            if(this.old_paymentlines){
                this.old_paymentlines.unbind(null,null,this);
            }
            var paymentlines = order.get('paymentLines');
                paymentlines.bind('add', this.add_paymentline, this);
                paymentlines.bind('change:selected', this.rerender_paymentline, this);
                paymentlines.bind('change:amount', function(line){
                        if(!line.selected && line.node){
                            line.node.value = line.amount.toFixed(this.pos.currency.decimals);
                        }
                        this.update_payment_summary();
                    },this);
                paymentlines.bind('remove', this.remove_paymentline, this);
                paymentlines.bind('all', this.update_payment_summary, this);

            this.old_paymentlines = paymentlines;

            if(this.old_orderlines){
                this.old_orderlines.unbind(null,null,this);
            }
            var orderlines = order.get('orderLines');
                orderlines.bind('all', this.update_payment_summary, this);

            this.old_orderlines = orderlines;
        },
        focus_selected_line: function(){
            var line = this.pos.get('selectedOrder').selected_paymentline;
            if(line){
                var input = line.node.querySelector('input');
                if(!input){
                    return;
                }
                var value = input.value;
                input.focus();

                if(this.numpad_state){
                    this.numpad_state.reset();
                }

                if(Number(value) === 0){
                    input.value = '';
                }else{
                    input.value = value;
                    input.select();
                }
            }
        },
        add_paymentline: function(line) {
            var list_container = this.el.querySelector('.payment-lines');
                list_container.appendChild(this.render_paymentline(line));
            
            if(this.numpad_state){
                this.numpad_state.reset();
            }
        },
        render_paymentline: function(line){
            var el_html  = openerp.qweb.render('Paymentline',{widget: this, line: line});
                el_html  = _.str.trim(el_html);

            var el_node  = document.createElement('tbody');
                el_node.innerHTML = el_html;
                el_node = el_node.childNodes[0];
                el_node.line = line;
                el_node.querySelector('.paymentline-delete')
                    .addEventListener('click', this.line_delete_handler);
                el_node.addEventListener('click', this.line_click_handler);
                el_node.querySelector('input')
                    .addEventListener('keyup', this.line_change_handler);

            line.node = el_node;

            return el_node;
        },
        rerender_paymentline: function(line){
            var old_node = line.node;
            var new_node = this.render_paymentline(line);
            
            old_node.parentNode.replaceChild(new_node,old_node);
        },
        remove_paymentline: function(line){
            line.node.parentNode.removeChild(line.node);
            line.node = undefined;
        },
        renderElement: function(){
            this._super();

            var paymentlines   = this.pos.get('selectedOrder').get('paymentLines').models;
            var list_container = this.el.querySelector('.payment-lines');

            for(var i = 0; i < paymentlines.length; i++){
                list_container.appendChild(this.render_paymentline(paymentlines[i]));
            }
            
            this.update_payment_summary();
        },
        update_payment_summary: function() {
            var currentOrder = this.pos.get('selectedOrder');
            var paidTotal = currentOrder.getPaidTotal();
            var dueTotal = currentOrder.getTotalTaxIncluded();
            var remaining = dueTotal > paidTotal ? dueTotal - paidTotal : 0;
            var change = paidTotal > dueTotal ? paidTotal - dueTotal : 0;

            this.$('.payment-due-total').html(this.format_currency(dueTotal));
            this.$('.payment-paid-total').html(this.format_currency(paidTotal));
            this.$('.payment-remaining').html(this.format_currency(remaining));
            this.$('.payment-change').html(this.format_currency(change));
            if(currentOrder.selected_orderline === undefined){
                remaining = 1;  // What is this ? 
            }
                
            if(this.pos_widget.action_bar){
                this.pos_widget.action_bar.set_button_disabled('validation', !this.is_paid());
                this.pos_widget.action_bar.set_button_disabled('invoice', !this.is_paid());
            }
        },
        is_paid: function(){
            var currentOrder = this.pos.get('selectedOrder');
            return (currentOrder.getTotalTaxIncluded() < 0.000001 
                   || currentOrder.getPaidTotal() + 0.000001 >= currentOrder.getTotalTaxIncluded());

        },
        validate_order: function(options) {
            var self = this;
            options = options || {};

            var currentOrder = this.pos.get('selectedOrder');

            if(currentOrder.get('orderLines').models.length === 0){
                this.pos_widget.screen_selector.show_popup('error',{
                    'message': _t('Empty Order'),
                    'comment': _t('There must be at least one product in your order before it can be validated'),
                });
                return;
            }

            var plines = currentOrder.get('paymentLines').models;
            for (var i = 0; i < plines.length; i++) {
                if (plines[i].get_type() === 'bank' && plines[i].get_amount() < 0) {
                    this.pos_widget.screen_selector.show_popup('error',{
                        'message': _t('Negative Bank Payment'),
                        'comment': _t('You cannot have a negative amount in a Bank payment. Use a cash payment method to return money to the customer.'),
                    });
                    return;
                }
            }

            if(!this.is_paid()){
                return;
            }

            // The exact amount must be paid if there is no cash payment method defined.
            if (Math.abs(currentOrder.getTotalTaxIncluded() - currentOrder.getPaidTotal()) > 0.00001) {
                var cash = false;
                for (var i = 0; i < this.pos.cashregisters.length; i++) {
                    cash = cash || (this.pos.cashregisters[i].journal.type === 'cash');
                }
                if (!cash) {
                    this.pos_widget.screen_selector.show_popup('error',{
                        message: _t('Cannot return change without a cash payment method'),
                        comment: _t('There is no cash payment method available in this point of sale to handle the change.\n\n Please pay the exact amount or add a cash payment method in the point of sale configuration'),
                    });
                    return;
                }
            }

            if (this.pos.config.iface_cashdrawer) {
                    this.pos.proxy.open_cashbox();
            }

            if(options.invoice){
                // deactivate the validation button while we try to send the order
                this.pos_widget.action_bar.set_button_disabled('validation',true);
                this.pos_widget.action_bar.set_button_disabled('invoice',true);

                var invoiced = this.pos.push_and_invoice_order(currentOrder);

                invoiced.fail(function(error){
                    if(error === 'error-no-client'){
                        self.pos_widget.screen_selector.show_popup('error',{
                            message: _t('An anonymous order cannot be invoiced'),
                            comment: _t('Please select a client for this order. This can be done by clicking the order tab'),
                        });
                    }else{
                        self.pos_widget.screen_selector.show_popup('error',{
                            message: _t('The order could not be sent'),
                            comment: _t('Check your internet connection and try again.'),
                        });
                    }
                    self.pos_widget.action_bar.set_button_disabled('validation',false);
                    self.pos_widget.action_bar.set_button_disabled('invoice',false);
                });

                invoiced.done(function(){
                    self.pos_widget.action_bar.set_button_disabled('validation',false);
                    self.pos_widget.action_bar.set_button_disabled('invoice',false);
                    self.pos.get('selectedOrder').destroy();
                });

            }else{
                // no quiero que la lance al server antes de asociar cosas
                // this.pos.push_order(currentOrder) 
                if(this.pos.config.iface_print_via_proxy){
                    var receipt = currentOrder.export_for_printing();
					ted = currentOrder.export_for_ted();
                    console.log('TED');
					ted.then(function(data){
						receipt['signature'] = data['signature'];   
                        receipt['name'] = data['folio']  ;
                        currentOrder.attributes.folio =  data['folio'] ;
                        currentOrder.attributes.signature = data['signature'];
                        currentOrder.attributes.caf = data['caf'];
                        currentOrder.sequence_number = data['folio'];
                        receipt['date'] = currentOrder.get_date_of_printed().toString('dd/MM/yyyy HH:mm:ss');
                        self.pos.push_order(currentOrder) ;
						self.pos.proxy.print_receipt(QWeb.render('XmlReceipt',{
                        	receipt: receipt, widget: self,
                    	}));
                   	 self.pos.get('selectedOrder').destroy();    //finish order and go back to scan screen
					});
                   
                }else{
                    this.pos_widget.screen_selector.set_current_screen(this.next_screen);
                }
            }

            // hide onscreen (iOS) keyboard 
            setTimeout(function(){
                document.activeElement.blur();
                $("input").blur();
            },250);
        },
        enable_numpad: function(){
            this.disable_numpad();  //ensure we don't register the callbacks twice
            this.numpad_state = this.pos_widget.numpad.state;
            if(this.numpad_state){
                this.numpad_state.reset();
                this.numpad_state.changeMode('payment');
                this.numpad_state.bind('set_value',   this.set_value, this);
                this.numpad_state.bind('change:mode', this.set_mode_back_to_payment, this);
            }
                    
        },
        disable_numpad: function(){
            if(this.numpad_state){
                this.numpad_state.unbind('set_value',  this.set_value);
                this.numpad_state.unbind('change:mode',this.set_mode_back_to_payment);
            }
        },
    	set_mode_back_to_payment: function() {
    		this.numpad_state.set({mode: 'payment'});
    	},
        set_value: function(val) {
            var selected_line =this.pos.get('selectedOrder').selected_paymentline;
            if(selected_line){
                selected_line.set_amount(val);
                selected_line.node.querySelector('input').value = selected_line.amount.toFixed(2);
            }
        },
    });



};
