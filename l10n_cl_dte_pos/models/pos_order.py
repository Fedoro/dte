# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT as DF

from datetime import datetime
from elaphe import barcode
import os, tempfile
import base64

class pos_order(models.Model):
	_inherit = 'pos.order'

	def _calc_chilean_datetime(self):
		return fields.Datetime.context_timestamp(self, timestamp=datetime.now())

	@api.model
	def create_from_ui(self, orders):
		res = super(pos_order,self).create_from_ui(orders)
		return res

	@api.v7
	def _order_fields(self, cr, uid, ui_order, context=None):
		order_data =  super(pos_order,self)._order_fields(cr, uid, ui_order, context=context)
		order_data['ted'] = ui_order['signature'] or None
		order_data['folio'] = ui_order['folio'] or None
		order_data['caf'] = ui_order['caf'] or None
		order_data['date_order'] = ui_order['date_order'] or None
		return order_data

	@api.multi
	def print_order(self):
		# dummy button
		pass

	@api.multi
	def data_for_print(self):
		self.ensure_one
		data = {}

		data['company'] = {}
		data['orderlines'] = []
		data['paymentlines'] = []
		data['tax_details'] = []

		data['connection'] = {}
		data['connection']['iface_print_via_proxy'] = self.session_id.config_id.iface_print_via_proxy
		data['connection']['proxy_ip'] = 'http://%s:8069' % self.session_id.config_id.proxy_ip

		company = self.session_id.config_id.journal_id.company_id
		data['company']['fantasy_name'] = company.fantasy_name
		data['company']['rut'] = company.rut.replace('.','')
		data['company']['name'] = company.name
		data['company']['giro'] = company.partner_id.line_of_business

		if company.street2:
			street = '%s %s, %s' % (company.street, company.street2 , company.city)
		else:
			street =  '%s, %s' % (company.street, company.city)

		data['company']['street'] = street
		data['company']['resolution_number'] = company.resolution_number
		data['company']['resolution_date'] =  '%s-%s-%s'   %(  company.resolution_date[8:10],company.resolution_date[5:7],company.resolution_date[0:4] )
		data['company']['website'] = company.website
		data['date'] = '%s' % (datetime.strptime(self.chilean_date, DF).strftime('%d/%m/%Y %H:%M:%S'))
		# receipt.date.localestring
#		print data['date']
		data['name'] = self.folio

		# receipt.name
		data['cashier'] = self.user_id.name
		# receipt.cashier

		for line in self.lines:
			if line.product_id.uom_id.name == 'Unit(s)':
				uom = 'Unidad(es)'
			else:
				uom = line.product_id.uom_id.name

			if line.product_id.ean13 :
				product_name = '[%s] %s' % (line.product_id.ean13 , line.product_id.name)
			elif line.product_id.default_code :
				product_name = '[%s] %s' % (line.product_id.default_code, line.product_id.name)
			else :
				product_name = line.product_id.name

			data['orderlines'].append({
				'discount': int(line.discount),
				'unit_name': uom,
				'quantity': int(line.qty),
				'product_name': product_name,
				'price': int(line.price_subtotal_incl),
				'price_display': int(line.price_subtotal_incl),
				'price_with_tax' : int(line.price_subtotal_incl)
				})

		data['subtotal'] = self.amount_total
		data['total_with_tax'] = self.amount_total
		change = 0
		for line in self.statement_ids:
			if line.amount >= 0:
				data['paymentlines'].append({
					'journal': '%s (%s)' % (line.journal_id.name, line.journal_id.currency.name if line.journal_id.currency else line.journal_id.company_id.currency_id.name  ),
					'amount': line.amount
					})
			else:
				change += line.amount

		data['change'] = change * -1
		data['signature'] = self.ted
		return data

	@api.multi
	@api.depends('lines.product_id')
	def _compute_amount(self):
		for order in self:
			for line in order.lines:
				if line.product_id is not None:
					for tax in line.product_id.taxes_id:
						if tax.description == 'EXEV':
							order.amount_exempt = order.amount_exempt + line.price_unit
					if line.price_subtotal:
						order.amount_untaxed = order.amount_untaxed + line.price_subtotal

	ted = fields.Text('Ted')
	folio = fields.Integer('Folio')
	caf = fields.Many2one('l10n_cl_dte.caf', 'CAF utilizado para este documento', domain="[('journal_id','=',journal_id)]", copy=False)
	amount_exempt = fields.Float('Amount Exent',  digits=dp.get_precision('Account'),
					store=True, readonly=True, compute='_compute_amount', track_visibility='always')
	amount_untaxed = fields.Float(string='Amount Untaxed', digits=dp.get_precision('Account'),
					store=True, readonly=True, compute='_compute_amount', track_visibility='always')
	chilean_date = fields.Datetime('chilean date', default=_calc_chilean_datetime)
	pdf_417 = fields.Binary(string='PDF 417', compute='_get_pdf417',copy=False)

	@api.depends('ted')
	def _get_pdf417(self):
		for record in self:
			if not record.ted:
				continue
			image_tmp_file = tempfile.NamedTemporaryFile()
			code = barcode('pdf417', record.ted.encode('iso-8859-1'), dict(columns=18, rows=5, eclevel=5) )
			image_path = image_tmp_file.name
			code.save(image_path, format='png')
			with open(image_path, 'rb') as imageFile:
				image = base64.encodestring(imageFile.read())
			record.pdf_417 = image
			image_tmp_file.close()

	@api.model
	def create(self , values):
		order = super(pos_order,self).create( values)
		ted = self.get_ted(order=order, data=None)
		order.folio = ted['folio']
		order.ted = ted['signature'] 
		order.caf = ted['caf']
		return order

	@api.model
	def folio_consume(self, order=None, journal_id=None):
	
		caf = self.env['l10n_cl_dte.caf']

		if order :
			journal_id = order.session_id.config_id.journal_id.id
		elif journal_id :
			journal_id = journal_id

		cafs = caf.search([('journal_id','=',journal_id),('state','=','active')])
		
		if not cafs:
			raise Warning(_('No active caf records found for this journal. Please Check'))
			#No se han encontrado CAF activos para este diario. Favor verificar
		for caf in cafs:
			if (caf.end_number - caf.next_number) < 0:
				raise Warning(_('There are no more folios for these documents'))
						#Ya no quedan folios para este tipo de documentos.
			folio = caf.folio_consume()
			
			return {'number' : folio[0] , 'caf' : caf}

	@api.model
	def get_ted(self , data=None, order=None, journal_id=None):
		# get folio number for this order

		if order:
			print "Es desde el backend"
			if order.folio == 0:
				print "No tiene folio"
				folio = self.folio_consume(order = order )
				print 
				number= folio['number']
				caf = folio['caf']
			else:
				print "Tiene folio"
				number = order.folio
				caf = order.caf
		elif journal_id:
			print "es desde el frontend"
			folio = self.folio_consume(journal_id = journal_id )
			number= folio['number']
			caf = folio['caf']
		# folio = self.folio_consume(order = order if order else None,journal_id= journal_id if journal_id else None )

		document_obj = type('Document', (object,), {})
		document = document_obj()

		if data is None:
			document._name = 'pos.order'
			document.rut_emisor = order.session_id.config_id.journal_id.company_id.rut
			document.document_type = order.session_id.config_id.journal_id.doc_type_id.sii_code
			document.folio =  number
			document.date =  order.chilean_date
			document.receptor_rut =   order.partner_id.rut if order.partner_id else '66666666-6'
			document.receptor_name =  order.partner_id.name if order.partner_id else 'CLIENTE OCACION'
			document.total_amount =  order.amount_total
			document.first_item =  order.lines[0].product_id.name
			document.caf =  caf.caf
		else:
			document._name = 'pos.order'
			document.rut_emisor = data['rut_emisor']
			document.document_type = data['document_type']
			document.folio =  number
			document.date =  data['date']
			document.receptor_rut =   data['receptor_rut']
			document.receptor_name =   data['receptor_name']
			document.total_amount =  int(data['total_amount'])
			document.first_item =  data['first_item']
			document.caf =  caf.caf

		render_ted = self.env['dte.templates'].get_ted(document)
		signature = self.env['l10n_cl_dte.signer'].signed_ted(render_ted, caf.private_key)
		signature =  signature.decode('iso-8859-1').encode('utf8')
		return  {'signature': signature, 'folio': number , 'caf': caf.id}
pos_order()
