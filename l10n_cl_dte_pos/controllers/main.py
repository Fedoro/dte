# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

import logging
import pprint
import urllib2
import werkzeug

from openerp import http, SUPERUSER_ID
from openerp.addons.web.http import request
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from openerp import fields

class WebsitePOS(http.Controller):
	@http.route('/pos/search', type='http', auth='public',website=True)
	def pos_search(self, **post):
		print post
		if len(post) == 0:
			return request.website.render("l10n_cl_dte_pos.search")
		else:
			cr, uid, context = request.cr, SUPERUSER_ID, request.context
			# date = fields.Date.from_string(post['date_of_order'])
			date = post['date_of_order']

			date = '%s-%s-%s' % (date[6:12],date[3:5],date[0:2])

			print date
			domain=[
			('chilean_date','>', date + ' 00:00:00'),
			('chilean_date','<', date + ' 23:59:59'),
			('folio','=',int(post['numero']))
			]

			data = request.registry['pos.order'].search(cr, uid, domain, context=context)
			if data:
				url = 'report/pdf/l10n_cl_dte_pos.report_pos_electronic/%s' % data[0]
				return werkzeug.utils.redirect(url)
			else:
				data = {}
				data['error'] = 'error'
				return request.website.render("l10n_cl_dte_pos.search", data)
WebsitePOS()
