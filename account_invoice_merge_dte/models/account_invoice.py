# -*- coding: utf-8 -*-

from openerp import models, api
from openerp.exceptions import Warning

class AccountInvoiceMergeDTE(models.Model):
	_inherit = "account.invoice"

	@api.model
	def _get_invoice_key_cols(self):
		return ['partner_id', 'user_id', 'type', 'account_id', 'currency_id', 'journal_id',
				'company_id', 'partner_bank_id', 'dte', 'tax_included', 'caf']

	@api.model
	def _get_invoice_line_key_cols(self):
		fields = ['name', 'origin', 'discount', 'invoice_line_tax_id', 'price_unit',
				'product_id', 'account_id', 'account_analytic_id', 'exension_index']

		for field in ['analytics_id']:
			if field in self.env['account.invoice.line']._fields:
				fields.append(field)
		return fields

	@api.model
	def _get_first_invoice_fields(self, invoice):
		res = super(AccountInvoiceMergeDTE, self)._get_first_invoice_fields(invoice)
		res.update({'dte': invoice.dte})
		return res
AccountInvoiceMergeDTE()
