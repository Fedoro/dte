# -*- coding: utf-8 -*-

{
    'name': 'Account Invoice Merge DTE Wizard',
    'version': '8.0.1',
    'category': 'Finance',
    'author': "[BTeC Ltda.]",
    'website': 'www.raiquen.cl',
    'description': """
Module add some dte fields to the merge function.
""",
    'depends': ['l10n_cl_dte_invoice', 'account_invoice_merge'],
    'installable': True
}
