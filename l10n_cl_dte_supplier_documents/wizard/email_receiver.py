# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _
import xml.etree.ElementTree as etree
import base64, re, logging

_logger = logging.getLogger(__name__)

class l10n_cl_dte_supplier_document_email_receiver(models.TransientModel):
	_name = 'l10n_cl_dte.email_receiver'
	_description = 'Incoming DTE Email'
	_inherit = ['mail.thread']

	def _what_is_it(self, root_tag):
		"""
			es una respuesta a un libro electronico ? si lo es responde un True
		"""
		doc_type = ''
		
		if 'ResultadoEnvioLibro' == root_tag.tag:
			doc_type = 'book'
		elif 'RESULTADO_ENVIO' == root_tag.tag:
			doc_type = 'document'
		elif 'ResultadoConsumoFolios' == root_tag.tag:
			doc_type = 'folio_consume'
		else:
			raise Warning('Se esta procesando una respuesta que no es una respueta de un dte, libro o consumo de folio')
		return doc_type

	def _identification_tag(self, root_tag, is_book=False, is_folio_consume=False):
		"""
			process the xml and return the identification tag, if has one 
			ALL DOCUMENTS
		"""

		identification = {}
		if is_folio_consume:
			identification_tag = root_tag.find('Identificacion').find('Envio')
			# send_tag = identification_tag.find('Envio')

		else:
			identification_tag = root_tag.find('Identificacion' if is_book else 'IDENTIFICACION')
			
		identification['track_id'] = identification_tag.find('TrackId' if is_book or is_folio_consume  else 'TRACKID').text
		identification['rut_envia'] = identification_tag.find('RutEnvia' if is_book or is_folio_consume else 'RUTENVIA').text
		identification['fecha_recepcion'] = identification_tag.find('TmstRecepcion' if is_book or is_folio_consume else 'TMSTRECEPCION').text
		if is_folio_consume:
			result_tag = root_tag.find('Resultado')
			status_tag = result_tag.find('Estado')
			identification['status'] = status_tag.text
		else:
			identification['status'] = identification_tag.find('EstadoEnvio' if is_book or is_folio_consume else 'ESTADO').text
		
		# en la identificacion puede tener mas campos si es libro electronico
		if is_book:
			if identification_tag.find('TipoSegmento') is not None :
				identification['segment_type'] = identification_tag.find('TipoSegmento').text
			if identification_tag.find('NroSegmento') is not None :
				identification['segment_number'] = identification_tag.find('NroSegmento').text
			if identification_tag.find('TipoLibro') is not None :
				identification['book_type'] = identification_tag.find('TipoLibro').text
			if identification_tag.find('TipoOperacion') is not None:
				identification['operation_type'] = identification_tag.find('TipoOperacion').text
			if identification_tag.find('PeriodoTributario') is not None:
				identification['period'] = identification_tag.find('PeriodoTributario').text
			if identification_tag.find('EstadoLibro') is not None:
				identification['book_status'] = identification_tag.find('EstadoLibro').text

		if is_folio_consume:
			if identification_tag.find('TamanoArchivo') is not None :
				identification['file_length'] = identification_tag.find('TamanoArchivo').text
			if identification_tag.find('TipoEnvio') is not None :
				identification['send_type'] = identification_tag.find('TipoEnvio').text

		return identification

	def _statistics_tag(self, root_tag):
		""" 
			Estadisticas del envio, solo para documentos
		"""
		statistics_tag = root_tag.find('ESTADISTICA')
		statistics = {}
		if statistics_tag is not None:
			subtotal_tag = statistics_tag.find('SUBTOTAL') 
			if subtotal_tag is not None: 
				if subtotal_tag.find('TIPODOC') is not None:
					statistics['doc_type'] =  subtotal_tag.find('TIPODOC').text
				if subtotal_tag.find('INFORMADO') is not None:
					statistics['informed'] =  subtotal_tag.find('INFORMADO').text
				if subtotal_tag.find('RECHAZO') is not None:
					statistics['status'] = 'rejected'   
				if subtotal_tag.find('ACEPTA') is not None:
					statistics['status'] = 'acepted'   
				if subtotal_tag.find('REPARO') is not None:
					statistics['status'] = 'objected'
		else:
			statistics = None 

		return statistics

	def _error_tag(self, root_tag, is_book=False, is_folio_consume=False):
		error= ''

		if is_folio_consume:			
			result_tag = root_tag.find('Resultado')
			if result_tag is not None:
				status_tag = result_tag.find('Estado')
				if status_tag is not None:
					if status_tag.text == 'ERRONEO':
						errors_tags = result_tag.find('Errores')
						if errors_tags is not None:
							for error_tag in errors_tags.findall('Error'):
								code = error_tag.find('Codigo').text
								description = error_tag.find('Descripcion').text
								details = error_tag.find('Detalle').text
								error += '%s - %s : %s\n' % (code,description,details)
					if status_tag.text == 'REPARO':	
						reparos_tags = result_tag.find('Reparos')
						if reparos_tags is not None:
							for reparo_tag in reparos_tags.findall('Reparo'):
								code = reparo_tag.find('Codigo').text
								description = reparo_tag.find('Descripcion').text
								details = reparo_tag.find('DETALLE').text
								error += '%s - %s : %s\n' % (code,description,details)
		elif is_book:
			error_tag = root_tag.find('ErrorEnvioLibro' if is_book else 'ERRORENVIO')
			if error_tag is not None:
				error_messajes_tag = error_tag.findall('DetErrEnvio' if is_book else 'DETERRENVIO')
				if error_messajes_tag is not None:
					for error_tag in error_messajes_tag:
						error += '%s\n' % error_tag.text 
		else:
			revision_tag = root_tag.find('REVISIONENVIO')
			if revision_tag is not None:
				dte_tag = revision_tag.find('REVISIONDTE')
				if dte_tag is not None:
					for element in dte_tag.iter():
						if element.tag == 'ESTADO':
							estado = element.text
						if element.tag == 'DETALLE':
							detalle = '%s\n' % (element.text)
					if 'RCH' in estado:
						error = detalle
		return error

	@api.model
	def _search_model(self, track_id, is_book=False, is_folio_consume=False):
		if is_book:
			return self.env['l10n_cl_dte.books'].search([('track_id','=',track_id)])
		elif is_folio_consume:
			return self.env['l10n_cl_dte_invoice_receipt.daily_book'].search([('track_id','=',track_id)])
		else:
			invoice =  self.env['account.invoice'].search([('track_id','=',track_id)])
			stock = self.env['l10n_cl_dte.stock'].search([('track_id','=',track_id)])
			if invoice:
				return invoice
			else:
				return stock

	@api.model
	def process_sii_answer(self, mail_xml):
		# Se tiene que analizar que te responde el servicio, dado que las respuesta de los libros,
		# consumo de folios, y envio de documentos es distinta 

		mail_xml = re.sub(' xmlns="[^"]+"', '', mail_xml, count=1)
		root = etree.fromstring(mail_xml)

		response_type = self._what_is_it(root)
			
		is_book = True if response_type == 'book' else False
		is_folio_consume = True if response_type == 'folio_consume' else False
		if is_folio_consume:
			root = root.find('DocumentoResultadoConsumoFolios')

		identification =  self._identification_tag(root, is_book=is_book, is_folio_consume=is_folio_consume)
		statistics =  self._statistics_tag(root)
		errors = self._error_tag(root, is_book=is_book, is_folio_consume=is_folio_consume)

		model = self._search_model(identification['track_id'], is_book=is_book, is_folio_consume=is_folio_consume)
		if model:
			if is_book:
				if 'LOK' in identification['status'] or 'LTC' in identification['status']:
					model.write({'state': 'validate', 'failed_text': None})
				elif 'LRT' in identification['status']:
					model.write({'state': 'rectified', 'failed_text': None})
				elif 'LSO' in identification['status']:
					model.write({'failed_text': _('Book in process of validation.')})
				elif 'LNC' in identification['status'] or 'LRH' in identification['status'] or 'LRS' in identification['status']:
					model.write({'failed_text': errors, 'state': 'error'})
				else:
					model.write({'failed_text': errors})
			elif is_folio_consume:
				if identification['status'] == 'CORRECTO':
					model.write({'state': 'validate', 'failed_text': None})
				elif identification['status'] == 'ERRONEO':
					model.write({'state': 'error', 'failed_text': errors})
				elif identification['status'] == 'REPARO':
					model.write({'state': 'validate', 'failed_text': errors})
			else:
				if statistics:
					if statistics['status'] == 'acepted':
						model.write({'failed_text': None, 'status_text': None})
						if model._name == 'account.invoice':
							model.signal_workflow('invoice_open')
						elif model._name == 'l10n_cl_dte.stock':
							model.write({'state': 'validated'})
					elif statistics['status'] == 'rejected':
						model.write({'failed_text': errors,  'status_text': None})
						if model._name == 'account.invoice':
							model.signal_workflow('waiting_to_failed')
						elif model._name == 'l10n_cl_dte.stock':
							model.write({'state': 'error', 'failed_text': errors})
					elif statistics['status'] == 'objected':
						model.write({'failed_text': None, 'status_text': errors})
						if model._name == 'account.invoice':
							model.signal_workflow('invoice_open')
						elif model._name == 'l10n_cl_dte.stock':
							model.write({'state': 'validated', 'status_text': errors})
				else:
					if errors:
						model.write({'failed_text': errors,  'status_text': None})
						if model._name == 'account.invoice':
							model.signal_workflow('waiting_to_failed')
						elif model._name == 'l10n_cl_dte.stock':
							model.write({'state': 'error', 'failed_text': errors})
		return identification['track_id']

	@api.one
	def incomming_mail(self):
		ir_pool = self.env['ir.attachment']
		sup_ids = []
		attach_ids = []
		white_list = ['siidte_error@sii.cl','siidte_reparo@sii.cl', 'siidte@sii.cl']

 		for mail in self:
			try:
				sup_doc = False
				message = self.env['mail.message'].search([('model','=','l10n_cl_dte.email_receiver'),('res_id','=',mail.id)])
				if message.email_from in white_list :
					attach_objs = ir_pool.search([('name','like','.xml'),('res_id','=',mail.id),
						('res_model','=','l10n_cl_dte.email_receiver')])
					for attach in attach_objs:
						attach_ids.append(attach.id)
						xml = base64.b64decode(attach.datas)
						self.process_sii_answer(xml)
				else:
					#En caso que no sea de sii, tengo que buscar los adjuntos que se crearon pa cachar que onda
					attach_objs = ir_pool.search([('name','like','.xml'),('res_id','=',mail.id),
										('res_model','=','l10n_cl_dte.email_receiver')])
					for attach in attach_objs:
						attach_ids.append(attach.id)
						xml = base64.b64decode(attach.datas)
						root = etree.fromstring(xml)
						for element in root.iter():
							if 'EnvioDTE' in element.tag:
								ctx = dict(self._context)
								sup_ids = self.env['l10n_cl_dte.supplier_document'].make_document(xml, \
													message.reply_to or message.email_from)
								for sup_id in sup_ids:

									new_attach = attach.copy()
									new_attach.write({'name': attach.name, 'res_id': sup_id.id,
												'res_model': 'l10n_cl_dte.supplier_document'})

									try:
										ctx['active_id'] = sup_id.id
										#Se debe validar el XML y la caratula de la recepcion
										#(pasar a funcion y los vals de respuesta se envian para
										#creacion del objeto.
										send_obj = self.env['l10n_cl_dte.send_ack'].create({
									'ack_type': 'ack', 'ack_state': '0', 'ack_doc_state': '0'})
										send_obj.with_context(ctx).send_ack()
									except:
										continue
						attach.unlink()
					for sup_id in sup_ids:
						for other in ir_pool.search([('res_model','=','l10n_cl_dte.email_receiver'),\
											('id','not in',attach_ids),('res_id','=',mail.id)]):
							new_other = other.copy()
							new_other.write({'name': other.name, 'res_id': sup_id.id,
										'res_model': 'l10n_cl_dte.supplier_document'})
							other.unlink()

			except Exception as e:
				_logger.error(e)
				_logger.error('Error en email %s id %s y asunto: %s' % (message.email_from, message.id, message.subject))
		return True
l10n_cl_dte_supplier_document_email_receiver()
