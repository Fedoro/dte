# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from datetime import datetime as dt
from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import Warning
import base64

class l10n_cl_dte_send_ack(models.TransientModel):
	_name = 'l10n_cl_dte.send_ack'

	ack_type = fields.Selection([('ack','Ack'),('commercial', 'Commercial Acceptance'),\
						('receiving_goods','Receiving Goods')], 'Ack Type', required=True)
	enclosure = fields.Char('Enclosure')
	contact_name = fields.Char('Contact Name')
	contact_phone = fields.Char('Contact Phone')
	contact_email = fields.Char('Contact Email')
	ack_state = fields.Selection([('0','Accepted'),
					('1','Schema Error'),
					('2','Sign Error'),
					('3','Receptor Rut'),
					('90','Duplicated File'),
					('91', 'Unreadable File'),
					('99', 'Other')], 'Ack State')
	ack_note = fields.Text('Ack Note')
	ack_doc_state = fields.Selection([('0', 'Accepted'),
					('1', 'Sign Error'),
					('2', 'Issuer RUT Error'),
					('3', 'Receiver RUT Error'),
					('4', 'Duplicated DTE'),
					('5', 'Others')], 'Ack Document State')
	ack_doc_note = fields.Text('Ack Document Note')
	acceptance_state = fields.Selection([('ACD','ACD'),('RCD','RCD')], 'Acceptance State',
						help='ACD: Acepta Contenido del Documento\n' +\
							'RCD: Reclamo al Contenido del Documento')
	acceptance_note = fields.Text('Acceptance Note')
	goods_state = fields.Selection([('ERM', 'ERM'), ('RFP','RFP'), ('RFT','RFT')], 'Goods Answer', 
						help='ERM: Otorga Recibo de Mercaderías o Servicios.\n'+\
							'RFP: Reclamo por Falta Parcial de Mercaderías.\n'+\
							'RFT: Reclamo por Falta Total de Mercaderías.')
	company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env['res.company'].\
									_company_default_get('l10n_cl_dte.send_ack'))

	@api.model
	def planned_send_mail(self, wizard, sup_doc):
		if wizard.ack_type == 'ack':
			template = self.env['ir.model.data'].get_object('l10n_cl_dte_supplier_documents', \
									'l10n_cl_dte_supplier_document_send_ack_template')
		elif wizard.ack_type == 'commercial':
			template = self.env['ir.model.data'].get_object('l10n_cl_dte_supplier_documents', \
								'l10n_cl_dte_supplier_document_commercial_acceptance_template')
		elif wizard.ack_type == 'receiving_goods':
			template = self.env['ir.model.data'].get_object('l10n_cl_dte_supplier_documents', \
									'l10n_cl_dte_supplier_document_receiving_goods_template')
		assert template._name == 'email.template'
		mail_id = template.send_mail(sup_doc.id, force_send=True, raise_exception=True)
		sup_doc.message_post(body=_('DTE Ack Sent'))
		return True

	@api.model
	def do_old_ack(self, wizard, cert, sup_doc, vals, ack_attach, send_type):
		template_pool = self.env['dte.templates']
		signer_pool = self.env['l10n_cl_dte.signer']

		ack_xml = template_pool.get_ack(vals, wizard.ack_type)
		signed_ack_send = signer_pool.signed_document(ack_xml, cert.cert, cert.private_key, cert.password)

		if sup_doc.tipo_dte in ['33', '34', '46']:
			doc_name = 'Factura'
		elif sup_doc.tipo_dte == '43':
			doc_name = 'Liquidacion-Factura'
		elif sup_doc.tipo_dte == 56:
			doc_name = 'Nota de Credito'
		elif sup_doc.tipo_dte == '61':
			doc_name = 'Nota de Debito'
		elif sup_doc.tipo_dte == '52':
			doc_name = 'Guia de Despacho'

		ack_attach.write({'datas': base64.b64encode(signed_ack_send), \
					'datas_fname': '[%s] %s.%s_%s.xml' % (send_type, sup_doc.tipo_dte, doc_name, sup_doc.folio)})
		self.planned_send_mail(wizard, sup_doc)
		return True

	@api.multi
	def send_ack(self):
		self.env['res.company'].comprobar_modulo()

		ir_attach_pool = self.env['ir.attachment']
		template_pool = self.env['dte.templates']
		sup_doc_pool = self.env['l10n_cl_dte.supplier_document']
		ir_data_pool = self.env['ir.model.data']
		signer_pool = self.env['l10n_cl_dte.signer']
		cert_pool = self.env['l10n_cl_dte_signer.certificate']
		sii_send = self.env['l10n_cl_dte.sii']

		try:
			ack_attach = ir_data_pool.get_object('l10n_cl_dte_supplier_documents', 'l10n_cl_dte_ack_attachment')
		except:
			raise Warning(_('The ack attachment has been deleted, please contact your support team'))

		for wizard in self:
			resp = None
			vals = {}
			states = {}
			#Pasar a modulo de envio
			cert = cert_pool.search([('company_id','=',wizard.company_id.id),('state','=','active')], limit=1)
			if not cert:
				raise Warning(_('You haven\'t an active certificate'))
			else:
				cert = cert[0]

			sup_doc = sup_doc_pool.browse(self._context['active_id'])

			vals.update({
				'rut_recep': sup_doc.rut_recep,
				'rut_emisor': sup_doc.rut_emisor,
				'id_resp': '%s%s%s' % (sup_doc.id, wizard.id, dt.now().strftime('%Y%m%d%H%M%S')),
				'contact_name': wizard.contact_name if wizard.contact_name else '',
				'contact_phone': wizard.contact_phone if wizard.contact_phone else '',
				'contact_email': wizard.contact_email if wizard.contact_email else '',
				'tipo_dte': sup_doc.tipo_dte,
				'folio': sup_doc.folio,
				'fch_emis': sup_doc.fch_emis,
				'mnt_total': sup_doc.mnt_total,
			})

			if wizard.ack_type == 'ack':
				if sup_doc.ack:
					raise Warning(_('You can only send once every ack.'))

				states.update({'ack': True})

				attach = ir_attach_pool.search([('res_model','=','l10n_cl_dte.supplier_document'),\
										('name','ilike','.xml'),
										('res_id','=',sup_doc.id)])
				if not attach:
					raise Warning(_('The supplier document %s hasn\'t an attachment') % (sup_doc.name))

				vals.update({
					'attach_name': attach.name,
					'create_date': sup_doc.create_date.replace(' ','T'),
					'send_cdg': '%s%s%s' % (sup_doc.id, wizard.id, dt.now().strftime('%Y%m%d%H%M%S')),
					'dte_id': '%s%s' % (sup_doc.id, wizard.id),
					#'digest': buscar en la firma,
					'ack_state': wizard.ack_state,
					'ack_note': wizard.ack_note if wizard.ack_state != '0' else 'Envio Recibido Conforme.',
					'ack_doc_state': wizard.ack_doc_state,
					'ack_doc_note': wizard.ack_doc_note if wizard.ack_doc_state != '0' else 'DTE Recibido Conforme.'
				})
				self.do_old_ack(wizard, cert, sup_doc, vals, ack_attach, 'AcuseRecibo')
			elif wizard.ack_type == 'commercial':
				if sup_doc.commercial_state != 'not_send':
					raise Warning(_('You can only send once every ack.'))

				states.update({'commercial_state': wizard.acceptance_state})

				vals.update({
					'send_cdg': '',#Codigo de envio que recibio el doc_recibido
					'acceptance_state': '1' if wizard.acceptance_state == 'RCD' else '0',
					'acceptance_note': wizard.acceptance_note if wizard.acceptance_state != '0' else 'DTE Aceptado Conforme.'
				})

				resp = sii_send.send_sii_ack(sup_doc, wizard.acceptance_state)
				if resp:
					if resp.get('codResp', None):
						if resp['codResp'] != 0:
							raise Warning(resp['descResp'])
				self.do_old_ack(wizard, cert, sup_doc, vals, ack_attach, 'AceptacionComercial')
			elif wizard.ack_type == 'receiving_goods':
				if sup_doc.receiving_goods_state != 'not_send':
					raise Warning(_('You can only send once every ack.'))

				states.update({'receiving_goods_state': wizard.goods_state})

				if wizard.goods_state == 'ERM':
					vals.update({
						'enclosure': wizard.enclosure if wizard.enclosure \
								else 'Comprobante Mercaderia/Servicio recibido.',
						'sign_rut': cert.owner_rut
					})

					recibo_xml = template_pool.get_receiving_goods(vals)
					vals.update({
						'sign_xml_recibo': signer_pool.signed_document(recibo_xml, \
										cert.cert, cert.private_key, cert.password)[22:]
					})

					resp = sii_send.send_sii_ack(sup_doc, wizard.goods_state)
					if resp:
						if resp.get('codResp', None):
							if resp['codResp'] != 0:
								raise Warning(resp['descResp'])
					self.do_old_ack(wizard, cert, sup_doc, vals, ack_attach, 'RecepcionMercaderia')
			sup_doc.write(states)
		return True

############### CARATULA ENVIO ################
#		RutResponde -> rut_recep (O rut en la company)
#		RutRecibe -> rut_emisor
#		IdRespuesta -> idDocumento+idWizard+timestamp (id_resp)
#		NroDetalles -> 1 (siempre envia uno no ma)
#		NmbContacto -> Personal para aclarar dudas (se anota a mano o define para company)
#		FonoContacto -> Se saca de la company o campo a crear para este fin (Deberia ser de la company.
#		MailContacto -> Lo mismo que el anterior.
#		TmstFirmaResp -> Sacar de otras firmas 
###############################################
############## RECEPCION ENVIO ################
#		NmbEnvio -> "nombre adjunto"
#		FchRecep -> create date -> Formatear aaaa-mm-ddTHH:MM:SS
#		CodEnvio -> idDocumento+idWizard+timestamp
#		EnvioDTEID -> Preguntar a NELSON -> ??????
#		Digest -> Del XML recibido sacar el DigestValue (OPCIONAL)
#		RutEmisor -> rut_emisor
#		RutReceptor -> rut_recep
#		EstadoRecepEnv -> ack_state -> wizard
#		RecepEnvGlosa -> ack_note -> wizard
#		NroDTE -> 1 (siempre se envia uno por acuse)
################################### RecepcionDTE ##########################################
####################### OPCIONAL EN CASO DE RECHAZO SE RECOMIENDA INCLUIR #################
#		TipoDTE -> tipo_dte
#		Folio -> folio
#		FchEmis -> fch_emis
#		RUTEmisor -> rut_emisor
#		RUTRecep -> rut_recep
#		MntTotal -> mnt_total
#		EstadoRecepDTE -> ack_doc_state -> wizard
#		RecepDTEGlosa -> ack_doc_note -> wizard
###########################################################################################
###############################################
################ RESULTADO DTE ################
#		TipoDte -> tipo_dte
#		Folio -> folio
#		FchEmis -> fch_emis
#		RUTEmisor -> rut_emisor
#		RUTRecep -> rut_recep
#		MntTotal -> mnt_total
#		CodEnvio -> ?????? -> Preguntar A NELSON (send_cdg)
#		EstadoDTE -> acceptance_state -> wizard
#		CodRchDsc -> acceptance_note -> wizard (Obligatorio si rechaza)
###############################################
l10n_cl_dte_send_ack()
