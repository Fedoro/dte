
from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _

class batch_send_acks(models.TransientModel):
	"""
	This wizard will send the selected aceptance to the selected suppliers documents
	"""

	_name = "batch_send_acks"
	_description = "Send the selected supplier documents acks"

	@api.multi
	def commercial_acceptance(self):
		wizard = self.env['l10n_cl_dte.send_ack'].create({'ack_type': 'commercial', 'acceptance_state': 'ACD',\
					'company_id': self.env['res.company']._company_default_get('l10n_cl_dte.supplier_document')})
		ctx = self._context.copy()

		for doc in self.env['l10n_cl_dte.supplier_document'].browse(self._context.get('active_ids', [])):
			ctx.update({'active_id': doc.id})
			wizard.with_context(ctx).send_ack()

		return {'type': 'ir.actions.act_window_close'}

	@api.multi
	def goods_acceptance(self):
		wizard = self.env['l10n_cl_dte.send_ack'].create({'ack_type': 'receiving_goods', 'goods_state': 'ERM',\
					'company_id': self.env['res.company']._company_default_get('l10n_cl_dte.supplier_document')})
		ctx = self._context.copy()

		for doc in self.env['l10n_cl_dte.supplier_document'].browse(self._context.get('active_ids', [])):
			ctx.update({'active_id': doc.id})
			wizard.with_context(ctx).send_ack()

		return {'type': 'ir.actions.act_window_close'}
batch_send_acks()
