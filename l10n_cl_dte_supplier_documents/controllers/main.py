# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import http
import logging, json
_logger = logging.getLogger(__name__)

class DteController(http.Controller):
    @http.route('/l10n_cl_dte/webhooks', type='json', auth='user', csrf=False, methods=['POST'])
    def execution_data(self, **data):
        if not data:
            data = json.loads(http.request.httprequest.data)
        if data['function'] and data['invoice_ids']:
            return self.do(data)
        return False

    def do(self, data):
        req = http.request
        invoices = req.env['account.invoice'].browse(data['invoice_ids'])
        if data['function'] == 'sign_document':
            return invoices.signal_workflow('sign_document')
        elif data['function'] == 'send_document':
            return invoices.signal_workflow('send_document')
        elif data['function'] == 'get_send_state':
            return invoices.get_send_state()

