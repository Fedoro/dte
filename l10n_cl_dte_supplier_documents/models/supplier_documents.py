# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _
import xml.etree.ElementTree as etree
# from collections import defaultdict

import base64

import xmltodict
import json 

from pprint import pprint
import logging

_logger = logging.getLogger(__name__)

DTE_FIELDS = {
	'TipoDTE': 'dte_type',
	'Folio': 'folio',
	'FchEmis': 'date_invoice',
	'IndNoRebaja': 'no_reduction',
	'TipoDespacho': 'dispatch_type',
	'IndTraslado': 'move_type',
	'TpoImpresion': 'print_type',
	'IndServicio': 'service_indicator',
	'MntBruto': 'gross_amount',
	'FmaPago': 'payment_form',
	'FmaPagExp': 'export_payment_form',
	'FchCancel': 'cancellation_date',
	'MntCancel': 'cancellation_amount',
	'SaldoInsol': 'outstanding_balance',
	'PeriodoDesde': 'period_from',
	'PeriodoHasta': 'period_until',
	'MedioPago': 'payment_method',
	'TpoCtaPago': 'bank_account_type',
	'NumCtaPago': 'bank_account_number',
	'BcoPago': 'bank_name',
	'TermPagoCdg': 'payment_term_code',
	'TermPagoGlosa': 'payment_term_description',
	'TermPagoDias': 'payment_term_days',
	'FchVenc': 'date_due',
	'RUTEmisor': 'rut',
	'RznSoc': 'name',
	'GiroEmis': 'line_of_business',
	'Telefono': 'phone',
	'CorreoEmisor': 'email',
	'Acteco': 'economic_activities',
	'Sucursal': 'branch_office',
	'CdgSIISucur': 'branch_office_code',
	'DirOrigen': 'street',
	'CmnaOrigen': 'city',
	'CiudadOrigen': 'city',
	'CdgVendedor': 'salesman_code',
	'IdAdicEmisor': 'additional_issuer_id',
	'RUTMandante': 'principal_rut',
	'RUTSolicita': 'agent_rut',
	'RUTRecep': 'rut',
	'CdgIntRecep': 'internal_code',
	'RznSocRecep': 'name',
	'Extranjero': 'foreign',
	'GiroRecep': 'line_of_business',
	'Contacto': 'contact',
	'CorreoRecep': 'email',
	'DirRecep': 'street',
	'CmnaRecep': 'city',
	'CiudadRecep': 'state_id',
	'DirPostal': 'postal_address',
	'CmnaPostal': 'postal_city',
	'CiudadPostal': 'postal_city',
	'MntNeto': 'amount_untaxed',
	'MntExe': 'amount_exempt',
	'MntBase': 'amount_meat_slaughter',
	'MntMargenCom': 'amount_margin',
	'TasaIVA': 'iva_rate',
	'IVA': 'iva',
	'IVAProp': 'own_iva',
	'IVATerc': 'third_party_iva',
	'IVANoRet': 'iva_not_withheld',
	'CredEC': 'credit_construction',
	'GrntDep': 'packaging_warranty',
	'MntTotal': 'amount_total',
	'MontoNF': 'amount_unbilled',
	'MontoPeriodo': 'amount_period',
	'SaldoAnterior': 'previous_amount',
	'VlrPagar': 'amount_charged',
	'TpoMoneda': 'currency',
	'TpoCambio': 'currency_rate',
	'MntNetoOtrMnda': 'amount_untaxed',
	'MntExeOtrMnda': 'amount_exempt',
	'MntFaeCarneOtrMnda': 'amount_meat_slaughter',
	'MntMargComOtrMnda': 'amount_margin',
	'IVAOtrMnda': 'iva_other_currency',
	'IVANoRetOtrMnda': 'iva_not_withheld',
	'MntTotOtrMnda': 'amount_total',
	'Patente': 'license_plate',
	'RUTTrans': 'rut',
	'DirDest': 'destination_addr',
	'CmnaDest': 'destination_comuna',
	'CiudadDest': 'destination_city',
	'RUTChofer': 'rut',
	'NombreChofer': 'name',
	'CodModVenta': 'selling_method',
	'CodClauVenta': 'selling_agreement',
	'TotClauVenta': 'selling_agreement_code',
	'CodViaTransp': 'transport_route ',
	'NombreTransp': 'transport_name',
	'RUTCiaTransp': 'transport_company_rut',
	'NomCiaTransp': 'transport_company_name',
	'IdAdicTransp': 'transport_adicional_id',
	'Booking': 'booking',
	'Operador': 'operator',
	'CodPtoEmbarque': 'shipment_port_code',
	'IdAdicPtoEmb': 'shipment_port_aditional_id',
	'CodPtoDesemb': 'disembarkation_port_code',
	'IdAdicPtoDesemb': 'disembarkation_port_aditional_id',
	'Tara': 'tare',
	'CodUnidMedTara': 'tare_unit_of_measure',
	'PesoBruto': 'gross_weight',
	'CodUnidPesoBruto': 'gross_weight_code',
	'PesoNeto': 'net_weight',
	'CodUnidPesoNeto': 'net_weight_code',
	'TotItems': 'total_items',
	'TotBultos': 'number_packages',
	'TipoBultos': 'packages_types',
	'MntFlete': 'amount_freight',
	'MntSeguro': 'amount_insurance',
	'CodPaisRecep': 'start_country_code',
	'CodPaisDestin': 'finish_country_code',
	'FchPago': 'date',
	'MntPago': 'amount',
	'GlosaPagos': 'description',
	'CdgTraslado': 'code',
	'FolioAut': 'authorization_folio',
	'FchAut': 'authorization_date',
	'TipoImp': 'tax_type',
	'TasaImp': 'tax_percent',
	'MontoImp': 'tax_amount',
	'ValComNeto': 'amount_untaxed',
	'ValComExe': 'amount_exempt',
	'ValComIVA': 'amount_iva',
	'NroLinDet': 'line_number',
	'IndExe': 'exemption_index',
	'NmbItem': 'name',
	'DscItem': 'description',
	'QtyRef': 'reference_qty',
	'UnmdRef': 'reference_uom',
	'PrcRef': 'reference_unit_price',
	'QtyItem': 'qty',
	'FchElabor': 'elaboration_date',
	'FchVencim': 'expiration_date',
	'UnmdItem': 'uom',
	'PrcItem': 'price_item',
	'DescuentoPct': 'discount_percent',
	'DescuentoMonto': 'discount_amount',
	'RecargoPct': 'charge_percent',
	'RecargoMonto': 'charge_amount',
	'CodImpAdic': 'aditional_tax_code',
	'MontoItem': 'amount_item',
	'SubQty': 'qty',
	'SubCod': 'code',
	'TipoDscto': 'discount_type',
	'ValorDscto': 'amount',
	'TipoRecargo': 'charge_type',
	'ValorRecargo': 'amount',
	'PrcOtrMon': 'price',
	'Moneda': 'currency',
	'FctConv': ' conversion_date',
	'DctoOtrMnda': 'discount',
	'RecargoOtrMnda': 'charge',
	'MontoItemOtrMnda': 'amount_item',
	'IndAgente': 'agent_code',
	'MntBaseFaena': 'amount_meat_slaughter',
	'MntMargComer': 'amount_margin',
	'PrcConsFinal': 'consumer_price',
	'TpoCodigo': 'code_type',
	'VlrCodigo': 'code',
	'NroSTI': 'number',
	'GlosaSTI': 'description',
	'OrdenSTI': 'print_order',
	'SubTotNetoSTI': 'amount_untaxed',
	'SubTotIVASTI': 'amount_iva',
	'SubTotAdicSTI': 'aditional_tax_amount',
	'SubTotExeSTI': 'aditional_exent_amount',
	'ValSubtotSTI': 'amount',
	'LineasDeta': 'detail_line',
	'NroLinDR': 'number',
	'TpoMov': 'move_type',
	'GlosaDR': 'description',
	'TpoValor': 'amount_type',
	'ValorDR': 'amount',
	'ValorDROtrMnda': 'amount_other_currency',
	'IndExeDR': 'exemption_index',
	'NroLinRef': 'number',
	'TpoDocRef': 'document_type',
	'IndGlobal': 'global_indicator',
	'FolioRef': 'reference_folio',
	'RUTOtr': 'rut_another_company',
	'FchRef': 'date',
	'CodRef': 'code',
	'RazonRef': 'reference',
	'NroLinCom': 'numnber',
	'TipoMovim': 'move_type',
	'Glosa': 'description',
	'TasaComision': 'commission_rate',
	'ValComNeto': 'amount_untaxed',
	'ValComExe': 'amount_exempt',
	'ValComIVA': 'amount_iva',
	'TpoTranVenta': 'type_of_sale',
	'TpoTranCompra': 'type_of_purchase'
}

class l10n_cl_dte_supplier_document(models.Model):
	_name = 'l10n_cl_dte.supplier_document'
	_description = 'Incoming Supplier Document'
	_inherit = ['mail.thread']
	_order = 'date_invoice DESC, id DESC'

	@api.one
	def aprove_commercial(self):
		return self.write({'commercial_state': 'ACD'})

	@api.one
	def reject_commercial(self):
		return self.write({'commercial_state': 'RCD'})

	@api.one
	def grant_merchandise(self):
		return self.write({'receiving_goods_state':'ERM'})

	@api.one
	def claim_partial_merchandise(self):
		return self.write({'receiving_goods_state':'RFP'})
	@api.one
	def claim_total_merchandise(self):
		return self.write({'receiving_goods_state':'RFT'})

	@api.model
	def get_header(self, ns, encabezado):
		vals = {}
		return vals

	@api.model
	def get_detail(self, ns, detalle):
		vals = {}
		return vals

	@api.model
	def get_reference(self, ns, ref):
		vals = {}
		return vals

	# @api.multi
	# def upload_document(self, xml):
	# 	root = etree.fromstring(xml)
	# 	for documento in root.getiterator('Documento'):
	# 		ns= None
	# 		print self.get_header(None, documento.find('Encabezado'))
	# 		print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
	# 	return True

	@api.model
	def write_document(self, xml, doc):
		ids = []
		vals = {}
		detail_vals = {}
		ref_vals = {}
		ns = {'sii': 'http://www.sii.cl/SiiDte'}

		root = etree.fromstring(xml)

		for documento in root.getiterator('{http://www.sii.cl/SiiDte}Documento'):
			ids.append(doc.id)

			encabezado = documento.find('sii:Encabezado', ns)
			detalles = documento.findall('sii:Detalle', ns)
			references = documento.findall('sii:Referencia', ns)
			ted = documento.find('sii:TED', ns)

			vals = self.get_header(ns, encabezado)

			if vals['folio'] == doc.folio and vals['tipo_dte'] == doc.tipo_dte and \
					vals['rut_emisor'] == self.env['res.partner'].only_format_rut(doc.rut_emisor):
				sii_email_obj = self.env['l10n_cl_dte.sii_email'].search([('rut','=',vals['rut_emisor'])])
				sii_email = sii_email_obj.email if sii_email_obj else None
				company = self.env['res.company'].search([('partner_id.rut','=',vals['rut_recep'])])

				vals.update({'company_id': company[0].id if company else None, 'email_to': sii_email})

				if ted is not None:
					vals.update({'tedxml': etree.tostring(ted, encoding='utf-8')})

				doc.write(vals)

				if detalles is not None:
					for detalle in detalles:
						detail_vals = self.get_detail(ns, detalle)
						detail_vals.update({'document_id': doc.id})
						self.env['l10n_cl_dte.supplier_document.lines'].create(detail_vals)

				if references is not None:
					for reference in references:
						ref_vals = self.get_reference(ns, reference)
						ref_vals.update({'document_id': doc.id})
						self.env['l10n_cl_dte.supplier_document.reference_lines'].create(ref_vals)
		return ids

	@api.multi
	def reload_xml(self):
		for doc in self:
			doc.line_ids.unlink()
			for attach in self.env['ir.attachment'].search([('name','like','.xml'),('res_id','=',doc.id),
										('res_model','=','l10n_cl_dte.supplier_document')]):
				xml = base64.b64decode(attach.datas)
				root = etree.fromstring(xml)
				for element in root.iter():
					if 'EnvioDTE' in element.tag or 'EnvioBOLETA' in element.tag:
						doc.write_document(xml, doc)
		return True

	@api.model
	def create_document(self, document_identification):

		docs = self.search([('folio','=',document_identification['folio']),('dte_type','=',document_identification['dte_type']),
			('date_invoice','=',document_identification['date_invoice'])], order='id DESC')
		if docs:
			docs = list(docs)
			resp = docs.pop()
			if len(docs) >= 1:
				for doc in docs:
					for att in self.env['ir.attachment'].search([('name','like','.xml'),('res_id','=',doc.id),
											('res_model','=','l10n_cl_dte.supplier_document')]):
						att.unlink()
					doc.unlink()
		else:
			resp = self.create({})
		return resp

	def convert2odoo_fields(self, dic):
		vals = {}
		for key in dic:
			if type(dic[key]) != type({}) and type(dic[key]) != type([]):
				vals[DTE_FIELDS[key]] = dic[key]
		return vals

	@api.model
	def save_dics(self, dic, odoo_model, relation=None, search_domain=None, sub_dic=None):
		ids = []
		child_dic = None
		if type(dic) == type({}):
			# es un diccionario entonces es guiardable 
			if sub_dic:
				if dic.has_key(sub_dic['dic']):
					child_dic = dic[sub_dic['dic']]
			dic = self.convert2odoo_fields(dic)
			if relation:
				dic.update(relation)
			if search_domain:
				search_record = self.env[odoo_model].search(search_domain, limit=1)
				if not search_record:
					return self.env[odoo_model].create(dic)
				else:
					return search_record
			record = self.env[odoo_model].create(dic)
			if sub_dic:
				if child_dic:
					child_model = sub_dic['model']
					child_relation = { sub_dic['relation_field'] : record.id } 
					self.save_dics(child_dic, child_model, relation=child_relation)
		elif type(dic) == type([]):
			# no es guardable directamente, asi que hay que descomponerlo de nuevo 
			for line in dic :
				self.save_dics(line,odoo_model,relation=relation,search_domain=search_domain,sub_dic=sub_dic)
		else:
			print "no guardable, no es diccionario ni una coleccion  de ellos "
			return False

	@api.multi
	def make_document(self, document_xml, email_to):
		ids = []
		vals = {}
		detail_vals = {}

		root = etree.fromstring(document_xml)

		if 'SetDTE' in xmltodict.parse(document_xml):
			documents = xmltodict.parse(document_xml)['SetDTE']['DTE']
		if 'EnvioDTE' in xmltodict.parse(document_xml):
			documents= xmltodict.parse(document_xml)['EnvioDTE']['SetDTE']['DTE']
		documents = json.dumps(documents)
		documents = json.loads(documents)
		if not isinstance(documents,list):
			documents = [documents]

		for documento in documents:
			id_doc = documento['Documento']['Encabezado']['IdDoc']
			totals = documento['Documento']['Encabezado']['Totales']
			vals.update(self.convert2odoo_fields(id_doc))
			vals.update(self.convert2odoo_fields(totals))
			if documento['Documento']['Encabezado'].has_key('RUTMandante'):
				vals[DTE_FIELDS['RUTMandante']] = documento['Documento']['Encabezado']['RUTMandante']

			doc = self.create_document(vals)

			doc_id = doc.write(vals)
			if documento['Documento']['Encabezado']['IdDoc'].has_key('MntPagos'):
				doc.payments.unlink()
				payments = documento['Documento']['Encabezado']['IdDoc']['MntPagos']
				self.save_dics(payments, 'l10n_cl_dte_supplier_document.payment_line', relation={'document_id': doc.id } )

			if documento['Documento']['Encabezado']['Totales'].has_key('ImptoReten'):
				doc.taxes.unlink()
				taxes =  documento['Documento']['Encabezado']['Totales']['ImptoReten']
				self.save_dics(taxes, 'l10n_cl_dte_supplier_document.taxes' ,relation={'document_id': doc.id })

			code_model = self.env['l10n_cl_dte.supplier_document.line.item_code']
			doc.lines.unlink()

			codes = {'dic':'CdgItem', 'model':'l10n_cl_dte.supplier_document.line.item_code','relation_field': 'line_id'}
			self.save_dics(documento['Documento']['Detalle'], 'l10n_cl_dte.supplier_document.lines', \
									relation={'document_id': doc.id}, sub_dic=codes)

			fake_partner_domain = [('rut','=',documento['Documento']['Encabezado']['Emisor']['RUTEmisor'])] 
			doc.supplier_id =  self.save_dics(documento['Documento']['Encabezado']['Emisor'] , 
				'l10n_cl_dte_supplier_document.fake_partner', 
				search_domain=fake_partner_domain)

			if documento['Documento'].has_key('Referencia'):
				doc.references.unlink()
				references = documento['Documento']['Referencia']
				self.save_dics(references, 'l10n_cl_dte.supplier_document.reference_lines', relation={'document_id': doc.id})
			if documento['Documento'].has_key('DscRcgGlobal'):
				doc.discounts_charges.unlink()
				discount_charges = documento['Documento']['DscRcgGlobal']
				self.save_dics(discount_charges, 'l10n_cl_dte_supplier_document.discount_charge', \
											relation={'document_id': doc.id})

			if documento['Documento']['Encabezado'].has_key('Transporte'):
				doc.transport.unlink()
				transport = documento['Documento']['Encabezado']['Transporte']
				driver = {'dic':'Chofer', 'model':'l10n_cl_dte_supplier_document.driver','relation_field': 'vehicle_id'}
				self.save_dics(transport, 'l10n_cl_dte_supplier_document.vehicle' , relation={'document_id': doc.id}, sub_dic=driver)
				if documento['Documento']['Encabezado']['Transporte'].has_key('Aduana'):
					customs = documento['Documento']['Encabezado']['Transporte']['Aduana']
					self.save_dics(customs, 'l10n_cl_dte_supplier_document.customs', relation={'document_id': doc.id})

			ids.append(doc)
		return ids

	@api.one
	@api.depends('dte_type', 'folio')
	def _get_name(self):
		tipo = ''
		folio = ''
		if self.dte_type:
			tipo = self.dte_type
		if self.folio:
			folio = self.folio
		self.name = '%s: %s - %s' % (self.supplier_name,tipo, folio)

	name = fields.Char('Name', compute=_get_name)
	dte_type = fields.Char('Document type')
	folio = fields.Char('Folio')
	date_invoice = fields.Date('Date Invoice')
	no_reduction = fields.Char('No Reduction Indicator')
	dispatch_type = fields.Char('Dispath Type')
	move_type = fields.Char('Move Type')
	print_type = fields.Char('Print type')
	service_indicator = fields.Char('Service Indicator') 
	gross_amount = fields.Char('Gross amount')
	payment_form = fields.Selection([('1','Contado'), ('2','Credito'), ('3','Sin costo')], 'Payment Form')
	export_payment_form = fields.Char('Export Payment Form')
	cancellation_date = fields.Date('Cancellation Date')
	cancellation_amount = fields.Char('Cancellation Amount')
	outstanding_balance = fields.Char('Outstanding Balance')
	period_from = fields.Char('Period from')
	period_until = fields.Char('Period until')
	payment_method = fields.Char('Payment Method')
	bank_account_type = fields.Char('Bank Account Type')
	bank_account_number = fields.Char('Banck Account Number')
	bank_name = fields.Char('bank name')
	payment_term_code = fields.Char('Payment Term Code')
	payment_term_description = fields.Char('Payment Term Description')
	payment_term_days = fields.Char('Payment Term Days')
	date_due = fields.Date('Date Due')
	principal_rut = fields.Char('Principal Rut')
	agent_rut = fields.Char('Agent Rut') 
	amount_untaxed = fields.Integer('Amount Untaxed')
	amount_exempt = fields.Integer('amount Exempt')
	amount_meat_slaughter = fields.Integer('Amount Meat Slaughter ')
	amount_margin = fields.Integer('Amount Margin')
	iva_rate = fields.Char('IVA rate')
	iva = fields.Integer('Amount IVA')
	own_iva = fields.Integer('Amount Own IVA')
	third_party_iva = fields.Integer('Third Party IVA')
	iva_not_withheld = fields.Integer('IVA not Witheld')
	credit_construction = fields.Integer('Amount Construction Credit ')
	packaging_warranty = fields.Integer('Packaging Warranty')
	amount_total = fields.Integer('Amount Total')
	amount_unbilled = fields.Integer('Amount Unbilled')
	amount_period = fields.Integer('Amount Period')
	previous_amount = fields.Integer('Previous Amount')
	amount_charged = fields.Integer('Amount Charged')
	type_of_sale = fields.Selection([('1','Ventas del Giro'), ('2','Venta Activo Fijo'), ('3','Venta Bien Raíz')], 'Type of sale')
	type_of_purchase = fields.Selection([('1','Compras del Giro'),
					('2','Compras en Supermercados o similares'),
					('3','Adquisición Bien Raíz.'),
					('4','Compra Activo Fijo'),
					('5',' Compra con IVA Uso Común'),
					('6','Compra sin derecho a credito'),
					('7','Compra que no corresponde incluir')],'Type of purchase')
	payments = fields.One2many('l10n_cl_dte_supplier_document.payment_line', 'document_id', 'Payments')
	discounts_charges = fields.One2many('l10n_cl_dte_supplier_document.discount_charge', 'document_id', 'Charges/Discounts')
	references = fields.One2many('l10n_cl_dte.supplier_document.reference_lines', 'document_id', 'References')
	taxes = fields.One2many('l10n_cl_dte_supplier_document.taxes', 'document_id', 'Taxes')
	lines = fields.One2many('l10n_cl_dte.supplier_document.lines', 'document_id', 'Lines')
	supplier_id = fields.Many2one('l10n_cl_dte_supplier_document.fake_partner', 'Supplier')
	supplier_name = fields.Char(related='supplier_id.name', store=True)
	ack = fields.Boolean('Ack Sent?', default=False)
	commercial_state = fields.Selection([('not_send','No enviada'),('ACD','Acepta Contenido'),\
					('RCD','Reclamo al Contenido')], 'Commercial Ack State', default='not_send')
	receiving_goods_state = fields.Selection([('not_send','No enviado'),('ERM','Otorga Recibo '),\
						('RFP','Reclamo por Falta Parcial de Mercaderías'),\
						('RFT','Reclamo por Falta Total de Mercaderías')],\
							'Receiving Goods Ack State', default='not_send')
	transport = fields.One2many('l10n_cl_dte_supplier_document.vehicle', 'document_id', "Transport")
	customs = fields.One2many( 'l10n_cl_dte_supplier_document.customs', 'document_id', 'Custom')
	invoice_id = fields.Many2one('account.invoice', 'Invoice')
	comments = fields.Text('Comments')
l10n_cl_dte_supplier_document()

class account_invoice(models.Model):
	_inherit = 'account.invoice'

	document_id = fields.One2many('l10n_cl_dte.supplier_document', 'invoice_id', 'Supplier Document')
	comments = fields.Text('Comments')
account_invoice()


class l10n_cl_dte_supplier_document_lines(models.Model):
	_name = 'l10n_cl_dte.supplier_document.lines'
	_order = 'line_number ASC'

	document_id = fields.Many2one('l10n_cl_dte.supplier_document', 'Supplier Document')

	line_number = fields.Integer('Line Number')
	exemption_index = fields.Char('No Tax Index')
	name = fields.Char('Item Name')
	description = fields.Char('Description')
	reference_qty = fields.Char('Reference Quantity')
	reference_uom = fields.Char('Reference UoM')
	reference_unit_price = fields.Char('Reference Unit Price')
	qty = fields.Float('Quantity')
	elaboration_date = fields.Char('Elaboration Date')
	expiration_date = fields.Char('Expiration Date')
	uom = fields.Char('UoM')
	price_item  = fields.Float('Price')
	discount_percent = fields.Float('Discount Percent')
	discount_amount = fields.Float('Discount Amount')
	charge_percent = fields.Float('Charge Percent')
	charge_amount = fields.Float('Charge Amount')
	aditional_tax_code = fields.Char('Aditional Taxs')
	amount_item  = fields.Float('Subtotal')


	codes = fields.One2many('l10n_cl_dte.supplier_document.line.item_code', 'line_id', 'Item Code')
	subqty = fields.One2many('l10n_cl_dte.supplier_document.line.subqty', 'line_id', 'SubQty')
l10n_cl_dte_supplier_document_lines()

class l10n_cl_dte_supplier_document_line_item_code(models.Model):
	_name = 'l10n_cl_dte.supplier_document.line.item_code'

	@api.one
	@api.depends('code_type', 'code')
	def _get_name(self):
		tipo = ''
		code = ''
		if self.code_type:
			tipo = self.code_type
		if self.code_type:
			code = self.code_type
		self.name = '[%s] %s' % (self.code_type, self.code)

	line_id = fields.Many2one('l10n_cl_dte.supplier_document.lines', 'Supplier Document Line')
	name = fields.Char('Name', compute=_get_name)
	code_type = fields.Char('Code Type')
	code = fields.Char('Code Value')
l10n_cl_dte_supplier_document_line_item_code()

class l10n_cl_dte_supplier_document_line_subqty(models.Model):
	_name = 'l10n_cl_dte.supplier_document.line.subqty'

	line_id = fields.Many2one('l10n_cl_dte.supplier_document.lines', 'Supplier Document Line')
	qty = fields.Char('qty')
	code = fields.Char('code')
l10n_cl_dte_supplier_document_line_subqty()

class l10n_cl_dte_supplier_document_discount_charge(models.Model):
	_name = 'l10n_cl_dte_supplier_document.discount_charge'

	document_id = fields.Many2one('l10n_cl_dte.supplier_document', 'Supplier Document')

	number = fields.Char('Line Number')
	move_type = fields.Char('Type of move')
	description = fields.Char('Description')
	amount_type = fields.Char('UoM')
	amount = fields.Char('Amount')
	amount_other_currency = fields.Char('Amount in other currency')
	exemption_index = fields.Char('Exemption index')
l10n_cl_dte_supplier_document_discount_charge()

class l10n_cl_dte_supplier_reference_lines(models.Model):
	_name = 'l10n_cl_dte.supplier_document.reference_lines'

	document_id = fields.Many2one('l10n_cl_dte.supplier_document', 'Supplier Document')
	number = fields.Char('Number')
	global_indicator = fields.Char('Global indicator')
	reference_folio = fields.Char('Folio')
	rut_another_company = fields.Char('Another company rut')
	date = fields.Date('Date')
	code = fields.Char('Code')
	reference = fields.Char('Reference')
	document_type = fields.Selection([('30', 'Factura'),
					('32', 'Factura de venta bienes y servicios no afectos o exentos de IVA '),
					('35', 'Boleta '),
					('38', 'Boleta exenta'),
					('45', 'Factura de compra'),
					('55', 'Nota de debito'),
					('60', 'nota de credito'),
					('103', 'Liquidacion'),
					('40', 'Liquidacion factura'),
					('43', 'Liquidacion-Factura electronica '),
					('33', 'Factura electronica'),
					('34', 'Factura no afecta o exenta electronica'),
					('39', 'Boleta electronica'),
					('41', 'Boleta exenta electronica'),
					('46', 'Factura de compra electronica. '),
					('56', 'Nota de debito electronica'),
					('61', 'Nota de credito electronica'),
					('50', 'Guia de despacho.'),
					('52', 'Guia de despacho electrónica '),
					('110', 'Factura de exportacion electronica'),
					('111', 'Nota de debito de exportacion electronica'),
					('112', 'Nota de credito de exportacion electronica'),
					('801', 'Orden de compra'),
					('802', 'Nota de pedido'),
					('803', 'Contrato'),
					('804', 'Resolucion'),
					('805', 'Proceso de ChileCompra'),
					('806', 'Ficha ChileCompra'),
					('807', 'DUS'),
					('808', 'B/L (Conocimiento de embarque)'),
					('809', 'AWB (Air Will Bill)'),
					('810', 'MIC/DTA'),
					('811', 'Carta de porte '),
					('812', 'Resolucion del SNA donde califica servicios de exportacion '),
					('813', 'Pasaporte'),
					('814', 'Certificado de deposito bolsa Prod. Chile'),
					('815', 'Vale de Prenda Bolsa Prod. Chile')], 'Type of document')
l10n_cl_dte_supplier_reference_lines()

class l10n_cl_dte_supplier_document_fake_partner(models.Model):
	_name = 'l10n_cl_dte_supplier_document.fake_partner'

	documents = fields.One2many('l10n_cl_dte.supplier_document', 'supplier_id', 'Documents')
	rut = fields.Char('Rut')
	name = fields.Char('Name')
	line_of_business = fields.Char('Line of business')
	phone = fields.Char('Phone')
	email = fields.Char('Email')
	economic_activities = fields.Char('Economic Activities')
	branch_office = fields.Char('Branch Office')
	branch_office_code = fields.Char('Branch Office Code')
	street = fields.Char('Street')
	city = fields.Char('City')
	comuna = fields.Char('Comuna')
	salesman_code = fields.Char('Salesman Code')
	additional_issuer_id = fields.Char('Additional Issuer')
	internal_code = fields.Char('Internal Code')
	foreign = fields.Char('Foreing ?')
	contact = fields.Char('Contact')
	state_id = fields.Char('State id')
	postal_address = fields.Char('Postal Address')
	postal_city = fields.Char('Postal City')
	postal_comuna = fields.Char('Postal Comuna')

	@api.multi
	def create_partner(self, partner_rut, partner_data):
		partner =  self.search([('rut','=',partner_rut)], order='id DESC')
		if partner:
			return partner
		else:
			return self.create(partner_data)
l10n_cl_dte_supplier_document_fake_partner()

class l10n_cl_dte_supplier_document_another_currency(models.Model):
	_name = 'l10n_cl_dte_supplier_document.another_currency'

	currency = fields.Char('Currency')
	currency_rate = fields.Date('Currency Rate')
	amount_untaxed = fields.Char('Amount Untaxed')
	amount_exempt = fields.Char('Amount Exempt')
	amount_meat_slaughter = fields.Char('Amount Meat Slaughter')
	amount_margin = fields.Char('Amount Margin')
	iva_other_currency = fields.Char('IVA tax ')
	iva_not_withheld = fields.Char('IVA tax Not Withheld')
	amount_total = fields.Char('Amount Total')
l10n_cl_dte_supplier_document_another_currency()

class l10n_cl_dte_supplier_document_vehicle(models.Model):
	_name = 'l10n_cl_dte_supplier_document.vehicle'

	document_id = fields.Many2one('l10n_cl_dte.supplier_document', 'Document')
	license_plate = fields.Char('License Plate') 
	name = fields.Char('Name')
	rut = fields.Char('Rut number') 
	destination_addr = fields.Char('Destination Address') 
	destination_comuna = fields.Char('Destination Comuna') 
	destination_city = fields.Char('Destination City') 
l10n_cl_dte_supplier_document_vehicle()

class l10n_cl_dte_supplier_document_driver(models.Model):
	_name = 'l10n_cl_dte_supplier_document.driver'

	vehicle_id = fields.Many2one('l10n_cl_dte_supplier_document.vehicle', 'Vehicle')
	rut = fields.Char('Rut')
	name = fields.Char('Name')
l10n_cl_dte_supplier_document_driver()

class l10n_cl_dte_supplier_document_customs(models.Model):
	_name = 'l10n_cl_dte_supplier_document.customs'

	document_id = fields.Many2one('l10n_cl_dte.supplier_document', 'Document') 
	selling_method = fields.Char('Selling Method')
	selling_agreement = fields.Char('Selling Agreement')
	selling_agreement_code = fields.Char('Selling Agreement Code')
	transport_route = fields.Char('Transport Route')
	transport_name = fields.Char('Transport Name')
	transport_company_rut = fields.Char('Transport Company Rut')
	transport_company_name = fields.Char('Transport Company Name')
	transport_adicional_id = fields.Char('Transport Aditional')
	booking = fields.Char('Booking')
	operator = fields.Char('Operator')
	shipment_port_code = fields.Char('Shipment Port Code')
	shipment_port_aditional_id = fields.Char('Shipment Port Aditional')
	disembarkation_port_code = fields.Char('Disembarkation Port Code')
	disembarkation_port_aditional_id = fields.Char('Disembarkation Port Aditional')
	tare = fields.Char('Tare')
	tare_unit_of_measure = fields.Char('Tare Unit of Measure')
	gross_weight = fields.Char('Gross Weight')
	gross_weight_code = fields.Char('Gross Weight Code')
	net_weight = fields.Char('Net Weight')
	net_weight_code = fields.Char('Net Weight Code')
	total_items = fields.Char('Total Items')
	number_packages = fields.Char('Number Packages')
	packages_types = fields.Char('Packages Types')
	amount_freight = fields.Char('Amount Freight')
	amount_insurance = fields.Char('Amount Insurance')
	start_country_code = fields.Char('Start Country Code')
	finish_country_code = fields.Char('Finish Country Code')
l10n_cl_dte_supplier_document_customs()

class l10n_cl_dte_supplier_document_taxes(models.Model):
	_name = 'l10n_cl_dte_supplier_document.taxes'

	document_id = fields.Many2one('l10n_cl_dte.supplier_document', 'Document')
	tax_type = fields.Char('Tax Type')
	tax_percent = fields.Char('Percent')
	tax_amount = fields.Char('Amount')
l10n_cl_dte_supplier_document_taxes()


class l10n_cl_dte_supplier_document_payment_line(models.Model):
	_name = 'l10n_cl_dte_supplier_document.payment_line'

	document_id = fields.Many2one('l10n_cl_dte.supplier_document', 'Document')
	date = fields.Date('Date')
	amount = fields.Char('Amount')
	description = fields.Char('Description')
l10n_cl_dte_supplier_document_payment_line()

class l10n_cl_dte_supplier_document_document2invoice_wizard(models.TransientModel):
	_name = 'l10n_cl_dte_supplier_document.document2invoice_wizard'

	@api.multi
	def search_supplier(self, supplier_data):
		partner = self.env['res.partner'].search([('rut','=',supplier_data['rut'])], limit=1)
		if partner :
			return partner
		else:
			return self.env['res.partner'].create(supplier_data)

	@api.multi
	def search_tax(self, tax):
		return self.env['account.tax'].search([('description','=',tax),('type_tax_use', 'in', ['all','purchase'])], limit=1)
		

	@api.multi
	def search_product(self, supplier_id, supplier_internal_code, supplier_product_name, ean13_code='', additional_tax=None):
		supplier_product = self.env['product.supplierinfo'].search([
			('name','=',supplier_id),
			('product_code','=',supplier_internal_code),
			('product_name','=',supplier_product_name)
			], limit=1)

		if supplier_product:
			return supplier_product.product_tmpl_id
		else:
			product =  self.env['product.template'].create({'name': supplier_product_name, 'ean13': ean13_code, 'type': 'product'})
			additional_tax = self.search_tax(additional_tax)
			if additional_tax:
				product.write({'supplier_taxes_id':  [(4, additional_tax.id, None)]})
			product_supplierinfo = self.env['product.supplierinfo'].create(
				{'name': supplier_id, 'product_name': supplier_product_name, 'product_code': supplier_internal_code , 'product_tmpl_id': product.id })
			return product


	@api.multi
	def make_supplier_invoice(self):
		supplier_document = self.env['l10n_cl_dte.supplier_document'].browse(self.env.context['active_id'])
		supplier = supplier_document.supplier_id
		# TODO: Agregar a la lista de campos las actividades economicas 
		invoice_id = None
		supplier_internal_code = ''
		ean13_code = ''
		if not supplier_document.invoice_id:
			supplier_data = {
				'is_company': True,
				'supplier' : True ,
				'foreign_rut': False,
				'name': supplier.name,
				'rut': self.env['res.partner'].check_rut(supplier.rut),
				'line_of_business' : supplier.line_of_business,
				'street': supplier.street,
				'city': supplier.city
			}

			supplier = self.search_supplier(supplier_data)

			invoice_data = {
				'partner_id': supplier.id,
				'type': 'in_invoice',
				'account_id': supplier.property_account_payable.id,
				'date_invoice' : supplier_document.date_invoice,
				'supplier_invoice_number': supplier_document.folio,
				'date_due': supplier_document.date_due,
			}

			supplier_invoice = self.env['account.invoice'].create(invoice_data)
			invoice_id = supplier_invoice.id
			supplier_document.invoice_id = supplier_invoice.id


			for line in supplier_document.lines:
				# self.env['account.invoice.line'].create()
				for code in line.codes:
					if code.code_type == 'INTERNO':
						supplier_internal_code = code.code
					if code.code_type == 'EAN13':
						ean13_code = code.code
				product = self.search_product(supplier.id, supplier_internal_code , line.name, ean13_code=ean13_code, additional_tax=line.aditional_tax_code)
				amount2percent = lambda qty,product_price,discount_amount : (100 * discount_amount ) / (qty*product_price)
				inv_line = self.env['account.invoice.line'].create({
					'invoice_id' : supplier_invoice.id,
					'name': line.description if line.description else line.name,
					'product_id' : product.product_variant_ids[0].id,
					'quantity': line.qty,
					'price_unit': line.price_item,
					'discount': line.discount_percent if line.discount_percent else amount2percent(line.qty, line.price_item ,line.discount_amount)  if line.discount_amount else 0
					})

				onchage_product= inv_line.product_id_change(product.product_variant_ids[0].id, None, qty=line.qty, name=line.description if line.description else line.name, type='in_invoice', partner_id=supplier.id, price_unit=line.price_item, currency_id=False)
				print onchage_product
				taxes = onchage_product['value']['invoice_line_tax_id']

				inv_line.invoice_line_tax_id = taxes
			supplier_invoice.button_reset_taxes()
		else:
			invoice_id = supplier_document.invoice_id.id

		return {
				'type': 'ir.actions.act_window',
				'name': 'Supplier Invoice',
				'res_model': 'account.invoice',
				'view_type': 'form',
				'view_mode': 'form',
				'res_id'    : invoice_id,
				'view_id': self.env.ref('account.invoice_supplier_form',False).id,
				'target': 'current',
			}
l10n_cl_dte_supplier_document_document2invoice_wizard()
