
import os

original = os.getcwd()
archivos = os.listdir(original)

for arch in archivos:
	if os.path.isdir(arch) and arch not in ('script_migracion', 'stock_valued', 'web_m2x_options', \
					'l10n_cl_dte_assignment', 'l10n_cl_toponyms', 'base_state_ubication', '.git'):
		print '\n\nBorrando dentro de %s' % arch
		os.chdir(arch)
		for name in os.listdir(os.getcwd()):
			if os.path.isfile(name) and '.pyc' not in name and name != '__openerp__.py' and '.xml' not in name:
				os.remove(name)
			elif os.path.isdir(name) and name in ('wizard', 'models'):
				os.chdir(name)
				for file_name in os.listdir(os.getcwd()):
					if os.path.isfile(file_name) and '.pyc' not in file_name and '.xml' not in file_name:
						os.remove(file_name)
				os.chdir('%s/%s' % (original, arch))
		os.chdir(original)

