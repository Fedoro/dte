# -*- coding: utf-8 -*-
##############################################################################
#
# Author: DBit EIRL.
# Copyright (c) 2016 DBit EIRL.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _

class sale_order_cancel(models.Model):
	_inherit = 'sale.order'

	state = fields.Selection(selection_add=[('refund', 'Refund')])

	@api.model
	def custom_invoice_cancel(self, sale, invoices):
		for inv in invoices:
			if inv.state == 'cancel' or inv.type != 'out_invoice':
				pass
			elif inv.state == 'paid':
				ctx = dict(self._context) or {}

				self.env['account.unreconcile'].\
					with_context({'active_ids': [pay.id for pay in inv.payment_ids]}).trans_unrec()
				ctx.update({'active_id': inv.id, 'active_ids': [inv.id], 'type': inv.type})

				if inv.journal_id.doc_type_id:
					if inv.journal_id.doc_type_id.sii_code in ('33', '34', '39', '41'):
						journal = self.env['account_journal'].search(
								[('type','=','sale_refund'),('doc_type_id.sii_code','=','61')])
					elif inv.journal_id.doc_type_id.sii_code == '110':
						journal = self.env['account_journal'].search(
							[('type','=','sale_refund'),('doc_type_id.sii_code','=','112')])
					else:
						journal = self.env['account_journal'].search(
							[('type','=','sale_refund'),('doc_type_id.sii_code','=','60')])

				else:
					journal = self.env['account_journal'].search([('type','=','sale_refund')])

				if journal:
					journal_id = journal[0].id
				else:
					raise Warning(_('Can\'t find a refund journal in the sistem. Please check.'))

				refund_id = None
				vals = {'filter_refund': 'cancel', 'description': 'Anula Venta %s' % (sale.name), 'journal_id': journal_id}

				inv_refund = self.env['account.invoice.refund'].with_context(ctx).create(vals)
				res = inv_refund.with_context(ctx).compute_refund(mode='cancel')

				for item in res['domain']:
					if 'id' in item and 'in' in item:
						refund_id = item[2][0]

				if refund_id:
					sql = """INSERT INTO sale_order_invoice_rel """
					sql += """(order_id, invoice_id) VALUES (%s, %s)""" % (sale.id, refund_id)
					self._cr.execute(sql)
				else:
					raise Warning(_('Can\'t find the refund document. Please contact your Help Desk'))
			else:
				inv.signal_workflow('invoice_cancel')
sale_order_cancel()

