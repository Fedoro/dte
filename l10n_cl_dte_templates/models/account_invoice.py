# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _
import base64

class l10n_cl_dte_templates_account_invoice(models.Model):
 	_inherit = 'account.invoice'

	@api.multi
	def generate_pdf(self):
		dte_attach = self.env['l10n_cl_dte.attachment']

		for document in self:
			pdf_report = self.env['report'].get_pdf(self , 'l10n_cl_dte_templates.report_chilean_invoice')

			result = base64.b64encode(pdf_report)
			attch_vals = dte_attach.get_attachment_values(document, result)
			dte_attach.add_attachment(document, attch_vals)
		return pdf_report

	@api.multi
	def invoice_print(self):
		if self.dte:
			return self.generate_pdf()
		else:
			return super(l10n_cl_dte_templates_account_invoice, self).invoice_print()
			
	@api.multi
	def sign_document(self):
		dte_attach = self.env['l10n_cl_dte.attachment']

		for invoice in self:
			res = super(l10n_cl_dte_templates_account_invoice, self).sign_document()		
			invoice.generate_pdf()
		return res
l10n_cl_dte_templates_account_invoice()
