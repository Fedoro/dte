# -*- coding: utf-8 -*-
##############################################################################
#
# Author: DBit EIRL.
# Copyright (c) 2016 DBit EIRL.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

import datetime, openerp.osv
from openerp.report import report_sxw
from openerp.report.report_sxw import rml_parse
# from openerp.tools.amount_to_text_cl import amount_to_text

class Parser(report_sxw.rml_parse):
	def __init__(self, cr, uid, name, context):
		super(Parser, self).__init__(cr, uid, name, context)
		
		self.cont_1 = 0
		self.cont_2 = 0

		self.localcontext.update({
			'dates': self.dates,
			'get_pdf': self.get_pdf,
			'get_company_logo': self.get_company_logo,
			'get_reason': self.get_reason,
			'sequence_1': self.sequence_1,
			'sequence_2': self.sequence_2,
			######## Lineas Factura #######
			'discount': self.discount,
			'l_subtotal': self.l_subtotal,
			'unit_price': self.unit_price,
			######## Lineas Boleta #######
			#'desc': self.desc,
			#'subtotal': self.subtotal,
			#'line_amount': self.line_amount,
			####### Dispatch Orders ######
			'get_amount_untaxed': self.get_amount_untaxed,
			'get_amount_tax': self.get_amount_tax,
			'get_amount_total': self.get_amount_total,
			'ind_tras': self.ind_tras
	        })

	def dates(self, option, date=None):
		if date:
			if int(option) == 0:
				return date.split('-')[2]
			elif int(option) == 1:
				return date.split('-')[1]
			elif int(option) == 2:
				return date.split('-')[0]
			elif int(option) == 3:
				splits = date.split('-')
				formatDate = str(splits[2])+"/"+str(splits[1])+"/"+str(splits[0])
				return formatDate
			elif int(option) == 4:
				today = datetime.date.today().strftime("%d/%m/%Y")
				return today
		return None

	def get_pdf(self, obj):
		return obj.pdf_417

	def get_company_logo(self, obj):
		return obj.company_id.partner_id.image_medium

	def get_reason(self, motivo):
		if motivo == 1:
			return 'Sobreescribe Documento'
		elif motivo == 2:
			return 'Corrige Texto'
		elif motivo == 3:
			return 'Corrige Monto'
		else:
			return ''

	def sequence_1(self):
		self.cont_1 += 1
		return self.cont_1
		
	def sequence_2(self):
		self.cont_2 += 1
		return self.cont_2

	############## Lineas Factura ##############
	def invoice_tax_rate(self, tax_lines):
		taxs_rate = 0
		for tax_line in tax_lines:
			if tax_line.price_include:
				taxs_rate += tax_line.amount
		return taxs_rate

	def unit_price(self, tax_lines, p_unit):
		tax = self.invoice_tax_rate(tax_lines)
		if tax > 0:
			return round(p_unit / (1 + tax) , 0)
		else:
			return round(p_unit, 0)

	def discount(self, tax_lines, p_unit, descuento, qty):
		neto = self.unit_price(tax_lines, p_unit)
		return qty * neto * descuento / 100

	def l_subtotal(self, tax_lines, p_unit, descuento, qty):
		neto = self.unit_price(tax_lines, p_unit)
		dcto = self.discount(tax_lines, p_unit, descuento, qty)
		return round((neto * qty) - dcto, 0)

	############## Lineas Boleta ##############
	def tax_rate(self, tax_lines):
		taxs_rate = 0
		for tax_line in tax_lines:
			if not tax_line.price_include:
				taxs_rate += tax_line.amount
		return taxs_rate

	def line_amount(self, tax_lines, p_unit):
		tax = self.tax_rate(tax_lines)
		if tax == 0:
			return round(p_unit, 0)
		else:
			return round(p_unit + (p_unit * tax), 0)

	def desc(self, tax_lines, p_unit, descuento, qty):
		neto = self.line_amount(tax_lines, p_unit)
		return qty * neto * descuento / 100

	def subtotal(self, tax_lines, p_unit, descuento, qty):
		neto = self.line_amount(tax_lines, p_unit)
		dcto = self.desc(tax_lines, p_unit, descuento, qty)
		return round((neto * qty) - dcto, 0)

	########################## Dispatch Orders ##############################
	def get_amount_untaxed(self, obj):
		return obj.amount_untaxed if obj.picking_id else obj.manual_amount_untaxed

	def get_amount_tax(self, obj):
		return obj.amount_tax if obj.picking_id else obj.manual_amount_tax

	def get_amount_total(self, obj):
		return obj.amount_total if obj.picking_id else obj.manual_amount_total

	def ind_tras(self, tras):
		if tras == 1:
			return 'Operacion Constituye Venta'
		elif tras == 2:
			return 'Ventas por Efectuar'
		elif tras == 3:
			return 'Consignaciones'
		elif tras == 4:
			return 'Entrega Gratuita'
		elif tras == 5:
			return 'Traslados Internos'
		elif tras == 6:
			return 'Otros Traslados no Venta'
		elif tras == 7:
			return 'Guia de Devolucion'
		elif tras == 8:
			return 'Traslado para Exportacion. (No Venta)'
		elif tras == 9:
			return 'Venta para Exportacion'
		else:
			return ''
