# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

{
	'name': 'Templates for documents in Chilean Localization',
	'version': '1.0',
	'author': '[BTeC Ltda.]',
	'sequence': 38,
	'website': 'www.raiquen.cl',
	'category': 'Accounting & Finance',
	'description': """
This module add the printing templates for chilean electronic documents 
=====================================================================================================================
""",
	'depends': ['base', 'report', 'l10n_cl_dte_stock'], #'report_aeroo', 'l10n_cl_dte_pos'],
	'data': [ 
#		'report/aeroo_report_documents.xml',
		'data/chilean_paperformat.xml',
		'report/electronic_document_widgets.xml',
		'report/electronic_document.xml',
		'views/account_invoice_view.xml',
		'views/l10n_cl_dte_stock_view.xml'
	],
	'contributors': [
		'Patricio Felipe Caceres <patricio.caceres@raiquen.cl>',
		'David Acevedo Toledo <david.acevedo@raiquen.cl>'
	]
}
