# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DF

from datetime import datetime
from elaphe import barcode
import os, tempfile
import base64

class AccountInvoice(models.Model):
	_inherit = 'account.invoice'

	@api.multi
	def print_thermal(self):
		# dummy button
		pass

	@api.multi
	def data_for_print(self):
		#TODO sacar toas las basuras decode cuando funcione bien la impresora -.-....
		self.ensure_one()

		user = self.env['res.users'].browse(self._uid)

		data = {}
		data['ref_exist'] = 0
		data['reason_exist'] = 0
		data['company'] = {}
		data['orderlines'] = []
		data['paymentlines'] = []
		data['reference_lines'] = []
		data['tax_details'] = []
		data['shop'] ={}

		data['origin'] = self.origin
		data['sii_code'] = self.journal_id.doc_type_id.sii_code
		if data['sii_code'] == '33':
			document_name = 'FACTURA ELECTRONICA'
		elif data['sii_code'] == '34':
			document_name = 'FACTURA NO AFECTA O EXENTA ELECTRONICA'
		elif data['sii_code'] == '39':
			document_name = 'BOLETA ELECTRONICA'
		elif data['sii_code'] == '41':
			document_name = 'BOLETA EXENTA ELECTRONICA'
		elif data['sii_code'] == '56':
			document_name = 'NOTA DE DEBITO ELECTRONICA'
		elif data['sii_code'] == '61':
			document_name = 'NOTA DE CREDITO ELECTRONICA'
		data['document_name'] = document_name

		data['connection'] = {}
		data['connection']['iface_print_via_proxy'] = True
		data['connection']['use_https'] = user.use_https or self.user_id.default_section_id \
							and self.user_id.default_section_id.use_https or False
		if user.proxy_ip or self.user_id.default_section_id and self.user_id.default_section_id.proxy_ip:
			if user.proxy_ip:
				data['connection']['proxy_ip'] = 'http://%s:8069' % user.proxy_ip
			elif self.user_id.default_section_id and self.user_id.default_section_id.proxy_ip:
				data['connection']['proxy_ip'] = 'http://%s:8069' % self.user_id.default_section_id.proxy_ip
		else:
			data['connection']['proxy_ip'] = 'http://localhost:8069'

		company = self.company_id
		data['company']['fantasy_name'] = company.partner_id.fantasy_name
		data['company']['rut'] = company.partner_id.rut.replace('.','')
		data['company']['name'] = company.partner_id.name
		data['company']['giro'] = company.partner_id.line_of_business
		if company.street2:
			street = '%s %s' % (company.partner_id.street, company.partner_id.street2)
		else:
			street = company.partner_id.street
		data['company']['street'] = street
		data['company']['city'] = '%s' % company.partner_id.city
		data['company']['sii_office'] = company.sii_office
		data['company']['resolution_number'] = company.resolution_number
		data['company']['resolution_date'] =  '%s-%s-%s' % (company.resolution_date[8:10], company.resolution_date[5:7], company.resolution_date[0:4])
		data['company']['website'] = company.website

		data['date'] = '%s' % (datetime.strptime(self.date_invoice, DF).strftime('%d/%m/%Y'))
		data['name'] = self.number

		# receipt.name
		data['cashier'] = self.user_id.name
		# receipt.cashier
		data['receptor'] = {}

		data['receptor']['name'] = '%s' % self.partner_id.name
		data['receptor']['rut'] = '%s' % self.partner_id.rut
		data['receptor']['giro'] = '%s' % self.partner_id.line_of_business
                if self.partner_id.street2:
			partner_street = '%s %s' % (self.partner_id.street, self.partner_id.street2)
		else:
			partner_street =  '%s' % (self.partner_id.street)
		data['receptor']['street'] = '%s' % partner_street
		data['receptor']['city'] = '%s' % self.partner_id.city

		data['shop']['number'] = self.user_id.default_section_id.sii_sub_code
		data['shop']['name'] = self.user_id.default_section_id.sii_sub_name
		data['shop']['address'] = self.user_id.default_section_id.sii_sub_street 

		for line in self.invoice_line:
			if line.uos_id.name == 'Unit(s)':
				uom = 'Unidad(es)'
			else:
				uom = line.uos_id.name.decode('ascii', errors='ignore')

			tax_rate = 0

			data['orderlines'].append({
				'discount': int(line.discount),
				'unit_name': uom,
				'quantity': int(line.quantity),
				'product_name': line.display_name.decode('ascii', errors='ignore'),
				'price': round(line.price_unit, 0),
				'price_with_tax': round(line.invoice_line_tax_id.compute_all(line.price_unit, line.quantity, product=line.product_id)['total_included'], 0),
				'price_no_tax': round(line.invoice_line_tax_id.compute_all(line.price_unit, line.quantity, product=line.product_id)['taxes'][0]['price_unit'], 2),
				'subtotal': round(line.price_subtotal, 0)
			})

		data['amount_untaxed'] = self.amount_untaxed
		data['amount_exempt'] = self.amount_exempt
		data['amount_tax'] = self.amount_tax
		data['total_with_tax'] = self.amount_total

		if data['sii_code'] in ('56', '61') and self.reference_lines:
			data['ref_exist'] = 1
			for line in self.reference_lines:
                                if line.reference_reason_text and not data['reason_exist']:
                                        data['reason_exist'] = 1

				if line.reference_reason == 1:
					reason = 'Sobre-escribe Documento'
				elif line.reference_reason == 2:
					reason = 'Corrige texto'
				elif line.reference_reason == 3:
					reason = 'Corrige Monto'

				data['reference_lines'].append({'type': line.reference_type.sii_code, 'folio': line.reference_folio,
								'date': line.reference_date, 'reason_text': line.reference_reason_text,
								'reason': reason})

		change = 0
		for payment in self.payment_ids:
			for line in payment.move_id.line_id:
				if line.credit > 0 and line.reconcile_id:
					data['paymentlines'].append({
						'journal': '%s (%s)' % (line.journal_id.name, line.journal_id.currency.name if line.journal_id.currency else line.journal_id.company_id.currency_id.name),
						'amount': line.credit
						})
				elif line.credit > 0 and not line.reconcile_id:
					change += line.credit

		data['change'] = change 
		data['signature'] = self.ted.decode('ascii', errors='ignore')
		return data
AccountInvoice()
