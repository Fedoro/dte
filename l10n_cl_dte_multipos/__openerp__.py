# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

{
	'name': 'Invoice Thermal Print',
	'version': '1.0',
	'author': '[BTeC Ltda.]',
	'website': 'www.raiquen.cl',
	'category': 'Localization',
	'description': """
	agrega la configuracion de la IP de la impresora en los equipos de ventas
""",
	'depends': ['base', 'l10n_cl_dte_invoice', 'sales_team'],
	'data' : [
		'views/crm_case_section.xml',
		'views/res_users_view.xml',
		'views/account_invoice.xml'
	],
	'js': ['static/src/js/pos_backend.js'],
	'qweb': ['static/src/xml/pos.xml'],
	'contributors': [
		'Patricio Felipe Caceres <patricio.caceres@raiquen.cl>',
		'David Acevedo Toledo <david.acevedo@raiquen.cl>'
	]
}
