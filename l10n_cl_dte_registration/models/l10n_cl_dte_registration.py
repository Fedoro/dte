# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

import openerp.addons.decimal_precision as dp
from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _
import base64

class l10n_cl_dte_registration(models.Model):
	"""Un registro o archivo de certificacion puede tener muchos casos, estos casos (documentos)
	contienen lineas de productos """
	_name = 'l10n_cl_dte.registration'

	state = fields.Selection([
		('draft','Draft'),
		('progress','In progress'),
		('sii_waiting','SII waiting'),
		('finished','Finished')], string='State', copy=False, default='draft', readonly=True)
	
	attencion_number = fields.Integer('Attencion Number', required=True)
	company_id = fields.Many2one('res.company', string="Company", default=lambda self: self.env['res.company']._company_default_get('account.invoice'))
	case_ids = fields.One2many('l10n_cl_dte.registration_case', 'registration_id', string="Cases")
	# registration_file = fields.Binary('Registration file')

	@api.multi
	def send_registration_requests(self):
		# Envia todos los documentos creados a el SII para su validacion del set de pruebas
		pass

	@api.multi
	def create_documents(self):
		# if not self.attencion_number:
		pass

	@api.multi
	def clean_documents(self):
		# buscar el campo is_certification_document en los modelos de certificacion
		pass
l10n_cl_dte_registration()

class l10n_cl_dte_registration_case(models.Model):
	_name = 'l10n_cl_dte.registration_case'

	case_number = fields.Integer('Case number', required=True)
	global_discount = fields.Float('Global discount')

	especials_discounts = fields.Boolean('Apply especials discounts?')
	discount_exempts = fields.Float('Discount to exempt items')
	discount_afecteds = fields.Float('Discount to afecteds items')

	doc_res_model = fields.Char('Reference model')
	doc_type = fields.Char('Type of document', help='if is account.invoice need this ')
	doc_res_id =  fields.Integer('Reference id')

	reference = fields.Char('Reference')
	reference_reason = fields.Char('Reference Reason')

	doc_type_id = fields.Many2one('l10n_cl_dte.document_type',required=True, string='Document Type', copy=False)
	registration_id = fields.Many2one('l10n_cl_dte.registration', string='Registration id')

	line_ids = fields.One2many('l10n_cl_dte.registration_case_line', 'case_id' , 'Lines of Case')

	@api.multi
	def create_documents(self):
		pass

	@api.depends('doc_type_id')
	@api.onchange('doc_type_id')
	def _model_name(self):

		costumer_invoices = ['30', '32', '33', '34', '101', '110', '901' ]
		supplier_invoices = ['45', '46']
		out_refound_invoices = ['60' , '61' , '112']
		in_refund_invoices = ['55', '56' ]

		invoices = costumer_invoices + supplier_invoices + out_refound_invoices + in_refund_invoices

		ballots = ['35', '38', '39', '41']
		moves = ['50', '52']

		for record in self:
			if record.doc_type_id.sii_code :
				if record.doc_type_id.sii_code in invoices :
					record.doc_res_model = 'account.invoice'
					if record.doc_type_id.sii_code in costumer_invoices:
						record.doc_type = 'out_invoice'
					elif record.doc_type_id.sii_code in supplier_invoices:
						record.doc_type = 'in_invoice'
					elif record.doc_type_id.sii_code in out_refound_invoices:
						record.doc_type = 'out_refund'
					elif record.doc_type_id.sii_code in in_refund_invoices:
						record.doc_type = 'in_refund'

				elif record.doc_type_id.sii_code in ballots:
					record.doc_res_model = 'pos.order'
					record.doc_type = None
				elif record.doc_type_id.sii_code in moves:
					 record.doc_res_model = 'l10n_cl_dte.stock'
					 record.doc_type = None
				else:
					record.doc_res_model = None
					record.doc_type = None
					raise Warning(_('Not suported type for certification'))
l10n_cl_dte_registration_case()

class l10n_cl_dte_registration_case_line(models.Model):
	_name = 'l10n_cl_dte.registration_case_line'

	item_name = fields.Char('Name of item', required=True)
	qty = fields.Integer('Qty of item', required=True)
	unit_price = fields.Integer('Unit price', required=True)
	discount = fields.Float('Discount', required=True)

	case_id = fields.Many2one('l10n_cl_dte.registration_case', 'Case')
	product_id = fields.Many2one('product.template', string='Product Related') 

	@api.depends('item_name')
	@api.onchange('item_name')
	def make_product(self):
		# Crea o busca el producto relacionado
		for record in self:
			product_id = self.env['product.template'].search([('name','=', record.item_name)])
			if product_id:
				record.product_id = product_id
			else:
				# crear producto
				if record.item_name :
					product_id = self.env['product.template'].create({
							'name': record.item_name,
							'certification_product': True,
							'type': 'service' if 'SERVICIO' in record.item_name else 'consu'
						})
					record.product_id = product_id

	@api.model
	def clean_products(self):
		#borra todos los productos creados por los set de pruebas
		products = self.env['product.template'].search([('certification_product','=',True)])
		if products:
			products.unlink()
l10n_cl_dte_registration_case_line()

# class ProductTemplate(models.Model):

# 	_inherit = 'product.template'

# 	certification_product = fields.Boolean('This product is used in certification?')


# class AccountInvoice(models.Model):

# 	_inherit = 'account.invoice'

# 	is_certification_document = fields.Boolean('Certification Document') 


# class PosOrder(models.Model):

# 	_inherit = 'pos.order'
# 	is_certification_document = fields.Boolean('Certification Document') 

# class stock(models.Model):

# 	_inherit = 'l10n_cl_dte.stock'

# 	is_certification_document = fields.Boolean('Certification Document') 
