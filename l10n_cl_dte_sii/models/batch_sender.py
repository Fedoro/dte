# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from datetime import datetime
from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _
import base64

class l10n_cl_dte_batch_sender(models.Model):
	_name = 'l10n_cl_dte.batch_sender'

	@api.one
	@api.depends('date','state')
	def _send_name(self):
		if self.date :
			date = fields.Date.from_string(self.date)
			self.name = 'Envio diario de documentos %s' % (date.strftime('%d-%m-%Y'))
		else:
			self.name = 'Envio diario de documentos sin fecha'

	def _calc_date(self):
		return fields.Date.context_today(self, timestamp = datetime.now())

	def _recolect_documents(self, model, company):
		xml = ''
		documents = self.env[model].search([
							('state','=','ready_to_send'),
							('batch_send','=',True),
							('company_id','=', company)
						])
		for document in documents:
			xml = xml +(base64.b64decode(document.signed_xml)).decode('iso-8859-1')
		return xml

	@api.multi
	def send_documents(self):
		companies = self.env['res.company'].search([('batch_send','=', True)]) 
		xml_send = ""
		deliveries= False
		if companies :
			for company in companies:
				models_to_search = ['account.invoice', 'l10n_cl_dte.stock']
				for model in models_to_search:
					xml_send = xml_send + self._recolect_documents(model, company.id)
		return True

	name = fields.Char('Name', compute='_send_name', store=True)
	company_id = fields.Many2one('res.company', 'Company' , copy=False)
	date = fields.Date('Day of book', default=_calc_date )
	state = fields.Selection([('draft','Draft'),('signed','Signed Send'),('error','Error'),
					 ('waiting_validation','Waiting Validation'),('validate','Validate')], 'State', default='draft')
l10n_cl_dte_batch_sender()
