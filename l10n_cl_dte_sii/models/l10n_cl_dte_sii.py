
# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.tools.translate import _

from lxml import etree
from datetime import datetime as dt
from suds.client import Client
from suds import WebFault
import requests, json, base64, collections, logging, time
_logger = logging.getLogger(__name__)

parser = etree.XMLParser(encoding='utf-8')
from custom_transport import *

class l10n_cl_dte_sii(models.Model):
	_name = 'l10n_cl_dte.sii'
	_description = 'Send Sii Functions'

	@api.model
	def send_sii(self, obj):
		self.env['res.company'].comprobar_modulo()
		ir_pool = self.env['ir.attachment']
		cert = self.env['l10n_cl_dte_signer.certificate'].get_certificate(obj.company_id.id)

		if not obj.company_id.partner_id.rut:
			raise Warning(_('You must set your company rut.'))

		if obj._name in ('l10n_cl_dte.books','l10n_cl_dte.assignment'):
			if obj._name == 'l10n_cl_dte.books':
				name = '%s.xml' % (obj.name)
			elif obj._name == 'l10n_cl_dte.assignment':
				name = 'aec_%s_%s_%s.xml' % (obj.provider_company_id.partner_id.rut.replace('.',''), obj.assignee_id.rut.replace('.',''), obj.id)
			attachs = ir_pool.search([('res_id','=',obj.id),('res_model','=',obj._name),('name','=',name)])
			attachs.ensure_one()
			signed_xml = (base64.b64decode(obj.signed_xml)).decode('iso-8859-1')
		elif obj._name in ('l10n_cl_dte_invoice_receipt.daily_book'):
			name = '%s.xml' % (obj.name)
			attachs = ir_pool.search([('res_id','=',obj.id),('res_model','=',obj._name),('name','=',name)])
			attachs.ensure_one()
			signed_xml = (base64.b64decode(attachs[0].datas)).decode('iso-8859-1')
		elif obj._name in ('account.invoice', 'l10n_cl_dte.stock'):
			xml = self.env['dte.templates'].get_dispatch(obj)
			signed_xml = self.env['l10n_cl_dte.signer'].signed_document(xml, cert.cert, cert.private_key, cert.password)

		######## OBTIENE TOKEN #########
		token = self.env['l10n_cl_dte.auth'].get_auth(obj, cert.cert, cert.private_key, cert.password)
		cookies = {'TOKEN': token}
		if obj._name == 'l10n_cl_dte.assignment':
			data = collections.OrderedDict([
					('emailNotif', obj.notification_email),
					('rutCompany', obj.company_id.rut.split('-')[0].replace('.','')),
					('dvCompany', obj.company_id.rut.split('-')[1])
				])
		else:
			#data = collections.OrderedDict([
					#('rutSender', cert.owner_rut.split('-')[0].replace('.','')),
					#('dvSender', cert.owner_rut.split('-')[1]),
					#('rutCompany', obj.company_id.rut.split('-')[0].replace('.','')),
					#('dvCompany', obj.company_id.rut.split('-')[1])
				#])
			data = {
				'rutSender': cert.owner_rut.split('-')[0].replace('.',''),
				'dvSender': cert.owner_rut.split('-')[1],
				'rutCompany': obj.company_id.rut.split('-')[0].replace('.',''),
				'dvCompany': obj.company_id.rut.split('-')[1]
			}
		files = {
			'archivo': ('envio.xml', signed_xml, 'Content-Type: text/xml')
		}

		headers = {
			'Referer': obj.company_id.partner_id.website or 'http://www.bustech.cl',
			'Accept-Language': 'es-cl',
			'User-Agent': 'Mozilla/4.0 (compatible; PROG 1.0; Windows NT 5.0; YComp 5.0.2.4)',
			'Cache-Control': 'no-cache'
		}

		sii_env = obj.company_id.get_sii_env()
		url = 'https://%s.sii.cl/cgi_dte/UPL/DTEUpload' % sii_env

		if obj._name == 'l10n_cl_dte.assignment':
			url = 'https://%s.sii.cl/cgi_rtc/RTC/RTCAnotEnvio.cgi' % sii_env
		elif obj._name == 'account.invoice':
			if obj.journal_id.doc_type_id.sii_code in ('39','41'):
				if obj.company_id.sii_env == 'prod':
					url = 'https://rahue.sii.cl/recursos/v1/boleta.electronica.envio'
				else:
					url = 'https://pangal.sii.cl/recursos/v1/boleta.electronica.envio'

		try:
			#if obj.journal_id.doc_type_id.sii_code in ('39','41'):
				#headers.update({'accept': 'application/json'})
			send = requests.post(url, files=files, data=data, headers=headers, cookies=cookies, verify=False)
			############ Para pruebas de envio #############
			#url = 'https://requestb.in/1gli0wx1'
			#requests.post(url, files=files, data=data, headers=headers, cookies=cookies)
			################################################
		except Exception as e:
			raise Warning(_('Error en el envio %s' % e ))

		message = ''
		track_id = None
		dte_status = 10
		state = 'error'
		if obj._name == 'account.invoice':
			if obj.journal_id.doc_type_id.sii_code in ('39','41'):
				resp = json.loads(send.text)
				if send.status_code == 200:
					if resp['estado'] == 'REC':
						dte_status = 0
						track_id = resp['trackid']
					else:
						dte_status = send.status_code
						message = send.text
				elif send.status_code in (400, 401, 405, 500):
					dte_status = send.status_code
					message = send.text
			else:
				root = etree.fromstring(send.text.encode('utf-8'))
				for element in root.iter():
					if element.tag == 'STATUS':
						dte_status = int(element.text)
					if element.tag == 'ERROR':
						message = element.text
					if element.tag == 'TRACKID':
						track_id = int(element.text)
		else:
			if send.status_code == 200:
				root = etree.fromstring(send.text.encode('utf-8'))
				for element in root.iter():
					if element.tag == 'STATUS':
						dte_status = int(element.text)
					if element.tag == 'ERROR':
						message = element.text
					if element.tag == 'TRACKID':
						track_id = int(element.text)

		if dte_status == 0:
			state = 'waiting_validation'
		if dte_status == 5:
			message = _('SII Internal Error, Please re-send Document.')

		vals = {'state': state, 'failed_text': message, 'track_id': track_id}
		if obj._name == 'account.invoice':
			vals.update({'dte_status': dte_status, 'send_date': time.strftime('%Y-%m-%d %H:%M:%S')})
			vals.pop('state')

		if obj._name in ('account.invoice', 'l10n_cl_dte.stock', 'l10n_cl_dte_invoice_receipt.daily_book'):
			msg = _("Enviado a SII con numero de seguimiento: %s.") % (track_id)
			obj.message_post(body=msg)

		_logger.info('Envio de documento electronico, los datos de respuesta son  %s' % vals)
		obj.sudo().write(vals)
		return True

	@api.model
	def get_document_status(self, obj):
		sii_env = obj.company_id.get_sii_env()
		url = 'https://%s.sii.cl/DTEWS/QueryEstDte.jws?WSDL' % sii_env
		cert = self.env['l10n_cl_dte_signer.certificate'].get_certificate(obj.company_id.id)

		#Llamado a cliente para webservice
#		client = Client(url)
		client = Client(url, transport=CustomTransport())

		######### OBTIENE TOKEN #########
		token = self.env['l10n_cl_dte.auth'].get_auth(obj, cert.cert, cert.private_key, cert.password)

		if obj._name == 'account.invoice':
			folio = obj.number
			date = obj.date_invoice
			amount = obj.obj.amount_total
		elif obj._name == 'l10n_cl_dte.stock':
			folio = obj.folio
			date = obj.date
			amount = obj.amount_total if obj.picking_id else obj.manual_amount_total

		resp = client.service.getEstDte(cert.owner_rut.split('-')[0].replace('.',''), cert.owner_rut.split('-')[1], \
							obj.company_id.partner_id.rut.split('-')[0].replace('.',''), \
							obj.company_id.partner_id.rut.split('-')[1], obj.partner_id.rut.split('-')[0], \
							obj.partner_id.rut.split('-')[1], obj.journal_id.doc_type_id.sii_code, \
							folio, date, amount, token)
		print resp
		root = etree.fromstring(resp.encode('utf-8'), parser=parser)
		raise Warning('asd')
#		for element in root.iter():
		return True

	@api.model
	def get_send_state_rest(self, obj):
		if obj.company_id.sii_env == 'prod':
			url = 'https://api.sii.cl/recursos/v1/boleta.electronica.envio'
		else:
			url = 'https://apicert.sii.cl/recursos/v1/boleta.electronica.envio'
		cert = self.env['l10n_cl_dte_signer.certificate'].get_certificate(obj.company_id.id)
		headers = {'accept': 'application/json'}
		base, dv = obj.company_id.rut.replace('.','').split('-')

		token = self.env['l10n_cl_dte.auth'].get_auth(obj, cert.cert, cert.private_key, cert.password)
		cookies = {'TOKEN': token}
		send = requests.get('%s/%s-%s-%s' % (url, base, dv, obj.track_id),  headers=headers, cookies=cookies, verify=False)
		resp = json.loads(send.text)
		if resp['estado'] in ('EPR','RPR'):
			if resp['estadistica'][0]['aceptados']:
				obj.signal_workflow('invoice_open')
			elif resp['estadistica'][0]['reparos']:
				obj.write({'status_text': resp['detalle_rep_rech']})
				obj.signal_workflow('invoice_open')
			elif resp['estadistica'][0]['rechazados']:
				obj.write({'failed_text': resp['detalle_rep_rech']})
				obj.signal_workflow('waiting_to_failed')
		elif resp['estado'] in ('REC','CRT','FOK','PRD','SOK'):
			obj.write({'failed_text': 'El documento se encuentra en proceso en SII.'})
		else:
			obj.write({'failed_text': resp['detalle_rep_rech']})
			obj.signal_workflow('waiting_to_failed')
		return True

	@api.model
	def get_send_state(self, obj):
		sii_env = obj.company_id.get_sii_env()
		url = 'https://%s.sii.cl/DTEWS/QueryEstUp.jws?WSDL' % sii_env
		cert = self.env['l10n_cl_dte_signer.certificate'].get_certificate(obj.company_id.id)

		#Llamado a cliente para webservice
#		client = Client(url)
		client = Client(url, transport=CustomTransport())

		######### OBTIENE TOKEN #########
		token = self.env['l10n_cl_dte.auth'].get_auth(obj, cert.cert, cert.private_key, cert.password)

		resp = client.service.getEstUp(obj.company_id.partner_id.rut.split('-')[0].replace('.',''), \
								obj.company_id.partner_id.rut.split('-')[1], obj.track_id, token)
		root = etree.fromstring(resp.encode('utf-8'), parser=parser)

		for element in root.iter():
			if element.tag == 'ESTADO':
				estado = element.text
			if element.tag == 'GLOSA':
				glosa = element.text
			if element.tag == 'ACEPTADOS':
				aceptado = element.text
			if element.tag == 'RECHAZADOS':
				rechazo = element.text
			if element.tag == 'REPAROS':
				reparo = element.text
#			if element.tag == '':
#				glosa = 

		if obj._name in ('account.invoice', 'l10n_cl_dte.stock'):
#			if 1 in (int(rechazo), int(reparo)):
#				asd = self.get_document_status(obj)
			if estado == 'EPR':
				if int(rechazo) == 1:
					if obj._name == 'account.invoice':
						obj.write({'failed_text': 'Favor revisar correo para errores'})
						obj.signal_workflow('waiting_to_failed')
					elif obj._name == 'l10n_cl_dte.stock':
						obj.write({'state': 'error', 'failed_text': 'Favor revisar correo para errores'})
				elif int(reparo) == 1:
					if obj._name == 'account.invoice':
						obj.write({'status_text': resp})
						obj.signal_workflow('invoice_open')
					elif obj._name == 'l10n_cl_dte.stock':
						obj.write({'state': 'validated', 'status_text': resp})
				elif int(aceptado) == 1:
					if obj._name == 'account.invoice':
						obj.signal_workflow('invoice_open')
					elif obj._name == 'l10n_cl_dte.stock':
						obj.write({'state': 'validated', 'status_text': None, 'failed_text': None})
			elif estado in ('RSC','RFR','RCT','05','PRD','-6'):
				if obj._name == 'account.invoice':
					obj.write({'failed_text': glosa})
					obj.signal_workflow('waiting_to_failed')
				elif obj._name == 'l10n_cl_dte.stock':
					obj.write({'state': 'error', 'failed_text': glosa})
			else:
				obj.write({'status_text': resp})
		elif obj._name in ('l10n_cl_dte.book', 'l10n_cl_dte_invoice_receipt.daily_book'):
			if estado[:3] in ('LOK', 'LTC'):
				obj.write({'state': 'validate', 'failed_text': None})
			elif estado[:3] in ('LNC', 'LRH', 'LRS'):
				obj.write({'failed_text': glosa, 'state': 'error'})
			elif estado[:3] == 'LSO':
				obj.write({'failed_text': _('Procesing Book')})
			elif estado == 'EPR':
				obj.write({'state': 'validate', 'failed_text': None})
			elif estado == 'RCH':
				obj.write({'failed_text': glosa, 'state': 'error'})
			else:
				obj.write({'failed_text': resp})
#Libro diario
#282                 try:
#283                         estado = tree[0].find('ESTADO')
#284                         if estado.text == 'LOK':
#285                         #       if int(tree[0].find('RECHAZADOS').text) == 1:
#286                         #               self.write({'failed_text': 'Favor revisar correo para errores', 'state': 'error'})
#287                         #       elif int(tree[0].find('REPAROS').text) == 1:
#288                         #               self.write({'status_text': resp, 'state': 'validate'})
#289                         #       else:
#290                                 self.write({'state': 'validate', 'failed_text': None})
#291                         elif estado.text in ('LNC', 'LRH', 'LRS'):
#292                                 self.write({'failed_text': tree[0].find('GLOSA').text, 'state': 'error'})
#293                         else:
#294                         #       else:
#295                         #       LSO -> Procesada caratula
#296                                 self.write({'failed_text': resp}) #Pasar a 'Procesando Envio'
#297                 except:
#298                         self.write({'failed_text': resp})
		return True

	@api.model
	def send_sii_ack(self, obj, action):
		self.env['res.company'].comprobar_modulo()
		cert = self.env['l10n_cl_dte_signer.certificate'].get_certificate(obj.company_id.id)

		######## OBTIENE TOKEN #########
		token = self.env['l10n_cl_dte.auth'].get_auth(obj, cert.cert, cert.private_key, cert.password)
		cookies = 'TOKEN=%s' % token

		sii_env = obj.company_id.get_sii_env()
		if sii_env == 'palena':
			url = 'https://ws1.sii.cl'
			wsdl = '%s/WSREGISTRORECLAMODTE/registroreclamodteservice?wsdl' % (url)
		elif sii_env == 'maullin':
			url = 'https://ws2.sii.cl'
			wsdl = '%s/WSREGISTRORECLAMODTECERT/registroreclamodteservice?wsdl' % (url)

		try:
#			client = Client(wsdl)
			client = Client(url, transport=CustomTransport())
			client.set_options(headers={'cookie': cookies})
			base, dv = obj.rut_emisor.replace('.','').split('-')
			res = client.service.ingresarAceptacionReclamoDoc(base, dv, obj.tipo_dte, obj.folio, action)
			return client.dict(res)
		except:
			raise Warning(_('Communication error with SII, please try again'))
l10n_cl_dte_sii()

class l10n_cl_dte_auth(models.TransientModel):
	_name = 'l10n_cl_dte.auth'
	_description = 'Auth SII manager model'

	seed = fields.Char('Seed')
	token = fields.Char('Token')
	rest = fields.Boolean('Rest Token?', default=False)
	state = fields.Char('state')

	@api.model
	def get_auth(self, document, cert, private_key, password, rest=False, sii_env=False):
		if document._name == 'account.invoice':
			if document.journal_id.doc_type_id.sii_code in ('39','41'):
				rest = True
				sii_env = document.company_id.sii_env

		now = dt.utcnow()
		active_auths = self.search([('rest','=',rest)] , order='id desc')
		if active_auths:
			last_auth = dt.strptime(active_auths[0].create_date, DEFAULT_SERVER_DATETIME_FORMAT)
			if (now - last_auth).seconds < 120:
				token = active_auths[0].token
				return token

		token, state = self.auth_process(document, cert, private_key, password, rest=rest, sii_env=sii_env)
		new_auth = {
			'token': token,
			'state': state,
			'rest': rest
		}
		self.create(new_auth)
		return token

	@api.model
	def auth_process(self, document, cert, private_key, password, rest=False, sii_env=False):
		env = document.company_id.get_sii_env()
		# se obtiene una semilla (es un numero entregado por el servicio )
		seed = self.get_seed(env, rest=rest, sii_env=sii_env)
		# se crea un xml firmado con la semilla obtenida
		template = self.env['dte.templates'].get_seed(seed)
		sign_seed = self.env['l10n_cl_dte.signer'].signed_document(template, cert, private_key, password)
		# se retorna el token (otro numero) y con este se pueden hacer los envios 
		return self.get_token(sign_seed, env, rest=rest, sii_env=sii_env)

	@api.model
	def get_seed(self, env, rest=False, sii_env=False):
		estado = ''
		semilla = ''
		sii_xml = ''

		if rest:
			if sii_env == 'prod':
				url = 'https://api.sii.cl/recursos/v1/boleta.electronica.semilla'
			else:
				url = 'https://apicert.sii.cl/recursos/v1/boleta.electronica.semilla'
		else:
			url = 'https://%s.sii.cl/DTEWS/CrSeed.jws?WSDL' % env

		cond = False
		while not cond:
			try:
				if rest:
#					r = requests.get(url)
					r = requests.get(url, verify=False)
					if r.status_code == 200:
						sii_xml = r.text
				else:
#					client = Client(url)
					client = Client(url, transport=CustomTransport())
					sii_xml = client.service.getSeed()
			except Exception, e:
				cond = False
				time.sleep(2)
				raise Warning('Ha ocurrido un error al intentar obtener la semilla.\nFavor reenviar a SII.\n%s.' % e)
			else:
				cond = True

		if not sii_xml:
			raise Warning('Ha ocurrido un error al intentar obtener la semilla.\nFavor Reenviar a SII.')

		root = etree.fromstring(sii_xml.encode('utf-8'), parser=parser)
		for element in root.iter():
			if element.tag == 'SEMILLA':
				semilla = element.text
			if element.tag == 'ESTADO':
				estado = element.text

		if estado != '00':
			raise Warning('Ha ocurrido un error al intentar obtener la semilla.\nFavor contacte con su Administrador.\n%s.' % sii_xml)
		if semilla == '':
			raise Warning('Ha ocurrido un error al intentar obtener la semilla.\nFavor contacte con su administrador.\n%s.' % sii_xml)
		return semilla

	@api.model
	def get_token(self, sign_seed, env, rest=False, sii_env=False):
		token = estado = ''
		if rest:
			if sii_env == 'prod':
				url = 'https://api.sii.cl/recursos/v1/boleta.electronica.token'
			else:
				url = 'https://apicert.sii.cl/recursos/v1/boleta.electronica.token'
		else:
			url = 'https://%s.sii.cl/DTEWS/GetTokenFromSeed.jws?WSDL' % env
#			client = Client(url)
			client = Client(url, transport=CustomTransport())

		ss = etree.tostring(etree.fromstring(sign_seed), pretty_print=True, encoding='iso-8859-1')
		cond = False
		headers = {'accept': 'application/xml', 'Content-Type': 'application/xml'}
		while not cond:
			try:
				if rest:
					send = requests.post(url, data=ss, headers=headers, verify=False)
					if send.status_code == 200:
						token_xml = send.text
				else:
					token_xml = client.service.getToken(ss)
			except Exception, e:
				#raise Warning('Ha ocurrido un error al intentar obtener el token.\nFavor Reenviar a SII.\n%s.' % e)
				time.sleep(2)
				cond = False
			else:
				cond = True

		if not token_xml:
			raise Warning('Ha ocurrido un error al intentar obtener el token.\nFavor Reenviar a SII.')

		root = etree.fromstring(token_xml.encode('utf-8'), parser=parser)

		for element in root.iter():
			if element.tag == 'TOKEN':
				token = element.text
			if element.tag == 'ESTADO':
				estado = element.text

		if estado != '00':
			raise Warning('Ha ocurrido un error al intentar obtener el token.\nFavor contacte con su administrador.\n%s.' % token_xml)
		if token == '':
			raise Warning('Ha ocurrido un error al intentar obtener el token.\nFavor contacte con su administrador.\n%s.' % token_xml)
		return token, estado

l10n_cl_dte_auth()
