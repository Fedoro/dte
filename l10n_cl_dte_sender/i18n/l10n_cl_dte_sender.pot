# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* l10n_cl_dte_sender
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 8.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-10-05 07:44+0000\n"
"PO-Revision-Date: 2018-10-05 07:44+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: l10n_cl_dte_sender
#: model:email.template,report_name:l10n_cl_dte_sender.account_invoice_customer_template
msgid "${object.journal_id.doc_type_id.sii_code or ''}.${object.journal_id.doc_type_id.sii_code in ('33','34','110') and 'Factura_' or object.journal_id.doc_type_id.sii_code in ('39','41') and 'Boleta_' or object.journal_id.doc_type_id.sii_code in ('61','112') and 'Nota de Credito_' or object.journal_id.doc_type_id.sii_code in ('56','111') and 'Nota de Debito_' or ''}${object.number or ''}.pdf"
msgstr "${object.journal_id.doc_type_id.sii_code or ''}.${object.journal_id.doc_type_id.sii_code in ('33','34','110') and 'Factura_' or object.journal_id.doc_type_id.sii_code in ('39','41') and 'Boleta_' or object.journal_id.doc_type_id.sii_code in ('61','112') and 'Nota de Credito_' or object.journal_id.doc_type_id.sii_code in ('56','111') and 'Nota de Debito_' or ''}${object.number or ''}.pdf"

#. module: l10n_cl_dte_sender
#: model:email.template,report_name:l10n_cl_dte_sender.dispatch_order_customer_template
msgid "${object.journal_id.doc_type_id.sii_code or ''}.Guía de Despacho_${object.folio}.pdf"
msgstr "${object.journal_id.doc_type_id.sii_code or ''}.Guía de Despacho_${object.folio}.pdf"

#. module: l10n_cl_dte_sender
#: model:email.template,body_html:l10n_cl_dte_sender.dispatch_order_customer_template
msgid "<html>\n"
"	<head>\n"
"		<meta charset=\"utf-8\"/>\n"
"	</head>\n"
"	<body>\n"
"		<p>Señor(es) ${object.partner_id.name}</p>\n"
"		<p>Adjunto le enviamos el siguiente documento tributario electrónico:<br/>\n"
"		<p>Tipo:        ${object.journal_id.doc_type_id.name}<br/>\n"
"		Folio:  ${object.folio}<br/>\n"
"		Fecha:  ${object.date}<br/>\n"
"	</body>\n"
"</html>"
msgstr "<html>\n"
"	<head>\n"
"		<meta charset=\"utf-8\"/>\n"
"	</head>\n"
"	<body>\n"
"		<p>Señor(es) ${object.partner_id.name}</p>\n"
"		<p>Adjunto le enviamos el siguiente documento tributario electrónico:<br/>\n"
"		<p>Tipo:        ${object.journal_id.doc_type_id.name}<br/>\n"
"		Folio:  ${object.folio}<br/>\n"
"		Fecha:  ${object.date}<br/>\n"
"	</body>\n"
"</html>"

#. module: l10n_cl_dte_sender
#: model:email.template,body_html:l10n_cl_dte_sender.account_invoice_customer_template
msgid "<html>\n"
"	<head>\n"
"		<meta charset=\"utf-8\"/>\n"
"	</head>\n"
"	<body>\n"
"		<p>Señor(es) ${object.partner_id.name}</p>\n"
"		<p>Adjunto le enviamos el siguiente documento tributario electrónico:<br/>\n"
"		<p>Tipo:        ${object.journal_id.doc_type_id.name}<br/>\n"
"		Folio:  ${object.number}<br/>\n"
"		Fecha:  ${object.date_invoice}<br/>\n"
"	</body>\n"
"</html>"
msgstr "<html>\n"
"	<head>\n"
"		<meta charset=\"utf-8\"/>\n"
"	</head>\n"
"	<body>\n"
"		<p>Señor(es) ${object.partner_id.name}</p>\n"
"		<p>Adjunto le enviamos el siguiente documento tributario electrónico:<br/>\n"
"		<p>Tipo:        ${object.journal_id.doc_type_id.name}<br/>\n"
"		Folio:  ${object.number}<br/>\n"
"		Fecha:  ${object.date_invoice}<br/>\n"
"	</body>\n"
"</html>"

#. module: l10n_cl_dte_sender
#: code:addons/l10n_cl_dte_sender/models/l10n_cl_dte_sender.py:99
#, python-format
msgid "Can't send the email, please check."
msgstr "Can't send the email, please check."

#. module: l10n_cl_dte_sender
#: field:l10n_cl_dte.sender,create_uid:0
msgid "Created by"
msgstr "Created by"

#. module: l10n_cl_dte_sender
#: field:l10n_cl_dte.sender,create_date:0
msgid "Created on"
msgstr "Created on"

#. module: l10n_cl_dte_sender
#: code:addons/l10n_cl_dte_sender/models/l10n_cl_dte_sender.py:51
#, python-format
msgid "DTE Document Sent"
msgstr "DTE Document Sent"

#. module: l10n_cl_dte_sender
#: field:l10n_cl_dte.stock,date_cancel:0
msgid "Date Cancel"
msgstr "Date Cancel"

#. module: l10n_cl_dte_sender
#: model:ir.model,name:l10n_cl_dte_sender.model_l10n_cl_dte_stock
msgid "Email Thread"
msgstr "Email Thread"

#. module: l10n_cl_dte_sender
#: code:addons/l10n_cl_dte_sender/models/l10n_cl_dte_sender.py:126
#, python-format
msgid "Envio de correo electronico XML Boleta a cliente iniciado."
msgstr "Envio de correo electronico XML Boleta a cliente iniciado."

#. module: l10n_cl_dte_sender
#: code:addons/l10n_cl_dte_sender/models/l10n_cl_dte_sender.py:141
#, python-format
msgid "Envio de correo electronico XML Boleta a cliente terminado."
msgstr "Envio de correo electronico XML Boleta a cliente terminado."

#. module: l10n_cl_dte_sender
#: code:addons/l10n_cl_dte_sender/models/l10n_cl_dte_sender.py:105
#, python-format
msgid "Envio de correo electronico XML Factura a cliente iniciado."
msgstr "Envio de correo electronico XML Factura a cliente iniciado."

#. module: l10n_cl_dte_sender
#: code:addons/l10n_cl_dte_sender/models/l10n_cl_dte_sender.py:120
#, python-format
msgid "Envio de correo electronico XML Factura a cliente terminado."
msgstr "Envio de correo electronico XML Factura a cliente terminado."

#. module: l10n_cl_dte_sender
#: code:addons/l10n_cl_dte_sender/models/l10n_cl_dte_sender.py:147
#, python-format
msgid "Envio de correo electronico XML Guia Despacho a cliente iniciado."
msgstr "Envio de correo electronico XML Guia Despacho a cliente iniciado."

#. module: l10n_cl_dte_sender
#: code:addons/l10n_cl_dte_sender/models/l10n_cl_dte_sender.py:163
#, python-format
msgid "Envio de correo electronico XML guia de despacho a cliente terminado."
msgstr "Envio de correo electronico XML guia de despacho a cliente terminado."

#. module: l10n_cl_dte_sender
#: model:email.template,subject:l10n_cl_dte_sender.account_invoice_customer_template
#: model:email.template,subject:l10n_cl_dte_sender.dispatch_order_customer_template
msgid "Envío de documento electrónico"
msgstr "Envío de documento electrónico"

#. module: l10n_cl_dte_sender
#: code:addons/l10n_cl_dte_sender/models/l10n_cl_dte_sender.py:93
#, python-format
msgid "Error, no distribution email set in customer"
msgstr "Error, no distribution email set in customer"

#. module: l10n_cl_dte_sender
#: code:addons/l10n_cl_dte_sender/models/l10n_cl_dte_sender.py:54
#, python-format
msgid "Fallo en envio o escritura para: Objeto: %s Id: %s."
msgstr "Fallo en envio o escritura para: Objeto: %s Id: %s."

#. module: l10n_cl_dte_sender
#: field:l10n_cl_dte.sender,id:0
msgid "ID"
msgstr "ID"

#. module: l10n_cl_dte_sender
#: model:ir.model,name:l10n_cl_dte_sender.model_account_invoice
msgid "Invoice"
msgstr "Invoice"

#. module: l10n_cl_dte_sender
#: field:l10n_cl_dte.sender,write_uid:0
msgid "Last Updated by"
msgstr "Last Updated by"

#. module: l10n_cl_dte_sender
#: field:l10n_cl_dte.sender,write_date:0
msgid "Last Updated on"
msgstr "Last Updated on"

#. module: l10n_cl_dte_sender
#: view:account.invoice:l10n_cl_dte_sender.l10n_cl_dte_sender_account_invoice_form_view
#: view:l10n_cl_dte.stock:l10n_cl_dte_sender.l10n_cl_dte_senders_stock_form_view
msgid "Resend Customer DTE Email"
msgstr "Resend Customer DTE Email"

#. module: l10n_cl_dte_sender
#: field:account.invoice,send:0
#: field:l10n_cl_dte.stock,send:0
msgid "Send"
msgstr "Send"

#. module: l10n_cl_dte_sender
#: code:addons/l10n_cl_dte_sender/models/l10n_cl_dte_sender.py:63
#, python-format
msgid "The electronic document attachment has been deleted, please contact your support team"
msgstr "The electronic document attachment has been deleted, please contact your support team"

