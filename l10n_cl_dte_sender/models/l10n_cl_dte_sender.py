# -*- coding: utf-8 -*-
##############################################################################
#
# Author: BTeC Ltda.
# Copyright (c) 2016 BTeC Ltda.
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

import base64, logging
_logger = logging.getLogger(__name__)

from openerp.tools.translate import _
from openerp.exceptions import Warning
from openerp import models, fields, api, netsvc

class l10n_cl_dte_sender(models.Model):
 	_name = 'l10n_cl_dte.sender'

	@api.model
	def planned_send_mail(self, document):
		if document._name == 'account.invoice':
			template = self.env['ir.model.data'].get_object('l10n_cl_dte_sender', 'account_invoice_customer_template')
		elif document._name == 'l10n_cl_dte.stock':
			template = self.env['ir.model.data'].get_object('l10n_cl_dte_sender', 'dispatch_order_customer_template')
			
		assert template._name == 'email.template'
		try:
			mail_id = template.send_mail(document.id, force_send=True, raise_exception=True)
			document.send = True
			document.message_post(body=_('DTE Document Sent'))
		except Exception as e:
			#TODO Traducir
			_logger.error(_('Fallo en envio o escritura para: Objeto: %s Id: %s.\n\n%s') % (document._name, document.id, e))
		return True

	@api.model
	def sign_attachment(self, document):
		self.env['res.company'].comprobar_modulo()
		try:
			attach = self.env['ir.model.data'].get_object('l10n_cl_dte_sender', 'l10n_cl_dte_electronic_attachment')
		except Exception as e:
			raise Warning(_('The electronic document attachment has been deleted, please contact your support team\n\n%s') % (e))

		cert = self.env['l10n_cl_dte_signer.certificate'].get_certificate(document.company_id.id)

		send_client_xml = self.env['dte.templates'].get_dispatch(document, client=True)
		signed_client_send = self.env['l10n_cl_dte.signer'].signed_document(send_client_xml, cert.cert, cert.private_key, cert.password)
		doc_type = document.journal_id.doc_type_id.sii_code
		if document._name == 'account.invoice':
			folio = document.number
			if document.type == 'out_invoice' and document.journal_id.doc_type_id.sii_code in ('33', '34'):
				#TODO Traducir
				doc_name = 'Factura'
			if document.type == 'out_invoice' and document.journal_id.doc_type_id.sii_code in ('39', '41'):
				#TODO Traducir
				doc_name = 'Boleta'
			elif document.type == 'out_invoice' and document.journal_id.doc_type_id.sii_code == '56':
				#TODO Traducir
				doc_name = 'Nota de Debito'
			elif document.type == 'out_refund' and document.journal_id.doc_type_id.sii_code == '61':
				#TODO Traducir
				doc_name = 'Nota de Credito'
		elif document._name == 'l10n_cl_dte.stock':
			folio = document.folio
			#TODO Traducir
			doc_name = 'Guia de Despacho'
		attach.write({'datas': base64.b64encode(signed_client_send), 'datas_fname': '%s.%s_%s_cliente.xml' % (doc_type, doc_name, folio)})

	@api.model
	def resend_customer_email(self, obj):
		if not (obj.partner_id.parent_id and obj.partner_id.parent_id.distribution_email) and not obj.partner_id.distribution_email:
			raise Warning(_('Error, no distribution email set in customer'))

		try:
			self.sign_attachment(obj)
			self.planned_send_mail(obj)
		except Exception as e:
			raise Warning(_('Can\'t send the email, please check.\n\n%s.') % (e))
		return True

	@api.model
	def _send_invoices(self, automatic=False, use_new_cursor=False):
		#TODO Traducir
		_logger.info(_('Envio de correo electronico XML Factura a cliente iniciado.'))
		for invoice in self.env['account.invoice'].search([('dte','=',True),('send','=',False),\
					('state','in',['open','paid']),('journal_id.doc_type_id.sii_code','in',['33','34','56','61'])]):
			try:
				if not (invoice.partner_id.parent_id and invoice.partner_id.parent_id.distribution_email) \
											and not invoice.partner_id.distribution_email:
					#TODO Traducir
					_logger.error('Error al enviar el documento %s de id %s, No tiene correo de distribucion asignado.' \
														% (invoice.number, invoice.id))
					continue
				self.sign_attachment(invoice)
				self.planned_send_mail(invoice)
				self._cr.commit()
			except:
				#TODO Traducir
				_logger.error('Error al enviar factura %s de id %s, favor verificar.' % (invoice.number, invoice.id))
		_logger.info(_('Envio de correo electronico XML Factura a cliente terminado.'))
		return True

	@api.model
	def _send_receipts(self, automatic=False, use_new_cursor=False):
		#TODO Traducir
		_logger.info(_('Envio de correo electronico XML Boleta a cliente iniciado.'))
		for invoice in self.env['account.invoice'].search([('dte','=',True),('send','=',False),\
					('state','in',['open','paid']),('journal_id.doc_type_id.sii_code','in',['39','41'])]):
			try:
				if not (invoice.partner_id.parent_id and invoice.partner_id.parent_id.distribution_email) \
											and not invoice.partner_id.distribution_email:
					#TODO Traducir
					_logger.error('Error al enviar el documento %s de id %s, No tiene correo de distribucion asignado.' \
														% (invoice.number, invoice.id))
					continue
				self.sign_attachment(invoice)
				self.planned_send_mail(invoice)
				self._cr.commit()
			except:
				#TODO Traducir
				_logger.error('Error al enviar boleta %s de id %s, favor verificar.' % (invoice.number, invoice.id))
		_logger.info(_('Envio de correo electronico XML Boleta a cliente terminado.'))
		return True

	@api.model
	def _send_dispatch_orders(self, automatic=False, use_new_cursor=False):
		#TODO Traducir
		_logger.info(_('Envio de correo electronico XML Guia Despacho a cliente iniciado.'))

		for order in self.env['l10n_cl_dte.stock'].search([('send','=',False),('state','=','validated')]):
			try:
				if not (order.partner_id.parent_id and order.partner_id.parent_id.distribution_email) \
											and not order.partner_id.distribution_email:
					#TODO Traducir
					_logger.error('Error al enviar el documento %s de id %s, No tiene correo de distribucion asignado.' \
														% (order.folio, order.id))
				self.sign_attachment(order)
				self.planned_send_mail(order)
				self._cr.commit()
			except:
				#TODO Traducir
				_logger.error('Error al enviar guia de despacho %s de id %s, favor verificar.' % (order.folio, order.id))

		#TODO Traducir
		_logger.info(_('Envio de correo electronico XML guia de despacho a cliente terminado.'))
		return True
l10n_cl_dte_sender()

